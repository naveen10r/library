UPDATE `br_sms_template` SET `template` = 'Your One Time Password for authentication of your mobile is {%OTP%}' WHERE (`id` = '101');
UPDATE `br_email_template` SET `template` = 'Dear User,<br>We received a request to reset the password associated with this e-mail address. <br>If you made this request, please c<span>lick the link below to reset your password using our secure server.<br><a href=\'{%RESET_LINK%}\' target=\"_blank\" rel=\"nofollow\">{%RESET_LINK%}<br></a><br>Thanks<br></span>{%THANKS_FROM%}<br>' WHERE (`id` = '100015');

/* DATED : 30-May-2020 */
UPDATE br_product_category SET slug='computer-internet',description='Computers & Internet' WHERE id=7;
UPDATE br_product_category SET slug='action-adventure' WHERE id=1;

/* DATED : 23-JAN-2021 */
UPDATE `br_product_category` SET `slug_code` = 'BGPH' WHERE `br_product_category`.`id` = 3;
UPDATE `br_product_category` SET `slug_code` = 'CMMZ' WHERE `br_product_category`.`id` = 6;
UPDATE `br_product_category` SET `slug_code` = 'TRMY' WHERE `br_product_category`.`id` = 10;
UPDATE `br_product_category` SET `slug_code` = 'FCTN' WHERE `br_product_category`.`id` = 14;
UPDATE `br_product_category` SET `slug_code` = 'PLTS' WHERE `br_product_category`.`id` = 21;
UPDATE `br_product_category` SET `slug_code` = 'RMNC' WHERE `br_product_category`.`id` = 24;
UPDATE `br_product_category` SET `slug_code` = 'STAM' WHERE `br_product_category`.`id` = 25;
UPDATE `br_product_category` SET `slug_code` = 'TASG' WHERE `br_product_category`.`id` = 28;
UPDATE `br_product_category` SET `slug_code` = 'LFSN' WHERE `br_product_category`.`id` = 38;
UPDATE `br_product_category` SET `slug_code` = 'BBKD' WHERE `br_product_category`.`id` = 40;
UPDATE `br_product_category` SET `slug_code` = 'GMAT' WHERE `br_product_category`.`id` = 41;
UPDATE `br_product_category` SET `slug_code` = 'BSEM' WHERE `br_product_category`.`id` = 42;
UPDATE `br_product_category` SET `slug_code` = 'GNKW' WHERE `br_product_category`.`id` = 43;