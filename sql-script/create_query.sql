/* CREATE TABLE br_product */

CREATE TABLE `br_product` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `author` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
    `date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    `miliseconds` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
    `content` LONGTEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
    `author_id` INT(10) NOT NULL,
    `publisher_id` INT(10) NOT NULL,
    `title` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
    `cover_image` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
    `isbn` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
    `excerpt` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
    `status` ENUM('publish','pending','deactive') NOT NULL DEFAULT 'pending' COLLATE 'utf8mb4_unicode_ci',
    `modified_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    `category_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
    `filter_param` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8mb4_unicode_ci',
    `is_delete` TINYINT(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    INDEX `type_status_date` (`status`, `date`, `id`),
    INDEX `post_parent` (`category_id`),
    INDEX `post_author` (`author`)
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;


CREATE TABLE `br_user_temp_password` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `send_at` ENUM('email', 'mobile') NOT NULL,
  `send_to` VARCHAR(255) NOT NULL,
  `send_unique_value` VARCHAR(45) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `created_at` DATETIME NULL,
  `valid_upto` DATETIME NULL,
  `is_verify` TINYINT NOT NULL,
  PRIMARY KEY (`id`));



CREATE TABLE `br_author` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(250) NULL DEFAULT NULL,
	`slug` VARCHAR(250) NULL DEFAULT NULL,
	`description` VARCHAR(250) NULL DEFAULT NULL,
	`is_active` TINYINT(1) NULL DEFAULT '1',
	`created_at` DATETIME NULL DEFAULT NULL,
	`modified_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `br_warehouse` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(250) NULL DEFAULT NULL,
	`description` VARCHAR(250) NULL DEFAULT NULL,
	`locality` VARCHAR(250) NULL DEFAULT NULL,
	`address` VARCHAR(250) NULL DEFAULT NULL,
	`pin_code` VARCHAR(250) NULL DEFAULT NULL,
	`landmark` VARCHAR(250) NULL DEFAULT NULL,
	`is_active` TINYINT(1) NULL DEFAULT '1',
	`created_at` DATETIME NULL DEFAULT NULL,
	`modified_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

/* Dated : 25-May-2020*/
CREATE TABLE `br_users_chats` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(250) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `br_users_chats` ADD `user_chat_from` INT NULL DEFAULT NULL AFTER `user_id`;

/* DATED : 05-JUNE-2020 */
CREATE TABLE AlgoBytes_BookLib.br_users_notification (
	nid INT auto_increment NOT NULL,
	user_id INT NOT NULL,
	title varchar(164) NOT NULL,
	description TEXT NULL,
	is_visible TINYINT DEFAULT 0 NULL,
	create_at DATETIME NULL,
	visible_at DATETIME NULL,
	notify_by INT NULL COMMENT 'Notify genereted user id',
	CONSTRAINT br_users_notification_PK PRIMARY KEY (nid)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;

/* DATED : 24-JUNE-2020 */

CREATE TABLE `br_third_party` (
  `tp_id` int NOT NULL AUTO_INCREMENT,
  `tp_name` varchar(255) NOT NULL,
  `tp_host_address` text NOT NULL,
  `tp_host_port` varchar(10) NOT NULL,
  `tp_host_username` varchar(255) NOT NULL COMMENT 'Encrypted value only',
  `tp_host_password` varchar(255) NOT NULL COMMENT 'Encrypted value only',
  `tp_start_date` date NOT NULL,
  `tp_end_date` date NOT NULL,
  `tp_create_at` datetime NOT NULL,
  PRIMARY KEY (`tp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


/* DATED : 25-JUN-2020 */
CREATE TABLE `br_users_order` (
  `oid` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `product_id` int NOT NULL,
  `order_id` varchar(24) NOT NULL,
  `product_mrp` float NOT NULL,
  `product_price` float NOT NULL,
  `order_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `order_status` enum('ordered','packed','shipped','delivered','return','cancel') NOT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- AlgoBytes_BookLib.br_users_order_status definition

CREATE TABLE `br_users_order_status` (
  `osid` int NOT NULL AUTO_INCREMENT,
  `order_id` varchar(24) NOT NULL,
  `status` varchar(24) NOT NULL,
  `description` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `create_by` int NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `is_delete` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`osid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/* br_product_question_answer definition*/

CREATE TABLE `br_product_question_answer` (
  `pqa_id` int NOT NULL,
  `question` varchar(160) NOT NULL,
  `Answer` varchar(255) NOT NULL,
  `product_id` int NOT NULL,
  `question_user_id` int NOT NULL,
  `answer_user_id` int NOT NULL,
  `question_ts` datetime NOT NULL,
  `answer_ts` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


/* DATED : 01-JAN-2020 */
CREATE TABLE AlgoBytes_BookLib.br_user_requester (
	ur_id INT NOT NULL AUTO_INCREMENT,
	ur_name varchar(100) NULL,
	ur_email varchar(255) NULL,
	ur_mobile varchar(16) NULL,
	ur_message varchar(260) NULL,
	ur_create_at DATETIME NULL,
	ur_is_response VARCHAR(10) NULL,
	ur_response_text varchar(260) NULL,
	ur_response_at varchar(260) NULL,
	CONSTRAINT br_user_requester_PK PRIMARY KEY (ur_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci
COMMENT='Requester details for book buying';

/* DATED : 03-JAN-2021 */
CREATE TABLE `br_product_review` (
  `pr_id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `review` text,
  `star_point` int DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `is_active` tinyint DEFAULT '1',
  PRIMARY KEY (`pr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8

/* DATED : 04-JAN-2021 */
CREATE TABLE `br_product_category_junction` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`product_id` INT(11) NOT NULL,
	`category_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `br_product_author_junction` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`product_id` INT(11) NOT NULL,
	`author_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

/* DATED : 16-JAN-2021 */
CREATE TABLE `br_product_tags` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(250) NULL DEFAULT NULL,
	`is_active` TINYINT(1) NULL DEFAULT '1',
	`created_at` DATETIME NULL DEFAULT NULL,
	`modified_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;

/* DATED : 20-JAN-2021 */
CREATE TABLE `br_subscribe` (
  `sid` int NOT NULL AUTO_INCREMENT,
  `email` varchar(164) NOT NULL,
  `insert_at` datetime DEFAULT NULL,
  `insert_from` varchar(100) DEFAULT NULL,
  `insert_addr` text,
  `unsubscribe` tinyint DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* DATED : 23-JAN-2021 */
CREATE TABLE `br_user_cart` (
  `cid` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `product_id` int NOT NULL,
  `qty` int NOT NULL,
  `added_at` datetime DEFAULT NULL,
  `expair_at` date DEFAULT NULL,
  `is_delete` tinyint DEFAULT '0',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;