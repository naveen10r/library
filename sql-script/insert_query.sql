INSERT INTO `br_sms_template` (`id`, `title`, `template`, `created_by`) VALUES ('108', 'TXTLCL', '{%OTP%} is your login OTP and is valid for 10 minutes.', '1');

INSERT INTO `br_email_template` (`id`, `unique_title`, `title`, `template`, `create_at`, `update_at`, `create_by`, `update_by`) VALUES
(100023, 'USR_CHANGE_PWD', 'Your Account Password has been changed successfully', 'Dear User,<br><br>The password for your {%SITE_NAME%} Account - ID: {%PK_ID%} has been successfully changed.<br><br>This request was received from {%IP_ADDRESS%} If you have not authorised this request, please contact us immediately using the information below.<br><br><b>Support</b><br>For any support with respect to your relationship with us you can always contact us directly using the following Information.<br><br><i><b>Technical Support</b></i><b>:</b><br>Tech Support Email : {%TECH_SUPPORT_EMAIL%}<br><div><br>Thanks & Regards,</div><div>{%THANKS_FROM%} Support Team</div><div>{%SUPPORT_EMAIL%}</div><div>Toll Free No: {%TOLL_FREE_NO%}</div><div>Phone: {%SUPPORT_MOBILES%}</div>', '2020-05-24 07:14:23', '2020-05-24 07:17:14', 1, 1);

/* DATED : 30-May-2020 */
INSERT INTO br_product_category (name,slug,description,is_active) VALUES ('Business, Investing and Management','business-investing-management','Business, Investing and Management',1);
INSERT INTO br_product_category (name,slug,description,is_active) VALUES ('Academic','academic','Academic',1);
INSERT INTO br_product_category (name,slug,description,is_active) VALUES ('Entrance Exams Preparation','entrance-exams','Entrance Exams Preparation',1);
INSERT INTO br_product_category (name,slug,description,is_active) VALUES ('Engineering','engineering','Engineering',1);

INSERT INTO br_product_category (parent_id,name,slug,description,is_active) VALUES (35,'Chemical Engineering','chemical-engineering','Chemical Engineering',1);
INSERT INTO br_product_category (parent_id,name,slug,description,is_active) VALUES (35,'Computer Science','computer-science','Computer Science',1);
