/*  **Author: Sandeep Date: 15:05:2020** */
ALTER TABLE `br_product` ADD COLUMN `isbn` VARCHAR(50) NOT NULL;
ALTER TABLE `br_product` ADD COLUMN `offerprice` VARCHAR(50) NOT NULL AFTER `isbn`;
ALTER TABLE `br_product` ADD COLUMN `baseprice` VARCHAR(50) NOT NULL AFTER `isbn`;
ALTER TABLE `br_product` ADD COLUMN `buying_mode` VARCHAR(50) NOT NULL AFTER `isbn`;

/*  **Author: Sandeep Date: 16:05:2020** */
ALTER TABLE `br_product_category` ADD COLUMN `modified_at` DATETIME NULL DEFAULT NULL AFTER `is_active`;
ALTER TABLE `br_product_category` ADD COLUMN `slug` VARCHAR(250) NULL DEFAULT NULL AFTER `name`, ADD COLUMN `description` VARCHAR(250) NULL DEFAULT NULL AFTER `slug`;

/*  **Author: Naveen Date: 24:05:2020** */
ALTER TABLE `br_users` ADD COLUMN `gender` ENUM('M','F','T') NULL;

ALTER TABLE `br_users_otp` ADD COLUMN `otp_for_value` VARCHAR(45) NULL AFTER `is_verify`;

ALTER TABLE `br_users_otp` CHANGE COLUMN `otp_for_value` `otp_for_value` VARCHAR(245) NULL DEFAULT NULL AFTER `otp_for`;

ALTER TABLE `br_users` ADD COLUMN `dob` DATE NULL AFTER `is_delete`;

ALTER TABLE `br_users_address` DROP FOREIGN KEY `FK_ADD_USER`;

ALTER TABLE `br_users_address` 
ADD COLUMN `is_current` TINYINT(1) NULL DEFAULT 0 AFTER `is_delete`,
ADD COLUMN `address_line_3` VARCHAR(100) NULL DEFAULT NULL AFTER `address_line_2`,
CHANGE COLUMN `user_id` `user_id` INT NOT NULL ,
CHANGE COLUMN `address_as` `address_as` VARCHAR(50) NOT NULL COMMENT 'Consinee name' ,
CHANGE COLUMN `address` `address_line_1` VARCHAR(100) NOT NULL ,
CHANGE COLUMN `locality` `address_line_2` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `landmark` `landmark` VARCHAR(255) NULL DEFAULT NULL ;
ALTER TABLE `br_users_address` 
ADD CONSTRAINT `FK_ADD_USER`
  FOREIGN KEY (`user_id`)
  REFERENCES `br_users` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `br_users_address` 
CHANGE COLUMN `id` `id` INT NOT NULL AUTO_INCREMENT ;

ALTER TABLE `br_users_address` 
DROP FOREIGN KEY `FK_ADD_CITY`;

ALTER TABLE `br_users_address` DROP INDEX `FK_ADD_CITY` ;

ALTER TABLE `br_users_otp` ADD COLUMN `sms_template_id` INT NULL AFTER `otp_for_value`;

ALTER TABLE `br_users_otp` CHANGE COLUMN `sms_template_id` `sms_template_id` VARCHAR(160) NULL ;

ALTER TABLE `br_users_otp` CHANGE COLUMN `sms_template_id` `otp_title` VARCHAR(160) NULL DEFAULT NULL ;

ALTER TABLE `br_email_template` ADD `unique_title` VARCHAR(64) DEFAULT NULL AFTER `id` ;
ALTER TABLE `br_email_template` ADD UNIQUE(`unique_title`);

/* DATED : 28-May-2020 */
ALTER TABLE `br_users` ADD `is_password_auto` TINYINT(1) NOT NULL DEFAULT '0' AFTER `password`;

/* DATED : 30-May-2020 */
ALTER TABLE `br_product_category` ADD CONSTRAINT br_product_category_UN UNIQUE KEY (slug);
ALTER TABLE `br_product_category` CHANGE `parent_id` `parent_id` INT NULL DEFAULT NULL;
ALTER TABLE `br_users_chats` ADD `user_chat_from` INT NULL DEFAULT NULL AFTER `user_id`;

/* DATED : 05-June-2020 */
ALTER TABLE br_users_notification ADD is_product INT NULL COMMENT 'Default NULL, if Not NULL then user product id';

/* DATED : 06-June-2020 */
ALTER TABLE `br_product_media` ADD `is_visible` ENUM('Y','N') NOT NULL DEFAULT 'Y' AFTER `create_ts`;
ALTER TABLE br_product_media MODIFY COLUMN create_ts DATETIME NULL;
ALTER TABLE br_product_media CHANGE orig_name file_for_label varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL;


/********************************************************************************************/
ALTER TABLE `br_product` ADD COLUMN `publish_year` DATETIME NULL DEFAULT NULL AFTER `modified_at`;

ALTER TABLE `br_product`
	ADD COLUMN `meta_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `filter_param`,
	ADD COLUMN `meta_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `meta_keywords`;

ALTER TABLE `br_product_category`	ADD COLUMN `slug_code` VARCHAR(250) NULL DEFAULT NULL AFTER `slug`;

ALTER TABLE `br_author` CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL AFTER `slug`;
ALTER TABLE `br_author` ADD COLUMN `author_image` VARCHAR(255) NULL DEFAULT NULL AFTER `description`;

/* DATED : 01-JAN-2021 */
ALTER TABLE AlgoBytes_BookLib.br_user_requester ADD ur_product_id INT NULL;
RENAME TABLE AlgoBytes_BookLib.br_user_requester TO AlgoBytes_BookLib.br_product_requester;
/* Date: 01_Jan_2021 */
ALTER TABLE `br_author`	ADD COLUMN `dob` DATETIME NULL DEFAULT NULL AFTER `is_active`;

/* Date: 02_Jan_2021 */
ALTER TABLE `br_product`	ADD COLUMN `other_information` LONGTEXT NOT NULL DEFAULT '' COMMENT 'JSON only' AFTER `meta_description`;

/* Date: 06_Jan_2021 */
ALTER TABLE `br_product` CHANGE COLUMN `category_id` `category_id` VARCHAR(255) NOT NULL DEFAULT '0' AFTER `publish_year`;
ALTER TABLE `br_product` CHANGE COLUMN `author_id` `author_id` VARCHAR(255) NOT NULL AFTER `content`;

/* Date: 16_Jan_2021 */
ALTER TABLE `br_author` ADD UNIQUE INDEX `slug` (`slug`);

/* Dated : 21-JAN-2021 */
ALTER TABLE `br_product` CHANGE `title` `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `eschogza_library`.`br_product` ADD INDEX `KEY_TITLE` (`title`(255));
ALTER TABLE `eschogza_library`.`br_author` ADD INDEX `KEY_AUTHOR_NAME` (`name`(250), `is_active`(1));

/* DATED : 23-JAN-2021 */
ALTER TABLE `br_product_category` ADD UNIQUE(`slug_code`);