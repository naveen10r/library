<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['meta']['description'] = 'Electrify store opencart Responsive Theme is designed for stationary, book, pen, ebook, office, work, comic, library, album, cover, game, education, photography, fashion, Shoes, Bag, Flower, Gift, Activity, minimal and multi purpose store. Electrify Theme is looking good with colors combination. It is very nice with its clean and professional look.';

$config['meta']['keywords'] = 'stationary, book, pen, ebook, office, work, comic, library, album, cover, game, education, photography, fashion, Shoes, Bag, Flower, Gift, Activity';

$config['site']['title'] = 'LeafLIB - Online Books Store';