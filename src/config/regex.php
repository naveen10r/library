<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 *@author : Naveen Rastogi
 */

/* PASSWORD : Password (UpperCase, LowerCase and Number) */
defined('PASSWORD') OR define('PASSWORD','^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$');
/* PASSWORD_WITH_LENGTH : Password (UpperCase, LowerCase, Number/SpecialChar and min 8 Chars) */
defined('PASSWORD_WITH_LENGTH') OR define('PASSWORD_WITH_LENGTH','(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$');
defined('INT_4_DIGIT') OR define('INT_4_DIGIT','([1-9])+(?:-?\d){4,}');
defined('ALPHA_NUM') OR define('ALPHA_NUM','[a-zA-Z0-9]+');
/* UN_WITH_LENGTH : Username with 2-20 chars */
defined('UN_WITH_LENGTH') OR define('UN_WITH_LENGTH','^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$');
defined('CREDIT_CARD') OR define('CREDIT_CARD','[0-9]{13,16}');
/* BASIC_DATE : Format[DD.MM.YYYY] */
defined('BASIC_DATE') OR define('BASIC_DATE','(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}');
defined('HEX_COLOR') OR define('HEX_COLOR','^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$');
defined('DOMAIN') OR define('DOMAIN','^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$');