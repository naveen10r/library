<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home/Home/Books';
$route['404_override'] = 'default/General/Page404';
$route['translate_uri_dashes'] = FALSE;

$route['home'] = 'home/Home/Books';
$route['mx-admin'] = 'mx-admin/adminAuth/index';
$route['mx-admin-dashboard'] = 'mx-admin-dashboard/dashboard/index';
$route['mx-admin/logout'] = 'mx-admin/adminAuth/logout';
$route['sign-out'] = 'users/ExternalUser/signout';
$route['login'] = 'users/ExternalUser/login';
$route['register'] = 'users/ExternalUser/Register';
$route['forgot-password'] = 'users/ExternalUser/forgotPassword';
$route['my-orders/(:any)'] = 'account/Services/MyOrders';
$route['choose-package'] = 'package/Package/choose_package';
$route['product-details/(:any)'] = 'product/products/product_details';
$route['invite-new-user'] = 'users/ExternalUser/SetNewPassword';
$route['author-details/(:any)'] = 'author/author/author_details';

/**********************************************************/
$route['contact-us'] = 'enquiry_form/contact_us';
$route['refer-a-friend'] = 'enquiry_form/refer_a_friend';
$route['request-a-book'] = 'enquiry_form/request_a_book';
$route['about-us'] = 'enquiry_form/about_us';
$route['privacy-policy'] = 'enquiry_form/privacy_policy';
$route['terms-and-condition'] = 'enquiry_form/terms_and_condition';
$route['careers'] = 'enquiry_form/careers';
/************************************************************/
$route['view-cart'] = 'user_cart/cart/ViewCart';
$route['verify-order'] = 'user_cart/cart/VerifyOrder';
$route['categories/(:any)'] = 'default/general/AllCategory';