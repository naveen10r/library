<?php

class TableName extends CI_Model {

    protected $masterUserRole;
    protected $adminUsers;
    protected $ciSession;
    protected $config;
    protected $emailTemplate;
    protected $language;
    protected $languageEncode;
    protected $masterBank;
    protected $masterBankBranch;
    protected $masterBoardUni;
    protected $masterCaptcha;
    protected $masterCity;
    protected $masterCountry;
    protected $masterMenu;
    protected $masterPinCode;
    protected $masterReligions;
    protected $roleHierarchy;
    protected $schools;
    protected $schoolClassSec;
    protected $masterState;
    protected $smsTemplate;
    protected $users;
    protected $usersAddress;
    protected $usersChats;
    protected $usersOtp;
    protected $usersPersonalDetails;
    protected $usersTempPassword;
    protected $userAddress;
    protected $updateLog;
    protected $usersNotify;
    protected $product;
    protected $productCategory;
    protected $productMedia;
    protected $author;
    protected $warehouse;
    protected $productRequester;
    protected $productReview;
    protected $product_author_junction;
    protected $product_category_junction;
    protected $product_tags;
    protected $subscribe;
    protected $user_cart;
    private $session_id = NULL;

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->masterUserRole = $this->db->dbprefix('admin_menus');
        $this->adminUsers = $this->db->dbprefix('admin_users');
        $this->ciSession = $this->db->dbprefix('ci_sessions');
        $this->config = $this->db->dbprefix('configuration');
        $this->emailTemplate = $this->db->dbprefix('email_template');
        $this->language = $this->db->dbprefix('language');
        $this->languageEncode = $this->db->dbprefix('language_encoding');
        $this->masterBank = $this->db->dbprefix('master_bank');
        $this->masterBankBranch = $this->db->dbprefix('master_bank_branch');
        $this->masterBoardUni = $this->db->dbprefix('master_board_university');
        $this->masterCaptcha = $this->db->dbprefix('master_captcha');
        $this->masterCity = $this->db->dbprefix('master_city');
        $this->masterCountry = $this->db->dbprefix('master_country');
        $this->masterMenu = $this->db->dbprefix('master_menus');
        $this->masterPinCode = $this->db->dbprefix('master_pin_data');
        $this->masterReligions = $this->db->dbprefix('master_religions');
        $this->roleHierarchy = $this->db->dbprefix('master_role_hierarchy');
        $this->schools = $this->db->dbprefix('master_school');
        $this->schoolClassSec = $this->db->dbprefix('master_school_class_sec');
        $this->masterState = $this->db->dbprefix('master_state');
        $this->product = $this->db->dbprefix('product');
        $this->productCategory = $this->db->dbprefix('product_category');
        $this->author = $this->db->dbprefix('author');
        $this->warehouse = $this->db->dbprefix('warehouse');
        $this->productMedia = $this->db->dbprefix('product_media');
        $this->smsTemplate = $this->db->dbprefix('sms_template');
        $this->users = $this->db->dbprefix('users');
        $this->usersAddress = $this->db->dbprefix('users_address');
        $this->usersChats = $this->db->dbprefix('users_chats');
        ;
        $this->usersOtp = $this->db->dbprefix('users_otp');
        $this->usersPersonalDetails = $this->db->dbprefix('users_personal_details');
        $this->usersTempPassword = $this->db->dbprefix('user_temp_password');
        $this->userAddress = $this->db->dbprefix('user_address');
        $this->updateLog = $this->db->dbprefix('log_for_update');
        $this->usersNotify = $this->db->dbprefix('users_notification');
        $this->productRequester = $this->db->dbprefix('product_requester');
        $this->productReview = $this->db->dbprefix('product_review');
        $this->product_author_junction = $this->db->dbprefix('product_author_junction');
        $this->product_category_junction = $this->db->dbprefix('product_category_junction');
        $this->product_tags = $this->db->dbprefix('product_tags');
        $this->subscribe = $this->db->dbprefix('subscribe');
        $this->user_cart = $this->db->dbprefix('user_cart');

        if ($this->session->userdata('ExUserSess')) {
            $sessionData = $this->session->userdata('ExUserSess');
            $this->session_id = $sessionData['id'];
        }
    }

    protected function trans_insert_data() {
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $lastId = $this->db->insert_id();
            $this->db->trans_commit();
            return $lastId;
        }
    }

    protected function trans_update_data() {
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $affectedId = $this->db->affected_rows();
            $this->db->trans_commit();
            return $affectedId;
        }
    }

    protected function set_update_log(string $table, array $where = array(), array $update = array()) {
        $select = array_keys($update);
        $returnResult = $this->db->select($select)->from($table)->where($where)->get()->result();
        $log_insert = array(
            'user_id' => $this->session_id,
            'table_name' => $table,
            'select_data' => json_encode($where),
            'update_data' => json_encode($returnResult),
            'change_at' => date(SYS_DB_DATE_TIME)
        );
        $this->db->insert($this->updateLog, $log_insert);
        return $this->db->insert_id();
    }

}

?>