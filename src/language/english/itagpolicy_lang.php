<?php

$lang['itag_username'] = 'Username field must be between 6 to 30 characters in length and {1} UPPERCASE, {1} lowercase and one {0-9} numeric characters in set. Special character not allowed.';

$lang['itag_password'] = 'Password field should be 6 to 30 characters in length and {1} UPPERCASE, {1} lowercase and one {0-9} numeric characters in set. One special charecter(s) in this [@#$%^&+*!=]';

$lang['itag_name'] = 'Only alphabets and space are allowed.';

$lang['itag_mobile'] = '10 digit mobile number only.';
$lang['itag_dob'] = 'Minimum '.REG_START_YEAR.' years of age for registraion.';