<?php
/* All systems errors manage from this page */
$lang['err_1001'] = 'Given mobile number is not our database. [#1001]';
$lang['err_1002'] = 'Invalid email or password. [#1002]';
$lang['err_1003'] = 'Mobile already verified! [#1003]';
$lang['err_1004'] = 'The %s field does not in valid dob range. [#1004]';
$lang['err_1005'] = 'Something went wrong. Please try again [#1005]';
$lang['err_1006'] = 'Mobile number not matched for our database. [#1006]';
$lang['err_1007'] = 'Current password does not matched! [#1007]';
$lang['err_1008'] = 'OTP not matched. [#1008]';
$lang['err_1009'] = 'No record(s) found in our database. [#1009]'; /* append form validation error */
$lang['err_1010'] = 'Something went wrong. Please add again. [#1010]';
$lang['err_1011'] = 'Mail OTP not matched. [#1011]';
$lang['err_1012'] = 'Mail opt expired. [#1012]';
$lang['err_1013'] = 'Please login to view this. [#1013]';
$lang['err_1014'] = 'No any update found. [#1014]';
$lang['err_1015'] = 'Given mobile number is activated on Do Not Distrurb (DND) service. [#1015]';
$lang['err_1016'] = 'Invalid OTP. [#1016]';
$lang['err_1017'] = 'OTP expired. [#1017]';
$lang['err_1018'] = 'Please wait... [#1018]';
$lang['err_1019'] = 'This email or mobile is not registered with us. [#1019]';
$lang['err_1020'] = 'Reset password link is not valid, Please try again. [#1020]';
$lang['err_1021'] = ' field is not in the correct format. [#1021]';
$lang['err_1022'] = 'The captcha image security is required. [#1022]';
$lang['err_1023'] = 'The captcha image security code doesn\'t matched. [#1023]';
$lang['err_1024'] = 'Please check at least one checkbox. [#1024]';
$lang['err_1025'] = 'Invalid URL [#1025]';
$lang['err_1026'] = 'Something went wrong. Please check manualy. [#1026]';
