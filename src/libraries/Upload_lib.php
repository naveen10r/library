<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Upload_Lib {

    public function createThumbs($pathToImages, $pathToThumbs, $thumbWidth): bool {
        $dir = fopen($pathToImages, 'r');
        $info = pathinfo($pathToImages);
        $fname = $info['basename'];
        if (strtolower($info['extension']) == 'jpg') {
            $img = imagecreatefromjpeg("{$pathToImages}");
        } elseif (strtolower($info['extension']) == 'png') {
            $img = imagecreatefrompng("{$pathToImages}");
        } elseif (strtolower($info['extension']) == 'gif') {
            $img = imagecreatefromgif("{$pathToImages}");
        } elseif (strtolower($info['extension']) == 'bmp') {
            $img = imagecreatefromwbmp("{$pathToImages}");
        } else {
            $img = imagecreatefromjpeg("{$pathToImages}");
        }
        $width = imagesx($img);
        $height = imagesy($img);
        $new_width = $thumbWidth;
        $new_height = floor($height * ($thumbWidth / $width));
        $tmp_img = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        if (strtolower($info['extension']) == 'jpg') {
            imagejpeg($tmp_img, "{$pathToThumbs}{$fname}");
        } elseif (strtolower($info['extension']) == 'png') {
            imagepng($tmp_img, "{$pathToThumbs}{$fname}");
        } elseif (strtolower($info['extension']) == 'gif') {
            imagegif($tmp_img, "{$pathToThumbs}{$fname}");
        } elseif (strtolower($info['extension']) == 'bmp') {
            imagewbmp($tmp_img, "{$pathToThumbs}{$fname}");
        } else {
            imagejpeg($tmp_img, "{$pathToThumbs}{$fname}");
        }
        fclose($dir);
        return true;
    }

    
    /*
     *@author : Naveen RAstogi
     *@param : config, array[thumbWidth, pathToImages, pathToThumbs]
     *          *All are mandatory elements in array
     *@return : bool
     *@email : ****@****.***
     *@mobile : **********
     */
    function createThumbConfig(array $config): bool {
        $return = [];
        foreach ($config['thumbWidth'] as $width) {
            $return[] = self::createThumbs($config['pathToImages'], $config['pathToThumbs'], $width);
        }
        return true;
    }
    
    /*
     *@author : Naveen RAstogi
     *@param : $setting [FULL_PATH, VALID_EXT, MAX_SIZE, IS_IMG, IS_THUMB, THUMB_SIZES] 
     *          FULL_PATH[directory_path_only], VALID_EXT[PNG|JPEG|JPG], MAX_SIZE[INT(1024)], IS_IMG[0/1], IS_THUMB[BOOL], THUMB_SIZES[array(100, 150, 240, ....)]
     *@return : bool
     *@email : ****@****.***
     *@mobile : **********
     */
    function upload_file(array $setting) {
        if($setting['MKDIR']=== true)
        {
            $this->createDir($setting['FULL_PATH']);
        }
        $config['upload_path'] = $setting['FULL_PATH'].'/image';
        $config['allowed_types'] = $setting['VALID_EXT'];
        $config['max_size'] = $setting['MAX_SIZE'];
        $config['is_image'] = $setting['IS_IMG'];
        $config['detect_mime'] = TRUE;
        $config['file_name'] = time().rand(10000,99999);
        $ci =& get_instance();
        $ci->load->library('upload', $config);
        if (!$ci->upload->do_upload('file')) {
            prx($ci->upload->display_errors());
        } else {
            $upRes = $ci->upload->data(); /* upRes : Upload Responce */
            if ($setting['IS_THUMB'] === true) {
                foreach($setting['THUMB_SIZES'] as $thumb)
                {
                    $this->createThumbConfig(array('pathToImages' => $upRes['full_path'], 
                        'pathToThumbs' => $upRes['file_path'] . $thumb. '_', 'thumbWidth' => array($thumb)));
                }
            }
            return $upRes;
        }
    }
    
    
    private function createDir($dir) {
        if (is_dir($dir)) {
            return false;
        } else {
            $isMake = mkdir($dir, 777);
            if ($isMake) {
                index_html($dir);
                mkdir($dir . '/image', 777);
                mkdir($dir . '/file', 777);
                mkdir($dir . '/video', 777);
                index_html($dir . '/image');
                index_html($dir . '/file');
                index_html($dir . '/video');
            }
            return true;
        }
    }

}
