<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

class MongoDb
{
    private $host = '';
    private $port = '';
    private $dbname = '';
    private $user = '';
    private $password = '';

    private $connString = null;

    private $conn = '';
    private $err = array();
    private $insertId = '';
    private $cursor = '';

    private $getOptions = array();
    private $where = '';
    
    public function __construct() {
        $ci = get_instance();
        $this->connString = $ci->config->item('Mongo_URI');
        $this->host = $ci->config->item('Mongo_Host');
        $this->port = $ci->config->item('Mongo_Port');
        $this->dbname = $ci->config->item('Mongo_DB');
        $this->user = $ci->config->item('Mongo_User');
        $this->password = $ci->config->item('Mongo_Password');
        self::connect();
    }
    
    private function connect()
    {
        try {
            if(!class_exists('MongoDB\Driver\Manager'))
            {
                log_message('Exception','MongoDB\Driver\Manager Class not installed @Line-40');
                return false;
            }
            if (empty($this->connString))
            {
                if(!empty($this->host) && !empty($this->port) && !empty($this->user) && !empty($this->password))
                {
                    $this->conn = new MongoDB\Driver\Manager($this->conn_str());
                } else {
                    die('MongoDB\Driver\Manager connection URI not found!');
                }
            } else {
                $this->conn = new MongoDB\Driver\Manager($this->connString);
                return TRUE;
            }
        } catch (Exception $e) {
            $this->mongo_die(['line'=>$e->getLine(), 'msg'=>$e->getMessage(), 'error_type'=>'Exception']);
        }
    }
    
    public function conn_str()
    {
        $str = "mongodb://";
        if(isset($this->user) && isset($this->password))
        {
            $str .= $this->user . ":" . $this->password;
        }

        $str .= "@" . $this->host . ":" . $this->port;

        if(isset($this->dbname))
        {
            $str .= "/" . $this->dbname;
        }

        return (string) $str;
    }
    
    /**
     * it will be created if does not exists, otherwise is switched to another database
     * @param string $db
     * @return $this
     */
    public function select_db($db)
    {
        $this->dbname = $db;
        return $ci;
    }
    
    public function insert($collection,$data)
    {
        try {
            if(empty($this->conn))
            {
                log_message('[MONGO][CONNECTION_ERROR]', 'Connection not found');
            }
            if(!is_array($data) || count($data) == 0){
                throw new Exception('No data found for insert');
            }
            
            $bulk = new MongoDB\Driver\BulkWrite();
            $bulk->insert($data);
            $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 100);
            $result = $this->conn->executeBulkWrite($this->dbname.'.'.$collection, $bulk, $writeConcern);
            
            if(isset($result)){
                return $result;
            }
        } catch(Exception $e) {
            self::mongo_die(['line'=>$e->getLine(), 'msg'=>$e->getMessage(), 'error_type'=>'Exception']);
        }
    }
    
    private function mongo_die($data)
    {
        echo '<pre>';
        print_r($data);
        die('</pre>');
    }
}

