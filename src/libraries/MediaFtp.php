<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MediaFtp {
    
    private $ftp_server;
    private $ftp_conn;
    private $ftp_username;
    private $ftp_userpass;


    public function __construct() {
        $this->ftp_server = '';
        $this->ftp_username = '';
        $this->ftp_userpass = '';
        $this->ftp_conn = $this->connect_ftp();
    }
    
    private function connect_ftp()
    {
        return false;
        $ftp_conn = ftp_connect($this->ftp_server) or die("Could not connect to ".$this->ftp_server);
        ftp_login($ftp_conn, $this->ftp_username, $this->ftp_userpass);
        return $ftp_conn;
    }
    
    public function put_object($config=array())
    {
        if (ftp_put($this->ftp_conn, $config['destination_filepath'], $config['source_filepath'], FTP_ASCII))
        {
            return [TRUE, $config['destination_filepath']];
        } else {
            return [FALSE, $config['source_filepath']];
        }
    }
    
    public function destroy()
    {
        ftp_close($this->ftp_conn);
    }
    
    public function ftpdir($config=array())
    {
		
        if (ftp_mkdir($this->ftp_conn, $config['dirname']))
        {
            return [TRUE, $config['dirname']];
        } else {
            return [FALSE, $config['dirname']];
        }
    }
	
	public function isftpdir($config=array())
    {
        if (ftp_nlist($this->ftp_conn, $config['dirname']) == false) {
			ftp_mkdir($this->ftp_conn, $config['dirname']);
			return [TRUE, $config['dirname']];
		} else {
            return [FALSE, $config['dirname']];
        }
    }
}