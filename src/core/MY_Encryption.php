<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Encryption extends CI_Encryption {

    public function __construct() {
        parent::__construct($params);
    }
    
    public function API_KEY($key, $hashAlog='gost-crypto')
    {
        if(!empty($key) && in_array($hashAlog, hash_algos()))
        {
            return hash($key, $hashAlog);
        } else {
            return FALSE;
        }
    }

}
