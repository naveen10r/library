<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Session extends CI_Session {

    public function __construct() {
        parent::__construct();
    }

    public function update_session(string $key, array $update)
    {
        $updateKey = key($update);
        $ExUserSess = $this->session->userdata($sessKey);
        $ExUserSess[$updateKey] = $update[$updateKey];
        $this->session->set_userdata($key, $ExUserSess);
        return true;
    }

}
