<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 */

class ProductService extends TableName {

    public function __construct() {
        parent::__construct();
    }

    public function saveProduct(array $insert): int {
        $this->db->trans_start();
        $this->db->insert($this->product, $insert);
        return $this->trans_insert_data();
    }

    public function UpdateProduct(array $update, array $where) {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->product, $update);
        return $this->trans_update_data();
    }

    public function getProductDetails(array $where, $where_like = NULL) {
        if ($where_like != NULL) {
            $this->db->select('title');
            $this->db->like('title', $where_like);
        } else {
            $this->db->select('*');
        }
        $this->db->from($this->product);
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function getProductAuthorJunctionDetails(array $where) {
        $this->db->select('author_id');
        $this->db->from($this->product_author_junction);
        $this->db->where($where);
        $result_data = $this->db->get()->result();
        return $result_data;
    }

    public function getProductCategoryJunctionDetails(array $where) {
        $this->db->select('*');
        $this->db->from($this->product_category_junction);
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function getallProduct(array $where) {
        $this->db->select('id,title,author_id,publisher_id,mrp,category_id,status, cover_image,modified_at');
        $this->db->from($this->product);
        $this->db->where($where);
        $this->db->order_by('id', 'DESC');
        return $this->db->get()->result();
    }

    public function GetAllProductMedia(array $where, array $select = array()) {
        $this->db->select('prm.id, prm.client_name, prm.file_for_label, prm.file_name, prm.full_path, prm.is_visible, prm.is_image, pr.title');
        $this->db->select($select);
        $this->db->from($this->product . ' AS pr');
        $this->db->join($this->productMedia . ' AS prm', 'pr.id = prm.product_id', 'LEFT');
        $this->db->where($where);
        $this->db->limit(15);
        return $this->db->get()->result();
    }

    public function saveProductMedia(array $insert): int {
        $this->db->trans_start();
        $this->db->insert($this->productMedia, $insert);
        return $this->trans_insert_data();
    }

    public function UpdateProductMedia(array $where, array $update): int {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->productMedia, $update);
        return $this->trans_update_data();
    }

    public function getProductMedia(array $where) {
        $this->db->select(array('product_id', 'file_name', 'file_path'));
        $this->db->from($this->productMedia);
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function getproductCategoryList(array $where) {
        $this->db->select('*');
        $this->db->from($this->productCategory);
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function getAuthorList(array $where) {
        $this->db->select('id,name');
        $this->db->from($this->author);
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function getProductTagsList(array $where) {
        $this->db->select('id,name');
        $this->db->from($this->product_tags);
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function saveMetaTags(array $insert): int {
        $this->db->trans_start();
        $this->db->insert($this->product_tags, $insert);
        $insert_id = $this->trans_insert_data();
        if ($insert_id) {
            return $insert_id;
        } else {
            return $this->db->error();
        }
    }

    public function saveProductAuthor(array $insert): int {
        $this->db->trans_start();
        $this->db->insert($this->product_author_junction, $insert);
        return $this->trans_insert_data();
    }

    public function saveProductCatgeory(array $insert): int {
        $this->db->trans_start();
        $this->db->insert($this->product_category_junction, $insert);
        return $this->trans_insert_data();
    }

    public function deleteProductAuthor(array $where) {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->delete($this->product_author_junction);
        return $this->trans_insert_data();
    }

    public function deleteProductCatgeory(array $where) {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->delete($this->product_category_junction);
        return $this->trans_insert_data();
    }
    
    public function get_last_isbn($insert_id, $sort_code) {
        $this->db->select('isbn');
        $this->db->from($this->product);
        $this->db->like('isbn', $sort_code, 'after');
        $this->db->order_by('id','DESC');
        $this->db->limit(1);
        $isbn = $this->db->get()->row();
        $newIsbn = '';
        if (empty($isbn)){
            $newIsbn = $sort_code.'000001';
        } else {
            $isbnCode = str_replace($sort_code, '', $isbn->isbn);
            $NextIsbn = (int)$isbnCode;
            $NextIsbn_1 = $NextIsbn + 1;
            
            switch(strlen((string)$NextIsbn_1)){
                case 1 : $newIsbn = $sort_code.'00000'.$NextIsbn_1; break;
                case 2 : $newIsbn = $sort_code.'0000'.$NextIsbn_1; break;
                case 3 : $newIsbn = $sort_code.'000'.$NextIsbn_1; break;
                case 4 : $newIsbn = $sort_code.'00'.$NextIsbn_1; break;
                case 5 : $newIsbn = $sort_code.'0'.$NextIsbn_1; break;
                default : $newIsbn = $sort_code.$NextIsbn_1; break;
            }
        }
        $this->db->trans_start();
        $this->db->where(array('id' => $insert_id));
        $this->db->update($this->product, array('isbn' => $newIsbn));
        return $this->trans_insert_data();
    }

}
