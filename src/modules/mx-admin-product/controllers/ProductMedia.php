<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait ProductMedia {

    public function Media($product_id) {
        $product_id = dsecure($product_id);
        $data['allMedia'] = $this->ProductService->GetAllProductMedia(array('prm.is_delete' => 'N', 'prm.product_id' => $product_id));
        $prTitle = '';
        if (!empty($data['allMedia'])) {
            $prTitle = $data['allMedia'][0]->title;
        }
        $data['prTitle'] = $prTitle;
        $data['product_id'] = $product_id;
        $this->load->view('Product_Media', $data);
    }
    
    private function new_client_name($string){
        $string = str_replace(' ', '-', $string);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    public function SaveProMedia() {
        if (verify_csrf() == false) {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'CSRF not verifyed'));
            redirect($this->agent->referrer());
        }
        $product_id = $this->input->post('product_id', TRUE);
        $allMedia = $this->ProductService->GetAllProductMedia(array('prm.is_delete' => 'N', 'prm.product_id' => $product_id));
        if (count($allMedia) >= 15) {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'No remain count for upload.'));
            redirect($this->agent->referrer());
        }
        $fileName = md5(rand_string(8) . time() . rand_string(8));
        $config['upload_path'] = 'upload/product/' . $product_id . '/image/';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size'] = 1024 * 1024;
        $config['file_name'] = $fileName;
        $config['detect_mime'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('ProductMedia')) {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->upload->display_errors()));
        } else {
            $isSave = $this->upload->data();
            $insert = array(
                'product_id' => $product_id,
                'file_name' => $isSave['file_name'],
                'file_path' => $isSave['file_path'],
                'full_path' => base_url('upload/product/' . $product_id . '/image/' . $isSave['file_name']),
                'raw_name' => $isSave['raw_name'],
                'file_for_label' => 'OTHER',
                'client_name' => $this->new_client_name($isSave['client_name']),
                'file_ext' => $isSave['file_ext'],
                'file_size' => $isSave['file_size'],
                'is_image' => $isSave['is_image'],
                'image_size_str' => $isSave['image_size_str'],
                'created_at' => $this->userId,
                'create_ts' => date(SYS_DB_DATE_TIME),
                'is_visible' => 'N',
                'is_delete' => 'N',
            );


            $FtpUploadconfig['destination_filepath'] = 'upload/product/' . $product_id . '/image/' . $isSave['file_name'];
            $FtpUploadconfig['source_filepath'] = base_url('upload/product/' . $product_id . '/image/' . $isSave['file_name']);
            /* $this->mediaftp->put_object($FtpUploadconfig); */
            $this->load->library('Upload_lib');
            $pathToImages = $isSave['full_path'];
            $pathToThumbs = $config['upload_path'] . '130_';
            $thumbWidth = 130;
            $this->upload_lib->createThumbs($pathToImages, $pathToThumbs, $thumbWidth);

            $FtpUploadThumbsconfig['destination_filepath'] = 'upload/product/' . $product_id . '/image/130_' . $isSave['file_name'];
            $FtpUploadThumbsconfig['source_filepath'] = base_url('upload/product/' . $product_id . '/image/130_' . $isSave['file_name']);
            /* $this->mediaftp->put_object($FtpUploadThumbsconfig); */

            $this->ProductService->saveProductMedia($insert);
            $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'File Uploaded'));
            /*
            unlink(getcwd() . '/upload/product/' . $product_id . '/image/' . $isSave['file_name']);
            unlink(getcwd() . '/upload/product/' . $product_id . '/image/130_' . $isSave['file_name']);
            */
        }
        redirect($this->agent->referrer());
    }

    public function UpdateProMedia() {
        $product_id = $this->input->post('product_id', TRUE);
        $media_for = $this->input->post('media_for', TRUE);
        $is_visible = $this->input->post('is_visible', TRUE);
        $img_id = $this->input->post('img_id', TRUE);
        $file_name_main = $this->input->post('file_name_main', TRUE);
        $fileName = md5(rand_string(8) . time() . rand_string(8));
        $config['upload_path'] = 'upload/product/' . $product_id . '/image/';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size'] = 1024 * 1024;
        $config['file_name'] = $fileName;
        $config['detect_mime'] = TRUE;
        $this->load->library('upload', $config);
        $isSave = array();
        $update = array();
        $coverPage = array();
        if (!empty($_FILES['updated_img'])) {
            if (!$this->upload->do_upload('updated_img')) {
                $isSave = $this->upload->display_errors();
            } else {
                $isSave = $this->upload->data();
                $update['file_name'] = $isSave['file_name'];
                $update['file_path'] = $isSave['file_path'];
                $update['full_path'] = base_url($config['upload_path'] . $isSave['file_name']);
                $update['raw_name'] = $isSave['raw_name'];
                $update['client_name'] = $this->new_client_name($isSave['client_name']);
                $update['file_ext'] = $isSave['file_ext'];
                $update['file_size'] = $isSave['file_size'];
                $update['is_image'] = $isSave['is_image'];
                $update['image_size_str'] = $isSave['image_size_str'];
                $coverPage = $update;
                $this->load->library('Upload_lib');
                $pathToImages = $isSave['full_path'];
                $pathToThumbs = $config['upload_path'] . '130_';
                $thumbWidth = 130;
                $this->upload_lib->createThumbs($pathToImages, $pathToThumbs, $thumbWidth);
            }
        }
        if (!empty($media_for)) {
            $update['file_for_label'] = $media_for;
        }
        if (!empty($is_visible)) {
            $update['is_visible'] = $is_visible;
        }
        $where = array('id' => $img_id);
        $isUpdate = $this->ProductService->UpdateProductMedia($where, $update);
        if ($isUpdate) {
            if($media_for == 'MAIN'){
                $main_update = array('cover_image' => $file_name_main);
                $main_where = array('id' => $product_id);
                $this->ProductService->UpdateProduct($main_update, $main_where);
            }
            $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Updated'));
        } else {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Not update, Please try again'));
        }
        redirect($this->agent->referrer());
    }
    
    public function CreateProductDir($dir)
    {
        if (is_int($dir)) {
            if (is_dir(PRODUCT_DIR . $dir)) {
                return [TRUE, 'DIR already exist'];
            } else {
                $isMake = mkdir(PRODUCT_DIR . $dir, 0777);
                if ($isMake) {
                    index_html(PRODUCT_DIR . $dir);
                    mkdir(PRODUCT_DIR . $dir . '/image', 0777);
                    mkdir(PRODUCT_DIR . $dir . '/file', 0777);
                    mkdir(PRODUCT_DIR . $dir . '/video', 0777);
                    index_html(PRODUCT_DIR . $dir . '/image');
                    index_html(PRODUCT_DIR . $dir . '/file');
                    index_html(PRODUCT_DIR . $dir . '/video');
                }
                return true;
            }
        } else {
            return [FALSE, 'Error in creating DIR'];
        }
            
    }
    
    public function SaveVideoLink(){
        $youtube_link = $this->input->post('youtube_link'); 
        $youtube_title = $this->input->post('youtube_title', true); 
        $this->form_validation->set_rules('youtube_title', 'youtube title', 'trim|required');
        $this->form_validation->set_rules('youtube_link', 'youtube link', 'trim|required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => validation_errors()));
            $this->session->set_flashdata('YouTubeAccess', array('link' => $youtube_link, 'title' => $youtube_title));
            redirect($this->agent->referrer());
        } else {
            $product_id = $this->input->post('product_id', true);
            $youLink = $this->input->post('you_tube_link', true);
            $insert = array();
            $insert['file_name'] = '';
            $insert['file_path'] = '';
            $insert['full_path'] = $youtube_link;
            $insert['raw_name'] = '';
            $insert['client_name'] = $youtube_title;
            $insert['file_ext'] = '';
            $insert['file_size'] = '';
            $insert['is_image'] = 0;
            $insert['image_size_str'] = '';
            $insert['is_visible'] = 'Y';
            if ($youLink == 'NEW'){
                $insert['file_for_label'] = 'VIDEO';
                $insert['product_id'] = $product_id;
                $this->ProductService->saveProductMedia($insert);
            } else {
                $where = array('product_id' => $product_id, 'file_for_label' => 'VIDEO');
                $this->ProductService->UpdateProductMedia($where, $insert);
            }
            
            $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Set YouTube like'));
            redirect($this->agent->referrer());
        }
            
    }

}
