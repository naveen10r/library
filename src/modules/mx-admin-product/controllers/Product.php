<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require_once realpath(dirname(__FILE__)) . '/ProductMedia.php';

class Product extends CI_Controller {

    use ProductMedia;

    final function __construct() {
        parent::__construct();
        $this->load->helper(array('admin-links/admin_menu', 'admin-links/admin_head', 'admin-links/admin_foot', 'string', 'captcha_security'));
        $this->load->model(array('ProductService'));
        $this->load->library(array('upload_lib'));
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = (object) $this->session->userdata('userAuth');
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
        } else {
            redirect('mx-admin/logout');
        }
    }

    public function index() {
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        $data['productCategoryList'] = $this->ProductService->getproductCategoryList(array('is_active' => 1));
        $data['AuthorList'] = $this->ProductService->getAuthorList(array('is_active' => 1));
        $data['allProduct'] = $this->ProductService->getallProduct(array('is_delete' => 0));
        $this->load->view('list_products', $data);
    }

    public function approveProduct() {
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        $data['allProduct'] = $this->ProductService->getallProduct(array('is_delete' => 0, 'status' => 'publish'));
        $this->load->view('list_approve_products', $data);
    }
    
    public function validate_captcha(){
        if($this->input->post('captchaWord') != $this->session->tempdata('captcha_word'))
        {
            $this->form_validation->set_message('validate_captcha', 'Wrong captcha code, hmm are you the Terminator?');
            return false;
        }else{
            return true;
        }

    }

    public function addProduct() {
        $this->load->helper('admin-links/admin_comman');
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        
        $data['productCategoryList'] = $this->ProductService->getproductCategoryList(array('is_active' => 1));
        $data['AuthorList'] = $this->ProductService->getAuthorList(array('is_active' => 1));
        $data['ProductTagsList'] = $this->ProductService->getProductTagsList(array('is_active' => 1));
        
        if (!$this->input->post()) {
            if (!empty($this->uri->segment('4')) && $this->uri->segment('4') != "") {
                $where = array('id' => $this->uri->segment('4'));
                $ProductDetails = $this->ProductService->getProductDetails($where);
                $data['ProductDetails'] = $ProductDetails[0];

                $product_where = array('product_id' => $this->uri->segment('4'));

                $ProductAuthorJunctionDetails = array();
                $ProductAuthorJunctionDetails_Data = $this->ProductService->getProductAuthorJunctionDetails($product_where);
                if (count($ProductAuthorJunctionDetails_Data)) {
                    foreach ($ProductAuthorJunctionDetails_Data as $PAJ_row) {
                        $ProductAuthorJunctionDetails[] = $PAJ_row->author_id;
                    }
                }
                $data['ProductAuthorJunctionDetails'] = $ProductAuthorJunctionDetails;

                $ProductCategoryJunctionDetails = array();
                $ProductCategoryJunctionDetails_Data = $this->ProductService->getProductCategoryJunctionDetails($product_where);
                if (count($ProductCategoryJunctionDetails_Data)) {
                    foreach ($ProductCategoryJunctionDetails_Data as $PCJ_row) {
                        $ProductCategoryJunctionDetails[] = $PCJ_row->category_id;
                    }
                }
                $data['ProductCategoryJunctionDetails'] = $ProductCategoryJunctionDetails;
            }
            $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
            $this->load->view('add_products', $data);
        } else {
            $this->form_validation->set_rules('title', 'Title', 'required|trim|xss_clean');
            $this->form_validation->set_rules('description', 'Description', 'required|trim|xss_clean');
            $this->form_validation->set_rules('pub_edi', 'publication/edition', 'required|trim|xss_clean');
            $this->form_validation->set_rules('MRP', 'MRP', 'required|trim|xss_clean');
            $this->form_validation->set_rules('ISBN', 'ISBN', 'required|trim|xss_clean');
            $this->form_validation->set_rules('publish_year', 'Publish Year', 'required|trim|xss_clean');
            $this->form_validation->set_rules('baseprice', 'baseprice', 'trim|xss_clean');
            $this->form_validation->set_rules('meta_description', 'meta description', 'trim|xss_clean');
            $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim|xss_clean');
            $this->form_validation->set_rules('captchaWord', 'captcha word', 'required|trim|xss_clean|callback_validate_captcha');

            if ($this->form_validation->run() == FALSE) {
                if (!empty($this->input->post('product_id', TRUE)) && $this->input->post('product_id', TRUE) != "") {
                    $where = array('id' => $this->input->post('product_id', TRUE));
                    $ProductDetails = $this->ProductService->getProductDetails($where);
                    $data['ProductDetails'] = $ProductDetails[0];
                }
                $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
                $this->load->view('add_products', $data);
            } else {
                $title = $this->input->post('title', TRUE);
                $description = $this->input->post('description', TRUE);
                $pub_edi = $this->input->post('pub_edi', TRUE);
                $MRP = $this->input->post('MRP', TRUE);
                $ISBN = $this->input->post('ISBN', TRUE);
                $existing_isbn = $this->input->post('existing_isbn', TRUE);
                $offerprice = $this->input->post('offerprice', TRUE);
                $baseprice = $this->input->post('baseprice', TRUE);
                $buying_mode = $this->input->post('buying_mode', TRUE);
                $meta_description = $this->input->post('meta_description', TRUE);
                $publish_year = $this->input->post('publish_year', TRUE);
                $product_id = $this->input->post('product_id', TRUE);
                $meta_keywords = $this->input->post('meta_keywords', TRUE);
                $author = $this->input->post('author', TRUE);
                $category_list = $this->input->post('category_list', TRUE);

                $author_val = "";
                if (isset($author) && count($author) > 0) {
                    $author_val = implode(",", $author);
                }

                $category_val = "";
                if (isset($category_list) && count($category_list) > 0) {
                    $category_val = implode(",", $category_list);
                }
                $meta_keywords_val = "";
                if (isset($meta_keywords) && count($meta_keywords) > 0) {
                    $meta_keywords_val = implode(",", $meta_keywords);
                }


                if (!empty($product_id)) {
                    $save = array(
                        'title' => $title,
                        'content' => $description,
                        'publisher_id' => $pub_edi,
                        /* 'offerprice' => $offerprice, */
                        'baseprice' => $baseprice,
                        'buying_mode' => json_encode($buying_mode),
                        'meta_description' => $meta_description,
                        'meta_keywords' => $meta_keywords_val,
                        'publish_year' => date("Y-m-d", strtotime($publish_year)),
                        'author_id' => $author_val,
                        'category_id' => $category_val,
                        'mrp' => $MRP,
                        'modified_at' => date(SYS_DB_DATE_TIME)
                    );

                    $where = array('id' => $product_id);
                    $isUpdate = $this->ProductService->UpdateProduct($save, $where);
                    if($existing_isbn != $ISBN){
                        get_product_isbn($product_id, $ISBN);
                    }
                    if ($isUpdate) {

                        if (count($author)) {
                            $where_delete = array('product_id' => $product_id);
                            $this->ProductService->deleteProductAuthor($where_delete);
                            foreach ($author as $auth_row) {
                                $authorsave = array();
                                $authorsave = array(
                                    'product_id' => $product_id,
                                    'author_id' => $auth_row,
                                    'create_date' => date(SYS_DB_DATE_TIME),
                                    'modified_at' => date(SYS_DB_DATE_TIME)
                                );
                                $isAuthorSave = $this->ProductService->saveProductAuthor($authorsave);
                            }
                        }

                        if (count($category_list)) {
                            $where_delete = array('product_id' => $product_id);
                            $this->ProductService->deleteProductCatgeory($where_delete);
                            foreach ($category_list as $cat_row) {
                                $catgeorysave = array();
                                $catgeorysave = array(
                                    'product_id' => $product_id,
                                    'category_id' => $cat_row,
                                    'create_date' => date(SYS_DB_DATE_TIME),
                                    'modified_at' => date(SYS_DB_DATE_TIME)
                                );
                                $isCategorySave = $this->ProductService->saveProductCatgeory($catgeorysave);
                            }
                        }
                        $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Product Update successfully.'));
                    } else {
                        $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Please try again.'));
                    }
                    redirect('mx-admin-product/product');
                } else {
                    $save = array(
                        'title' => $title,
                        'content' => $description,
                        'publisher_id' => $pub_edi,
                        'offerprice' => $offerprice,
                        'baseprice' => $baseprice,
                        'buying_mode' => json_encode($buying_mode),
                        'mrp' => $MRP,
                        'author_id' => $author_val,
                        'category_id' => $category_val,
                        'publish_year' => date("Y-m-d", strtotime($publish_year)),
                        'meta_description' => $meta_description,
                        'meta_keywords' => $meta_keywords_val,
                        'date' => date(SYS_DB_DATE_TIME),
                        'modified_at' => date(SYS_DB_DATE_TIME)
                    );

                    $isSave = $this->ProductService->saveProduct($save);
                    $this->CreateProductDir($isSave);
                    if (count($author)) {
                        foreach ($author as $auth_row) {
                            $authorsave = array();
                            $authorsave = array(
                                'product_id' => $isSave,
                                'author_id' => $auth_row,
                                'create_date' => date(SYS_DB_DATE_TIME),
                                'modified_at' => date(SYS_DB_DATE_TIME)
                            );
                            $isAuthorSave = $this->ProductService->saveProductAuthor($authorsave);
                        }
                    }

                    if (count($category_list)) {
                        foreach ($category_list as $cat_row) {
                            $catgeorysave = array();
                            $catgeorysave = array(
                                'product_id' => $isSave,
                                'category_id' => $cat_row,
                                'create_date' => date(SYS_DB_DATE_TIME),
                                'modified_at' => date(SYS_DB_DATE_TIME)
                            );
                            $isCategorySave = $this->ProductService->saveProductCatgeory($catgeorysave);
                        }
                    }
                    get_product_isbn($isSave, $ISBN);
                }

                if ($isSave) {
                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Product Saved successfully.'));
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Something went wrong.'));
                }
                redirect('mx-admin-product/product');
            }
        }
    }

    /*
     * @author : Sandeep 
     */

    function randomHex() {
        $chars = 'ABCDEF0123456789';
        $color = '#';
        for ($i = 0; $i < 6; $i++) {
            $color .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $color;
    }

    /*
     * @author : Naveen 
     */

    public function productname_check() {

        $product_title_Input = $this->input->post('product_title_Input');
        $ProductDetails = $this->ProductService->getProductDetails(array(), $product_title_Input);

        if (count($ProductDetails)) {
            $auth_name_arr = array();
            foreach ($ProductDetails as $row_pd) {
                $random_color = $this->randomHex();
                $auth_name_arr[] = "<span style='color:$random_color'>" . $row_pd->title . "</span>";
            }
            $response = array(
                'status' => 'success',
                'message' => "Product Name already exists like " . implode(", ", $auth_name_arr)
            );
        } else {
            $response = array(
                'status' => 'success',
                'message' => "<p style='color:green;'>Now available </p>"
            );
        }



        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
    }

    /*
     * @author : Sandeep 
     */

    public function updateStatusProduct() {


        if (!$this->input->post()) {
            $response = array(
                'status' => 'error',
                'message' => "<p>Something went wrong.</p>"
            );
        } else {

            $this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean');
            $this->form_validation->set_rules('productid', 'Product ID', 'required|trim|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $response = array(
                    'status' => 'error',
                    'message' => validation_errors()
                );
            } else {
                $status = $this->input->post('status', TRUE);
                $productid = $this->input->post('productid', TRUE);

                $update = array(
                    'status' => $status,
                    'modified_at' => date(SYS_DB_DATE_TIME)
                );
                $where = array('id' => $productid);
                $isSave = $this->ProductService->UpdateProduct($update, $where);
                $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Product Update successfully.'));
                $response = array(
                    'status' => 'success',
                    'message' => "<p>Update successfully.</p>"
                );
            }
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
    }

    /*
     * @author : Sandeep 
     */

    public function SaveMetaTags() {


        if (!$this->input->post()) {
            $response = array(
                'status' => 'error',
                'message' => "<p>Something went wrong.</p>"
            );
        } else {

            $this->form_validation->set_rules('meta_tags', 'Meta Tags', 'required|trim|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $response = array(
                    'status' => 'error',
                    'message' => validation_errors()
                );
            } else {
                $meta_tags = $this->input->post('meta_tags', TRUE);

                $insertData = array(
                    'name' => $meta_tags,
                    'created_at' => date(SYS_DB_DATE_TIME),
                    'modified_at' => date(SYS_DB_DATE_TIME)
                );
                $isSave = $this->ProductService->saveMetaTags($insertData);
                if ($isSave) {
                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Tag Add Successfully.'));
                    $response = array(
                        'status' => 'success',
                        'message' => "<p style='color:green;'>Tag Add Successfully.</p>"
                    );
                }
            }
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
    }

    private function fileWithValidation($picture, $fileExt) {
        $pictures = !empty($picture) ? $picture : 'pictures';
        $upImg = $_FILES['uploadImg'];
        if (empty($upImg['name'])) {
            $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is required.');
            return false;
        } else {
            $realExt = strtolower($upImg['name']);
            $ext = explode('.', $realExt);
            $extension = end($ext);
            $fileExt = explode('|', $fileExt);
            if (!in_array($extension, $fileExt)) {
                $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is not in the correct format.');
                return false;
            }
        }
        return true;
    }

    private function captchaWord($word) {
        $captcha_word = $this->session->tempdata('captcha_word');
        if (empty($word)) {
            $this->form_validation->set_message('captchaWord', 'The captcha field is required.');
            return false;
        } elseif ($captcha_word != $word) {
            $this->form_validation->set_message('captchaWord', 'The captcha security code doesn\'t matched.');
            return false;
        } else {
            return true;
        }
    }

}
