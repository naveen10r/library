<?php ?>

<!DOCTYPE html>

<html lang="en">

    <head>

        <title>Matrix Admin</title>

        <meta charset="UTF-8" />

        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />

        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />

        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />

        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/select2.css'); ?>" />

        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />

        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />

        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css' />

    </head>

    <body>

        <!--Header-part-->

        <?php echo admin_head(); ?>

        <!--close-Header-part--> 



        <!--top-Header-menu-->

        <?php echo admin_head_menu(); ?>



        <!--start-top-serch-->

        <?php echo admin_head_search(); ?>

        <!--close-top-serch--> 



        <!--sidebar-menu-->

		<?php echo admin_menu('product'); ?>

        <!--sidebar-menu-->

        <div id="content">

            <div id="content-header">

                <div id="breadcrumb"> 

                    <a href="#" class="tip-bottom"><i class="icon-home"></i> Home</a> 

                    <a href="#" class="current">Products</a> 

                </div>

            </div>

            <div class="container-fluid">

                <div class="row-fluid">

                    <div id="alertResponce">

                        <?php

                        if ($this->session->flashdata('alert')) {

                            $alert = $this->session->flashdata('alert');

                            echo $alert['color']($alert['responce']);

                        }

                        ?> 

                    </div>

                    <div class="span11">

                        <div class="widget-box">

                            <div class="widget-title">

                                <span class="icon">

                                    <input type="checkbox" id="title-checkbox" name="title-checkbox" />

                                </span>

                                <h5><?php echo 'All Approve Product'; ?></h5>

                            </div>

                            <div class="widget-content nopadding">

                                <?php

                                echo form_open('#', array('id' => 'changeStatus', 'method' => 'post'));

                                echo input_csrf();

                                ?>

                                <table class="table table-bordered data-table">

                                    <thead >

                                        <tr >

                                            <th><i class="icon-resize-vertical"></i></th>

                                            <th ><?php echo 'Title'; ?></th>

                                            <th><?php echo 'Author'; ?></th>

                                            <th><?php echo 'Publisher'; ?></th>

                                            <th><?php echo 'MRP'; ?></th>

                                            <th><?php echo 'Category'; ?></th>

                                            <th><?php echo 'Status'; ?></th>

                                    </tr>

                                    </thead>

                                    <tbody>

                                        <?php

                                        $i = 0;

                                        foreach ($allProduct as $iu):

                                            $i++;

                                            $class = '';

                                            if ($i == 1) {

                                                $class = 'gradeX';

                                            } elseif ($i == 2) {

                                                $class = 'gradeC';

                                            } else {

                                                $class = 'gradeA';

                                            }
											//id,title,,,,category_id,123

                                            ?>

                                            <tr <?php echo 'class="' . $class . '"'; ?>>

                                                <td width="4%"><input type="checkbox" name="checked[]" value="<?php echo $iu->id; ?>" />

                                                </td>

                                                <td width="16%">

                                       <?php echo (isset($iu->title) && $iu->title!=""?$iu->title:""); ?>

                                        </td>

                                        <td width="15%"><?php echo (isset($iu->author_id) && $iu->author_id!=""?$iu->author_id:""); ?></td>

                                        <td width="20%"><?php echo (isset($iu->publisher_id) && $iu->publisher_id!=""?$iu->publisher_id:""); ?></td>

                                        <td width="10%"><?php echo (isset($iu->mrp) && $iu->mrp!=""?$iu->mrp:""); ?></td>

                                        <td width="15%"><?php echo (isset($iu->category_id) && $iu->category_id!=""?$iu->category_id:""); ?></td>

                                        <td class="taskStatus" width="10%">

                                            <?php echo (isset($iu->status) && $iu->status!=""?ucwords($iu->status):""); ?>

                                        </td>

                                        </tr>

                                    <?php endforeach; ?> 

                                    </tbody>


                                </table>

                                <?php echo form_close(); ?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
		


        <!--Footer-part-->

        <?php echo admin_footer(); ?>

        <!--end-Footer-part-->

        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 

        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 

        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 

        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 

        <script src="<?php echo base_url('web-inf/js/select2.min.js'); ?>"></script> 

        <script src="<?php echo base_url('web-inf/js/jquery.dataTables.min.js'); ?>"></script> 

        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script> 

        <script src="<?php echo base_url('web-inf/js/matrix.tables.js'); ?>"></script>

        <script type="text/javascript">
            $('.showaction').on('click',function(){
				root = location.protocol + '//' + location.host;
				var status = $(this).attr('data-value');
				var productid = $(this).attr('data-productid');
				$.ajax({
					url: "<?php echo base_url('mx-admin-product/Product/updateStatusProduct'); ?>",
					type: 'post',
					data:'status='+ status+'&productid='+ productid,
					success: function(response){
						if(response.status == 'success') {
							root = location.protocol + '//' + location.host;
							var delay = 10; 
							setTimeout(function(){ window.location = "<?php echo base_url('mx-admin-product/product'); ?>";}, delay);
						}
					}
				});
            });

        </script>

    </body>

</html>