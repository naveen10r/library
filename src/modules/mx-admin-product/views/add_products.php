<?php

$id = '';
$author = '';
$date = '';
$miliseconds = '';
$author_id = '';
$publisher_id = '';
$title = '';
$description = '';
$cover_image = '';
$isbn = '<option value="">---select---</option>';
$isbnCode = '';
$buying_mode = [];
$baseprice = '';
$offerprice = '';
$mrp = '';
$excerpt = '';
$status = '';
$modified_at = '';
$publish_year = '';
$category_id = '';
$filter_param = '';
$meta_keywords = '';
$meta_description = '';
if (!empty($ProductDetails)){
    $id = $ProductDetails->id;
    $author = $ProductDetails->author;
    $date = $ProductDetails->date;
    $miliseconds = $ProductDetails->miliseconds;
    $author_id = $ProductDetails->author_id;
    $publisher_id = $ProductDetails->publisher_id;
    $title = $ProductDetails->title;
    $description = $ProductDetails->content;
    $cover_image = $ProductDetails->cover_image;
    $isbn = $ProductDetails->isbn;
    $existingISBN = isset($ProductDetails->isbn) ? $ProductDetails->isbn : "";
    $isbnCode = !empty($existingISBN) ? substr($existingISBN, 0, 4) : "";
    $buying_mode = $ProductDetails->buying_mode;
    $baseprice = $ProductDetails->baseprice;
    $offerprice = $ProductDetails->offerprice;
    $mrp = $ProductDetails->mrp;
    $excerpt = $ProductDetails->excerpt;
    $status = $ProductDetails->status;
    $modified_at = $ProductDetails->modified_at;
    $publish_year = $ProductDetails->publish_year;
    $category_id = $ProductDetails->category_id;
    $filter_param = $ProductDetails->filter_param;
    $meta_keywords = $ProductDetails->meta_keywords;
    $meta_description = $ProductDetails->meta_description;
}
if (!empty(set_value('title'))){ $title = set_value('title'); }
if (!empty(set_value('description'))){ $description = set_value('description'); }
if (!empty(set_value('pub_edi'))){ $publisher_id = set_value('pub_edi'); }
if (!empty(set_value('MRP'))){ $mrp = set_value('MRP'); }
if (!empty(set_value('ISBN'))){ $isbn = set_value('ISBN'); }
if (!empty(set_value('publish_year'))){ $publish_year = set_value('publish_year'); }
if (!empty(set_value('baseprice'))){ $baseprice = set_value('baseprice'); }
if (!empty(set_value('meta_description'))){ $meta_description = set_value('meta_description'); }
if (!empty(set_value('$meta_keywords'))){ $meta_description = set_value('meta_keywords'); }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-wysihtml5.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/datepicker.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/select2.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    </head>
    <body>

        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu('product'); ?>
        <!--sidebar-menu-->
        <!--close-left-menu-stats-sidebar-->

        <div id="content">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="span10">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                <h5>Add Product</h5>
                                <?php echo MANDATORY; ?>
                            </div>
                            <div class="widget-content nopadding">
                                <?php
                                echo form_open_multipart('mx-admin-product/Product/addProduct', array('class' => 'form-horizontal', 'id' => 'saveProduct'));
                                echo input_csrf();
                                ?>
                                <div class="control-group">
                                    <label class="control-label">Title <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Title of product [Ex: SQL, PL/SQL by Ivan Bayross]"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" autocomplete="off"  maxlength="100" class="span11 product_title" name="title" value="<?php echo $title; ?>" required="true" />
                                        <span class="help-inline red_error product_title_check"><?php echo form_error('title'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Category List <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" ><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <select name="category_list[]" id="category_list" multiple>
                                            <?php
                                            echo "<option value=''>Select Category</option>";
                                            foreach ($productCategoryList as $cat_row) {
                                                $selected = "";
                                                if (in_array($cat_row->id, $ProductCategoryJunctionDetails)) {
                                                    $selected = "selected='selected'";
                                                }
                                                echo "<option value='" . $cat_row->id . "' " . $selected . ">" . $cat_row->name . "</option>";
                                                if ($isbnCode == $cat_row->slug_code){
                                                    $isbn .= "<option value='" . $cat_row->slug_code . "' selected='selected'>" . $cat_row->name . "</option>";
                                                } else {
                                                    $isbn .= "<option value='" . $cat_row->slug_code . "'>" . $cat_row->name . "</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="help-inline red_error"><?php echo form_error('category_list'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Author <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Author Name"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <select name="author[]" id="author" multiple>
                                            <?php
                                            echo "<option value=''>Select Author</option>";
                                            foreach ($AuthorList as $author_row) {
                                                $selected = "";
                                                if (in_array($author_row->id, $ProductAuthorJunctionDetails)) {
                                                    $selected = "selected='selected'";
                                                }
                                                echo "<option value='" . $author_row->id . "' " . $selected . ">" . $author_row->name . "</option>";
                                            }
                                            ?>
                                        </select>
                                        <span class="help-inline red_error"><?php echo form_error('author'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Books for Section 
                                        <a class="tip-top" data-original-title="Books for"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <select name="bookeagesection" id="bookeagesection" >
                                            <option value=''>All </option>
                                            <option value='0-to-3-years' >0-to-3-years</option>
                                            <option value='3-to-5-years' >3-to-5-years</option>
                                            <option value='5-to-8-years' >5-to-8-years</option>
                                            <option value='8-to-12-years' >8-to-12-years</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Publication/Edition <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Publication company & edition with year"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text"  maxlength="80" class="span11" name="pub_edi" value="<?php echo $publisher_id; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('pub_edi'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Description <sup class="red_error">&#042;</sup>
                                        <a class="tip-top"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls-ckeditor">
                                        <textarea id="description" style="width: 600px; height: 170px;" name="description" cols="50" rows="10" ><?php echo $description; ?></textarea>

                                        <span class="help-inline red_error"><?php echo form_error('description'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">MRP <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="MRP"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="number"  autocomplete="off"  maxlength="10" class="span11" name="MRP" value="<?php echo $mrp; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('MRP'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Offer Price <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Offer Price"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="number"  autocomplete="off"  maxlength="10" class="span11" name="offerprice" value="<?php echo $offerprice; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('offerprice'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Base Price <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Base Price"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="number"  autocomplete="off" maxlength="10" class="span11" name="baseprice" value="<?php echo $baseprice; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('baseprice'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">ISBN <sup class="red_error">&#042;</sup>
                                    </label>
                                    <div class="controls">
                                        <select name="ISBN" style="width: 400px;">
                                            <?php echo $isbn; ?>
                                        </select>
                                        Current ISBN Code : <strong><?php echo $existingISBN; ?></strong>
                                        <input type="hidden" name="existing_isbn" value="<?php echo $isbnCode; ?>" />
                                        <span class="help-inline red_error"><?php echo form_error('ISBN'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Publish Year <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="ISBN"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">

                                        <div  data-date="12-02-2012" class="input-append date datepicker">
                                            <input type="text" value="<?php echo (!empty($publish_year)) ? date('m-d-Y', strtotime($publish_year)) : date('m-d-Y'); ?>"  required="true" name="publish_year" data-date-format="mm-dd-yyyy" class="span11" >
                                            <span class="add-on"><i class="icon-th"></i></span> 
                                        </div>
                                        <span class="help-inline red_error"><?php echo form_error('publish_year'); ?></span> 
                                    </div>

                                </div>
                                <div class="control-group">
                                    <label class="control-label">SEO MetaKeywords <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" ><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
<?php $ProductTagsListDetails = explode(",", $ProductDetails->meta_keywords); ?>
                                        <select name="meta_keywords[]" id="meta_keywords" multiple="">
<?php
echo "<option value='Best Reading Book'>Best Reading Book</option>"; 
foreach ($ProductTagsList as $product_tags_row) {
    $selected = "";
    if (in_array($product_tags_row->name, $ProductTagsListDetails)) {
        $selected = "selected='selected'";
    }
    echo "<option value='" . $product_tags_row->name . "' " . $selected . ">" . $product_tags_row->name . "</option>";
}
?>
                                        </select>
                                        <input type="text" style=" margin-top: 12px;" maxlength="255" class="add_meta_tags" id="add_meta_tags" name="add_meta_tags" value=""  />
                                        <span class="help-inline save_meta_tags" style="color: blue; padding-top: 10px;cursor: pointer;">Add New Tag</span><br/>
                                        <span class="help-inline red_error save_meta_tags_error"><?php echo form_error('meta_keywords'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Meta Description <sup class="red_error">&#042;</sup>
                                        <a class="tip-top"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <textarea id="meta_description"  maxlength="255" style="width: 627px; height: 70px;" name="meta_description" cols="30" rows="10" ><?php echo $meta_description; ?></textarea>

                                        <span class="help-inline red_error"><?php echo form_error('meta_description'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Buying Mode <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Buying Mode"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <label class="radio-inline">
<?php
$rent = '';
if (!empty($ProductDetails->buying_mode)) {
    $buying_mode = json_decode($ProductDetails->buying_mode);
    if (in_array('RENT', $buying_mode)) {
        $rent = 'checked="checked"';
    }
}
?>
                                            <input type="checkbox" name="buying_mode[]" value="RENT" <?php echo $rent; ?> />Rent
                                            <input type="checkbox" name="buying_mode[]" value="BUY" checked="checked" />Buy
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Check Robotic <sup class="red_error">&#042;</sup>
                                    </label>
                                    <div class="controls">
                                        <div class="span2">
                                            <img src="<?php echo base_url('captcha/' . $captcha['filename']); ?>" id="captcha_security" />
                                        </div>
                                        <div class="span1"><a href="javascript:void(0)" onclick="refresh_image();"><i class="icon-repeat"></i></a></div>
                                        <input type="text" name="captchaWord" class="span3" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('captchaWord'); ?></span> 
                                    </div>
                                </div>
                                <div class="form-actions">
<?php
if (isset($ProductDetails->id) && !empty($ProductDetails->id)) {
    echo '<input type="hidden" name="product_id" id="product_id" value="' . $ProductDetails->id . '" />';
    echo '<button type="submit" class="btn btn-success"><i class="icon-save"></i> Update</button>';
} else {
    echo '<button type="submit" class="btn btn-success"><i class="icon-save"></i> Save</button>';
}
?>
                                </div>
                                    <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 2013 &copy; Matrix Admin.</div>
        </div>
        <!--end-Footer-part--> 
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap-wysihtml5.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/matrix.popover.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/bootstrap-datepicker.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/select2.min.js'); ?>"></script> 
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('.datepicker').datepicker({maxDate: $.now()});
                $("#author").select2();
                $("#category_list").select2();
                $("#meta_keywords").select2();


                CKEDITOR.replace('description');

                /***************** Add Tags ********************/
                $('.save_meta_tags').on('click', function () {
                    var meta_tags = $("#add_meta_tags").val();
                    if (meta_tags != "")
                    {
                        $.ajax({
                            url: "<?php echo base_url('mx-admin-product/product/SaveMetaTags'); ?>",
                            type: 'post',
                            data: 'meta_tags=' + meta_tags,
                            success: function (response) {
                                if (response.status == 'success') {
                                    $('#meta_keywords').append('<option value="' + meta_tags + '">' + meta_tags + '</option>');
                                    $(".save_meta_tags_error").html(response.message);
                                    $("#add_meta_tags").val('');
                                } else {
                                    $(".save_meta_tags_error").html(response.message);
                                }
                            }
                        });
                    }
                });
                /*************************************/

                /*************************************/
                $('.product_title').keyup(function () {
                    var product_title_Input = this.value;
                    product_title_Input = product_title_Input.toLowerCase();
                    //dInput = dInput.replace(/[^a-zA-Z0-9]+/g,'-');
                    //$(".slug").val(dInput);  
                    if (product_title_Input.length > 3)
                    {
                        console.log(product_title_Input);
                        $.ajax({
                            url: '<?php echo site_url("mx-admin-product/product/productname_check"); ?>',
                            type: 'post',
                            data: 'product_title_Input=' + product_title_Input,
                            success: function (response) {
                                console.log(response);
                                $(".product_title_check").html(response);
                                if (response.status == 'success')
                                {
                                    $(".product_title_check").html(response.message);
                                } else
                                {
                                    $(".product_title_check").html(response.message);
                                }
                            }
                        });
                    }
                });
                /*************************************/

            });

            function refresh_image(){
                $.ajax({
                    url: "<?php echo base_url('default/general/refresh_captcha'); ?>",
                    type: 'get',
                    success: function (response) {
                        json_str = JSON.parse(response);
                        if (json_str.status === true) {
                            filename_txt = json_str.response_data.filename;
                            source_img = "<?php echo base_url('captcha/'); ?>" + filename_txt;
                            $('#captcha_security').attr("src", source_img);
                        }
                    }
                });
            }

        </script>
    </body>
</html>