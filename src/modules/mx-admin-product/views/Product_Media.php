<?php
$this->load->library('encryption');
$mediaType = array();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <meta charset="UTF-8" />
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 
        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>
        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 
        <!--sidebar-menu-->
        <?php echo admin_menu(); ?>
        <!--sidebar-menu-->
        <div id="content">
            <div id="content-header">
                <div id="breadcrumb"> 
                    <a href="<?php echo site_url('mx-admin'); ?>" class="tip-bottom"><i class="icon-home"></i> Home</a> 
                    <a href="<?php echo site_url('mx-admin-product/product'); ?>">Products</a> 
                    <a href="#" class="current">Media</a> 
                </div>
                <h1>Product Gallery</h1>  
            </div>
            <div class="container-fluid"><hr>
                <div class="row-fluid">
                    <div class="span12">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-picture"></i> </span>
                                <h5>TITLE : <?php echo $prTitle; ?></h5>
                            </div>
                            <div class="widget-content">
                                <ul class="thumbnails">
                                    <?php
                                    $youTubeTitle = '';
                                    $youTubeLink = '';
                                    $youLink = "NEW";
                                    if ($allMedia):
                                        foreach ($allMedia as $md):
                                            $mediaType[$md->file_for_label][] = $md->file_for_label;
                                            if ($md->file_for_label == 'VIDEO'){
                                                $li = '';
                                                $li .= '<li class="span2" title="' . $md->client_name . ' [' . $md->file_for_label . ']">';
                                                $li .= '<a href="#">';
                                                $li .= '<iframe width="130" height="185" src="https://www.youtube.com/embed/'.$md->full_path.'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                                                $li .= '</a>';
                                                $li .= '</li>';
                                                echo $li;
                                                $youTubeTitle = $md->client_name;
                                                $youTubeLink = html_entity_decode($md->full_path);
                                                $youLink = 'OLD';
                                            } else {
                                                $imgSrc = base_url('upload/product/' . $product_id . '/image/' . $md->file_name);
                                                $imgSrc_130 = base_url('upload/product/' . $product_id . '/image/130_' . $md->file_name);
                                                $li = '';
                                                $li .= '<li class="span2" title="' . $md->client_name . ' [' . $md->file_for_label . ']">';
                                                $li .= '<a href="#">';
                                                $li .= '<img src="' . $imgSrc_130 . '" alt="' . $md->client_name . ' [' . $md->file_for_label . ']">';
                                                $li .= '</a>';
                                                $li .= '<div class="actions">';
                                                $li .= '<a class="" href="javascript:void(0)" data-toggle="modal" data-target="#EditImages" onclick="return EditImagesModal(' . $md->id . ')"><i class="icon-pencil"></i></a>';
                                                $li .= '<a class="lightbox_trigger" href="' . $imgSrc . '"><i class="icon-search"></i></a>';
                                                $li .= '</div>';
                                                $li .= '<input type="hidden" id="img_' . $md->id . '" value=\'' . json_encode($md, JSON_HEX_APOS) . '\' />';
                                                $li .= '</li>';
                                                echo $li;
                                            }
                                        endforeach;
                                    endif;
                                    $remain = 15 - count($allMedia);
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="widget-content nopadding">
                            <ul class="activity-list">
                                <li><i class="icon-picture"></i> <strong>Totla Images</strong> Only 15 Item(s) allowed for media and 10 is active condition </li>
                                <li><i class="icon-picture"></i> <strong>Remain uploads </strong> <span><?php echo $remain; ?></span></li>
                                <?php
                                foreach (PRODUCT_IMG_CATEGORY as $key => $cat):
                                    $count = !empty($mediaType[$key]) ? count($mediaType[$key]) : 0;
                                    if($key == 'VIDEO') {
                                        $site_url = site_url('mx-admin-product/product/SaveVideoLink');
                                        echo '<li><i class="icon-picture"></i> <strong>' . $cat . '</strong> <span> (' . $count . ')</span><br /><form method="post" action="'.$site_url.'"><div class="form-group"><input type="text" class="form-control" id="youtube_title" name="youtube_title" placeholder="YouTube Page Title" value="'.$youTubeTitle.'" /><input type="text" class="form-control" id="youtube_link" name="youtube_link" placeholder="YouTube embed link" value="'.$youTubeLink.'" /><input type="hidden" name="product_id" value="'.$product_id.'" /><input type="hidden" name="you_tube_link" value="'.$youLink.'" /><button type="submit" class="btn btn-primary">Submit</button></div></form></li>';
                                    } else {
                                        echo '<li><i class="icon-picture"></i> <strong>' . $cat . '</strong> <span> (' . $count . ')</span></li>';
                                    }                                    
                                endforeach;
                                ?>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        if ($remain > 0):
                            echo form_open_multipart('mx-admin-product/product/SaveProMedia', array('id' => 'product_img', ''));
                            echo csrf_token();
                            echo form_hidden(array('product_id' => $product_id));
                            echo '<input type="file" name="ProductMedia" onchange="return SaveMedia();" />';
                            echo form_close();
                        else :
                            echo '<div class="alert alert-info">No remain upload</div>';
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="EditImages" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    </div>
                    <?php
                    echo form_open_multipart('mx-admin-product/product/UpdateProMedia', array('id' => 'update_product_img', 'class' => ''));
                    ?>
                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" id="product_id" />
                    <input type="hidden" name="img_id" value="<?php echo $product_id; ?>" id="img_id" />
                    <input type="hidden" name="file_name_main" value="" id="file_name_main" />
                    <div class="modal-body">
                        <div class="widget-content nopadding">
                            <div class="control-group">
                                <div class="controls">
                                    <img src="" id="popup_img" style="height: 150px;" class="rounded pull-left" height="200" />
                                    <input type="file" class="pull-right" name="updated_img" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Media for</label>
                                <div class="controls">
                                    <select name="media_for">
                                        <?php
                                        $option = '<option value="">--- select ---<option>';
                                        foreach (PRODUCT_IMG_CATEGORY as $key => $img_cat) {
                                            $option .= '<option value="' . $key . '">' . $img_cat . '<option>';
                                        }
                                        echo $option;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Visible On Page</label>
                                <div class="controls">
                                    <select name="is_visible">
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    <?php
                    echo form_close();
                    ?>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <?php echo admin_footer(); ?>
        <!--end-Footer-part-->
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.dataTables.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.tables.js'); ?>"></script>
        <script type="text/javascript">
            function SaveMedia()
            {
                $('#product_img').submit();
            }

            function EditImagesModal(pid)
            {
                var img_data = $('#img_' + pid).val();
                var json_data = JSON.parse(img_data);
                var full_path = json_data.full_path;
                $('#popup_img').attr("src", full_path);
                $('#img_id').val(json_data.id);
                $('#file_name_main').val(json_data.file_name);
            }
        </script>
    </body>
</html>