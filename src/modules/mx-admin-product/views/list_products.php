<?php 
$this->load->library('encryption');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <meta charset="UTF-8" />
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/select2.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 
        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>
        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 
        <!--sidebar-menu-->

		<?php echo admin_menu('product'); ?>

        <!--sidebar-menu-->
        <div id="content">
            <div id="content-header">
                <div id="breadcrumb"> 
                    <a href="#" class="tip-bottom"><i class="icon-home"></i> Home</a> 
                    <a href="#" class="current">Products</a> 
                </div>
            </div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="span11">
                        <div class="widget-box">
                            <div class="widget-title">
                                <span class="icon">
                                    <input type="checkbox" id="title-checkbox" name="title-checkbox" />
                                </span>
                                <h5><?php echo 'All Product ('.count($allProduct).')'; ?></h5>
                            </div>
                            <div class="widget-content nopadding">
                                <?php
                                echo form_open('#', array('id' => 'changeStatus', 'method' => 'post'));
                                echo input_csrf();
                                ?>
                                <table class="table table-bordered data-table">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th ><?php echo 'Thumb'; ?></th>
                                            <th ><?php echo 'Title'; ?></th>
                                            <th><?php echo 'Author'; ?></th>
                                            <th><?php echo 'Publisher'; ?></th>
                                            <th><?php echo 'MRP'; ?></th>
                                            <th><?php echo 'Category'; ?></th>
                                            <th><?php echo 'Modified_at'; ?></th>
                                            <th><?php echo 'Action'; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
										function array_search_id($search_value, $array, $id_path) { 
      
											if(is_array($array) && count($array) > 0) { 
												  
												foreach($array as $key => $value) { 
										  
													$temp_path = $id_path; 
													  
													// Adding current key to search path 
													array_push($temp_path, $key); 
										  
													// Check if this value is an array 
													// with atleast one element 
													if(is_array($value) && count($value) > 0) { 
														$res_path = array_search_id( 
																$search_value, $value, $temp_path); 
										  
														if ($res_path != null) { 
															return $res_path; 
														} 
													} 
													else if($value == $search_value) { 
														return join(" --> ", $temp_path); 
													} 
												} 
											} 
											  
											return null; 
										} 
                                        foreach ($allProduct as $iu):
                                            $i++;
                                            $class = '';
                                            if ($i == 1) {
                                                $class = 'gradeX';
                                            } elseif ($i == 2) {
                                                $class = 'gradeC';
                                            } else {
                                                $class = 'gradeA';
                                            }
                                            $key = esecure($iu->id);
                                            $imgSrc_130 = PRODUCT_DIR . $iu->id . '/image/130_' . $iu->cover_image;
                                            if(file_exists($imgSrc_130)){
                                                $thumb = base_url(PRODUCT_DIR_URL . $iu->id . '/image/130_' . $iu->cover_image);
                                            } else {
                                                $thumb = base_url('web-inf/img/icons/32/book.png');
                                            }
                                            
                                            ?>
                                            <tr <?php echo 'class="' . $class . '"'; ?>>
                                                <td width="4%"><?php 
													 if (isset($iu->status) && $iu->status == "publish") { echo "publish"; }
													 else{  echo "un-publish"; }
													?>
                                                </td>
                                                <td width="5%">
                                                    <a href="<?php echo site_url('mx-admin-product/product/Media/'. $key); ?>" style="color:#003bb3; text-decoration: underline;"><img src="<?php echo $thumb; ?>" /></a>
                                                </td>
                                                <td width="20%"><?php echo (isset($iu->title) && $iu->title != "" ? $iu->title : ""); ?></td>
                                                <td width="15%">
												<?php 
												if(isset($iu->author_id) && $iu->author_id != "" )
												{
													$author_id_show=array();
													$ar_author_id=explode(",",$iu->author_id);
													foreach($AuthorList as $pa_val)
													{
														if(in_array($pa_val->id,$ar_author_id))
														{
															$author_id_show[]=$pa_val->name;
														}
													}
													echo implode(",",$author_id_show);	
												}
												?>
												</td>
                                                <td width="10%"><?php echo (isset($iu->publisher_id) && $iu->publisher_id != "" ? $iu->publisher_id : ""); ?></td>
                                                <td width="10%"><?php echo (isset($iu->mrp) && $iu->mrp != "" ? $iu->mrp : ""); ?></td>
                                                <td width="10%">
												<?php 
													if(isset($iu->category_id) && $iu->category_id != "" )
													{
														$category_id_show=array();
														$ar_category_id=explode(",",$iu->category_id);
														foreach($productCategoryList as $pc_val)
														{
															if(in_array($pc_val->id,$ar_category_id))
															{
																$category_id_show[]=$pc_val->name;
															}
														}
														echo implode(",",$category_id_show);
													}
													 
												?>
												</td>
                                                <td class="taskStatus" width="10%">
                                                    <?php echo (isset($iu->modified_at) && $iu->modified_at != "" ? ucwords($iu->modified_at) : ""); ?>
                                                </td>
                                                <td class="center" width="10%">
                                                    <?php
                                                    if (isset($iu->status) && $iu->status != "publish") {
                                                        echo '<a href="javascript:void(0);" style="background:#1a42d6; color: #fff;" class="btn btn-mini showaction" data-productid="' . $iu->id . '" data-value="publish" title="Active"><i class="icon-check" ></i></a>;';
                                                    } else if (isset($iu->status) && $iu->status == "publish") {
                                                        echo '<a href="javascript:void(0);" style="background:#ec4464; color: #fff;" class="btn btn-mini showaction" data-productid="' . $iu->id . '" data-value="deactive" title="De-Active"><i class="icon-remove" ></i></a>;';
                                                    }
                                                    echo '<a href="' . base_url() . 'mx-admin-product/Product/addProduct/' . $iu->id . '" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>';
                                                    echo '<a href="javascript:void(0);" class="btn btn-mini" title="Deactive"><i class="icon-trash"></i></a>';
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?> 
                                    </tbody>
                                </table>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <?php echo admin_footer(); ?>
        <!--end-Footer-part-->
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/select2.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.dataTables.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.tables.js'); ?>"></script>
        <script type="text/javascript">
            $('.showaction').on('click', function () {
                root = location.protocol + '//' + location.host;
                var status = $(this).attr('data-value');
                var productid = $(this).attr('data-productid');
                $.ajax({
                    url: "<?php echo base_url('mx-admin-product/Product/updateStatusProduct'); ?>",
                    type: 'post',
                    data: 'status=' + status + '&productid=' + productid,
                    success: function (response) {
                        if (response.status == 'success') {
                            root = location.protocol + '//' + location.host;
                            var delay = 10;
                            setTimeout(function () {
                                window.location = "<?php echo base_url('mx-admin-product/product'); ?>";
								}, delay);
                        }
                    }
                });
            });
        </script>
    </body>
</html>