<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SitemapService extends TableName {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function GetAuthorsList(array $where, array $select=array()){
        if(!empty($select))
        {
            $this->db->select($select);
        }
        $this->db->from($this->author);
        $this->db->where($where);
        return $this->db->get()->result();
    }
	public function GetAllLatest(array $where=array(), array $select=array())
    {
        $this->db->select($select);
        $this->db->from($this->product);
        $this->db->where($where);
        return $this->db->get()->result();
    }
	
	public function GetAllCategory(array $where=array(), array $select=array())
    {
        $this->db->select($select);
        $this->db->from($this->productCategory.' AS pc');
        $this->db->where($where);
        return $this->db->get()->result();
    }
}