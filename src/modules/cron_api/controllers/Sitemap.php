<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller
{

    function __construct ()
    {
        parent::__construct();
		$this->load->model(array('SitemapService'));
    }

    function index ()
    {
        
        // Load XML writer library
        $this->load->library('xml_writer');
        
        // Initiate class
	
        $xml = new Xml_writer();
        $xml->setRootName('urlset');
        $xml->initiate(array("xmlns"=>"http://www.sitemaps.org/schemas/sitemap/0.9","xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance","xsi:schemaLocation"=>"http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"));
		
        //gmdate('Y-m-d\TH:i:s+00:00', strtotime($row['modified']['value']))
        $xml->startBranch('url');
        $xml->addNode('loc', 'https://www.kovitus.com/');
        $xml->addNode('lastmod', gmdate('Y-m-d\TH:i:s+00:00'));
        $xml->addNode('priority', 1.0);
        $xml->endBranch();

		$main_site_link=array("login","contact-us","about-us","author","register","home/books","privacy-policy","terms-and-condition");
		foreach($main_site_link as $row_link)
		{
			$xml->startBranch('url');
			$xml->addNode('loc', 'https://www.kovitus.com/'.$row_link);
			$xml->addNode('lastmod', gmdate('Y-m-d\TH:i:s+00:00'));
			$xml->addNode('priority', 0.8);
			$xml->endBranch();
		}
		
		$selectb = array('pc.id', 'pc.name', 'pc.slug', 'pc.modified_at');
        $whereb = array('pc.slug !=' => "", 'pc.is_active' => 1);
        $category_list = $this->SitemapService->GetAllCategory($whereb,$selectb);
		foreach($category_list as $row_cat)
		{
			$xml->startBranch('url');
			$xml->addNode('loc', 'https://www.kovitus.com/home/books/'.$row_cat->slug);
			$xml->addNode('lastmod', gmdate('Y-m-d\TH:i:s+00:00',strtotime($row_cat->modified_at)));
			$xml->addNode('priority', 0.8);
			$xml->endBranch();
		}
	

		$pwhere = array('status' => "publish");
		$select = array('id','title','date','modified_at');
        $All_product_list = $this->SitemapService->GetAllLatest($pwhere,$select);
		foreach($All_product_list as $row_prod)
		{
			$xml->startBranch('url');
			//$xml->addNode('title', $row_prod->title, array(), true);
			$pDetails = preety_url($row_prod->title) . '-' . toAlpha($row_prod->id);
			$href = site_url('product-details/' . $pDetails);
			$xml->addNode('loc', $href);
			$xml->addNode('lastmod', gmdate('Y-m-d\TH:i:s+00:00',strtotime($row_prod->modified_at)));
			$xml->addNode('priority', 0.8);
			$xml->endBranch();
		}
		
		$pwhere = array('is_active' => 1);
		$select = array('id','name','slug','created_at','modified_at');
        $authors_list = $this->SitemapService->GetAuthorsList($pwhere,$select);
		foreach($authors_list as $row_auth)
		{
			$xml->startBranch('url');
			//$xml->addNode('title', $row_auth->name, array(), true);
			$xml->addNode('loc', 'https://www.kovitus.com/author-details/'.$row_auth->slug);
			$xml->addNode('lastmod', gmdate('Y-m-d\TH:i:s+00:00',strtotime($row_auth->modified_at)));
			$xml->addNode('priority', 0.8);
			$xml->endBranch();
		}
		
        // Pass the XML to the view
        $data = array();
        //$data['xml'] = $xml->getXml(FALSE);
        //$this->load->view('xml', $data);
        $xml = $xml->getXml(FALSE);
		header('Content-type: text/xml; charset=utf-8');
		echo $xml;
    }
}