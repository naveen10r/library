<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
class Author extends CI_Controller {

    final function __construct() {
        parent::__construct();
        $this->load->helper(array('admin-links/admin_menu', 'admin-links/admin_head', 'admin-links/admin_foot', 'string', 'captcha_security'));
        $this->load->model(array('AuthorService'));
        $this->load->library('upload_lib');
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = (object) $this->session->userdata('userAuth');
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
        } else {
            redirect('mx-admin/logout');
        }
    }

	public function index() {
		$data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
		$data['allauthors'] = $this->AuthorService->getallAuthor(array());
		$this->load->view('list_authors', $data);
	}
	
    public function addAuthor() {
		
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        if (!$this->input->post()) {
			if(!empty($this->uri->segment('4')) && $this->uri->segment('4')!="")
			{
				$where = array('id' => $this->uri->segment('4'));
				$AuthorDetails = $this->AuthorService->getallAuthor($where);
				$data['AuthorDetails'] = $AuthorDetails[0];
			}
            $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
            $this->load->view('add_author', $data);
        } else {
            $this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');
            $this->form_validation->set_rules('slug', 'Slug', 'required|trim|xss_clean');
            $this->form_validation->set_rules('captchaWord', 'captcha word', 'required|trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
                $this->load->view('add_author', $data);
            } else {
                $name = $this->input->post('name', TRUE);
                $description = $this->input->post('description', TRUE);
                $slug = $this->input->post('slug', TRUE);
                $dob = $this->input->post('dob', TRUE);
	

				$author_img_file='';
				$fileName = md5(rand_string(8) . time() . rand_string(8));
				$config['upload_path'] = 'upload/author/';
				$config['allowed_types'] = 'jpg|png|jpeg|gif';
				$config['max_size'] = 1024 * 1024;
				$config['file_name'] = $fileName;
				$config['detect_mime'] = TRUE;
				$this->load->library('upload', $config);
				$isSave = array();
				$update = array();
				if (!empty($_FILES['uploadImg'])) {
					if (!$this->upload->do_upload('uploadImg')) {
						$isSave = $this->upload->display_errors();
						$author_img_file='';
					} else {
						$isSave = $this->upload->data();
						$author_img_file=$isSave['file_name'];
						$update['file_name'] = $isSave['file_name'];
						$update['file_path'] = $isSave['file_path'];
						$update['full_path'] = base_url($config['upload_path'] . $isSave['file_name']);
						$update['raw_name'] = $isSave['raw_name'];
						$update['client_name'] = $isSave['client_name'];
						$update['file_ext'] = $isSave['file_ext'];
						$update['file_size'] = $isSave['file_size'];
						$update['is_image'] = $isSave['is_image'];
						$update['image_size_str'] = $isSave['image_size_str'];
						$this->load->library('Upload_lib');
						$pathToImages = $isSave['full_path'];
						$pathToThumbs = $config['upload_path'] . '130_';
						$thumbWidth = 130;
						$this->upload_lib->createThumbs($pathToImages, $pathToThumbs, $thumbWidth);
					}
				}
				
				
				if(!empty($this->input->post('author_id', TRUE)))
				{
					$save = array(
						'name' => $name,
						'description' => $description,
						'slug' => $slug,
						'author_image' => $author_img_file,
						'dob' => date("Y-m-d",strtotime($dob)),
						'modified_at' => date(SYS_DB_DATE_TIME)
					);
					
					$where = array('id' => $this->input->post('author_id', TRUE));
					$this->AuthorService->Updateauthor($save,$where);
					$isSave=0;
					$this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Author Update successfully.'));
					redirect(base_url() . 'mx-admin-author/author');
				}else{
					$save = array(
						'name' => $name,
						'description' => $description,
						'slug' => $slug,
						'author_image' => $author_img_file,
						'is_active' => 0,
						'dob' => date("Y-m-d",strtotime($dob)),
						'created_at' => date(SYS_DB_DATE_TIME),
						'modified_at' => date(SYS_DB_DATE_TIME)
					);
					
					
					$isSave = $this->AuthorService->saveAuthor($save);
				}
				
                if ($isSave) {
					$this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Author Saved successfully.'));
					redirect(base_url() . 'mx-admin-author/author');
					
                }
            }
        }
    }
	
	/*
     *@author : Sandeep 
     */
    public function updateStatusauthor()
    { 
        
		
        if (!$this->input->post()) {
			$response = array(
                'status' => 'error',
                'message' => "<p>Something went wrong.</p>"
            );
        } else {
			
            $this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean');
            $this->form_validation->set_rules('author_id', 'Author ID', 'required|trim|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $response = array(
					'status' => 'error',
					'message' => validation_errors()
				);
            } else {
                $status = $this->input->post('status', TRUE);
                $author_id = $this->input->post('author_id', TRUE);

                $update = array(
					'is_active' => $status,
					'modified_at' => date(SYS_DB_DATE_TIME)
				);
				$where = array('id' => $author_id);
				$isSave = $this->AuthorService->Updateauthor($update,$where);
				$this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Author Update successfully.'));
				$response = array(
						'status' => 'success',
						'message' => "<p>Update successfully.</p>"
				);
            }
        }
		
		$this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }
    
    /*
     *@author : Naveen 
     */
    public function author_check()
    {
       
		$dInput=$this->input->post('dInput');
		$data_auhr=$this->AuthorService->getallAuthor(array(),$dInput);
		if(count($data_auhr))
		{	$auth_name_arr=array();
			foreach($data_auhr as $row_auth)
			{
				$auth_name_arr[]= $row_auth->name;
			}
			$response = array(
                'status' => 'success',
                'message' => "Author already exists like ".implode(",",$auth_name_arr)
            );
		}else{
			$response = array(
                    'status' => 'success',
                    'message' => "<p style='color:green;'>Now available </p>"
                );
		}
		
		
		
		 $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));	
    }

    private function fileWithValidation($picture, $fileExt) {
        $pictures = !empty($picture) ? $picture : 'pictures';
        $upImg = $_FILES['uploadImg'];
        if (empty($upImg['name'])) {
            $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is required.');
            return false;
        } else {
            $realExt = strtolower($upImg['name']);
            $ext = explode('.', $realExt);
            $extension = end($ext);
            $fileExt = explode('|', $fileExt);
            if (!in_array($extension, $fileExt)) {
                $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is not in the correct format.');
                return false;
            }
        }
        return true;
    }

    private function captchaWord($word) {
        $captcha_word = $this->session->tempdata('captcha_word');
        if (empty($word)) {
            $this->form_validation->set_message('captchaWord', 'The captcha field is required.');
            return false;
        } elseif ($captcha_word != $word) {
            $this->form_validation->set_message('captchaWord', 'The captcha security code doesn\'t matched.');
            return false;
        } else {
            return true;
        }
    }

}
