<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 */

class AuthorService extends TableName
{
    public function __construct() {
        parent::__construct();
    }
    
    public function saveAuthor(array $insert):int
    {
        $this->db->trans_start();
        $this->db->insert($this->author, $insert);
        return $this->trans_insert_data();
    }
	
	public function getallAuthor(array $where,$where_like=NULL)
    {
        $this->db->select('id,name,slug,is_active,description,modified_at,author_image,dob');
        $this->db->from($this->author);
        $this->db->where($where);
		if($where_like!=NULL)
		{
			$this->db->like('slug', $where_like);
		}
		$this->db->order_by('id','DESC');
        return $this->db->get()->result();
    }
	
	public function Updateauthor(array $update, array $where) {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->author, $update);
        return $this->trans_update_data();
    }
}