<?php ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-wysihtml5.css'); ?>" />
		<link rel="stylesheet" href="<?php echo base_url('web-inf/css/datepicker.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>

        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu('Author'); ?>
        <!--sidebar-menu-->
        <!--close-left-menu-stats-sidebar-->

        <div id="content">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="span10">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                <h5>Add Author</h5>
                                <?php echo MANDATORY; ?>
                            </div>
                            <div class="widget-content nopadding">
                                <?php 
                                    echo form_open_multipart('mx-admin-author/Author/addAuthor', array('class' => 'form-horizontal', 'id'=>'saveProduct')); 
                                    echo input_csrf();
                                ?>
                                <div class="control-group">
                                    <label class="control-label">Name <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Author Name"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" autocomplete="off" id="inputError" class="span11 name" name="name" value="<?php echo (isset($AuthorDetails->name))? $AuthorDetails->name:""; ?>" required="true" />
                                        <span class="help-inline red_error author_name_check"><?php echo form_error('name'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Description
                                    <a class="tip-top"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <textarea id="description" style="width: 627px; height: 67px;" name="description" rows="4" ><?php echo (isset($AuthorDetails->description))? $AuthorDetails->description:""; ?></textarea>

                                        <span class="help-inline red_error"><?php echo form_error('description'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Slug <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Slug"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text"  autocomplete="off" class="span11 slug" name="slug" value="<?php echo (isset($AuthorDetails->slug))? $AuthorDetails->slug:""; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('slug'); ?></span> 
                                    </div>
                                </div>
								<div class="control-group">
                                    <label class="control-label">Date of Birth 
                                        <a class="tip-top" data-original-title="DOB"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">

                                       <div  data-date="12-02-2012" class="input-append date datepicker">
											<input type="text" value="<?php echo (isset($AuthorDetails->dob))? $AuthorDetails->dob:"mm-dd-yyyy"; ?>"  name="dob" data-date-format="mm-dd-yyyy" class="span11" >
                                             <span class="add-on"><i class="icon-th"></i></span> 
										</div>
										<span class="help-inline red_error"><?php echo form_error('dob'); ?></span> 
                                    </div>

                                 </div>
								<div class="control-group">
                                    <label class="control-label">Pictures
                                        <a class="tip-top" data-original-title="PNG|JPG|JPEG Only"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
										<?php 
										if(!empty($AuthorDetails->author_image))
										{ ?> <img src="<?php echo base_url("upload/author/")."130_".$AuthorDetails->author_image ?>"  /><?php } ?>
                                        <input type="file" name="uploadImg"  class="span11" />
                                        <span class="help-inline red_error"><?php echo form_error('uploadImg'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Check Robotic <sup class="red_error">&#042;</sup>
                                    </label>
                                    <div class="controls">
                                        <div class="span2">
                                            <img src="<?php echo base_url('captcha/'.$captcha['filename']); ?>" />
                                        </div>
                                        <div class="span1"><a href="javascript:void(0)" onclick="document.getElementById('saveProduct').submit();"><i class="icon-repeat"></i></a></div>
                                        <input type="text" name="captchaWord" class="span3" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('captchaWord'); ?></span> 
                                    </div>
                                </div>
                                <div class="form-actions">
								<?php if(isset($AuthorDetails->id) && !empty($AuthorDetails->id))
										{
											echo '<input type="hidden" name="author_id" id="author_id" value="'.$AuthorDetails->id.'" />';
											echo '<button type="submit" class="btn btn-success"><i class="icon-save"></i> Update</button>';
										}else
										{
											echo '<button type="submit" class="btn btn-success"><i class="icon-save"></i> Save</button>';
										}
								?>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 2013 &copy; Matrix Admin.</div>
        </div>
		
        <!--end-Footer-part--> 
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap-wysihtml5.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/matrix.popover.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/bootstrap-datepicker.js'); ?>"></script>
        <script>
            $(document).ready(function () {
				 $('.datepicker').datepicker({maxDate: $.now()});
            });
			$('.name').keyup(function() {
				var dInput = this.value;
				dInput = dInput.toLowerCase();
				dInput = dInput.replace(/[^a-zA-Z0-9]+/g,'-');
				$(".slug").val(dInput);  
				if(dInput.length>3)
				{
					console.log(dInput);
					$.ajax({
						url: '<?php echo site_url("mx-admin-author/author/author_check"); ?>',
						type: 'post',
						data:'dInput='+ dInput,
						success: function(response){
							console.log(response);
							$(".author_name_check").html(response);  
							if (response.status == 'success') 
							{
								$(".author_name_check").html(response.message);
							} 
							else 
							{
								$(".author_name_check").html(response.message);
							}
						}
					});
				}
			});
        </script>
    </body>
</html>