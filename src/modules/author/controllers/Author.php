<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Author extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('site-link/menus', 'site-link/web_view_header', 'site-link/web_view_footer', 'captcha_security'));
        $this->load->model(array('home/HomeService', 'AuthorService'));
    }

    public function index($alphabet = null) {
        $data = array();
        $data['alphabet'] = $alphabet;
        $select = array('pcc.id', 'pcc.name', 'pcc.slug');
        $isChild = FALSE;
        $where = array('pcc.slug != ' => '', 'pcc.parent_id' => NULL, 'pcc.is_active' => 1);
        $data['all_category'] = $this->HomeService->GetAllCategory($where, $select, $isChild);
        $pselect = array('id', 'name', 'slug','description','author_image','is_active','dob');
        $pwhere = array('is_active' => 1, 'author_start' => $alphabet);
        $data['authors_list'] = $this->AuthorService->GetAuthorsList($pwhere,$pselect);
        $data['GetAuthors_listCount'] = $this->AuthorService->GetAuthorsListCount($pwhere, $pselect);
        $this->load->view('authors', $data);
    }

    public function author_details() {

        $data = array();
        $select = array('pcc.id', 'pcc.name', 'pcc.slug');
        $isChild = FALSE;
        $where = array('pcc.slug != ' => '', 'pcc.parent_id' => NULL, 'pcc.is_active' => 1);
        $data['all_category'] = $this->HomeService->GetAllCategory($where, $select, $isChild);

        $pwhere = array('is_active' => 1, 'slug' => $this->uri->segment(2));
        $data['authors_details'] = $this->AuthorService->GetAuthorsList($pwhere);
        $data['GetAuthorLatestBooks'] = $this->AuthorService->GetAuthorLatestBooks($data['authors_details'][0]->id);
        $this->load->view('author_details', $data);
    }

    private function ajaxResponse($data) {
        exit(json_encode($data));
    }
    
    public function load_more(){
        $startpoint = $this->input->post('start_point');
        $delta_data = $this->input->post('delta_data');
        $alphabet = $this->input->post('select_by');
        
        $start_point = $startpoint * HOME_PAGE_PER_RECORDS;
        
        $pselect = array('id', 'name', 'slug','description','author_image','is_active','dob');
        $pwhere = array('is_active' => 1, 'author_start' => $alphabet);
        $authors_list = $this->AuthorService->GetAuthorsList($pwhere, $pselect, $start_point);
        
        $delta = false;
        if (($start_point <= $delta_data) and count($authors_list) >= HOME_PAGE_PER_RECORDS){
            $delta = true;
        }
        $author_div = '';
        if (!empty($authors_list)) {
            foreach ($authors_list as $author_row) {
                $imagePath = base_url() . "upload/author/130_" . $author_row->author_image;
                if (@getimagesize($imagePath)) {
                } else {
                    $imagePath = base_url() . "web_view/image/catalog/KovitUS_default_author.png";
                }
                $author_desc = '';
                $authorSlug = site_url('author-details/' . $author_row->slug);
                $read_more = '<a href="' . $authorSlug . '"> <em>Read More</em></a>';
                $goToPage = '<a href="' . $authorSlug . '">' . $author_row->name . '</a>';
                $noDesc = "No author description available.";
                if (empty($author_row->description) || $author_row->description == $noDesc) {
                    $author_desc = $noDesc;
                } else {
                    $author_description_without_tags = strip_tags($author_row->description);
                    $author_desc = (strlen($author_description_without_tags) > 150) ? substr_word($author_description_without_tags, 148) . $read_more : $author_description_without_tags;
                }
                $author_div .= '<div class="col-sm-6 ">';
                $author_div .= '<div class="col-sm-5" style="max-height: 150px;"><a href="'.site_url('author-details/' . $author_row->slug).'" ><img src="'.$imagePath.'" height="130" width="135" /></a></div>';
                $author_div .= '<div class="col-sm-7" style="height: 200px;">';
                $author_div .= '<h2>'.$goToPage.'</h2>';
                $author_div .= '<div style="text-align: justify;">'.$author_desc.'</div>';
                $author_div .= '</div>';
                $author_div .= '</div>';
            }
            $responce = array(
                'status' => TRUE,
                'responce' => array(
                    'error' => '',
                    'message' => 'Page Next',
                    'data' => $author_div,
                    'delta' => $delta
                ),
                'code' => 200
            );
            $this->ajaxResponse($responce);
        } else {
            $responce = array(
                'status' => false,
                'responce' => array(
                    'error' => 'No more data found.',
                    'message' => '',
                    'data' => '',
                    'delta' => $delta
                ),
                'code' => 200
            );
            $this->ajaxResponse($responce);
        }
        
    }

}
