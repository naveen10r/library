<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AuthorService extends TableName {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function GetAuthorsList(array $where, array $select=array(),$start=0){
        if(!empty($select))
        {
            $this->db->select($select);
        }
        $this->db->from($this->author);
        if (!empty($where['author_start'])){
            $this->db->like('name', $where['author_start'], 'after');
        }
        unset($where['author_start']);
        $this->db->where($where);
        $this->db->order_by('name', 'ASC');
        $this->db->limit(HOME_PAGE_PER_RECORDS, $start);
        return $this->db->get()->result();
    }
	
	public function GetAuthorsListCount(array $where=array(), $join=false)
    {
		$this->db->from($this->author);
        if (!empty($where['author_start'])){
            $this->db->like('name', $where['author_start'], 'after');
        }
        unset($where['author_start']);
        $this->db->where($where);
        return $this->db->get()->num_rows();;
    }
	
	public function GetAuthorLatestBooks($id=0)
    {

        $this->db->select( array('p.id','p.title', 'p.author', 'p.offerprice', 'p.mrp', 'p.status', 'p.cover_image', 'p.buying_mode'));
        $this->db->from($this->product." as p");
        $this->db->join($this->product_author_junction ." as aj", 'aj.product_id = p.id', 'LEFT');
        $this->db->where(array("aj.author_id"=>$id));
        $result= $this->db->get()->result();
		//echo $this->db->last_query();
		return $result;
    }
    
}