<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . ' Authors' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <meta name="title" content="<?php echo!empty($pdetails->title) ? $pdetails->title . " | " . SITE_DEFAULT_META_TITLE : SITE_DEFAULT_META_TITLE; ?>" />
        <meta name="keywords" content="<?php echo!empty($pdetails->meta_keywords) ? $pdetails->meta_keywords : SITE_DEFAULT_META_KEYWORDS; ?>" />
        <meta name="description" content="<?php echo!empty($pdetails->meta_description) ? $pdetails->meta_description : SITE_DEFAULT_META_DESCRIPTION; ?>" />

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js',
            'web_view/catalog/view/javascript/jquery/swiper/js/swiper.jquery.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js'
        );
        echo load_js($js);
        $css = array(
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/swiper.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/opencart.css',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css',
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_css($css);
        ?>
        <link href="<?php echo site_url('author'); ?>" rel="canonical" />
        <style type="text/css">
            .customNavigation > a {
                background-color: #e91e76 !important;
            }
            a{ color: #4C345E; text-decoration: none;}
        </style>
    </head>
    <body class="product-product-47 layout-2 left-col">
        <header>
            <?php
            header_container();
            header_top_inner();
            ?>
        </header>
    </div>

</header>
<div class="wrap-breadcrumb parallax-breadcrumb">
    <div class="container"></div>
</div>

<div id="product-product" class="container">
    <ul class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo site_url("author"); ?>"><i class="fa fa-home"></i> Authors</a></li>
        <li><a href="#">Author Details</a>
    </ul>
    <div class="row">
        <aside id="column-left" class="col-sm-3 hidden-xs">
            <?php
            if (!empty($all_category)) {
                ?>
                <div id="verticalmenublock" class="box category box-category ">
                    <div class="box-heading">All Categories</div>
                    <div class="box-content box-content-category">
                        <ul id="nav-one" class="dropmenu">
                            <?php
                            foreach ($all_category as $cat) {
                                echo '<li class="top_level main"><a href="' . site_url('home/books/' . $cat->slug) . '">' . $cat->name . '</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </aside>
        <div id="content" class="col-sm-9">
            <div class="row">
                <div class="col-sm-3" >
                    <?php
                    $imagePath = base_url() . "upload/author/130_" . $author_row->author_image;
                    if (@getimagesize($imagePath)) {
                        
                    } else {
                        $imagePath = base_url() . "web_view\image/catalog/KovitUS_default_author.png";
                    }
                    echo "<img src='" . $imagePath . "' />";
                    ?>

                </div>
                <div class="col-sm-9" >
                    <h2><?php echo $authors_details[0]->name; ?> (Author)</h2>
                    <div><?php echo $authors_details[0]->description; ?></div>

                </div>
                <div class="col-sm-12" >
                    <?php
                    $authorauthorproducts_li = '';
                    if (!empty($GetAuthorLatestBooks)) {
                        foreach ($GetAuthorLatestBooks as $latestRow) {
                            $PROD_IMG_DIR = PRODUCT_DIR . $latestRow->id . '/image/' . $latestRow->cover_image;
                            $PRODUCT_IMG = '';
                            if (file_exists($PROD_IMG_DIR)) {
                                $PRODUCT_IMG = base_url(PRODUCT_DIR_URL . $latestRow->id . '/image/' . $latestRow->cover_image);
                            } else {
                                continue;
                            }
                            $pDetails = preety_url($latestRow->title) . '-' . toAlpha($latestRow->id);
                            $href = site_url('product-details/' . $pDetails);
                            $SubTitle = strlen($latestRow->title) <= 45 ? $latestRow->title : substr($latestRow->title, 0, 45) . '...';
                            $buyingMode = !empty($latestRow->buying_mode) ? json_decode($latestRow->buying_mode, TRUE) : array();
                            $authors = '';

                            $products_li = '';
                            $products_li .= '<li class="double-slideitem slider-item">';
                            $products_li .= '<div class="slider-item">';
                            $products_li .= '<div class="product-block product-thumb transition">';
                            $products_li .= '<div class="product-block-inner">';
                            $products_li .= '<div class="image">';
                            $products_li .= '<a href="' . $href . '">';
                            $products_li .= '<img src="' . $PRODUCT_IMG . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '" class="img-responsive reg-image" style="height: 260px;" />';
                            $products_li .= '<img class="img-responsive hover-image" src="' . $PRODUCT_IMG . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '" style="height: 260px;" />';
                            $products_li .= '</a>';
                            if (in_array('RENT', $buyingMode)) {
                                $products_li .= '<div class="percentsaving">RENT</div>';
                            }
                            $products_li .= '</div>';
                            $products_li .= '<div class="product-details">';
                            $products_li .= '<div class="caption">';
                            $products_li .= '<h4 style="height: 40px;"><a href="' . $href . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '">' . $SubTitle . '</a></h4>';
                            $products_li .= '<p class="price">';
                            if ($latestRow->offerprice == $latestRow->mrp) {
                                $products_li .= '<span class="price-new">' . RUPEE_LOGO . $latestRow->offerprice . '</span>';
                            } else {
                                $products_li .= '<span class="price-new">' . RUPEE_LOGO . $latestRow->offerprice . '</span> <span class="price-old">' . RUPEE_LOGO . $latestRow->mrp . '</span>';
                            }
                            $products_li .= '<span class="price-tax">Ex Tax: ' . RUPEE_LOGO . $latestRow->offerprice . '</span>';
                            $products_li .= '</p>';
                            $products_li .= '</div>';
                            $products_li .= '</div>';
                            $products_li .= '</div>';
                            $products_li .= '</div>';
                            $products_li .= '</div>';
                            $products_li .= '</li>';
                            $authorauthorproducts_li .= $products_li;
                        }
                    }
                    ?>
                    <div class="container" style=" margin-top: 80px;"></div>
                    <h2><b>Books by <?php echo $authors_details[0]->name; ?></b></h2>
                    <div id="tab-latest" class="tab-content">
                        <div class="box">
                            <div class="box-content">
                                <div class="box-product product-carousel" style="display:block;">
                                    <?php echo $authorauthorproducts_li; ?>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<?php footer_tag(); ?>
<script type="text/javascript">
    $(document).ready(function () {

    });

//--></script> 
</script>
</body>
</html> 
