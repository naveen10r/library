<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . ' Authors' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <meta name="title" content="<?php echo!empty($pdetails->title) ? $pdetails->title . " | " . SITE_DEFAULT_META_TITLE : SITE_DEFAULT_META_TITLE; ?>" />
        <meta name="keywords" content="<?php echo!empty($pdetails->meta_keywords) ? $pdetails->meta_keywords : SITE_DEFAULT_META_KEYWORDS; ?>" />
        <meta name="description" content="<?php echo!empty($pdetails->meta_description) ? $pdetails->meta_description : SITE_DEFAULT_META_DESCRIPTION; ?>" />

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js',
            'web_view/catalog/view/javascript/jquery/swiper/js/swiper.jquery.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js'
        );
        echo load_js($js);
        $css = array(
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/swiper.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/opencart.css',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css',
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_css($css);
        ?>
        <link href="<?php echo site_url('author'); ?>" rel="canonical" />
        <style type="text/css">
            .customNavigation > a {
                background-color: #81b851 !important;
            }
            a {
                color: #4C345E;
                text-decoration: none;
            }
            .custom_class {
                margin-left: 5%;
            }
            .custom_select {
                color: #ffffff !important;
                background-color: #81b851 !important;
                border-color: #81b851 !important;
            }
        </style>
    </head>
    <body class="product-product-47 layout-2 left-col">
        <header>
            <?php
            header_container();
            header_top_inner();
            ?>
        </header>
    </div>
    <div class="wrap-breadcrumb parallax-breadcrumb">
        <div class="container"></div>
    </div>

    <div id="product-product" class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Authors</a></li>
        </ul>
        <div class="row">

            <nav aria-label="Page navigation example">
                <ul class="pagination custom_class">
                    <li class="page-item"><a class="page-link <?php echo empty($alphabet) ? 'custom_select' : ''; ?>" href="<?php echo site_url('author/index'); ?>">All</a></li>
                    <?php
                    for ($i = 'A'; $i <= 'Z'; $i++) {
                        $url = site_url('author/index/' . $i);
                        $custom_select = '';
                        if ($alphabet == $i) {
                            $custom_select = 'custom_select';
                        }
                        echo '<li class="page-item"><a class="page-link ' . $custom_select . '" href="' . $url . '">' . $i . '</a></li>';
                        if ($i == 'Z') {
                            break;
                        }
                    }
                    ?>
                </ul>
            </nav>

            <aside id="column-left" class="col-sm-3 hidden-xs">
                <?php
                if (!empty($all_category)) {
                    ?>
                    <div id="verticalmenublock" class="box category box-category ">
                        <div class="box-heading">All Categories</div>
                        <div class="box-content box-content-category">
                            <ul id="nav-one" class="dropmenu">
                                <?php
                                $i = 0;
                                foreach ($all_category as $cat) {
                                    $classHighlt = '';

                                    if ($cat->slug == $cat_slug) {
                                        $classHighlt = 'class="slug_hilight"';
                                        $CategoryId = $cat->id;
                                    }
                                    echo '<li class="top_level main"><a ' . $classHighlt . ' href="' . site_url('home/books/' . $cat->slug) . '">' . $cat->name . '</a></li>';
                                    if ($i == 9) {
                                        break;
                                    }
                                    $i++;
                                }
                                echo '<li class="top_level"><a href="' . site_url('categories/all') . '">All</a></li>';
                                ?>

                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </aside>
            <div id="content" class="col-sm-9">
                <div class="row box-product">
                    <?php
                    if (!empty($authors_list)) {
                        foreach ($authors_list as $author_row) {
                            $imagePath = base_url() . "upload/author/130_" . $author_row->author_image;
                            if (@getimagesize($imagePath)) {
                                
                            } else {
                                $imagePath = base_url() . "web_view/image/catalog/KovitUS_default_author.png";
                            }
                            $author_desc = '';
                            $authorSlug = site_url('author-details/' . $author_row->slug);
                            $read_more = '<a href="' . $authorSlug . '"> <em>Read More</em></a>';
                            $goToPage = '<a href="' . $authorSlug . '">' . $author_row->name . '</a>';
                            $noDesc = "No author description available.";
                            if (empty($author_row->description) || $author_row->description == $noDesc) {
                                $author_desc = $noDesc;
                            } else {
                                $author_description_without_tags = strip_tags($author_row->description);
                                $author_desc = (strlen($author_description_without_tags) > 150) ? substr_word($author_description_without_tags, 148) . $read_more : $author_description_without_tags;
                            }
                            ?> 
                            <div class="col-sm-6 ">
                                <div class="col-sm-5" style="max-height: 150px;"><a href="<?php echo site_url('author-details/' . $author_row->slug); ?>" > <?php echo "<img src='" . $imagePath . "' height='130' width='135' />"; ?> </a></div>
                                <div class="col-sm-7" style="height: 200px;">
                                    <h2><?php echo $goToPage; ?></h2> 
                                    <div style="text-align: justify;"><?php echo $author_desc; ?></div> 
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <?php
                if (HOME_PAGE_PER_RECORDS < $GetAuthors_listCount) {
                    echo '<div class="btn btn-primary load_more">Load More</div>';
                }
                ?>
            </div>
        </div>
    </div>
    <input type="hidden" id="page_count" value="1" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.load_more').click(function () {
                var page_count = $('#page_count').val();
                var select_by = '<?php echo!empty($alphabet) ? $alphabet : ""; ?>';
                $(this).html('<img src="<?php echo LOADING_IMG_GIF; ?>" height="20px" width="20px" /> Loading...');
                $.ajax({
                    type: "POST",
                    url: '<?php echo site_url('author/Author/load_more'); ?>',
                    data: {"start_point": page_count, 'delta_data': '<?php echo $GetAuthors_listCount; ?>', "select_by": select_by},
                    success: function (returntxt)
                    {
                        var parseJson = JSON.parse(returntxt);
                        console.log(parseJson.responce.delta);
                        $('.load_more').html('');
                        $('.load_more').html('Load More');

                        if (parseJson.responce.delta === false) {
                            $('.load_more').remove();
                        }
                        if (parseJson.status === true) {
                            new_pageCount = parseInt(page_count) + 1;
                            $('#page_count').val(new_pageCount);
                            $('.box-product').append(parseJson.responce.data);
                        } else {
                            $('#server_response').addClass('alert alert-danger');
                            $('#review_response').html(parseJson.responce.error);
                        }
                    }
                });
            });
        });


    </script>	
    <?php footer_tag(); ?>
</body>
</html> 
