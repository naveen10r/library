<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ProductService extends TableName
{
    public function __construct() {
        parent::__construct();
    }
    
    public function saveProduct(array $insert):int
    {
        $this->db->trans_start();
        $this->db->insert($this->product, $insert);
        return $this->trans_insert_data();
    }
}