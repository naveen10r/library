<?php

class AdminAuthModel extends tableName
{
    
    public function validateAuth(array $where=array())
    {
        $where['is_status'] = 1;
        $where['is_delete'] = 0;
        $this->db->select('id,username,password AS passwordHash');
        $this->db->from($this->adminUsers);
        $this->db->where($where);     
        return $this->db->get()->row();
    }
    
    public function resetPasswordLink(array $where=array())
    {
        $this->db->select('id,is_status,is_delete');
        $this->db->from($this->adminUsers);
        $this->db->or_where($where);     
        return $this->db->get()->row();
    }
    
    public function getUserAccessMenu()
    {
        $userAuth = $this->session->userdata('userAuth');
        $lang = $this->session->userdata('LANGUAGE');
        $lang = !empty($lang) ? $lang : 'english';
        $this->db->select('IF(le.'.$lang.' IS NOT NULL, le.'.$lang.',pmenu.title) AS title', FALSE);
        $this->db->select('IF(mle.'.$lang.' IS NOT NULL, mle.'.$lang.', cmenu.title) AS parentTitle', FALSE);
        $this->db->select('pmenu.id,pmenu.href, pmenu.fa_icon, pmenu.is_visible_in_menu, pmenu.active_at');
        $this->db->from(SYS_DB_PREFIX.'master_menus AS pmenu');
        $this->db->join(SYS_DB_PREFIX.'master_menus AS cmenu','cmenu.id = pmenu.is_parent','left'); 
        $this->db->join(SYS_DB_PREFIX.'admin_menus AS am','am.menu_id = pmenu.id'); 
        $this->db->join(SYS_DB_PREFIX.'language_encoding AS le','le.english = pmenu.title','left'); 
        $this->db->join(SYS_DB_PREFIX.'language_encoding AS mle','mle.english = cmenu.title','left'); 
        $this->db->where(array('am.user_id' => $userAuth['u_id'], 'pmenu.is_delete' => 'N', 'am.is_delete' => 'N','pmenu.is_location'=>'admin')); 
        $this->db->order_by('pmenu.id','ASC');
        return $this->db->get()->result_array();
    }
}

?>