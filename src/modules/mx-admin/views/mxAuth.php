<?php
$lang = language_encode(array('password', 'login', 'forgot_password'));
?>
<!DOCTYPE html>
<html lang="<?php echo $lang['html_lang_encode']; ?>">
    <head>
        <title>Matrix Admin</title><meta charset="UTF-8" />
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-login.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div id="loginbox">    
            <div id="alertResponce">
                <?php
                if ($this->session->flashdata('alert')) {
                    $alert = $this->session->flashdata('alert');
                    echo $alert['color']($alert['responce']);
                }
                ?> 
            </div>
            <?php echo form_open('mx-admin/adminAuth/validateAuth', array('id' => 'loginform', 'class' => 'form-vertical', 'method' => 'post')); ?>
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
            <div class="control-group normal_text">
                <h3>
                    <img src="<?php echo base_url('web-inf/img/logo.png'); ?>" alt="Logo" />
                </h3>
            </div>
            <div class="control-group">
                <div class="controls">
                    <div class="main_input_box">
                        <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" name="username" placeholder="" value="<?php echo set_value('username'); ?>" />
                        <?php echo form_error('username'); ?>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <div class="main_input_box">
                        <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" placeholder="<?php echo $lang['password']; ?>" name="password" value="<?php echo set_value('password'); ?>" />
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <span class="pull-left"><a href="javascript:void(0);" class="flip-link btn btn-info" id="to-recover"><?php echo $lang['forgot_password']; ?>?</a></span>
                <span class="pull-right"><button type="submit" class="btn btn-success" /> <?php echo $lang['login']; ?></button></span>
            </div>
            <?php echo form_close(); ?>
            <div id="password-recovery">
                <?php echo form_open('mx-admin/adminAuth/forgotPassword', array('id' => 'recoverform', 'class' => 'form-vertical', 'method' => 'post')); ?>
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <p class="normal_text">Enter your e-mail/username below and we will send you instructions how to recover a password.</p>

                <div class="controls">
                    <div class="main_input_box">
                        <span class="add-on bg_lo"><i class="icon-envelope"></i></span>
                        <input type="text" placeholder="E-mail address or username" name="username" />
                    </div>
                </div>

                <div class="form-actions">
                    <span class="pull-left"><a href="javascript:void(0);" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><button class="btn btn-info" type="submit">Recovery</button></span>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script>  
        <script src="<?php echo base_url('web-inf/js/matrix.login.js'); ?>"></script> 
    </body>

</html>