<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Product extends CI_Controller {

    final function __construct() {
        parent::__construct();
        $this->load->helper(array('admin-links/admin_menu', 'admin-links/admin_head', 'admin-links/admin_foot', 'string', 'captcha_security'));
        $this->load->model(array('ProductService'));
        $this->load->library('upload_lib');
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = (object) $this->session->userdata('userAuth');
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
        } else {
            redirect('mx-admin/logout');
        }
    }

    public function addProduct() {
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        if (!$this->input->post()) {
            $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
            $this->load->view('product/add_products', $data);
        } else {
            $this->form_validation->set_rules('title', 'title', 'required|trim|xss_clean|regex_match[/' . NAME_PATTERN . '/]');
            $this->form_validation->set_rules('description', 'description', 'required|trim|xss_clean|alpha_space');
            $this->form_validation->set_rules('writter', 'writter', 'required|trim|xss_clean|alpha_space');
            $this->form_validation->set_rules('pub_edi', 'publication/edition', 'required|trim|xss_clean');
            /*
              $this->form_validation->set_rules('uploadImg', 'pictures', 'callback_valid_files_ext[jpeg|jpg|png]');
             */
            $this->form_validation->set_rules('captchaWord', 'captcha word', 'required|trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
                $this->load->view('product/add_products', $data);
            } else {
                $title = $this->input->post('title', TRUE);
                $description = $this->input->post('description', TRUE);
                $writter = $this->input->post('writter', TRUE);
                $pub_edi = $this->input->post('pub_edi', TRUE);
                $save = array(
                    'title' => $title,
                    'description' => $description,
                    'writter' => $writter,
                    'publication_edition' => $pub_edi
                );
                $isSave = $this->ProductService->saveProduct($save);
                if ($isSave) {
                    $dir = getcwd() . '/upload/product/' . $isSave;
                    $ImageCount = count($_FILES['uploadImg']['name']);
                    for ($i = 0; $i < $ImageCount; $i++) {
                        $_FILES['file']['name'] = $_FILES['uploadImg']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['uploadImg']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['uploadImg']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['uploadImg']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['uploadImg']['size'][$i];
                        $setting = array('MKDIR' => TRUE, 'FULL_PATH' => $dir, 'VALID_EXT' => 'JPEG|JPG|PNG|GIF|jpg', 'MAX_SIZE' => 1024, 'IS_IMG' => 1, 'IS_THUMB' => TRUE, 'THUMB_SIZES' => array(130));
                        $this->upload_lib->upload_file($setting);
                    }
                }
            }
        }
    }
    
    /*
     *@author : Naveen 
     */
    public function createIndexes()
    {
        
    }

    private function fileWithValidation($picture, $fileExt) {
        $pictures = !empty($picture) ? $picture : 'pictures';
        $upImg = $_FILES['uploadImg'];
        if (empty($upImg['name'])) {
            $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is required.');
            return false;
        } else {
            $realExt = strtolower($upImg['name']);
            $ext = explode('.', $realExt);
            $extension = end($ext);
            $fileExt = explode('|', $fileExt);
            if (!in_array($extension, $fileExt)) {
                $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is not in the correct format.');
                return false;
            }
        }
        return true;
    }

    private function captchaWord($word) {
        $captcha_word = $this->session->tempdata('captcha_word');
        if (empty($word)) {
            $this->form_validation->set_message('captchaWord', 'The captcha field is required.');
            return false;
        } elseif ($captcha_word != $word) {
            $this->form_validation->set_message('captchaWord', 'The captcha security code doesn\'t matched.');
            return false;
        } else {
            return true;
        }
    }

}
