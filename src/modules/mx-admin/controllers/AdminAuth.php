<?php
	
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ob_start();
ob_clean();
ob_flush();

class AdminAuth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('AdminAuthModel'));
    }

    public function index() {
        $data['csrf'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        $this->load->view('mxAuth', $data);
    }

    public function validateAuth() {
        $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => validation_errors()));
            redirect($this->agent->referrer());
        }
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);
        $data = array('username' => $username);
        $isVerify = $this->AdminAuthModel->validateAuth($data);
        if (password_verify($password, $isVerify->passwordHash)) {
            $this->session->set_userdata('userAuth', array('u_code' => $isVerify->username, 'u_id' => $isVerify->id));
            redirect('mx-admin-dashboard');
        } else {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'The login is invalid.'));
            redirect($this->agent->referrer());
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('mx-admin');
    }

    public function forgotPassword() {
        $this->form_validation->set_rules('username', 'email/username', 'trim|required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'F : Reset password link has been sent to mail id'));
            redirect($this->agent->referrer());
        } else {
            $username = $this->input->post('username', true);
            $where = ['email' => $username, 'username' => $username];
            $isTrue = $this->AdminAuthModel->resetPasswordLink($where);
            if ($isTrue) {
                if ($isTrue->is_status == 1 AND $isTrue->is_delete == 0) {
                    $this->email_lib->send_email(array('MESSAGE' => 'Test OK'));
                }
            }
            $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Reset password link has been sent to mail id'));
            redirect($this->agent->referrer());
        }
    }

    function csrf_error_responce() {
        $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Exception 302 token mismatch.'));
        redirect($this->agent->referrer());
    }

}
