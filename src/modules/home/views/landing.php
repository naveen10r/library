<?php
$products_li = '';
$CategoryId = 0;
if (!empty($GetAllLatest)) {
    foreach ($GetAllLatest as $latestRow) {
        $PROD_IMG_DIR = PRODUCT_DIR . $latestRow->id . '/image/' . $latestRow->cover_image;
        $PRODUCT_IMG = '';
        if (file_exists($PROD_IMG_DIR)) {
            $PRODUCT_IMG = base_url(PRODUCT_DIR_URL . $latestRow->id . '/image/' . $latestRow->cover_image);
        } else {
            continue;
        }
        $pDetails = preety_url($latestRow->title) . '-' . toAlpha($latestRow->id);
        $href = site_url('product-details/' . $pDetails);
        $SubTitle = strlen($latestRow->title) <= 40 ? $latestRow->title : substr($latestRow->title, 0, 40) . '...';
        $buyingMode = !empty($latestRow->buying_mode) ? json_decode($latestRow->buying_mode, TRUE) : array();
        $discount_percentage = (100 - get_percentage($latestRow->mrp, $latestRow->offerprice));
        $authors = '';
        if (!empty($latestRow->author_id)) {
            $author_id = explode(',', $latestRow->author_id);
            $a = 0;
            $info = '';
            $auth = [];
            foreach ($author_id as $aid) {
                $auth[] = $AllAuthor[$aid]->name;
                if ($a == 0) {
                    $main_author = (strlen($AllAuthor[$aid]->name) <= 16) ? $AllAuthor[$aid]->name : substr($AllAuthor[$aid]->name, 0, 15) . '...';
                    if (count($author_id) == 1) {
                        $authors = '<p title="' . $AllAuthor[$aid]->name . '" style="cursor: help;">By - ' . $main_author;
                    } else {
                        $authors = '<p>By - ' . $main_author;
                    }
                } else {
                    $info = ' <i class="fa fa-info-circle" title="' . join(', ', $auth) . '"></i>';
                }
                $a++;
            }
            $authors = $authors . $info . '</p>';
        }

        $products_li .= '<li class="double-slideitem slider-item">';
        $products_li .= '<div class="slider-item">';
        $products_li .= '<div class="product-block product-thumb transition">';
        $products_li .= '<div class="product-block-inner">';
        $products_li .= '<div class="image">';
        $products_li .= '<a href="' . $href . '">';
        $products_li .= '<img src="' . $PRODUCT_IMG . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '" class="img-responsive reg-image" style="height: 260px;" />';
        $products_li .= '<img class="img-responsive hover-image" src="' . $PRODUCT_IMG . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '" style="height: 260px;" />';
        $products_li .= '</a>';
        if ($discount_percentage > 0) {
            $products_li .= '<div class="ribbon ribbon-top-right"><span>' . $discount_percentage . '% OFF</span></div>';
        }
        if (in_array('RENT', $buyingMode)) {
            $products_li .= '<div class="percentsaving">RENT</div>';
        }
        $products_li .= '</div>';
        $products_li .= '<div class="product-details">';
        $products_li .= '<div class="caption">';
        $products_li .= '<h4><a href="' . $href . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '">' . $SubTitle . '</a></h4>';
        $products_li .= $authors;
        $products_li .= '<p class="price">';
        if ($latestRow->offerprice == $latestRow->mrp) {
            $products_li .= '<span class="price-new">' . RUPEE_LOGO . number_format($latestRow->offerprice, 2) . '</span>';
        } else {
            $products_li .= '<span class="price-new">' . RUPEE_LOGO . number_format($latestRow->offerprice, 2) . '</span> <span class="price-old">' . RUPEE_LOGO . $latestRow->mrp . '</span>';
        }
        $products_li .= '<span class="price-tax">Ex Tax: ' . RUPEE_LOGO . $latestRow->offerprice . '</span>';
        $products_li .= '</p>';
        $products_li .= '</div>';
        $products_li .= '</div>';
        $products_li .= '</div>';
        $products_li .= '</div>';
        $products_li .= '</div>';
        $products_li .= '</li>';
    }
} else {
    $products_li .= '<li class="double-slideitem slider-item">';
    $products_li .= '<div class="slider-item"><h3>&nbsp;&nbsp;&nbsp;&nbsp;No book found!</h3></div>';
    $products_li .= '</li>';
}
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo STATIC_SITE_NAME; ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <meta name="description" content="S. Radhakrishnan,E. Balagurusamy,Dhyan Chand,Miche, Action & Adventure,Arts, Film & Photography,Biographies, Diaries & True Accounts,Business & Economics,Children's & Young Adult,Comics & Mangas,Computers & Internet,Crafts), )Hobbies & Home,Crime,Thriller & Mystery,Science Fiction & Fantasy,Health, Family & Personal Development,Historical Fiction,History,Humour,Language, Linguistics & Writing,Law,Literature & Fiction,Maps & Atlases,Politics,Reference,Religion & Spirituality,Romance,Sciences, Technology & Medicine,Society & Social Sciences,Sports,Textbooks & Study Guides,Travel & Tourism,Academic,Business, Investing and Management,Entrance Exams Preparation,Engineering,Computer Science,Chemical Engineering" />
        <meta name="keywords" content="Action & Adventure,Arts, Film & Photography,Biographies, Diaries & True Accounts,Business & Economics,Children's & Young Adult,Comics & Mangas,Computers & Internet,Crafts), )Hobbies & Home,Crime,Thriller & Mystery,Science Fiction & Fantasy,Health, Family & Personal Development,Historical Fiction,History,Humour,Language, Linguistics & Writing,Law,Literature & Fiction,Maps & Atlases,Politics,Reference,Religion & Spirituality,Romance,Sciences, Technology & Medicine,Society & Social Sciences,Sports,Textbooks & Study Guides,Travel & Tourism,Academic,Business, Investing and Management,Entrance Exams Preparation,Engineering,Computer Science,Chemical Engineering" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/swiper.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/opencart.css',
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/jquery/swiper/js/swiper.jquery.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js',
            'web_view/catalog/view/javascript/jquery.awesomeCloud-0.2.js',
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_js($js);
        ?>
        <style type="text/css">
            .caption > h4 {
                min-height: 40px;
            }
            #loading-img {
                display: none;
                position:absolute;
                top:0px;
                right:0px;
                width:100%;
                height:100%;
                margin-top: 0 auto;
                background-color:#666;
                background-image:url(<?php echo LOADING_IMG_GIF; ?>);
                background-repeat:no-repeat;
                background-position:center;
                z-index:10000000;
                opacity: 0.4;
                filter: alpha(opacity=40);
            }
            .slug_hilight {
                color: #fff !important;
                font-size: 13px !important;
                font-weight: 900 !important;
                background: #e91e76 !important;
                border-radius: 5px !important;
            }
            .wordcloud {

                width: 260px;
                height: 350px;
                margin: 0.5in auto;
                padding: 0;
                page-break-after: always;
                page-break-inside: avoid;
            }


        </style>
    </head>
    <body class="common-home layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>

        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>
        <script type="text/javascript">

            function quickbox() {
                if ($(window).width() > 767) {
                    $('.quickview-button').magnificPopup({
                        type: 'iframe',
                        delegate: 'a',
                        preloader: true,
                        tLoading: 'Loading image #%curr%...',
                    });
                }
            }
            jQuery(document).ready(function () {
                quickbox();
            });
            jQuery(window).resize(function () {
                quickbox();
            }); //style="clear: both;float: left; width: 100%;height:220px;" 
        </script>
        <div id="loading-img"></div>

        <div class="container visible-xs">
            <div class="content-top">
                <div class="box" id="czservicecmsblock">
                    <div class="service_container">
                        <div class="service-area">

                        </div>
                    </div>
                </div>

            </div>
        </div>  
        <div class="visible-lg" >

            <?php
            foreach (HOME_PAGE_BANNERS as $banner) {
                echo '<div class=""><a href="#"><img src="' . $banner . '" alt="Left banner" class="img-responsive" /></a></div>';
            }
            ?>
        </div>

        <div class="visible-xs" >
            <div class="mobile_header">
                <h1>India’s Largest<br/>
                    <strong>Online BookHUB</strong><br/>
                    Rent/Buy
                </h1>	
            </div>
        </div>
        <script type="text/javascript">
            /* Can also be used with $(document).ready() */
            $(window).load(function () {
                $("#spinner").fadeOut("slow");
            });
        </script>
        <div id="common-home" class="container" style="margin-top: 3%;">
            <div class="row">
                <aside id="column-left" class="col-sm-3 hidden-xs">
                    <?php
                    if (!empty($all_category)) {
                        ?>
                        <div id="verticalmenublock" class="box category box-category ">
                            <div class="box-heading">All Categories</div>
                            <div class="box-content box-content-category">
                                <ul id="nav-one" class="dropmenu">
                                    <?php
                                    $i = 0;
                                    foreach ($all_category as $cat) {
                                        $classHighlt = '';

                                        if ($cat->slug == $cat_slug) {
                                            $classHighlt = 'class="slug_hilight"';
                                            $CategoryId = $cat->id;
                                        }
                                        echo '<li class="top_level main"><a ' . $classHighlt . ' href="' . site_url('home/books/' . $cat->slug) . '">' . $cat->name . '</a></li>';
                                        if ($i == 9) {
                                            break;
                                        }
                                        $i++;
                                    }
                                    echo '<li class="top_level"><a href="' . site_url('categories/all') . '">All</a></li>';
                                    ?>
                                </ul>
                            </div>
                        </div>
<?php } ?>
                    <div class="box special">
                        <div class="swiper-slide">
                            <div id="wordcloud1" class="wordcloud">
<?php echo $GetTagCloud_HTML; ?>
                            </div>
                        </div>
                    </div>
                    <div class="box special">
                        <div class="swiper-slide"><a href="#"><img src="<?php echo base_url('web_view/'); ?>image/catalog/sidebannertop2.png" alt="Left banner" class="img-responsive" /></a></div>
                    </div>
                    <div class="box special" style="    min-height: 200px;">
                        <div class="swiper-slide">
                        </div>
                    </div>
                    <span class="special_default_width" style="display:none; visibility:hidden"></span>

                    <div class="swiper-viewport">
                        <div id="banner0" class="swiper-container  single-banner ">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><a href="#"><img src="<?php echo base_url('web_view/'); ?><?php echo site_url('web_view'); ?>/image/cache/catalog/left-banner-1-260x350.jpg" alt="Left banner" class="img-responsive" /></a></div>
                            </div>
                            <!-- If we need pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-heading">Testimonials</div>
                        <div class="box-content" id="cztestimonialcmsblock">
                            <div class="testimonial_wrapper">
                                <div class="testimonial-area">
                                    <ul id="testimonial-carousel" class="products product-carousel">
                                        <li class="slider-item">
                                            <div class="testimonial-item">
                                                <div class="item cms_face">
                                                    <div class="quote_img"></div>
                                                    <div class="title">
                                                        <div class="name"><a href="#">Saurabh Kumar</a><span>(Member)</span></div>
                                                    </div>
                                                    <div class="product_inner_cms">
                                                        <div class="des">Great concept, great collection and a very prompt service. LeafLIB has made reading books really convenient and affordable.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="slider-item">
                                            <div class="testimonial-item">
                                                <div class="item cms_face">
                                                    <div class="quote_img"></div>
                                                    <div class="title">
                                                        <div class="name"><a href="#">Ravinder Sharma</a><span>(Student)</span></div>
                                                    </div>
                                                    <div class="product_inner_cms">
                                                        <div class="des">Your service - I can think of one word - "Excellent". This has impressed us so much that we have decided to become a member of your library permanently.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>

                <div id="content" class="col-sm-9"> <div class="hometab box">
                        <div class="container">
                            <div class="tab-head">
                                <?php echo $breadcrumb; ?>
                                <div class="hometab-heading box-heading">Latest Collection</div>
                                <div id="tabs" class="htabs">
                                    <ul class='etabs'>
                                        <li class='tab'>
                                            <a href="#tab-latest"><?php
                                                if (!empty($query)) {
                                                    echo $GetLatestCount . ' record(s) for your search "' . $query . '"';
                                                } else {
                                                    echo !empty($category_name) ? $category_name : 'Latest';
                                                    echo ' (' . $GetLatestCount . ')';
                                                }
                                                ?></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div id="tab-latest" class="tab-content">
                                <div class="box">
                                    <div class="box-content">
                                        <div class="box-product product-home"  style="display:block;">
                                        <?php echo $products_li; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <?php
                    if (HOME_PAGE_PER_RECORDS < $GetLatestCount) {
                        echo '<div class="btn btn-primary load_more">Load More</div>';
                    }
                    ?>
                    <div id="server_response"></div>
                </div>
            </div>
        </div>
<?php footer_tag(); ?>
        <input type="hidden" id="page_count" value="1" />
    </body>
</html>
<script type="text/javascript">
    $('#tabs a').tabs();
</script> 
<script type="text/javascript">
    $(document).ready(function () {
        $('#testimonial-carousel').owlCarousel({
            singleItem: true,
            navigation: false,
            pagination: true,
            autoPlay: false
        });
        // Custom Navigation Events
        $(".cztestimonial_next").click(function () {
            $('#testimonial-carousel').trigger('owl.next');
        })
        $(".cztestimonial_prev").click(function () {
            $('#testimonial-carousel').trigger('owl.prev');
        });

        $("#wordcloud1").awesomeCloud({
            "size": {
                "grid": 4,
                "normalize": false
            },
            "options": {
                "color": "random-dark",
                "rotationRatio": 0.35,
                "printMultiplier": 3,
                "sort": "random"
            },
            "font": "'Times New Roman', Times, serif",
            "shape": "square"
        });
    });

    $('.load_more').click(function () {
        var page_count = $('#page_count').val();
        $(this).html('<img src="<?php echo LOADING_IMG_GIF; ?>" height="20px" width="20px" /> Loading...');
        var category_id = '<?php echo $category_id; ?>';
        var query_search = '<?php echo $query; ?>';
        $.ajax({
            type: "POST",
            url: '<?php echo site_url('product/products/load_more'); ?>',
            data: {"start_point": page_count, 'category_id': category_id, 'query_search': query_search},
            success: function (returntxt)
            {
                $('.load_more').html('');
                $('.load_more').html('Load More');
                var parseJson = JSON.parse(returntxt);
                if (parseJson.responce.next_itr === false) {
                    $('.load_more').remove();
                }
                if (parseJson.status === true && parseInt(parseJson.responce.counter) > 0) {
                    new_pageCount = parseInt(page_count) + 1;
                    $('#page_count').val(new_pageCount);
                    $('.box-product').append(parseJson.responce.data);
                } else {
                    $('#server_response').addClass('alert alert-danger');
                    $('#review_response').html(parseJson.responce.error);
                }
            }
        });
    });
</script>
