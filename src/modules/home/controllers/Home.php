<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('site-link/web_view_header', 'site-link/web_view_footer'));
        $this->load->model(array('HomeService'));
    }

    public function Books($cat_slug = null) {
        $data['cat_slug'] = !empty($cat_slug) ? $cat_slug : '';
        $data['query'] = !empty($this->input->get('query')) ? $this->input->get('query') : '';
        $where_slug = false;
        $data['breadcrumb'] = '';
        $result = true;
        $all_category = $this->HomeService->GetCategory(array('slug !=' => '', 'is_active' => 1), array('result' => $result));
        $data['all_category'] = $all_category;
        $selectL = array('bp.title', 'bp.author_id', 'bp.offerprice', 'bp.mrp', 'bp.id', 'bp.status', 'bp.cover_image', 'bp.buying_mode');
        $whereL = array('bp.status' => 'publish', 'bp.cover_image !=' => '');

        if (!empty($cat_slug)) {
            foreach ($all_category as $ac) {
                if ($ac->slug == $cat_slug) {
                    $data['category_name'] = $ac->name;
                    $data['category_id'] = $ac->id;
                    $whereL['bpc.id'] = $ac->id;
                    $where_slug = true;
                    break;
                }
            }
        }

        if (!empty($data['query'])) {
            $whereL['bp.title'] = $data['query'];
        }

        $authorList = array();
        $data['GetAllLatest'] = $this->HomeService->GetAllLatest($whereL, $selectL, $where_slug);
        $data['GetLatestCount'] = $this->HomeService->GetCountOfLatest($whereL, $where_slug);

        $AllAuthor = $this->HomeService->GetAllAuthor(array('slug !=' => '', 'is_active' => 1));
        foreach ($AllAuthor as $author) {
            $authorList[$author->id] = $author;
        }
		$GetTagCloud = $this->HomeService->GetTagCloud();
		$GetTagCloud_HTML="";
		foreach($GetTagCloud as $gtc)
		{
			$GetTagCloud_HTML.='<span data-weight="'.rand(9,12).'"><a href="#">'.$gtc->name.'</a></span>';
		}
		
        $data['GetTagCloud_HTML'] = $GetTagCloud_HTML;
        $data['AllAuthor'] = $authorList;
        $this->load->view('landing', $data);
    }
	

    public function facebook() {

        $this->load->view('facebook');
    }

}
