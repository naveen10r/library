<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class HomeService extends TableName
{
    public function __construct() {
        parent::__construct();
    }
    
    public function GetAllCategory(array $where=array(), array $select=array(), $join=false)
    {
        $this->db->select($select);
        $this->db->from($this->productCategory.' AS pcc');
        if($join===true)
        {
            $this->db->join($this->productCategory.' AS pcm','pcc.id = pcm.parent_id');
        }
        $this->db->where($where);
        $this->db->order_by('pcc.name','ASC');
        return $this->db->get()->result();
    }
	
	
    public function GetAllLatest(array $where=array(), array $select=array(), $join=false, $start=0)
    {
        $this->db->select($select);
        $this->db->from($this->product.' AS bp');
        if($join){
            $this->db->join($this->product_category_junction.' AS bpcj', 'bpcj.product_id = bp.id', 'LEFT');
            $this->db->join($this->productCategory.' AS bpc', 'bpc.id = bpcj.category_id', 'LEFT');
        }
        if(!empty($where['bp.title'])){
            $this->db->like('bp.title', $where['bp.title']);
            unset($where['bp.title']);
        }
        $this->db->where($where);
        $this->db->group_by('bp.id');
        $this->db->order_by('bp.modified_at','DESC');
        $this->db->limit(HOME_PAGE_PER_RECORDS, $start);
        return $this->db->get()->result();
    }
    
    public function GetCountOfLatest(array $where=array(), $join=false)
    {
        $this->db->from($this->product.' AS bp');
        if($join){
            $this->db->join($this->product_category_junction.' AS bpcj', 'bpcj.product_id = bp.id', 'LEFT');
            $this->db->join($this->productCategory.' AS bpc', 'bpc.id = bpcj.category_id', 'LEFT');
        }
        if(!empty($where['bp.title'])){
            $this->db->like('bp.title', $where['bp.title']);
            unset($where['bp.title']);
        }
        $this->db->where($where);
        $this->db->group_by('bp.id');
        return $this->db->get()->num_rows();;
    }
    
    public function GetCategory(array $where=array(), array $config=array(), array $select=array()){
        $this->db->select($select);
        $this->db->from($this->productCategory);
        $this->db->where($where);
        $this->db->order_by('name','ASC');
        if($config['result'] === true){
            return $this->db->get()->result();
        } else {
            return $this->db->get()->row();
        }
    }
    
    public function GetAllAuthor(array $where=array(), array $config=array(), array $select=array()){
        $this->db->select($select);
        $this->db->from($this->author);
        $this->db->where($where);
        $this->db->order_by('name','ASC');
        if($config['row'] === true){
            return $this->db->get()->row();
        } else {
            return $this->db->get()->result();
        }
    }
	
	public function GetTagCloud(){
        $this->db->select("name");
        $this->db->from($this->product_tags);
        $this->db->where("is_active",1);
        $this->db->where("LENGTH(name)<",18,false);
        $this->db->order_by('RAND()');
        $this->db->limit(20);
        $result_set=$this->db->get()->result();
		return $result_set;
    }
	
	
    
}