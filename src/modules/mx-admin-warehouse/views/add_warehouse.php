<?php ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-wysihtml5.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>

        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu('Warehouse'); ?>
        <!--sidebar-menu-->
        <!--close-left-menu-stats-sidebar-->

        <div id="content">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="span10">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                <h5>Add WareHouse</h5>
                                <?php echo MANDATORY; ?>
                            </div>
                            <div class="widget-content nopadding">
                                <?php 
                                    echo form_open_multipart('mx-admin-warehouse/warehouse/addwarehouse', array('class' => 'form-horizontal', 'id'=>'saveWarehouse')); 
                                    echo input_csrf();
                                ?>
                                <div class="control-group">
                                    <label class="control-label">WareHouse Title <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Author Name"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" autocomplete="off"  class="span11" name="title" value="<?php echo (isset($WarehouseDetails->name))? $WarehouseDetails->name:""; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('title'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Description <sup class="red_error">&#042;</sup>
                                    <a class="tip-top" data-original-title="Alphabets and space allowed only."><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <textarea id="description" style="width: 627px; height: 67px;" name="description" rows="4" ><?php echo (isset($WarehouseDetails->description))? $WarehouseDetails->description:""; ?></textarea>

                                        <span class="help-inline red_error"><?php echo form_error('description'); ?></span> 
                                    </div>
                                </div>
								
								<div class="control-group">
                                    <label class="control-label">Address <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="address"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text"  autocomplete="off" class="span11" name="address" value="<?php echo (isset($WarehouseDetails->address))? $WarehouseDetails->address:""; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('address'); ?></span> 
                                    </div>
                                </div>
								
								<div class="control-group">
                                    <label class="control-label">Landmark <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="landmark"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text"  autocomplete="off" class="span11" name="landmark" value="<?php echo (isset($WarehouseDetails->landmark))? $WarehouseDetails->landmark:""; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('landmark'); ?></span> 
                                    </div>
                                </div>
                                
								<div class="control-group">
                                    <label class="control-label">Pin Code <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="pin_code"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" maxlength="6"  autocomplete="off" class="span11" name="pin_code" id="pin_code" value="<?php echo (isset($WarehouseDetails->pin_code))? $WarehouseDetails->pin_code:""; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('pin_code'); ?></span> 
                                    </div>
                                </div>
								
								<div class="control-group">
                                    <label class="control-label">Locality <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="locality"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" readonly autocomplete="off" class="span11" name="locality" id="locality" value="<?php echo (isset($WarehouseDetails->locality))? $WarehouseDetails->locality:""; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('locality'); ?></span> 
                                    </div>
                                </div>
								
								<div class="control-group">
                                    <label class="control-label">District <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="district"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" readonly autocomplete="off"  class="span11" name="district" id="district" value="<?php echo (isset($WarehouseDetails->district))? $WarehouseDetails->district:""; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('district'); ?></span> 
                                    </div>
                                </div> 
								
								<div class="control-group">
                                    <label class="control-label">State Name <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="address"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" readonly autocomplete="off" class="span11" name="state_name" id="state_name" value="<?php echo (isset($WarehouseDetails->state_name))? $WarehouseDetails->state_name:""; ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('state_name'); ?></span> 
                                    </div>
                                </div>

								
								
								
								
								
								
                                <div class="control-group">
                                    <label class="control-label">Check Robotic <sup class="red_error">&#042;</sup>
                                    </label>
                                    <div class="controls">
                                        <div class="span2">
                                            <img src="<?php echo base_url('captcha/'.$captcha['filename']); ?>" />
                                        </div>
                                        <div class="span1"><a href="javascript:void(0)" onclick="document.getElementById('saveProduct').submit();"><i class="icon-repeat"></i></a></div>
                                        <input type="text" name="captchaWord" class="span3" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('captchaWord'); ?></span> 
                                    </div>
                                </div>
                                <div class="form-actions">
								<?php if(isset($WarehouseDetails->id) && !empty($WarehouseDetails->id))
										{
											echo '<input type="hidden" name="warehouse_id" id="warehouse_id" value="'.$WarehouseDetails->id.'" />';
											echo '<button type="submit" class="btn btn-success"><i class="icon-save"></i> Update</button>';
										}else
										{
											echo '<button type="submit" class="btn btn-success"><i class="icon-save"></i> Save</button>';
										}
								?>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 2013 &copy; Matrix Admin.</div>
        </div>
        <!--end-Footer-part--> 
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap-wysihtml5.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/matrix.popover.js'); ?>"></script>
        <script>
            $(document).ready(function () {
             
				
				 $('#pin_code').on('blur', function () { 
					var pincode = $('#pin_code').val();
					GetPinCodeDetails(pincode);
				});
				
				function GetPinCodeDetails(pincode)
				{
					
					var postalUrl = '<?php echo site_url("mx-admin-warehouse/warehouse/GetPinDetails"); ?>';
				
					 $.ajax({
						url: postalUrl,
						type: 'post',
						data: {'pincode': pincode},
						success: function (pin_details) {
							  
							var JsonData = JSON.parse(pin_details);
							if (JsonData.status == true)
							{
								var PostOffice = JsonData.response.data;
								$('#locality').val(PostOffice[0].area_name);
								$('#district').val(PostOffice[0].district);
								$('#state_name').val(PostOffice[0].state);
							} else {
								$('#city_state').html('');
								$('#city_state').append('<option value="0">' + JsonData.response.error + '</option>');
							} 
						}
					}); 
				}
				<?php echo (isset($WarehouseDetails->pin_code))? "GetPinCodeDetails(".$WarehouseDetails->pin_code.")":""; ?>
            });
        </script>
    </body>
</html>