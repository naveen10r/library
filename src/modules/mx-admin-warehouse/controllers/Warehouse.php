<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Warehouse extends CI_Controller {

    final function __construct() {
        parent::__construct();
        $this->load->helper(array('admin-links/admin_menu', 'admin-links/admin_head', 'admin-links/admin_foot', 'string', 'captcha_security'));
        $this->load->model(array('WarehouseService'));
        $this->load->library('upload_lib');
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = (object) $this->session->userdata('userAuth');
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
        } else {
            redirect('mx-admin/logout');
        }
    }

	public function index() {
		$data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
		$data['allwarehouse'] = $this->WarehouseService->getallwarehouse(array());
		$this->load->view('list_warehouses', $data);
	}
	
    public function addwarehouse() {
		
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        if (!$this->input->post()) {
			if(!empty($this->uri->segment('4')) && $this->uri->segment('4')!="")
			{
				$where = array('id' => $this->uri->segment('4'));
				$WarehouseDetails = $this->WarehouseService->getallwarehouse($where);
				$data['WarehouseDetails'] = $WarehouseDetails[0];
			}
            $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
            $this->load->view('add_warehouse', $data);
        } else {
	
            $this->form_validation->set_rules('title', 'Title', 'required|trim|xss_clean|alpha_space');
            $this->form_validation->set_rules('description', 'Description', 'required|trim|xss_clean|alpha_space');
            $this->form_validation->set_rules('address', 'Address', 'required|trim|xss_clean');
            $this->form_validation->set_rules('landmark', 'Landmark', 'required|trim|xss_clean');
            $this->form_validation->set_rules('pin_code', 'pin_code', 'required|trim|xss_clean');
            $this->form_validation->set_rules('locality', 'Locality', 'required|trim|xss_clean');
			
            $this->form_validation->set_rules('captchaWord', 'captcha word', 'required|trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
                $this->load->view('add_warehouse', $data);
            } else {
                $title = $this->input->post('title', TRUE);
                $description = $this->input->post('description', TRUE);
                $address = $this->input->post('address', TRUE);
                $landmark = $this->input->post('landmark', TRUE);
                $pin_code = $this->input->post('pin_code', TRUE);
                $locality = $this->input->post('locality', TRUE);
                
				
				if(!empty($this->input->post('warehouse_id', TRUE)))
				{
					$save = array(
						'name' => $title,
						'description' => $description,
						'address' => $address,
						'landmark' => $landmark,
						'pin_code' => $pin_code,
						'locality' => $locality,
						'modified_at' => date(SYS_DB_DATE_TIME)
					);
					
					$where = array('id' => $this->input->post('warehouse_id', TRUE));
					$this->WarehouseService->UpdateWarehouse($save,$where);
					$isSave=0;
					$this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'WareHouse Update successfully.'));
					redirect(base_url() . 'mx-admin-warehouse/warehouse');
				}else{
					$save = array(
						'name' => $title,
						'description' => $description,
						'address' => $address,
						'landmark' => $landmark,
						'pin_code' => $pin_code,
						'locality' => $locality,
						'is_active' => 0,
						'created_at' => date(SYS_DB_DATE_TIME),
						'modified_at' => date(SYS_DB_DATE_TIME)
					);
					
					
					$isSave = $this->WarehouseService->SaveWarehouse($save);
				}
				
                if ($isSave) {
					$this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'WareHouse Saved successfully.'));
					redirect(base_url() . 'mx-admin-warehouse/warehouse');
					
                }
            }
        }
    }
	
	public function GetPinDetails() {
	
        $this->form_validation->set_rules('pincode', 'pincode', 'trim|required');
		
        if ($this->form_validation->run() == false) {
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => validation_errors(),
                    'error_code' => 1001,
                    'message' => ''
                ),
                'code' => 200
            ); 
			
            exit(json_encode($return));
        } else {
            $pincode = $this->input->post('pincode', TRUE);
            $select = array('id', 'pin_code', 'area_name', 'district', 'state_name AS state');
            $where = array('pin_code' => $pincode);
            $getPin = $this->WarehouseService->GetPinMasterData($where, $select);
			
            if (!empty($getPin)) {
                $return = array(
                    'status' => true,
                    'response' => array(
                        'error' => '',
                        'error_code' => 0,
                        'message' => count($getPin) . ' row(s) found!',
                        'data' => $getPin
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            } else {
                $return = array(
                    'status' => false,
                    'response' => array(
                        'error' => 'No data found',
                        'error_code' => 0,
                        'message' => '0 row(s) found!'
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            }
        }
    }
	
	 
    /*
     *@author : Sandeep 
     */
    public function updateStatuswarehouse()
    { 
        
		
        if (!$this->input->post()) {
			$response = array(
                'status' => 'error',
                'message' => "<p>Something went wrong.</p>"
            );
        } else {
			
            $this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean');
            $this->form_validation->set_rules('warehouse_id', 'Warehouse ID', 'required|trim|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $response = array(
					'status' => 'error',
					'message' => validation_errors()
				);
            } else {
                $status = $this->input->post('status', TRUE);
                $warehouse_id = $this->input->post('warehouse_id', TRUE);

                $update = array(
					'is_active' => $status,
					'modified_at' => date(SYS_DB_DATE_TIME)
				);
				$where = array('id' => $warehouse_id);
				$isSave = $this->WarehouseService->UpdateWarehouse($update,$where);
				$this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'WareHouse Update successfully.'));
				$response = array(
						'status' => 'success',
						'message' => "<p>Update successfully.</p>"
				);
            }
        }
		
		$this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }
	
	/*
     *@author : Sandeep 
     */
    public function updateStatusauthor()
    { 
        
		
        if (!$this->input->post()) {
			$response = array(
                'status' => 'error',
                'message' => "<p>Something went wrong.</p>"
            );
        } else {
			
            $this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean');
            $this->form_validation->set_rules('author_id', 'Author ID', 'required|trim|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $response = array(
					'status' => 'error',
					'message' => validation_errors()
				);
            } else {
                $status = $this->input->post('status', TRUE);
                $author_id = $this->input->post('author_id', TRUE);

                $update = array(
					'is_active' => $status,
					'modified_at' => date(SYS_DB_DATE_TIME)
				);
				$where = array('id' => $author_id);
				$isSave = $this->WarehouseService->Updateauthor($update,$where);
				$this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Author Update successfully.'));
				$response = array(
						'status' => 'success',
						'message' => "<p>Update successfully.</p>"
				);
            }
        }
		
		$this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }
    
    /*
     *@author : Naveen 
     */
    public function createIndexes()
    {
        
    }

    private function fileWithValidation($picture, $fileExt) {
        $pictures = !empty($picture) ? $picture : 'pictures';
        $upImg = $_FILES['uploadImg'];
        if (empty($upImg['name'])) {
            $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is required.');
            return false;
        } else {
            $realExt = strtolower($upImg['name']);
            $ext = explode('.', $realExt);
            $extension = end($ext);
            $fileExt = explode('|', $fileExt);
            if (!in_array($extension, $fileExt)) {
                $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is not in the correct format.');
                return false;
            }
        }
        return true;
    }

    private function captchaWord($word) {
        $captcha_word = $this->session->tempdata('captcha_word');
        if (empty($word)) {
            $this->form_validation->set_message('captchaWord', 'The captcha field is required.');
            return false;
        } elseif ($captcha_word != $word) {
            $this->form_validation->set_message('captchaWord', 'The captcha security code doesn\'t matched.');
            return false;
        } else {
            return true;
        }
    }

}
