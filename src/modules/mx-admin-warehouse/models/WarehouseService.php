<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 */

class WarehouseService extends TableName
{
    public function __construct() {
        parent::__construct();
    }
    
    public function SaveWarehouse(array $insert):int
    {
        $this->db->trans_start();
        $this->db->insert($this->warehouse, $insert);
        return $this->trans_insert_data();
    }
	
	public function getallwarehouse(array $where)
    {
        $this->db->select('id,name,is_active,description,modified_at,pin_code,locality,address,landmark');
        $this->db->from($this->warehouse);
        $this->db->where($where);
        return $this->db->get()->result();
    }
	
	public function UpdateWarehouse(array $update, array $where) {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->warehouse, $update);
        return $this->trans_update_data();
    }
	
	public function GetPinMasterData($where, $select, $isRow=FALSE)
    { 
        $this->db->select($select);
        $this->db->from($this->masterPinCode);
        $this->db->where($where);
        if($isRow){
            return $this->db->get()->row();
        } else {
            return $this->db->get()->result();
        }
    }
}