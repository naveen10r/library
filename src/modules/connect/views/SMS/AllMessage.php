<?php
$lang = language_encode(array('sms_lists', 'sms_sort_code', 'sms_body', 'action', 'edit', 'sms_add', 'permission_denied', 'submit'));
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <meta charset="UTF-8" />
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu('connect'); ?>
        <!--sidebar-menu-->
        <div id="content">
            <div id="content-header">
                <div id="breadcrumb"> 
                    <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
                    <a href="#" class="current">ALL SMS</a> 
                </div>
            </div>
            <div class="container-fluid">
                <div id="alertResponce">
                    <?php
                    if ($this->session->flashdata('alert')) {
                        $alert = $this->session->flashdata('alert');
                        echo $alert['color']($alert['responce']);
                    }
                    ?> 
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box">
                            <div class="widget-title">
                                <h5><?php echo $lang['sms_lists']; ?></h5>
                                <?php if (auth_uri_menu('connect/SmsAction/AllSms')) { ?>
                                    <div class="pull-right">
                                        <a href="javascript:void(0)" class="btn btn-success btn-mini pull-right" title="Add new sms" style="margin: 7% 9px 0% 0%;"><i class="icon-plus-sign" data-toggle="modal" data-target="#addSmsModal"></i>&nbsp;<?php echo $lang['sms_add']; ?></a></div>
                                <?php } ?>

                            </div>
                            <div class="widget-content nopadding">
                                <table class="table table-bordered data-table">
                                    <thead>
                                        <tr>
                                            <th><i class="icon-resize-vertical"></i></th>
                                            <th><?php echo $lang['sms_sort_code']; ?></th>
                                            <th><?php echo $lang['sms_body']; ?></th>
                                            <th width='10%'><center><?php echo $lang['action']; ?></center></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($allSMS as $temp):
                                            $i++;
                                            $class = '';
                                            if ($i == 1) {
                                                $class = 'gradeX';
                                            } elseif ($i == 2) {
                                                $class = 'gradeC';
                                            } else {
                                                $class = 'gradeA';
                                            }
                                            ?>
                                            <tr <?php echo 'class="' . $class . '"'; ?>>
                                                <td><?php echo $temp->id; ?></td>
                                                <td><?php echo trim($temp->title, "'"); ?></td>
                                                <td><?php echo trim($temp->template, "'"); ?></td>
                                                <td class="center">
                                                    <?php
                                                    if (auth_uri_menu('connect/Mail/ModifyMailTemplate')) {
                                                        echo '<a href="' . base_url('connect/Mail/ModifyMailTemplate/' . esecure($temp->id)) . '" class="btn btn-inverse btn-mini"><i class="icon-pencil"></i>' . $lang['edit'] . '</a>';
                                                    } else {
                                                        echo '<a href="javascript:void(0);" class="btn btn-inverse btn-mini" title="' . $lang['permission_denied'] . '"><i class="icon-pencil"></i>' . $lang['edit'] . '</a>';
                                                    }
                                                    ?>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?> 
                                    </tbody>
                                    <?php if (auth_uri_menu('connect/SmsAction/AllSms')) { ?>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4">
                                                    <a href="javascript:void(0)" class="btn btn-success btn-mini pull-right" title="Add new sms" data-toggle="modal" data-target="#addSmsModal"><i class="icon-plus-sign"></i>&nbsp;<?php echo $lang['sms_add']; ?></a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if($this->session->flashdata('OpenModal')){
            echo '<script type="text/javascript">';
            echo '$("#myModal").modal()';
            echo '</script>';
        }
        ?>
        <div class="modal fade" id="addSmsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold"><?php echo $lang['sms_add']; ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?php echo site_url('connect/SmsAction/AllSms'); ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label label class="control-label"><strong>Sort Code</strong> <sup class="red_error">&#042;</sup></label>
                                <select name="sort_code" class="span4">
                                    <?php
                                    foreach ($sort_code_list as $scl) {
                                        echo '<option value="' . $scl . '">' . $scl . '</option>';
                                    }
                                    ?>
                                </select>
                                <span class="help-inline red_error"><?php echo form_error('sort_code'); ?></span>
                            </div>
                            <div class="form-group">
                                <label label class="control-label"><strong>SMS Text</strong> <sup class="red_error">&#042;</sup></label>
                                <textarea class="span4" name="sms_txt"><?php echo set_value('sms_txt'); ?></textarea>
                                <span class="help-inline red_error"><?php echo form_error('sms_txt'); ?></span>
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <button type="submit" class="btn btn-default"><?php echo $lang['submit']; ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.dataTables.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.tables.js'); ?>"></script>
    </body>
</html>