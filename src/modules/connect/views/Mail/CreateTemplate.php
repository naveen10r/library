<?php
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-wysihtml5.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>

        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu('connect'); ?>
        <!--sidebar-menu-->
        <!--close-left-menu-stats-sidebar-->

        <div id="content">
            <div id="content-header">
                <div id="breadcrumb"> 
                    <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
                    <a href="#" class="tip-bottom">Form elements</a> 
                    <a href="#" class="current">Common elements</a> 
                </div>
            </div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="span10">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                <h5>Add Email Template</h5>
                                <?php echo MANDATORY; ?>
                            </div>
                            <div class="widget-content nopadding">
                                <?php echo form_open('connect/Mail/CreateTemplate', array('class' => 'form-horizontal')); ?>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <div class="control-group">
                                    <label class="control-label">Mail Subject <sup class="red_error">&#042;</sup></label>
                                    <div class="controls">
                                        <input type="text" id="inputError" class="span11" name="subject" value="<?php echo set_value('subject'); ?>" />
                                        <span class="help-inline red_error"><?php echo form_error('subject'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Unique Slug<sup class="red_error">&#042;</sup></label>
                                    <div class="controls">
                                        <input type="text" class="span11" name="unique_slug" value="<?php echo set_value('unique_slug'); ?>" style="text-transform: uppercase;" />
                                        <span class="help-inline red_error"><?php echo form_error('unique_slug'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Mail Body <sup class="red_error">&#042;</sup></label>
                                    <div class="controls">
                                        <textarea class="textarea_editor span11" rows="6" placeholder="Enter text ..." name="body"><?php echo set_value('body'); ?></textarea>
                                        <span class="help-inline red_error"><?php echo form_error('body'); ?></span> 
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success"><i class="icon-save"></i> Save</button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 2013 &copy; Matrix Admin.</div>
        </div>
        <!--end-Footer-part--> 
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/wysihtml5-0.3.0.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap-wysihtml5.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script>
        <script>
            $('.textarea_editor').wysihtml5();
        </script>
    </body>
</html>
