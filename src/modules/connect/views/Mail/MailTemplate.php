<?php
$lang = language_encode(array('email_templates', 'mail_subject', 'mail_body', 'action', 'edit', 'menu_email_add', 'permission_denied'));
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <meta charset="UTF-8" />
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/select2.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu('connect'); ?>
        <!--sidebar-menu-->
        <div id="content">
            <div id="content-header">
                <div id="breadcrumb"> 
                    <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
                    <a href="#" class="current">Tables</a> 
                </div>
            </div>
            <div class="container-fluid">
                <div id="alertResponce">
                    <?php
                    if ($this->session->flashdata('alert')) {
                        $alert = $this->session->flashdata('alert');
                        echo $alert['color']($alert['responce']);
                    }
                    ?> 
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box">
                            <div class="widget-title">
                                <h5><?php echo $lang['email_templates']; ?></h5>
                            </div>
                            <div class="widget-content nopadding">
                                <table class="table table-bordered data-table">
                                    <thead>
                                        <tr>
                                            <th><i class="icon-resize-vertical"></i></th>
                                            <th><?php echo 'Slug'; ?></th>
                                            <th><?php echo $lang['mail_subject']; ?></th>
                                            <th><?php echo $lang['mail_body']; ?></th>
                                            <th width='7%'><center><?php echo $lang['action']; ?></center></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($mailer as $temp):
                                            $i++;
                                            $class = '';
                                            if ($i == 1) {
                                                $class = 'gradeX';
                                            } elseif ($i == 2) {
                                                $class = 'gradeC';
                                            } else {
                                                $class = 'gradeA';
                                            }
                                            ?>
                                            <tr <?php echo 'class="' . $class . '"'; ?>>
                                                <td><?php echo $temp->id; ?></td>
                                                <td><?php echo $temp->unique_title; ?></td>
                                                <td><?php echo trim($temp->title, "'"); ?></td>
                                                <td><?php echo trim($temp->template, "'"); ?></td>
                                                <td class="center">
                                                    <?php
                                                    if (auth_uri_menu('connect/Mail/ModifyMailTemplate')) {
                                                        echo '<a href="' . base_url('connect/Mail/ModifyMailTemplate/' . esecure($temp->id)) . '" class="btn btn-inverse btn-mini"><i class="icon-pencil"></i>' . $lang['edit'] . '</a>';
                                                    } else {
                                                        echo '<a href="javascript:void(0);" class="btn btn-inverse btn-mini" title="' . $lang['permission_denied'] . '"><i class="icon-pencil"></i>' . $lang['edit'] . '</a>';
                                                    }
                                                    ?>

                                                </td>
                                            </tr>
                                    <?php endforeach; ?> 
                                    </tbody>
<?php if (auth_uri_menu('connect/Mail/CreateTemplate')) { ?>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5">
                                                    <a href="<?php echo base_url('connect/Mail/CreateTemplate'); ?>" class="btn btn-success btn-mini pull-right" title="Add mail template"><i class="icon-plus-sign"></i><?php echo $lang['menu_email_add']; ?></a>
                                                </td>
                                            </tr>
                                        </tfoot>
<?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 2013 &copy; Matrix Admin.</div>
        </div>
        <!--end-Footer-part-->
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/select2.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.dataTables.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.tables.js'); ?>"></script>
    </body>
</html>