<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class SmsAction extends CI_Controller {

    private $userId;
    private $userCode;
    public $smsCode = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('admin-links/admin_menu', 'admin-links/admin_head', 'admin-links/admin_foot'));
        $this->load->model(array('SmsService'));
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = $this->session->userdata('userAuth');
            $userAuth = (object) $userAuth;
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
            $this->smsCode = $this->config->item('SMS_CODES');
        } else {
            redirect('mx-admin/logout');
        }
    }
    
    public function AllSms()
    {
        $data['sort_code_list'] = $this->smsCode;
        $data['allSMS'] = $this->SmsService->getAllSms();
        if($this->input->post())
        {
            $this->form_validation->set_rules('sort_code','sort code', 'trim|required|xss_clean');
            $this->form_validation->set_rules('sms_txt','sms text', 'trim|required|xss_clean');
            if($this->form_validation->run() == FALSE)
            {
                $this->session->set_flashdata('OpenModal','found');
                $this->load->view('SMS/AllMessage', $data);
            }
            $sort_code = $this->input->post('sort_code', TRUE);
            $sms_txt = $this->input->post('sms_txt', TRUE);
            $save = array(
                'title' => $sort_code,
                'template' => $sms_txt,
                'created_by' => $this->userId
            );
            $this->SmsService->InsertSMS($save);
            redirect('connect/SmsAction/AllSms', 'refresh');
        } else {
            $this->load->view('SMS/AllMessage', $data);
        }
    }
    
}