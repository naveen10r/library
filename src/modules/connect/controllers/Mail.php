<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {

    private $userId;
    private $userCode;

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('admin-links/admin_menu', 'admin-links/admin_head', 'admin-links/admin_foot'));
        $this->load->model(array('MailModel'));
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = $this->session->userdata('userAuth');
            $userAuth = (object) $userAuth;
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
        } else {
            redirect('mx-admin/logout');
        }
    }

    public function MailTemplate() {
        $data['mailer'] = $this->MailModel->getAllTemplate();
        $this->load->view('Mail/MailTemplate', $data);
    }

    public function CreateTemplate() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('subject', 'mail subject', 'trim|required|min_length[10]');
            $this->form_validation->set_rules('body', 'mail body', 'trim|required|min_length[20]');
            $this->form_validation->set_rules('unique_slug', 'slug', 'trim|required|alpha_numeric_spaces|max_length[120]|is_unique[br_email_template.unique_title]');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('Mail/CreateTemplate');
            } else {
                $subject = $this->input->post('subject', TRUE);
                $body = $this->input->post('body', TRUE);
                $slug = $this->input->post('unique_slug', TRUE);
                $insert = array(
                    'title' => $subject,
                    'unique_title' => str_replace(' ', '_', $slug),
                    'template' => $body,
                    'create_at' => date(SYS_DB_DATE_TIME),
                    'update_at' => NULL,
                    'create_by' => $this->userId,
                    'update_by' => NULL,
                );
                $respone = self::insertTemplate($insert);
                if (is_int($respone)) {
                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Data saved.'));
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $respone));
                }
                redirect($this->agent->referrer());
            }
        } else {
            $this->load->view('Mail/CreateTemplate');
        }
    }

    private function insertTemplate(array $insert = array()) {
        try {
            $isInsert = $this->MailModel->createEmailTemplate($insert);
            SaveInsertLogData($insert, array('last_insert_id' => $isInsert) , 'br_email_template', $this->userId);
            return $isInsert;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function ModifyMailTemplate($srtId = '') {
        if (auth_uri_menu('connect/Mail/ModifyMailTemplate') == false OR empty($srtId)) {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Access denied!'));
            redirect($this->agent->referrer());
        } else {
            if ($this->input->post()) {
                $this->updateTemplate($srtId);
            } else {
                self::getTemplate($srtId);
            }
        }
    }

    final function getTemplate($id) {
        $srtId = dsecure($id);
        $data['mailer'] = $this->MailModel->getAllTemplate(array('id' => $srtId));
        $this->load->view('Mail/ModifyTemplate', $data);
    }

    private function updateTemplate($id) {
        $id = dsecure($id);
        $this->form_validation->set_rules('subject', 'mail subject', 'trim|required|min_length[10]');
        $this->form_validation->set_rules('body', 'mail body', 'trim|required|min_length[20]');
        if ($this->form_validation->run() == FALSE) {
            self::getTemplate($id);
        } else {
            $update = array(
                'title' => $this->input->post('subject',TRUE),
                'template' => $this->input->post('body',TRUE),
                'update_by' => $this->userId,
                'update_at' => date(SYS_DB_DATE_TIME)
            );
            $where = array('id' => $id);
            $isUpdate = $this->updateMailTemplate($update,$where);
            if(!empty($isUpdate) AND is_numeric($isUpdate))
            {
                SaveLogData($where, $update, 'br_email_template', $this->userId);
                $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Modifyied successfull.'));
            } else {
                $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Error try again.'));
            }
            redirect($this->agent->referrer());
        }
    }
    
    private function updateMailTemplate(array $update, array $where)
    {
        try {
            return $this->MailModel->updateTemplate($update, $where);
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

}
