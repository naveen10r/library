<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SmsService extends TableName {
    
    public function getAllSms(array $where = array()) {
        $this->db->select('id,title,template');
        $this->db->from($this->smsTemplate);
        $this->db->where($where);
        return $this->db->get()->result();
    }
    
    public function InsertSMS($insert)
    {
        $this->db->trans_start();
        $this->db->insert($this->smsTemplate, $insert);
        return $this->trans_insert_data();
    }
    
}