<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PackageService extends TableName
{
    public function __construct() {
        parent::__construct();
    }
    
    public function GetSchoolList(array $where=array())
    {
        
        $this->db->select(array('id','name','address','pin_code'));
        $this->db->from($this->schools);
        $this->db->where($where);
        return $this->db->get()->result();
    }
    
    public function GetSchoolClasses(array $where=array())
    {
        $this->db->select(array('id','class', 'section'));
        $this->db->from($this->schoolClassSec);
        $this->db->where($where);
        $this->db->group_by('class');
        $this->db->order_by('class_order_by');
        return $this->db->get()->result();
    }
}