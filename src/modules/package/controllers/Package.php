<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Package extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('site-link/web_view_header', 'string', 'site-link/web_view_footer'));
        $this->load->library(array('email_lib')); /* ,'MongoDb' */
        $this->lang->load(array('itagpolicy','error_message'));
        $this->load->model(array('PackageService'));
    }
    
    public function choose_package()
    {
        $data['all_schools'] = $this->PackageService->GetSchoolList(array('is_active' => 1));
        $this->load->view('choose_package', $data);
    }
    
    public function get_school_classes()
    {
        $this->form_validation->set_rules('school_id', 'school id', 'trim|required|is_natural_no_zero');
        if($this->form_validation->run() == false)
        {
            $array = array('status' => false, 'code' => 1009, 'error_text' => validation_errors().$this->lang->line('err_1009'));
            api_response($array);
        } else {
            $school_id = $this->input->post('school_id', true);
            $isClassess = $this->PackageService->GetSchoolClasses(array('is_active' => 1, 'school_id' => $school_id));
            if(!empty($isClassess))
            {
                $array = array('status' => true, 'code' => 200, 'success_text' => 'data fetched', 'success_data' => $isClassess);
                api_response($array);
            } else {
                $array = array('status' => false, 'code' => 1005, 'error_text' => $this->lang->line('err_1005'));
                api_response($array);
            }
        }
    }
    
    
    public function get_school_section()
    {
        $this->form_validation->set_rules('class_id', 'class', 'trim|required|is_natural_no_zero');
        if($this->form_validation->run() == false)
        {
            $array = array('status' => false, 'code' => 1009, 'error_text' => validation_errors().$this->lang->line('err_1009'));
            api_response($array);
        } else {
            $class_id = $this->input->post('class_id', true);
            $isClassess = $this->PackageService->GetSchoolClasses(array('is_active' => 1, 'id' => $class_id));
            if(!empty($isClassess))
            {
                $array = array('status' => true, 'code' => 200, 'success_text' => 'data fetched', 'success_data' => $isClassess);
                api_response($array);
            } else {
                $array = array('status' => false, 'code' => 1005, 'error_text' => $this->lang->line('err_1005'));
                api_response($array);
            }
        }
    }
    
}