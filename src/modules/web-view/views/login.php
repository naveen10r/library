<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo APP_TITLE; ?></title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url('web-inf/web-view/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo base_url('web-inf/web-view/css/modern-business.css'); ?>" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="<?php echo base_url('web-inf/web-view/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    </head>
    <body class="back">
        <!-- Navigation -->
        <div id="myCarousel" class="carousel slide">
            <!-- Indicators -->
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="fill" >
                        <div class="container">
                            <h2>Welcome To PathWay</h2>
                            <div class="col-md-12 col-xs-12">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <form>
                                    <div class="group">
                                        <input type="text" /><span class="highlight"></span><span class="bar"></span>
                                        <label>User Name</label>
                                    </div>
                                    <div class="group">
                                        <input type="email"><span class="highlight"></span><span class="bar"></span>
                                        <label>Password</label>
                                    </div>
                                    <button type="button" class="button buttonBlue">
                                        Login
                                        <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
                                    </button>
                                    <p><a href="<?php echo site_url('users/AppUsers/registration'); ?>">Click here</a> to free signup</p>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Content -->
        <!-- /.container -->
        <!-- jQuery -->
        <script src="<?php echo base_url('web-inf/web-view/js/jquery.js'); ?>"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url('web-inf/web-view/js/bootstrap.min.js'); ?>"></script>
        <!-- Script to Activate the Carousel -->
        <script>
            $('.carousel').carousel({
                interval: 5000 //changes the speed
            })
        </script>
    </body>
</html>