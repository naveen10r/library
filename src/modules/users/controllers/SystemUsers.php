<?php

/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require_once realpath(dirname(__FILE__)).'/SystemUserTrait.php';

class SystemUsers extends CI_Controller {
    use SystemUserTrait;
    private $userId;
    private $userCode;

    final function __construct() {
        parent::__construct();
        $this->load->helper(array('admin-links/admin_menu', 'admin-links/admin_head', 'admin-links/admin_foot', 'string', 'captcha_security'));
        $this->load->model('users/SystemUsersDB');
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = (object) $this->session->userdata('userAuth');
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
        } else {
            redirect('mx-admin/logout');
        }
    }

    public function newInternalUser() {
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        if (!$this->input->post()) {
            $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
            $this->load->view('Internal/adminUsers', $data);
        } else {
            $this->form_validation->set_rules('username', 'username', 'required|trim|xss_clean|min_length[6]|is_unique[admin_users.username]|regex_match[/' . FOR_REG_CAP_ALPHA_NUMERIC . '/]');
            $this->form_validation->set_rules('name', 'name', 'required|trim|xss_clean|alpha_space');
            $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email|is_unique[admin_users.email]');
            $this->form_validation->set_rules('mobile', 'mobile', 'required|trim|xss_clean|exact_length[10]|regex_match[/' . FOR_REG_MOBILE . '/]');
            $this->form_validation->set_rules('uploadImg', 'pictures', 'callback_fileWithValidation[jpeg|jpg|png]');
            $this->form_validation->set_rules('captchaWord', 'captcha word', 'required|trim|xss_clean|callback_captchaWord');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
                $this->load->view('Internal/adminUsers', $data);
            } else {
                $username = $this->input->post('username', TRUE);
                $name = $this->input->post('name', TRUE);
                $email = $this->input->post('email', TRUE);
                $mobile = $this->input->post('mobile', TRUE);
                $insert = ['username' => $username, 'name' => $name, 'email' => $email, 'email_key' => random_string('alnum', 32),
                    'mobile' => $mobile, 'mobile_key' => rand(10000, 99999), 'create_at' => date(SYS_DB_DATE_TIME),
                    'create_by' => $this->userId,
                ];
                $isSave = $this->createAdminUsers($insert);
                if (is_int($isSave)) {
                    $this->createDir($isSave);
                    $isUpload = $this->uploadPic($isSave, TRUE);
                    if (empty($isUpload)) {
                        $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'User create successfully and activation send to given mail id'));
                    } else {
                        $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'User create successfully but ' . $isUpload));
                    }
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $isSave));
                }
                redirect($this->agent->referrer());
            }
        }
    }

    public function allInternalUser() {
        $data['internalUsers'] = $this->SystemUsersDB->getAllInternalUsers(array('is_delete' => 0));
        $this->load->view('Internal/allInternalUser', $data);
    }

    public function changeStatus($action) {
        $checked = $this->input->post('checked', true);
        $isUpdate = [];
        $update = [];
        $where = false;
        switch ($action) {
            case 'active' : $update = ['is_status' => 1];
                break;
            case 'deactive' : $update = ['is_status' => 0];
                break;
            case 'delete' : $update = ['is_delete' => 1];
                $where = true;
                break;
            default : $this->noIdSelected(true);
                break;
        }
        if (count($checked) > 0) {
            for ($i = 0; $i < count($checked); $i++) {
                if ($where === FALSE) {
                    $isUpdate[] = $this->SystemUsersDB->updateAdminUser(array('id' => $checked[$i]), $update);
                } else {
                    $isUpdate[] = $this->SystemUsersDB->updateAdminUserInQuery(array('id' => $checked[$i]), $update);
                }
            }
        } else {
            $this->noIdSelected(false);
        }
        $this->setResponce($isUpdate);
    }

    public function ResetPassword($id) {
        $userId = dsecure($id);
        if (is_numeric($userId)) {
            $randPwd = random_string('alnum', 8);
            $passwordHash = password_hash($randPwd, PASSWORD_DEFAULT);
            $isUpdate = $this->SystemUsersDB->updateAdminUser(array('id' => $userId), array('password' => $passwordHash));
            if ($isUpdate) {
                $this->sendResetPasswordMail();
            }
        } else {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Please do not change uri string.'));
        }
        redirect($this->agent->referrer());
    }
    
    private function sendResetPasswordMail()
    {
        return true;
    }
    
    public function ExternalUsers($pageNo=null)
    {
        $page_No = (!empty($pageNo) or ($pageNo == 1)) ? 0 : $pageNo * 100;
        $start = $page_No;
        $data['start_at'] = empty($page_No) ? $page_No : 1;
        $data['ExternalUsers'] = $this->SystemUsersDB->getAllUsers(array(), $page_No);
        $data['ExternalUsersCount'] = $this->SystemUsersDB->getAllUsersCount();
        $data['limit_at'] = $data['ExternalUsersCount'];
        $this->load->view('Internal/ExternalUsers', $data);
    }
}
