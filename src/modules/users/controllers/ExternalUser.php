<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once realpath(dirname(__FILE__)) . '/ExUserAddressNAccount.php';

class ExternalUser extends CI_Controller {

    use ExUserAddressNAccount;

    public function __construct() {
        parent::__construct();
        $this->load->model(array('ExternalUserModel', 'AppUserModel', 'connect/MailModel'));
        $this->load->helper(array('site-link/web_view_header', 'string', 'site-link/web_view_footer'));
        $this->load->library(array('email_lib'));
        $this->lang->load('itagpolicy');
    }

    public function Register() {
        $data = array();
        if (!$this->input->post()) {
            $this->load->view('External/registration', $data);
        } else {
            $this->form_validation->set_rules('fullname', 'Full name', 'trim|xss_clean|required|max_length[16]');
            $this->form_validation->set_rules('email', 'email', 'trim|xss_clean|required|valid_email|is_unique[' . SYS_DB_PREFIX . 'users.email]');
            $this->form_validation->set_message('is_unique', 'This %s is already registered, please choose another one');
           $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|regex_match[/' . PASSWORD_PATTERN . '/]');
            $is_password_auto = 0;
            

            if ($this->form_validation->run() == false) {
                $this->load->view('External/registration', $data);
            } else {
                $fullname = $this->input->post('fullname', true);
                $email = $this->input->post('email', true);
                $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                $saveData = array(
                    'name' => $fullname,
                    'username' => NULL,
                    'email' => strtolower($email),
                    'is_email_verify' => 0,
                    'password' => $passwordHash,
                    'is_password_auto' => $is_password_auto,
                    'is_mobile_verify' => 0,
                    'create_at' => date(SYS_DB_DATE_TIME),
                    'user_agent' => $this->input->ip_address(),
                    'is_active' => 1,
                    'is_delete' => 0,
                    'account_by' => NULL
                );
                $userInfo = array('name' => $uname, 'password' => $password, 'emailTo' => strtolower($email), 'pwd_auto' => $is_password_auto);
                $isSave = $this->createUsers($saveData);
                if (!empty($isSave)) {
                    $isSave['mobile'] = $mobile;
                    self::sendEmailAndSMS($isSave, $userInfo);
                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Congratulation : Check your email for more details.'));
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
                }
                redirect($this->agent->referrer());
            }
        }
    }

    private function createUsers(array $insert = array()) {
        $isSave = $this->ExternalUserModel->createUser($insert);
        if ($isSave) {
            $emailOTP = random_string('alnum', '32');
            $mobileOTP = rand(10000, 99999);
            $insertMobileOTP = array(
                'user_id' => $isSave, 'otp' => $mobileOTP, 'otp_for' => 'M',
                'create_at' => date(SYS_DB_DATE_TIME), 'valid_at' => date(SYS_DB_DATE_TIME, strtotime('+2 days')),
                'is_verify' => 0
            );
            $isMobile = $this->AppUserModel->saveOTP($insertMobileOTP);
            /*
            $insertEmailOTP = array(
                'user_id' => $isSave, 'otp' => $emailOTP, 'otp_for' => 'E', 'create_at' => date(SYS_DB_DATE_TIME),
                'valid_at' => date(SYS_DB_DATE_TIME, strtotime('+7 days')), 'is_verify' => 0
            );
            */
            $isEmail = ''; /* $this->AppUserModel->saveOTP($insertEmailOTP); */
            $return['userId'] = $isSave;
            if ($isMobile) {
                $return['mobile_otp'] = $mobileOTP;
            }
            if ($isEmail) {
                $return['email_otp'] = $emailOTP;
            }
            return $return;
        }
    }

    private function sendEmailAndSMS($isSave=null, $userInfo) {
        $mailTemp = $this->MailModel->getAllTemplate(array('id' => 100016));
        $subject = $mailTemp[0]->title;
        $template = $mailTemp[0]->template;
        $dearName = ucwords(strtolower($userInfo['name']));
        $userPwd = $userInfo['password'];
        if($userInfo['pwd_auto'])
        {
            $randStr = strtoupper(random_string());
            $userPwd = '<strong>Self Genereted</strong> <br />Please verify your email by using below code <br /><strong>EMAIL OTP : </strong>' . $randStr;
            $emailInsert = array(
                'user_id' => $isSave['userId'],
                'otp' => $randStr,
                'otp_for' => 'E',
                'otp_for_value' => $userInfo['emailTo'],
                'otp_title' => 'USER_EMAIL_VERIFICATION',
                'create_at' => date(SYS_DB_DATE_TIME),
                'valid_at' => date(SYS_DB_DATE_TIME, strtotime('+1Year'))
            );
            $this->ExternalUserModel->InsertUserOTP($emailInsert);
        }
        $newText = str_replace(
                array('{%NAME%}', '{%EMAIL%}', '{%PASSWORD%}', '{%THANKS_FROM%}', '{%LOGIN_URL%}', '{%TECH_SUPPORT_EMAIL%}', '{%TECH_SUPPORT_MOBILE%}', '{%SUPPORT_EMAIL%}', '{%TOLL_FREE_NO%}', '{%SUPPORT_MOBILES%}', '{%CONTACT_US_URL%}'),
                array($dearName, $userInfo['emailTo'], $userPwd, EMAIL_THANKS, site_url('login'), EMAIL_TECH_SUPPORT, MOBILE_TECH_SUPPORT, EMAIL_SUPPORT, TOLL_FREE_NO, MOBILE_SUPPORT, site_url(CONTACT_US_URL)),
                $template
        );
        $WelcomeEmail = array(
            'SUBJECT' => $subject,
            'MESSAGE' => $newText,
            'MAIL_TO' => $userInfo['emailTo']
        );
        $this->email_lib->send_email($WelcomeEmail);
        return true;
    }

    private function MobileVerificationSMS($getMobileOTP) {
        $getSMStemp = $this->MailModel->getSMSTemplate(array('id' => 101));
        $text = str_replace(array('{%OTP%}', '{%NOW%}'), array($getMobileOTP['mobile_otp'], date(SYS_DATE_TIME)), $getSMStemp->template);
        $smsArray = array('numbers' => $getMobileOTP['mobile'], 'text' => $text);
        sendSMS($smsArray);
        return true;
    }
    
    public function verifyEmail($uid, $emailToken) {
        $verify = array(
            'user_id' => base64_decode($uid),
            'otp' => $emailToken
        );
        $isVerify = $this->AppUserModel->verifyOTP($verify);
        if ($isVerify) {
            $where = array('id' => base64_decode($uid));
            $update = array('is_email_verify' => 1);
            $this->AppUserModel->updateUsers($where, $update);
        }
        $this->session->set_tempdata('responce', 'Email verify successfully.');
        redirect('default/General/responce');
    }

    public function login() {
        $data = array();
        if (!$this->input->post()) {
            if (!empty($this->session->userdata('ExUserSess'))) {
                redirect('account/Services/MyAccount');
            }
            $this->load->view('External/login', $data);
        } else {
            $this->form_validation->set_rules('loginEmail', 'email', 'trim|xss_clean|required|valid_email');
            $this->form_validation->set_rules('loginPassword', 'password', 'trim|xss_clean|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => validation_errors()));
                redirect($this->agent->referrer());
            } else {
                $email = $this->input->post('loginEmail', true);
                $password = $this->input->post('loginPassword', true);
                $where = array('email' => $email, 'is_delete' => 0);
                $select = array('is_email_verify', 'is_mobile_verify', 'mobile', 'password', 'is_password_auto');
                $getUser = $this->ExternalUserModel->getUserData($where, $select);
                if (!empty($getUser)) {
                    if ($getUser->is_email_verify == 0) {
                        $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Please verify your email before login.'));
                        redirect($this->agent->referrer());
                    }
                    if (password_verify($password, $getUser->password)) {
                        $this->createSession($getUser);
                        $this->UserCartInfo($getUser->id);
                        $keepMe = $this->input->post('keepMe', true);
                        if ($keepMe == 1) {
                            self::createKeepMeCookie(array('email' => $email, 'password' => $password));
                        }
                        if ($getUser->is_mobile_verify == 0) {
                            $session_data = $this->session->userdata('ExUserSess');
                            $session_data['is_mobile_verify'] = FALSE;
                            $this->session->set_userdata("ExUserSess", $session_data);
                        }
                        $page_link = $this->session->userdata('page_link');
                        if(!empty($page_link)){
                            $this->session->unset_userdata('page_link');
                            redirect($page_link);
                        } else {
                            redirect($this->agent->referrer());
                        }
                    } else {
                        $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1002')));
                    }
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1002')));
                }
                redirect($this->agent->referrer());
            }
        }
    }
    
    private function isEmailVerify()
    {
        return true;
    }

    private final function createSession($userData) {
        $exUrSes = array(
            'id' => $userData->id, 'name' => ucwords(strtolower($userData->name)), 'pic' => $userData->profile_pic,
            'email' => $userData->email, 'mobile' => $userData->mobile
        );
        if (empty($userData->is_email_verify) && empty($userData->is_password_auto)) {
            $where = array('user_id' => $userData->id, 'otp_for' => 'E');
            $isExist = $this->ExternalUserModel->verifyOTP($where);
            if (!empty($isExist)) {
                $this->ExternalUserModel->updateUsers(array('id' => $userData->id), array('is_email_verify' => 1));
            }
        }
        $where = array('user_id' => $userData->id, 'is_visible' => 0);
        $notify = $this->ExternalUserModel->NewUserNotify($where);
        $exUrSes['new_notification'] = $notify;
        $this->session->set_userdata('ExUserSess', $exUrSes);
        return true;
    }

    private function createKeepMeCookie(array $auth) {
        $this->load->helper('cookie');
        $cookieEmail = array('name' => 'lEmail', 'value' => $auth['email'], 'expire' => '300', 'secure' => TRUE);
        set_cookie($cookieEmail);
        $cookiePwd = array('name' => 'lPwd', 'value' => $auth['password'], 'expire' => '300', 'secure' => TRUE);
        set_cookie($cookiePwd);
    }

    public function validateMobile() {
        $this->load->view('default/page_not_found');
    }

    public function OTPSend() {
        $email_mobile = $this->input->post('email_mobile', TRUE);
        $isEmail = strpos($email_mobile, '@');
        $isMobile = false;
        if ($isEmail !== false) {
            $this->form_validation->set_rules('email_mobile', 'email', 'trim|xss_clean|required|valid_email');
        } else {
            $this->form_validation->set_rules('email_mobile', 'mobile', 'trim|xss_clean|required|is_natural_no_zero|exact_length[10]');
            $isMobile = true;
        }
        if ($this->form_validation->run() === false) {
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => validation_errors(),
                    'error_code' => 1001,
                    'message' => ''
                ),
                'code' => 200
            );
            exit(json_encode($return));
        } else {
            if ($isMobile) {

                $this->load->model(array('account/ServicesModel', 'connect/SmsService'));
                $userId = $this->ExternalUserModel->getUserData(array('mobile' => $email_mobile), array('id'), true);
                if (empty($userId)) {
                    $return = array(
                        'status' => false,
                        'response' => array(
                            'error' => $this->lang->line('err_1001'),
                            'error_code' => 1002,
                            'message' => ''
                        ),
                        'code' => 200
                    );
                    exit(json_encode($return));
                }
                $getSMStemp = $this->SmsService->getAllSms(array('id' => 108));
                $template = $getSMStemp[0];
                $mobileOTP = rand(10000, 99999);
                $text = str_replace(array('{%OTP%}'), array($mobileOTP), $template->template);
                $smsArray = array('numbers' => $email_mobile, 'text' => $text);
                $isSend = sendSMS($smsArray);
                if ($isSend[0] == true) {
                    $SaveOTP = array();
                    $SaveOTP['user_id'] = $userId->id;
                    $SaveOTP['otp'] = $mobileOTP;
                    $SaveOTP['otp_for'] = 'M';
                    $SaveOTP['otp_for_value'] = $email_mobile;
                    $SaveOTP['otp_title'] = 'LOGIN_WITH_OTP';
                    $SaveOTP['create_at'] = date(SYS_DB_DATE_TIME);
                    $SaveOTP['valid_at'] = date(SYS_DB_DATE_TIME, strtotime('+10min'));
                    $SaveOTP['is_verify'] = 0;
                    $this->ServicesModel->InsertUserOTP($SaveOTP);
                    $return = array(
                        'status' => true,
                        'response' => array(
                            'error' => '',
                            'message' => 'SMS sent'
                        ),
                        'code' => 200
                    );
                    exit(json_encode($return));
                } else {
                    $error = ($isSend[1]['code'] == 51) ? $this->lang->line('err_1015') : $isSend[1]['message'];
                    $return = array(
                        'status' => false,
                        'response' => array(
                            'error_code' => $isSend[1]['code'],
                            'error' => $error,
                            'message' => ''
                        ),
                        'code' => 300
                    );
                    exit(json_encode($return));
                }
            }
        }
    }

    public function ValidateLoginOTP() {
        $this->form_validation->set_rules('email_mobile', 'mobile', 'trim|xss_clean|required|is_natural_no_zero|exact_length[10]');
        $this->form_validation->set_rules('VerifyUserOTP', 'otp', 'trim|xss_clean|required|is_natural_no_zero|exact_length[5]');
        if ($this->form_validation->run() === false) {
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => validation_errors(),
                    'error_code' => 1001,
                    'message' => ''
                ),
                'code' => 200
            );
            exit(json_encode($return));
        } else {
            $email_mobile = $this->input->post('email_mobile', TRUE);
            $VerifyUserOTP = $this->input->post('VerifyUserOTP', TRUE);
            $where = array('otp' => $VerifyUserOTP, 'otp_for_value' => $email_mobile);
            $getOtpUser = $this->ExternalUserModel->getLoginOTP($where);
            if(empty($getOtpUser))
            {
                $return = array(
                    'status' => false,
                    'response' => array(
                        'error' => $this->lang->line('err_1016'),
                        'error_code' => 1003,
                        'message' => ''
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            }
            $to_time = time();
            $from_time = strtotime($getOtpUser->valid_at);
            $timeDiff = round(abs($to_time - $from_time) / 60, 2);
            if ($timeDiff > 10) {
                $return = array(
                    'status' => false,
                    'response' => array(
                        'error' => $this->lang->line('err_1017'),
                        'error_code' => 1002,
                        'message' => ''
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            } else {
                $where = array('id' => $getOtpUser->user_id, 'is_delete' => 0);
                $select = array('is_email_verify', 'is_mobile_verify', 'mobile', 'password');
                $getUser = $this->ExternalUserModel->getUserData($where, $select);
                $this->createSession($getUser);
                $this->ExternalUserModel->UpdateUserOTP(array('id' => $getOtpUser->id), array('is_verify' => 1));
                $return = array(
                    'status' => true,
                    'response' => array(
                        'error' => '',
                        'error_code' => 1002,
                        'message' => $this->lang->line('err_1018')
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            }
        }
    }

    public function forgotPassword() {
        $data = array();
        if (!$this->input->post()) {
            $this->load->view('External/forgot_password', $data);
        } else {
            $email = $this->input->post('email', TRUE);
            $mobile_no = $this->input->post('mobile_no', TRUE);
            $this->form_validation->set_rules('email', 'email', 'trim|xss_clean|required|valid_email');
            $isEmail = false;
            $where = array();
            if (!empty($email)) {
                $isEmail = true;
                $where = array('email' => $email);
            } else {
                $where = array('mobile' => $mobile_no);
                $this->form_validation->set_rules('mobile_no', 'mobile no', 'trim|xss_clean|required|is_natural_no_zero|exact_length[10]');
            }
            if ($this->form_validation->run() === false) {
                $this->load->view('External/forgot_password', $data);
            } else {
                $isSent = array();
                $getUser = $this->ExternalUserModel->getUserDataForPassword($where);
                if (empty($getUser)) {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1019')));
                    redirect('forgot-password');
                }
                $saveTempPassword = array();
                if ($isEmail === true) {
                    $isSent = $this->sendForgotEmail($email, $getUser);
                    if ($isSent[0] == true) {
                        $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Reset password link sent in email.'));
                    }
                    $saveTempPassword['send_at'] = 'email';
                    $saveTempPassword['send_to'] = $email;
                } else {
                    $isSent = $this->sendForgotSMS($mobile_no);
                    $saveTempPassword['send_at'] = 'mobile';
                    $saveTempPassword['send_to'] = $mobile_no;
                }

                $saveTempPassword['send_unique_value'] = $isSent['randStr'];
                $saveTempPassword['user_id'] = $getUser->id;
                $saveTempPassword['created_at'] = date(SYS_DB_DATE_TIME);
                $saveTempPassword['valid_upto'] = date(SYS_DB_DATE_TIME, strtotime('+7days'));
                $saveTempPassword['is_verify'] = 0;
                $this->ExternalUserModel->SaveUserDataForPassword($saveTempPassword);
                redirect('forgot-password');
            }
        }
    }

    private function sendForgotEmail($email, $getUser) {
        $mailTemp = $this->MailModel->getAllTemplate(array('id' => 100015));
        $subject = $mailTemp[0]->title;
        $template = $mailTemp[0]->template;
        $randStr = random_string() . time() . random_string();
        $link = $getUser->id . '_' . $randStr;
        $esecure = esecure($link);
        $resetLink = site_url('users/ExternalUser/SetNewPassword/' . $esecure);
        $newText = str_replace(
                array('{%RESET_LINK%}', '{%THANKS_FROM%}'),
                array($resetLink, EMAIL_THANKS),
                $template
        );
        $WelcomeEmail = array(
            'SUBJECT' => $subject,
            'MESSAGE' => $newText,
            'MAIL_TO' => $email
        );
        $res = $this->email_lib->send_email($WelcomeEmail);
        return [true, 'status' => $res, 'randStr' => $randStr];
    }

    public function SetNewPassword($isString) {
        $this->session->sess_destroy();
        $dsecure = dsecure($isString);
        list($id, $str) = explode('_', $dsecure);
        $where = array(
            'send_unique_value' => $str,
            'user_id' => $id,
            'is_verify' => 0
        );
        $isTepm = $this->ExternalUserModel->GetTempPassword($where);
        if (!empty($isTepm)) {
            $data['user_id'] = $isTepm->user_id;
            $data['reset_type'] = $isTepm->send_at;
        } else {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1020')));
            redirect('forgot-password');
        }
        if (!$this->input->post()) {
            $this->load->view('External/set_new_password', $data);
        } else {
            $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|regex_match[/' . PASSWORD_PATTERN . '/]');
            $this->form_validation->set_rules('confirm', 'confirm password', 'trim|required|xss_clean|matches[password]');
            if($this->form_validation->run() == false){
                $this->load->view('External/set_new_password', $data);
            } else {
                $password = $this->input->post('password',TRUE);
                $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                $isTepm = $this->ExternalUserModel->updateUsers(array('id' => $isTepm->user_id),array('password' => $passwordHash));
                if($isTepm)
                {
                    $this->ExternalUserModel->UpdateTempPassword(array('id' => $isTepm->id), array('is_verify' => 1));
                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'New password saved successful, Please use login.'));
                    redirect('login');
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
                    redirect($this->agent->referrer());
                }
            }
        }
    }

    public function signout() {
        $this->session->sess_destroy();
        redirect('');
    }
    
    public function login_required(){
        $this->session->set_userdata('page_link',$this->agent->referrer());
        redirect('login');
    }
    
    public function UserCartInfo($getUserId)
    {
        $this->load->model('user_cart/CartService');
        $currentCart = $this->CartService->CartCount(array('user_id' => $getUserId));
        $this->session->set_userdata('CartCount', $currentCart);
    }

}