<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait SystemUserTrait
{
    private function createAdminUsers($insert=array()) {
        $result = $this->SystemUsersDB->insertAdminUser($insert);
        if (is_int($result)) {
            return $result;
        } else {
            throw new Exception('Some');
        }
    }

    private function createDir($dir) {
        if (is_dir(UPLOAD_DIR . '/internal/' . $dir)) {
            return false;
        } else {
            $isMake = mkdir(UPLOAD_DIR . '/internal/' . $dir, 0777);
            if ($isMake) {
                index_html(UPLOAD_DIR . '/internal/' . $dir);
                mkdir(UPLOAD_DIR . '/internal/' . $dir . '/image', 0777);
                mkdir(UPLOAD_DIR . '/internal/' . $dir . '/file', 0777);
                mkdir(UPLOAD_DIR . '/internal/' . $dir . '/video', 0777);
                index_html(UPLOAD_DIR . '/internal/' . $dir . '/image');
                index_html(UPLOAD_DIR . '/internal/' . $dir . '/file');
                index_html(UPLOAD_DIR . '/internal/' . $dir . '/video');
            }
            return true;
        }
    }

    private function uploadPic($dir, $isThumb = false) {
        $config['upload_path'] = UPLOAD_DIR . '/internal/' . $dir . '/image';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 1024;
        $config['is_image'] = 1;
        $config['detect_mime'] = TRUE;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('uploadImg')) {
            return $this->upload->display_errors();
        } else {
            $doUpload = array('upload_data' => $this->upload->data());
            $upRes = $doUpload['upload_data']; /* upRes : Upload Responce */
            if ($isThumb === true) {
                $this->load->library('upload_lib');
                $this->upload_lib->createThumbs($upRes['full_path'], $upRes['file_path'] . '130_' . $upRes['raw_name'] . $upRes['file_ext'], 130);
                $this->SystemUsersDB->updateAdminUser(array('id' => $dir), array('media' => $upRes['raw_name'] . $upRes['file_ext']));
            }
            return '';
        }
    }
    
    final function fileWithValidation($picture, $fileExt) {
        $pictures = !empty($picture) ? $picture : 'pictures';
        $upImg = $_FILES['uploadImg'];
        if (empty($upImg['name'])) {
            $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is required.');
            return false;
        } else {
            $realExt = strtolower($upImg['name']);
            $ext = explode('.', $realExt);
            $extension = end($ext);
            $fileExt = explode('|', $fileExt);
            if (!in_array($extension, $fileExt)) {
                $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . $this->lang->line('err_1021'));
                return false;
            }
        }
        return true;
    }

    final function captchaWord($word) {
        $captcha_word = $this->session->tempdata('captcha_word');
        if (empty($word)) {
            $this->form_validation->set_message('captchaWord', $this->load->line('err_1022'));
            return false;
        } elseif ($captcha_word != $word) {
            $this->form_validation->set_message('captchaWord', $this->lang->line('err_1023'));
            return false;
        } else {
            return true;
        }
    }
    
    private function noIdSelected($isUpdate = false) {
        if ($isUpdate === false) {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1024')));
        } else {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1025')));
        }
        redirect($this->agent->referrer());
    }

    private function setResponce($isResponce=array()) {
        if (in_array(0, $isResponce)) {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1026')));
        } else {
            $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Event successfully performed.'));
        }
        redirect($this->agent->referrer());
    }
}
