<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait ExUserAddressNAccount {

    private $updateUser = array();
    private $updateUserFK = array();

    private final function IsLoginUser() {
        $ExUserSess = $this->session->userdata('ExUserSess');
        if (!empty($ExUserSess)) {
            $this->updateUserId = array('id' => $ExUserSess['id']);
            $this->updateUserFK = array('user_id' => $ExUserSess['id']);
            return $ExUserSess;
        } else {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1013')));
            redirect('');
        }
    }

    public function MyAccount($formType = NULL) {
        $isLogin = self::IsLoginUser();
        if ($this->input->post()) {
            switch ($formType) {
                case 'PersonalInfo':
                    $this->updatePersonalInfo();
                    break;
                default :
                    $this->session->set_flashdata('alert', array('color' => 'warning', 'responce' => $this->lang->line('err_1014')));
                    break;
            }
            $this->load->view('External/MyAccount', $data);
        } else {
            $data = array();
            switch ($formType) {
                case 'Personal-Info':
                    $data['personalDetails'] = $this->ExternalUserModel->getUserDetails(array('user_id' => $isLogin['id']));
                    break;
                case 'Address':
                    $data['address'] = $this->ExternalUserModel->getUserAddress(array('user_id' => $isLogin['id']));
                    break;
                default :
                    $data['personalDetails'] = $this->ExternalUserModel->getUserDetails(array('user_id' => $isLogin['id']));
                    break;
            }
            $this->load->view('External/MyAccount', $data);
        }
    }

    private function updatePersonalInfo() {
        $this->form_validation->set_rules('name', 'name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('dob', 'Date of birth', 'trim|xss_clean|required');
        if ($this->form_validation->run() === FALSE) {
            $data['v_errors'] = validation_errors();
            $this->load->view('External/MyAccount', $data);
        } else {
            $name = $this->input->post('name', true);
            $dob = $this->input->post('dob', true);
            $dob = date('Y-m-d', strtotime($dob));
            $update = array('name' => $name);
            $isUpdateUser = $this->ExternalUserModel->updateUsers($this->updateUserId, $update);
            $isUpdateUsrDtls = $this->ExternalUserModel->updatePersonalDetails($this->updateUserFK, array('dob' => $dob));
            if ($isUpdateUser OR $isUpdateUsrDtls) {
                $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Info has been successfully updated.'));
            } else {
                $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
            }
            redirect($this->agent->referrer());
        }
    }

}
