<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AppUsers extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('AppUserModel'));
        $this->load->helper('string');
        $this->load->library(array('email_lib'));
        is_web_view();
    }

    public function registration() {
        if (!$this->input->post()) {
            $this->load->view('External/registration');
        } else {
            $this->form_validation->set_rules('uname', 'name', 'trim|xss_clean|required');
            $this->form_validation->set_rules('email', 'email', 'trim|xss_clean|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('mobile', 'mobile', 'required|trim|xss_clean|exact_length[10]|regex_match[/' . FOR_REG_MOBILE . '/]');
           if ($this->form_validation->run() == FALSE) {
                $this->load->view('External/registration');
            } else {
                $uname = $this->input->post('uname',true);
                $email = $this->input->post('email',true);
                $mobile = $this->input->post('mobile',true);
                $password = random_string('alnum', 8);
                $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                $insert = array(
                    'name' => $uname,
                    'username' => NULL,
                    'email' => strtolower($email),
                    'is_email_verify' => 0,
                    'password' => $passwordHash,
                    'mobile' => $mobile,
                    'is_mobile_verify' => 0,
                    'create_at' => date(SYS_DB_DATE_TIME),
                    'user_agent' => $this->input->ip_address(),
                    'is_active' => 1,
                    'is_delete' => 0,
                    'account_by' => 'SYSTEM'
                );
                $userInfo = array('name' => $uname,'password' => $password, 'emailTo' => strtolower($email));
                $isSave = $this->createUsers($insert);
                if(!empty($isSave))
                {
                    self::sendEmailAndSMS($isSave,$userInfo);
                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Congratulation : Check your email for more details.'));
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
                }
                redirect($this->agent->referrer());
            }
        }
    }
    
    private function createUsers(array $insert=array())
    {
        $isSave = $this->AppUserModel->createUser($insert);
        if($isSave)
        {
            $emailOTP = random_string('alnum', '32');
            $mobileOTP = rand(10000, 99999);
            $insertMobileOTP = array(
                'user_id' => $isSave,
                'otp' => $mobileOTP,
                'create_at' => date(SYS_DB_DATE_TIME),
                'valid_at' => date(SYS_DB_DATE_TIME,  strtotime('+2 days')),
                'is_verify' => 0
            );
            $insertEmailOTP = array(
                'user_id' => $isSave,
                'otp' => $emailOTP,
                'create_at' => date(SYS_DB_DATE_TIME),
                'valid_at' => date(SYS_DB_DATE_TIME,  strtotime('+7 days')),
                'is_verify' => 0
            );
            $isMobile = $this->AppUserModel->saveOTP($insertMobileOTP);
            $isEmail = $this->AppUserModel->saveOTP($insertEmailOTP);
            $return['userId'] = $isSave;
            if($isMobile)
            {    
                $return['mobile'] = $mobileOTP;
            } 
            if($isEmail) {
                $return['email'] = $emailOTP;
            }
            return $return;
        }
    }
    
    private function sendEmailAndSMS($isSave,$userInfo)
    {
        $this->load->model('connect/MailModel');
        $mailTemp = $this->MailModel->getAllTemplate(array('id'=>100016));
        $subject = $mailTemp[0]->title;
        $template = $mailTemp[0]->template;
        $newText = str_replace(array('{%NAME%}','{%EMAIL%}','{%PASSWORD%}'),array($userInfo['name'],$userInfo['email'],$userInfo['password']),$template);
        $WelcomeEmail = array(
            'SUBJECT' => $subject,
            'MESSAGE' => $newText,
            'MAIL_TO'=> $userInfo['emailTo']
        );
        $mailInfo = $this->MailModel->getAllTemplate(array('id'=>100017));
        $text = str_replace(
                array('{%NAME%}','{%EMAIL_LINK%}','{%THANKS_FROM%}'), 
                array($userInfo['name'],  site_url('users/AppUsers/verifyEmail/'.  base64_encode($isSave['userId']).'/'.$isSave['email']), EMAIL_THANKS), 
                $mailInfo[0]->template);
        $InfoEmail = array(
            'SUBJECT' => $mailInfo[0]->title,
            'MESSAGE' => $text,
            'MAIL_TO'=> $userInfo['emailTo']
        );
        $this->email_lib->send_email($WelcomeEmail);
        $this->email_lib->send_email($InfoEmail);
        return true;
    }
    
    public function verifyEmail($uid,$emailToken)
    {
        $verify = array(
            'user_id' => base64_decode($uid),
            'otp' => $emailToken
        );
        $isVerify = $this->AppUserModel->verifyOTP($verify);
        if($isVerify)
        {
            $where = array('id' => base64_decode($uid));
            $update = array('is_email_verify' => 1);
            $this->AppUserModel->updateUsers($where,$update);
        }
        $this->session->set_tempdata('responce', 'Email verify successfully.');
        redirect('default/General/responce');
    }

}
