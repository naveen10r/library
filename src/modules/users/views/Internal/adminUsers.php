<?php ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-wysihtml5.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>

        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu(); ?>
        <!--sidebar-menu-->
        <!--close-left-menu-stats-sidebar-->

        <div id="content">
            <div id="content-header">
                <div id="breadcrumb"> 
                    <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
                    <a href="#" class="tip-bottom">Form elements</a> 
                    <a href="#" class="current">Common elements</a> 
                </div>
            </div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="span10">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                <h5>Add Admin/Staff Member</h5>
                                <?php echo MANDATORY; ?>
                            </div>
                            <div class="widget-content nopadding">
                                <?php echo form_open_multipart('users/SystemUsers/newInternalUser', array('class' => 'form-horizontal', 'id'=>'addAdmin')); ?>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <div class="control-group">
                                    <label class="control-label">Username <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="For username field must be between 6 to 30 characters in length and {1} UPPERCASE, {1} lowercase and one {0-9} numeric characters in set. Special character not allowed."><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="inputError" class="span11" name="username" value="<?php echo set_value('username'); ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('username'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Name <sup class="red_error">&#042;</sup>
                                    <a class="tip-top" data-original-title="Alphabets and space allowed only."><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="inputError" class="span11" name="name" value="<?php echo set_value('name'); ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('name'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Email <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Unique email"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="email" id="inputError" class="span11" name="email" value="<?php echo set_value('email'); ?>" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('email'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Mobile <sup class="red_error">&#042;</sup>
                                    <a class="tip-top" data-original-title="10 digit numeric only."><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="mobile" id="inputError" class="span11" name="mobile" value="<?php echo set_value('mobile'); ?>" maxlength="10" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('mobile'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Pictures <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="PNG|JPG|JPEG Only"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="file" name="uploadImg" required="true" class="span11" />
                                        <span class="help-inline red_error"><?php echo form_error('uploadImg'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Captcha <sup class="red_error">&#042;</sup>
                                    </label>
                                    <div class="controls">
                                        <div class="span2">
                                            <img src="<?php echo base_url('captcha/'.$captcha['filename']); ?>" />
                                        </div>
                                        <div class="span1"><a href="javascript:void(0)" onclick="document.getElementById('addAdmin').submit();"><i class="icon-repeat"></i></a></div>
                                        <input type="text" name="captchaWord" class="span3" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('captchaWord'); ?></span> 
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success"><i class="icon-save"></i> Save</button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 2013 &copy; Matrix Admin.</div>
        </div>
        <!--end-Footer-part--> 
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap-wysihtml5.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/matrix.interface.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.popover.js'); ?>"></script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </body>
</html>