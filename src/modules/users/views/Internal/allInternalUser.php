<?php ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <meta charset="UTF-8" />
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/select2.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu(); ?>
        <!--sidebar-menu-->
        <div id="content">
            <div id="content-header">
                <div id="breadcrumb"> 
                    <a href="#" class="tip-bottom"><i class="icon-home"></i> Home</a> 
                    <a href="#" class="current">Tables</a> 
                </div>
            </div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="span11">
                        <div class="widget-box">
                            <div class="widget-title">
                                <span class="icon">
                                    <input type="checkbox" id="title-checkbox" name="title-checkbox" />
                                </span>
                                <h5><?php echo 'All Internal Users'; ?></h5>
                            </div>
                            <div class="widget-content nopadding">
                                <?php
                                echo form_open('#', array('id' => 'changeStatus', 'method' => 'post'));
                                echo input_csrf();
                                ?>
                                <table class="table table-bordered data-table">
                                    <thead>
                                        <tr>
                                            <th><i class="icon-resize-vertical"></i></th>
                                            <th><?php echo 'Picture'; ?></th>
                                            <th><?php echo 'Name'; ?></th>
                                            <th><?php echo 'Email'; ?></th>
                                            <th><?php echo 'Mobile'; ?></th>
                                            <th><?php echo 'Next Step'; ?></th>
                                            <th><?php echo 'Status'; ?></th>
                                            <th width='7%'><center><?php echo 'Action'; ?></center></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($internalUsers as $iu):
                                            $i++;
                                            $class = '';
                                            if ($i == 1) {
                                                $class = 'gradeX';
                                            } elseif ($i == 2) {
                                                $class = 'gradeC';
                                            } else {
                                                $class = 'gradeA';
                                            }
                                            ?>
                                            <tr <?php echo 'class="' . $class . '"'; ?>>
                                                <td width="4%"><input type="checkbox" name="checked[]" value="<?php echo $iu->id; ?>" />
                                                </td>
                                                <td width="6%">
                                        <center>
                                            <?php
                                            if (file_exists(UPLOAD_DIR . 'internal/' . $iu->id . '/image/' . $iu->media)) {
                                                echo img(array('src' => UPLOAD_URL . 'internal/' . $iu->id . '/image/' . $iu->media, 'alt' => 'alt information'));
                                            } else {
                                                echo img(array('src' => 'web-inf/img/icons/32/user.png', 'alt' => 'alt information'));
                                            }
                                            ?>
                                        </center>
                                        </td>
                                        <td width="20%"><?php echo $uname = ucwords($iu->name); ?></td>
                                        <td width="20%"><?php echo hide_email($iu->email); ?></td>
                                        <td width="10%"><?php echo hide_mobile($iu->mobile); ?></td>
                                        <td width="15%"><?php
                                            if (DB_INTERNAL_USER_FORM_STEP == $iu->form_step) :
                                                echo 'Done';
                                            elseif (!empty(DB_INT_USER_NXT_FORM_STEP[$iu->form_step])) :
                                                echo '<strong><a href="javascript:void(0)" class="menuModal" data-toggle="modal" data-target="#menuModal" data-id="' . $uname. '_' . esecure($iu->id) . '">' . ucwords(DB_INT_USER_NXT_FORM_STEP[$iu->form_step]) . '</a></strong>';
                                            endif;
                                            ?></td>
                                        <td class="taskStatus" width="10%">
                                            <?php echo empty($iu->is_status) ? '<span class="pending">Deactive</span>' : '<span class="done">Active</span>'; ?>
                                        </td>
                                        <td class="center" width="15%">
                                            <?php
                                            if (auth_uri_menu('users/SystemUsers/ResetPassword')) :
                                                echo '<a href="javascript:void(0)" class="btn btn-inverse btn-mini resetPwdModalC" data-toggle="modal" data-target="#resetPwdModal" data-id="' . esecure($iu->id) . '"><i class="icon-pencil"></i>Reset Passwprd</a>';
                                            else :
                                                echo '<a href="javascript:void(0);" class="btn btn-inverse btn-mini" title=""><i class="icon-pencil"></i>Edit</a>';
                                            endif;
                                            ?>
                                        </td>
                                        </tr>
                                    <?php endforeach; ?> 
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="8">
                                                <?php if (auth_uri_menu('users/SystemUsers/changeStatus/active')) { ?><button type="button" class="btn btn-success btn-mini" onclick="document.getElementById('changeStatus').action = '<?php echo base_url('users/SystemUsers/changeStatus/active'); ?>'; document.getElementById('changeStatus').submit(); return false;"><i class="icon-eye-open"></i> <?php echo 'Active'; ?></button>
                                                <?php } if (auth_uri_menu('users/SystemUsers/changeStatus/deactive')) { ?><button type="button" class="btn btn-default btn-mini" onclick="document.getElementById('changeStatus').action = '<?php echo base_url('users/SystemUsers/changeStatus/deactive'); ?>'; document.getElementById('changeStatus').submit(); return false;"><i class="icon-eye-close"></i> <?php echo 'Deactive'; ?></button>
                                                <?php } if (auth_uri_menu('users/SystemUsers/changeStatus/delete')) { ?><button type="button" class="btn btn-danger btn-mini" onclick="document.getElementById('changeStatus').action = '<?php echo base_url('users/SystemUsers/changeStatus/delete'); ?>'; document.getElementById('changeStatus').submit(); return false;"><i class="icon-trash"></i> <?php echo 'Delete'; ?></button> <?php } ?>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal For Reset Password -->
        <div class="modal fade" id="resetPwdModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <?php
                    echo form_open(base_url('users/SystemUsers/ResetPassword'), array('id' => 'resetPwdForm', 'method' => 'post'));
                    echo input_csrf();
                    ?>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Reset Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="control-label">Password <sup class="red_error">&#042;</sup>
                            </label>
                            <div class="controls">
                                <input type="text" id="inputError" class="span4" name="password" value="" required="true" pattern="<?php echo PASSWORD_PATTERN; ?>" />
                                <span class="help-inline red_error"><?php echo form_error('password'); ?></span> 
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Retype Password <sup class="red_error">&#042;</sup>
                                <a class="tip-right" data-original-title="For password field must be between 6 to 30 characters in length. At-leaset one alphabet and one numeric charectors in set."><i class="icon-info-sign"></i></a>
                            </label>
                            <div class="controls">
                                <input type="text" id="inputError" class="span4" name="retypePassword" value="" required="true" pattern="<?php echo PASSWORD_PATTERN; ?>" />
                                <span class="help-inline red_error"><?php echo form_error('retypePassword'); ?></span> 
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save change</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <!-- Modal for menu -->
        <div id="menuModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Set User Menu for <span id="username"></span></h4>
                    </div>
                    <?php
                    echo form_open(base_url('users/SystemUsers/SetMenus'), array('id' => 'setMenuForm', 'method' => 'post'));
                    echo input_csrf();
                    ?>
                    <input type="hidden" name="userId" value="" id="userId" />
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <?php echo form_button(array('type'=>'submit','class'=>'btn btn-default', 'content'=>'Submit')); ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>

            </div>
        </div>
        <!--Footer-part-->
        <?php echo admin_footer(); ?>
        <!--end-Footer-part-->
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/select2.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.dataTables.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.tables.js'); ?>"></script>
        <script type="text/javascript">
            $('.resetPwdModalC').on('click', function () {
                var formAct = $('#resetPwdForm').attr('action');
                $('#resetPwdForm').attr('action', formAct + '/' + $(this).data('id'));
            });
            
            $('.menuModal').on('click',function(){
                var menuModalId = $(this).data('id');
                var nameNId = menuModalId.split('_');
                $('#username').html(nameNId[0]);
                $('#userId').val(nameNId[0]);
            });
        </script>
    </body>
</html>