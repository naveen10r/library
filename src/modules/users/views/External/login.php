<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . 'Login' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />

        <script src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="<?php echo base_url('web_view/catalog/view/'); ?>javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/stylesheet.css" rel="stylesheet" />

        <!-- Codezeel - Start -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>javascript/jquery/magnific/magnific-popup.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/custom.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/lightbox.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/animate.css" />
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/custom.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/jstree.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/codezeel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/jquery.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/jquery.formalize.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/lightbox/lightbox-2.6.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/tabs.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/jquery.elevatezoom.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/bootstrap-notify.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/doubletaptogo.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
        <!-- Codezeel - End -->

        <script src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/common.js" type="text/javascript"></script>

    </head>
    <body class="account-register layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>
        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>

        <!-- ======= Quick view JS ========= -->
        <script>

            function quickbox() {
                if ($(window).width() > 767) {
                    $('.quickview-button').magnificPopup({
                        type: 'iframe',
                        delegate: 'a',
                        preloader: true,
                        tLoading: 'Loading image #%curr%...',
                    });
                }
            }
            jQuery(document).ready(function () {
                quickbox();
            });
            jQuery(window).resize(function () {
                quickbox();
            });

        </script>
        <style type="text/css">
            .validation_error{ color:#FF0000; }
        </style>
        <div id="account-register" class="container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Account</a></li>
                <li><a href="#">Login</a></li>
            </ul>
            <div class="row">
                <aside id="column-left" class="col-sm-3 hidden-xs">
                    <?php account_service('login') ?>
                    <?php information(); ?>
                </aside>

                <div id="content" class="col-sm-9">
				
				<div class="col-sm-12">
				 
					<div class="container" style="border: 1px solid #cccccc; padding-top: 6%;">

    <div class="row">
				<?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?>
      <div class="vl">
        <span class="vl-innertext">OR</span>
      </div>

      <div class="col">
        <a href="#" class="fb social_btn">
          <i class="fa fa-facebook fa-fw"></i> Login with Facebook
        </a>
        <a href="#" class="twitter social_btn">
          <i class="fa fa-twitter fa-fw"></i> Login with Twitter
        </a>
        <a href="#" class="google social_btn">
          <i class="fa fa-google fa-fw"></i> Login with Google+
        </a>
      </div>

      <div class="col">
	   <?php  echo form_open('users/ExternalUser/login', array('method' => 'post', 'name' => 'loginfrm','id' => 'loginfrm','class' => 'form-horizontal'));  ?>
        <div class="hide-md-lg">
          <p>Or sign in manually:</p>
        </div>
		<div class="form-group required">
			<input type="email" name="loginEmail" value="<?php echo set_value('email'); ?>" placeholder="E-Mail/Username" id="input-email" class="form-control" />
			<?php echo form_error('loginEmail', '<div class="validation_error">', '</div>'); ?>
        </div>
		<div class="form-group required">
			<input type="password" autocomplete="off" name="loginPassword" value="<?php echo set_value('password'); ?>" placeholder="Password" id="input-password" class="form-control" />
            <?php echo form_error('loginPassword', '<div class="validation_error">', '</div>'); ?>
        </div>
						
        <input type="submit" class="btn btn-primary login_btn" value="Login">
		<div class="">
			<span>Don't have password ? <a href="javascript:void(0)" data-toggle="modal" data-target="#VerifyOTP"><b>Try another way</b></a></span>

		</div>
		<?php echo form_close(); ?>
      </div>

    </div>
  <div >
  <div class="row" style="padding-top: 6%;">
    <div class="col" style="    width: 100%;    text-align: center;      background-color: #eef6fd;    padding: 5px;  border-top: 1px solid #cccccc;">
      <a style="color:#359be8" href="<?php echo site_url('users/ExternalUser/Register'); ?>"><strong> Sign up </strong></a> | 
	  <a style="color:#359be8" href="<?php echo site_url("forgot-password"); ?>"><strong>Forgot Password</strong></a>
    </div>
  </div>
</div>
</div>


				</div>
				
                </div>
            </div>
        </div>
        <?php footer_tag(); ?>
    </body>
    <!-- Modal -->
    <div class="modal fade" id="VerifyOTP" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:#FFFFFF; font-weight: bold;">Login with OTP</h5>
                </div>
                <div class="modal-body">
                    <div id="responseId"></div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Mobile No</label>
                        <div class="col-sm-5">
                            <input type="text" autocomplete="off" value="" class="form-control" id="email_mobile" maxlength="10" minlength="10" required="required" />
                        </div>
                        <div class="col-sm-4">
                            <a href="javascript:void(0)" id="resendOTP">Send OTP</a>
                        </div>
                    </div>
                    <div class="form-group row" id="verifyInput">
                        <label class="col-sm-3 control-label">OTP </label>
                        <div class="col-sm-5">
                            <input type="text" autocomplete="off" class="form-control" id="VerifyUserOTP" minlength="5" maxlength="5" required="required" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeVerify">Close</button>
                    <button type="button" class="btn btn-primary" id="SubmitOTP">Verify</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#resendOTP').click(function(){
            $.ajax({
                url : '<?php echo site_url("users/ExternalUser/OTPSend"); ?>',
                method : 'POST',
                data : {'email_mobile':$('#email_mobile').val()},
                success : function(OTP_responce){
                    var jsonData = JSON.parse(OTP_responce);
                    var alert_flag = '';
                    var alert_res = '';
                    if (jsonData.status === true)
                    {
                        alert_flag = 'alert-info';
                        alert_res = jsonData.response.message;
                        $('#email_mobile').prop("readonly", true);
                    } else {
                        alert_flag = 'alert-danger';
                        alert_res = jsonData.response.error;
                    }
                    $('#responseId').html('<div class="alert '+alert_flag+'">' + alert_res + '</div>');
                }
            });
        });
        
        $('#SubmitOTP').click(function(){
            var email_mobile = $('#email_mobile').val();
            var VerifyUserOTP = $('#VerifyUserOTP').val();
            $.ajax({
                url : '<?php echo site_url("users/ExternalUser/ValidateLoginOTP"); ?>',
                method : 'POST',
                data : {'email_mobile':email_mobile, 'VerifyUserOTP' :VerifyUserOTP},
                success : function(OTP_responce){
                    var jsonData = JSON.parse(OTP_responce);
                    if (jsonData.status === true)
                    {
                        setTimeout(function() {
                            $('#responseId').html('<div class="alert alert-success">' + jsonData.response.message + '</div>');
                        }, 3000);
                        location.href = "<?php echo site_url('account/Services/MyAccount'); ?>";
                    } else {
                        $('#responseId').html('<div class="alert alert-danger">' + jsonData.response.error + '</div>');
                    }
                }
            });
        });
		$('#loginfrm').submit(function () {
			var inputemail = $.trim($('#input-email').val()); 
			var inputpassword = $.trim($('#input-password').val()); 
			$('#input-email').css("border","1px solid #e5e5e5");
			$('#input-password').css("border","1px solid #e5e5e5");
			var errorflag=0;
			if (inputemail  === '') {
				$('#input-email').css("border","1px solid red");
				errorflag=1;
			}
			if (inputpassword  === '') {
				$('#input-password').css("border","1px solid red");
				errorflag=1;
			}
			if (errorflag  === 1) {
				return false;
			}
		});
    </script>
</html> 