<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Register Account</title>
        <base  />

        <script src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="<?php echo base_url('web_view/catalog/view/'); ?>javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/stylesheet.css" rel="stylesheet" />

        <!-- Codezeel - Start -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>javascript/jquery/magnific/magnific-popup.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/custom.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/lightbox.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('web_view/catalog/view/'); ?>theme/PublicHub/stylesheet/codezeel/animate.css" />
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/custom.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/jstree.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/codezeel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/jquery.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/jquery.formalize.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/lightbox/lightbox-2.6.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/tabs.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/jquery.elevatezoom.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/bootstrap-notify.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/doubletaptogo.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/codezeel/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
        <!-- Codezeel - End -->

        <script src="<?php echo base_url('web_view/catalog/view/'); ?>javascript/common.js" type="text/javascript"></script>
    </head>
    <body class="account-register layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>
        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>

        <!-- ======= Quick view JS ========= -->
        <script>

            function quickbox() {
                if ($(window).width() > 767) {
                    $('.quickview-button').magnificPopup({
                        type: 'iframe',
                        delegate: 'a',
                        preloader: true,
                        tLoading: 'Loading image #%curr%...',
                    });
                }
            }
            jQuery(document).ready(function () {
                quickbox();
            });
            jQuery(window).resize(function () {
                quickbox();
            });

        </script>
        <style type="text/css">
            .validation_error{ color:#FF0000; }
        </style>
        <div id="account-register" class="container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Account</a></li>
                <li><a href="#">Forgot Password</a></li>
            </ul>
            <div class="row">
                <aside id="column-left" class="col-sm-3 hidden-xs">
                    <?php account_service('forgot-password') ?>
                    <?php information(); ?>
                </aside>

                <div id="content" class="col-sm-9">
                    <?php
                    echo form_open('forgot-password', array('method' => 'post', 'class' => 'form-horizontal'));
                    ?>
                    <fieldset id="account">
                        <legend>Forgot Password</legend>
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">E-Mail</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control" />
                                <?php echo form_error('email', '<div class="validation_error">', '</div>'); ?>
                            </div>
                        </div>
                        <!-- 
                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10"><strong>OR</strong></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mobile No</label>
                            <div class="col-sm-10">
                                <input type="text" autocomplete="off" name="mobile_no" value="<?php echo set_value('mobile_no'); ?>" class="form-control" />
                                <?php echo form_error('mobile_no', '<div class="validation_error">', '</div>'); ?>
                            </div>
                        </div>
                        -->
                    </fieldset>
                    <div class="buttons">
                        <div class="pull-right">
                            <input type="submit" value="Get New Password" class="btn btn-primary" />
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <?php footer_tag(); ?>
    </body>
    <script type="text/javascript">
        
    </script>
</html>