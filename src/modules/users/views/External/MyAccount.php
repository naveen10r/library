<?php
/*
  $lEmail = !empty(get_cookie('lEmail')) ? get_cookie('lEmail') : '';
  $lPwd = !empty(get_cookie('lPwd')) ? get_cookie('lPwd') : '';
 */
$linkOf = $this->uri->segment(4);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>My Account : Manage Address</title>
        <link href="<?php echo base_url('web-www/css/bootstrap.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/font-awesome.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/prettyPhoto.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/price-range.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/animate.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/main.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/responsive.css'); ?>" rel="stylesheet" />
        <!--[if lt IE 9]>
        <script src="<?php echo base_url('web-www/js/html5shiv.js'); ?>"></script>
        <script src="<?php echo base_url('web-www/js/respond.min.js'); ?>"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="<?php echo base_url('web-www/images/ico/favicon.ico'); ?>" />
    </head>
    <!--/head-->
    <body>
        <header id="header"><!--header-->
            <!--header_top-->
            <?php echo header_top(); ?>
            <!--/header_top-->
            <!--header-middle-->
            <?php echo header_middle(); ?>
            <!--header-bottom-->
            <?php echo header_bottom(); ?>
            <!--/header-bottom-->
        </header><!--/header-->
        <section id="form"><!--form-->
            <div class="container">
                <div class="row">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="personal personal-detail">
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <img src="<?php echo site_url('web-www/images/img/human_default.png') ?>" class="personal-img">
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <p>Hi Naveen</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="personal-detail">
                            <div class="col-md-2 col-xs-2">
                                <h4><i class="fa fa-shopping-cart"></i></h4>
                            </div>
                            <div class="col-md-10 col-xs-10">
                                <div class="row">
                                    <h4><strong>MY ORDERS</strong></h4>
                                </div>
                            </div>
                            <div class="col-md-12 " style="border-top: 1px solid #fff;">
                            </div>
                            <div class="col-md-2 col-xs-2">
                                <h4><i class="fa fa-user"></i></h4>
                            </div>
                            <div class="col-md-10 col-xs-10">
                                <div class="row">
                                    <h4><strong>ACCOUNT SETTINGS</strong></h4>
                                    <?php account_setting($linkOf); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-9 col-xs-12">
                        <?php if (empty($linkOf) OR $linkOf == 'PersonalInfo'): ?>
                            <div class="personal-detail">
                                <div class="signup-form">
                                    <!--sign up form-->
                                    <h4>Personal Information</h4>
                                    <?php
                                    echo form_open('users/ExternalUser/MyAccount/PersonalInfo', array('method' => 'POST'));
                                    echo input_csrf();
                                    ?>
                                    <input type="text" placeholder="Name" name="name" value="<?php echo ucwords(strtolower($personalDetails->name)); ?>" />
                                    <input type="text" placeholder="Email Address" value="<?php echo $personalDetails->email; ?>" disabled="disabled" />
                                    <input type="text" placeholder="Mobile No" value="<?php echo $personalDetails->mobile; ?>" disabled="disabled" />
                                    <input type="text" placeholder="Date of birth" name="dob" value="<?php echo nice_date($personalDetails->dob, SYS_DATE); ?>" required="required" readonly="readonly" id="datepicker" />
                                    <button type="submit" class="btn btn-default">UPDATE</button>
                                    <?php echo form_close(); ?>
                                </div>
                            </div><!--/sign up form-->
                        <?php elseif ($linkOf == 'Address') : ?>
                            <div class="row">
                                <div class="personal-detail"><div class="signup-form"><h4>Manage Address</h4></div></div>

                                <?php
                                if (!empty($address)) {
                                    foreach ($address as $ads) {
                                        $ad = '';
                                        $ad .= '<div class="col-md-4">';
                                        $ad .= '<p>' . $ads->address_as . '</p>';
                                        $ad .= '</div>';
                                        echo $ad;
                                    }
                                }
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/form-->
    <!-- Footer -->
    <footer id="footer">
        <?php
        echo footer_up();
        echo footer_menu();
        echo footer_contact_us();
        ?>
    </footer>
    <!-- /Footer -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url('web-www/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('web-www/js/jquery.scrollUp.min.js'); ?>"></script>
    <script src="<?php echo base_url('web-www/js/price-range.js'); ?>"></script>
    <script src="<?php echo base_url('web-www/js/jquery.prettyPhoto.js'); ?>"></script>
    <script src="<?php echo base_url('web-www/js/main.js'); ?>"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript">
        function InvalidMsg(typeOf, textbox) {
            var responce = '';
            switch (typeOf)
            {
                case 'mobile' :
                    responce = 'Not a valid mobile no.';
                    break;
                case 'uname' :
                    responce = 'Only alphabets and space are allowed.';
                    break;
                default:
                    responce = '';
                    break;
            }
            if (textbox.validity.patternMismatch) {
                textbox.setCustomValidity(responce);
            } else {
                textbox.setCustomValidity('');
            }
            return true;
        }
        $(function () {
            $("#datepicker").datepicker({changeMonth: true, changeYear: true, maxDate: '-12M', "dateFormat": 'dd-M-yy'});
        });
    </script>
</body>
</html>