<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AppUserModel extends TableName {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function createUser(array $insert=array())
    {
        $this->db->insert($this->users,$insert);
        return $this->db->insert_id();
    }
            
    public function saveOTP(array $insert=array())
    {
        $this->db->insert($this->usersOtp,$insert);
        return $this->db->insert_id();
    }
    
    public function verifyOTP(array $where)
    {
        $where['valid_at <='] = date(SYS_DB_DATE_TIME);
        return $this->db->from($this->usersOtp)
                ->where($where)
                ->get()->num_rows();
    }
    
    public function updateUsers(array $where,array $update)
    {
        $this->set_update_log($this->users, $where, $update);
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->users,$update);
        return $this->trans_update_data();
    }
}