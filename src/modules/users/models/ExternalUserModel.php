<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ExternalUserModel extends TableName
{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function createUser(array $insert=array())
    {
        $this->db->trans_start();
        $this->db->insert($this->users,$insert);
        return $this->trans_insert_data();
    }
    
    public function getUserOTP(array $where=array())
    {
        $this->db->select('usr.mobile, uo.otp');
        $this->db->from($this->users.' AS usr');
        $this->db->join($this->usersOtp.' AS uo','uo.user_id = usr.id');
        $this->db->where($where);
        return $this->db->get()->row();
    }
    
    public function EmailUserOTP(array $where)
    {
        $this->db->select('id');
        $this->db->from($this->usersOtp);
        $this->db->where($where);
        return $this->db->get()->row();
    }
    
    public function UpdateUserOTP(array $where, array $update)
    {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->usersOtp, $update);
        return $this->trans_update_data();
    }
    
    public function getUserData(array $where=array(), array $select=array())
    {
        $where['is_delete'] = 0;
        $this->db->select($select);
        $this->db->select('id, email, is_active, name, profile_pic');
        $this->db->from($this->users);
        $this->db->where($where);
        return $this->db->get()->row();
    }

    public function verifyOTP(array $where)
    {
        $where['valid_at >='] = date(SYS_DB_DATE_TIME);
        $where['is_verify'] = 0;
        return $this->db->from($this->usersOtp)
                ->where($where)
                ->get()->num_rows();
    }
    
    public function updateUsers(array $where,array $update)
    {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->users,$update);
        return $this->trans_update_data();
    }
    
    public function getUserDetails(array $where=array(), array $select=array())
    {
        $this->db->select($select);
        $this->db->select('usr.id AS user_id, upd.gender, upd.dob, usr.profile_pic, usr.name, usr.email, usr.mobile');
        $this->db->from($this->usersPersonalDetails.' AS upd');
        $this->db->join($this->users.' AS usr','usr.id = upd.user_id');
        $this->db->where($where);
        return $this->db->get()->row();
    }
    
    public function updatePersonalDetails(array $where,array $update)
    {
        $this->set_update_log($this->usersPersonalDetails, $where, $update);
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->usersPersonalDetails,$update);
        return $this->trans_update_data();
    }
    
    public function getUserAddress(array $where=array(), array $select=array())
    {
        $where['is_delete'] = 0;
        $this->db->select($select);
        $this->db->select('ads.address_as, ads.address, ads.pin_code, ads.locality, ads.landmark');
        $this->db->select('cty.city_name, cty.city_id, sts.state_id, sts.state_name, sts.short_name, ctr.country_name');
        $this->db->from($this->userAddress.' AS ads');
        $this->db->join($this->masterCity.' AS cty','cty.city_id = ads.city_id');
        $this->db->join($this->masterState.' AS sts','sts.state_id = cty.state_id');
        $this->db->join($this->masterCountry.' AS ctr','ctr.country_id = sts.country_id');
        $this->db->where($where);
        return $this->db->get()->result();
    }
    
    public function getLoginOTP(array $where)
    {
        $where['otp_for'] = 'M';
        $where['otp_title'] = 'LOGIN_WITH_OTP';
        $where['is_verify'] = 0;
        $this->db->select('id, user_id, create_at, valid_at');
        $this->db->from($this->usersOtp);
        $this->db->where($where);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    public function getUserDataForPassword($where)
    {
        $where['is_active'] = 1;
        $where['is_delete'] = 0;
        $this->db->select('id, name');
        $this->db->from($this->users);
        $this->db->where($where);
        return $this->db->get()->row();
    }
    
    public function SaveUserDataForPassword($insert)
    {
        $this->db->trans_start();
        $this->db->insert($this->usersTempPassword,$insert);
        return $this->trans_insert_data();
    }
    
    public function GetTempPassword($where)
    {
        $this->db->select('id, send_at, valid_upto, user_id');
        $this->db->from($this->usersTempPassword);
        $this->db->where($where);
        return $this->db->get()->row();
    }
    
    public function UpdateTempPassword(array $where, array $update)
    {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->usersTempPassword,$update);
        return $this->trans_update_data();
    }
    
    public function InsertUserOTP(array $insert=array())
    {
        $this->db->trans_start();
        $this->db->insert($this->usersOtp,$insert);
        return $this->trans_insert_data();
    }
    
    
    public function NewUserNotify($where)
    {
        $this->db->where($where);
        return $this->db->count_all_results($this->usersNotify);
    }
}
