<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CartService extends TableName {

    public function __construct() {
        parent::__construct();
    }
    
    public function GetProduct(array $where, array $select=array())
    {
        $where['is_delete'] = 0;
        /* $where['expair_at >'] = date(SYS_DB_DATE, strtotime('+1Year')); */
        $this->db->select($select);
        $this->db->from($this->user_cart);
        $this->db->where($where);
        return $this->db->get()->result();
    }
    
    public function UpdateCart(array $where, array $update){
        $where['is_delete'] = 0;
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->user_cart, $update);
        return $this->trans_update_data();
    }
    
    public function CartCount(array $where){
        unset($where['product_id']);
        $where['is_delete'] = 0;
        $this->db->where($where);
        return $this->db->count_all_results($this->user_cart);
    }
    
    public function AddToCart(array $insert) : int
    {
        $this->db->trans_start();
        $this->db->insert($this->user_cart, $insert);
        return $this->trans_insert_data();
    }
    
    public function GetProductDetaisByCart(array $where, array $select=array(), $start=0){
        $where['uc.is_delete'] = 0;
        $this->db->select($select);
        $this->db->from($this->user_cart.' AS uc');
        $this->db->join($this->product.' pd', 'pd.id = uc.product_id');
        $this->db->where($where);
        $this->db->order_by('id','DESC');
        $this->db->limit(CART_PAGE_LIMIT, $start);
        return $this->db->get()->result();
    }
    
    public function GetUserAddesses(array $where, array $select=array(), $start=0){
        $where['ua.is_delete'] = 0;
        $this->db->select($select);
        $this->db->select('mpc.area_name, mpc.district, mpc.state_name');
        $this->db->from($this->usersAddress.' AS ua');
        $this->db->join($this->masterPinCode.' AS mpc', 'mpc.pin_code = ua.pin_code');
        $this->db->where($where);
        $this->db->group_by('ua.id');
        $this->db->order_by('ua.is_current','DESC');
        return $this->db->get()->result();
    }

}
