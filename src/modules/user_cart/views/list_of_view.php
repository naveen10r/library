<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . 'User Cart' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css'
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            /* 'web_view/catalog/javascript/jquery/magnific/jquery.magnific-popup.min.js',  */
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_js($js);
        ?>
    </head>
    <body class="account-register layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>
        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>

        <style type="text/css">
            .validation_error{ color:#FF0000; }
            .bootbox-accept { background: #388e3c !important; border-color: #388e3c !important; }
        </style>
        <div id="account-register" class="container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Account</a></li>
                <li><a href="#">Your Cart</a></li>
            </ul>
            <div class="row">
                <?php
                if ($this->session->flashdata('alert')) {
                    $alert = $this->session->flashdata('alert');
                    echo $alert['color']($alert['responce']);
                }
                ?>
                <div class="col-md-7">
                    <div class="panel-default">
                        <?php
                        $itemCount = 0;
                        $total_price = 0;
                        $total_mrp = 0;
                        if (!empty($ExistCart)) {
                            foreach ($ExistCart as $cart) {
                                $itemCount++;
                                $total_price = $total_price + $cart->offerprice;
                                $total_mrp = $total_mrp + $cart->mrp;
                                $prodImg = base_url(PRODUCT_DIR_URL . $cart->product_id . '/image/' . $cart->cover_image);
                                $remove = site_url('user_cart/cart/remove_item/' . esecure($cart->cart_id));
                                $pDetails = preety_url($cart->title) . '-' . toAlpha($cart->product_id);
                                $href = site_url('product-details/' . $pDetails);
                                $author = explode(',', $cart->author_id);
                                $author_list = array();
                                foreach ($author as $aid) {
                                    $author_list[] = $AllAuthor[$aid]->name;
                                }
                                $notifyDiv = '';
                                $notifyDiv .= '<div class="panel panel-default">';
                                $notifyDiv .= '<div class="panel-body">';
                                /* NOTIFY TITLE */
                                $notifyDiv .= '<div class="blog-left-content">';
                                $notifyDiv .= '<h5 class="blog-title"><a href="' . $href . '">' . $cart->title . '</a></h5>';
                                $auth_list = join(', ', $author_list);
                                $notifyDiv .= '<em><strong>Author By </em>: </strong> ' . $auth_list;
                                $notifyDiv .= '</div>';
                                /* NOTIFY DESCRIPTION */
                                $notifyDiv .= '<div class="blog-right-content">';
                                $notifyDiv .= '<div class="blog-date-comment">';
                                $notifyDiv .= '<div class="blog-date">Expected : ' . date(EXPECTED_DATE, strtotime('+20Days')) . ' | Delivery Charges : ' . RUPEE_LOGO . ' 50.00</div>';
                                $notifyDiv .= '<div class="comment-wrapper">';
                                $notifyDiv .= '<div class="write-comment"><a href="#">Chat with us</a></div>';
                                $notifyDiv .= '</div>';
                                $notifyDiv .= '</div>';
                                $notifyDiv .= '</div>';

                                $notifyDiv .= '<div class="blog-desc">';
                                $notifyDiv .= '<br />';
                                $notifyDiv .= '<div class="row">';
                                $notifyDiv .= '<div class="col-md-2">';
                                $notifyDiv .= '<a href="' . $href . '"><img src="' . $prodImg . '" class="img-rounded" alt="' . $cart->title . '" style="width: 80px; height: 110px;" /></a>';
                                $notifyDiv .= '</div>';
                                $notifyDiv .= '<div class="col-md-6">';
                                $notifyDiv .= '<p><strong>MRP : </strong>' . RUPEE_LOGO . ' ' . number_format($cart->mrp, 2) . '</p>';
                                $notifyDiv .= '<p><strong>Offer Price : </strong>' . RUPEE_LOGO . ' ' . number_format($cart->offerprice, 2) . '</p>';
                                $notifyDiv .= '<p><strong>Available At : </strong>' . implode(', ', json_decode($cart->buying_mode)) . '</p>';
                                $notifyDiv .= '<p><strong>Quantity : </strong>' . $cart->qty;
                                $notifyDiv .= ' | <a href="javascript:void(0)" onclick="return confirm_remove(\'' . $remove . '\');"><strong>REMOVE</strong></a>';
                                $notifyDiv .= '</p>';
                                $notifyDiv .= '</div>';
                                $notifyDiv .= '</div>';
                                $notifyDiv .= '</div>';
                                $notifyDiv .= '</div>';
                                $notifyDiv .= '</div>';
                                echo $notifyDiv;
                            }
                        } else {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <center>
                                        <h3><strong>Your cart is empty!</strong></h3>
                                        <br />
                                        <a class="btn btn-primary" href="<?php echo site_url(); ?>">Shop Now</a>
                                    </center>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-sm-5" id="price_panel">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php
                            $price_of = array();
                            ?>
                            <h4 style="color:#e91e76">PRICE DETAILS</h4>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><strong>MRP Amount of <?php echo ($itemCount > 1) ? $itemCount . ' Items' : $itemCount . ' Item'; ?></strong></td>
                                        <td><?php echo RUPEE_LOGO . ' ' . number_format($total_mrp, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Offering price</strong></td>
                                        <td><?php
                                            echo RUPEE_LOGO . ' ' . number_format($total_price, 2);
                                            $price_of[] = (int)$total_price;
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Delivery Charges</strong></td>
                                        <td><?php
                                            if ($total_price >= 1000) {
                                                echo '<font color="#388e3c" weight="bold">FREE</font>';
                                            } else {
                                                $delivery = RUPEE_LOGO . ' ' . number_format(($itemCount * 50), 2);
                                                echo '<font weight="bold">' . $delivery . '</font>';
                                                $price_of[] = (int)($itemCount * 50);
                                            }
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Total Amount</h4></td>
                                        <td><?php echo RUPEE_LOGO . ' ' . (int)array_sum($price_of) . '.00'; ?></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2">
                                            <a href="<?php echo site_url('verify-order'); ?>" class="btn btn-warning pull-right">Verify Order</a>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php footer_tag(); ?>
        <script type="text/javascript" src="<?php echo base_url('web_view/bootbox/bootbox.min.js'); ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $(".alert").hide("slow");
                }, 5000);
            });
            function confirm_remove(site_url) {
                bootbox.confirm({
                    message: "Are you sure want to remove this item into cart ?",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result === true) {
                            location.href = site_url;
                        }
                    }
                });
            }
        </script>
    </body>
</html>