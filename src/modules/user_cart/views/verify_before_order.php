<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . 'Place Order' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css'
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            /* 'web_view/catalog/javascript/jquery/magnific/jquery.magnific-popup.min.js',  */
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_js($js);
        ?>
        <style type="text/css">
            .validation_error{ color:#FF0000; }
            .bootbox-accept { background: #388e3c !important; border-color: #388e3c !important; }
            a:hover,a:focus{
                text-decoration: none;
                outline: none;
            }
            #accordion .panel{
                border: none;
                border-radius: 5px;
                box-shadow: none;
                margin-bottom: 5px;
            }
            #accordion .panel-heading{
                padding: 0;
                border: none;
                border-radius: 5px 5px 0 0;
            }
            #accordion .panel-title a{
                display: block;
                padding: 20px 30px;
                background: #f7f6f6;
                font-size: 17px;
                font-weight: bold;
                color: #ea526f;
                letter-spacing: 1px;
                text-transform: uppercase;
                border: 1px solid #e0e0e0;
                border-radius: 5px 5px 0 0;
                position: relative;
            }
            #accordion .panel-title a.collapsed{
                border-color: #e0e0e0;
                border-radius: 5px;
            }
            #accordion .panel-title a:before,
            #accordion .panel-title a.collapsed:before,
            #accordion .panel-title a:after,
            #accordion .panel-title a.collapsed:after{
                content: "\f103";
                font-family: "Font Awesome 5 Free";
                font-weight: 900;
                width: 30px;
                height: 30px;
                line-height: 30px;
                border-radius: 5px;
                background: #ea526f;
                font-size: 20px;
                color: #fff;
                text-align: center;
                position: absolute;
                top: 15px;
                right: 30px;
                opacity: 1;
                transform: scale(1);
                transition: all 0.3s ease 0s;
            }
            #accordion .panel-title a:after,
            #accordion .panel-title a.collapsed:after{
                content: "\f101";
                background: transparent;
                color: #000;
                opacity: 0;
                transform: scale(0);
            }
            #accordion .panel-title a.collapsed:before{
                opacity: 0;
                transform: scale(0);
            }
            #accordion .panel-title a.collapsed:after{
                opacity: 1;
                transform: scale(1);
            }
            #accordion .panel-body{
                padding: 20px 30px;
                background: #FFF;
                border-top: none;
                font-size: 15px;
                color: #000;
                line-height: 28px;
                letter-spacing: 1px;
                border-radius: 0 0 5px 5px;
                border : #E0E0E0;
                border: #E0E0E0 1px solid;
                border-top: none;
            }

            .cart_body_4{ padding: 0% 4% 0% 4%;}
        </style>
    </head>
    <body class="account-register layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Place Order</a></li>
                    </ul>
                    <?php
                    if ($this->session->flashdata('alert')) {
                        $alert = $this->session->flashdata('alert');
                        echo $alert['color']($alert['responce']);
                    }
                    $amounts = array();
                    $isRent = false;
                    $itemCount = 0;
                    $price_of = array();
                    ?>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <?php
                                        if (!empty($ExistCart)) {
                                            echo (count($ExistCart) > 1) ? 'Items' : 'Item';
                                        } else {
                                            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'No item in cart'));
                                            redirect('view-cart');
                                        }
                                        ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <?php
                                    if (!empty($ExistCart)):
                                        foreach ($ExistCart as $ec):
                                            $itemCount++;
                                            $prodImg = base_url(PRODUCT_DIR_URL . $ec->product_id . '/image/' . $ec->cover_image);
                                            ?>
                                            <div class="col-sm-12 cart_body_4">
                                                <hr />
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="row text-left">
                                                            <div class="col-sm-3" style="width: 11%;">
                                                                <img src="<?php echo $prodImg; ?>" class="img-responsive" alt="<?php echo $ec->title; ?>" style="width: 110px; height: 140px;" />

                                                            </div>
                                                            <div class="col-sm-9">
                                                                <?php
                                                                echo '<p><strong>' . $ec->title . '</strong>' . '</p>';
                                                                echo '<p><strong>MRP : </strong>' . RUPEE_LOGO . ' ' . number_format($ec->mrp, 2) . '</p>';
                                                                echo '<p><strong>Offering : </strong>' . RUPEE_LOGO . ' ' . number_format($ec->offerprice, 2) . '</p>';
                                                                echo '<p><strong>Available At : </strong>' . implode(', ', json_decode($ec->buying_mode)) . '</p>';
                                                                $amounts['MRP'][] = (int) $ec->mrp;
                                                                $amounts['OFFER'][] = (int) $ec->offerprice;
                                                                ?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Addresses
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div class="row">
                                        <?php
                                        if ($addresses):
                                            foreach ($addresses as $add):
                                                $type = ($add->is_current == 1) ? 'style="color:#4f9e1e";' : '';
                                                ?>
                                                <div class="col-sm-4 cart_body_4">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <h5 class="card-title" <?php echo $type; ?>><strong><?php echo $add->address_as; ?></strong></h5>
                                                            <p style="min-height: 80px;"><?php echo $add->address_line_1 . ', ' . $add->address_line_2 . ', ' . $add->address_line_3 . ', ' . $add->area_name; ?></p>
                                                            <p style="min-height: 60px;"><?php echo ucwords(strtolower($add->district)) . ', ' . $add->state_name . ' - ' . $add->pin_code; ?></p>
                                                            <p>Near By : <?php echo $add->landmark; ?></p>
                                                            <a href="#" class="btn btn-primary" <?php
                                                            if ($add->is_current == 1) {
                                                                echo 'style="background: #e91e76; border: #e91e76; margin-top: 10px;"';
                                                            } else {
                                                                echo 'style="margin-top: 10px;"';
                                                            }
                                                            ?>> <?php
                                                                   if ($add->is_current == 1) {
                                                                       echo 'Default Address';
                                                                   } else {
                                                                       echo 'Deliver To This Address';
                                                                   }
                                                                   ?></a>
                                                        </div>
                                                    </div>
                                                </div>
        <?php
    endforeach;
endif;
?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Payments
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <table class="table" style="width:40%">
                                        <tbody>
                                            <tr>
                                                <td>MRP Amount of <?php echo ($itemCount > 1) ? $itemCount . ' Items' : $itemCount . ' Item'; ?></td>
                                                <td><?php echo RUPEE_LOGO . ' ' . number_format(array_sum($amounts['MRP']), 2); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Offering price</td>
                                                <td><?php
                                                $total_price = (int) array_sum($amounts['OFFER']);
                                                echo RUPEE_LOGO . ' ' . number_format($total_price, 2);
                                                $price_of[] = $total_price;
                                                ?></td>
                                            </tr>
                                            <tr>
                                                <td>Delivery Charges</td>
                                                <td><?php
                                                    if ($total_price >= 1000) {
                                                        echo '<font color="#388e3c" weight="bold">FREE</font>';
                                                    } else {
                                                        $delivery = RUPEE_LOGO . ' ' . number_format(($itemCount * 50), 2);
                                                        echo '<font weight="bold">' . $delivery . '</font>';
                                                        $price_of[] = (int) ($itemCount * 50);
                                                    }
                                                    ?></td>
                                            </tr>
                                            <tr>
                                                <td><h4>Total Amount</h4></td>
                                                <td><?php echo RUPEE_LOGO . ' ' . (int) array_sum($price_of) . '.00'; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="flexRadioDefault" disabled="disabled" />
                                        <label class="form-check-label" for="flexRadioDefault1">Credit Card / Debit Card </label>
                                    </div>

                                    <!-- Default checked radio -->
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
                                        <label class="form-check-label" for="flexRadioDefault2"> Cash On Delivery </label>
                                    </div>
                                    <a href="" class="btn btn-success">Continue</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php footer_tag(); ?>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $(".alert").hide("slow");
                }, 5000);
                $('.collapse').collapse();
            });

        </script>
    </body>
</html> 
