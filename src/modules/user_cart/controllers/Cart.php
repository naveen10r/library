<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    private $user_id;
    public function __construct() {
        parent::__construct();
        if (!empty($this->session->userdata('ExUserSess'))) {
            $ExUserSess = $this->session->userdata('ExUserSess');
            $this->user_id = $ExUserSess['id'];
            $this->user_name = $ExUserSess['name'];
        } else {
            redirect('');
        }
        $this->load->model(array('CartService','home/HomeService'));
        $this->load->helper(array('site-link/web_view_header', 'string', 'site-link/web_view_footer'));
    }

    public function ViewCart() {
        $data = $this->getCartData();
        $this->load->view('list_of_view', $data);
    }

    public function AddToCart($product_id) {
        $where = array();
        $where['user_id'] = $this->user_id;
        $where['product_id'] = $product_id;
        $isExist = $this->CartService->GetProduct($where);
        if (empty($isExist)) {
            $insert = $where;
            $insert['qty'] = 1;
            $insert['added_at'] = date(SYS_DB_DATE_TIME);
            $insert['expair_at'] = date(SYS_DB_DATE, strtotime('+1Years'));
            $insert['is_delete'] = 0;
            $isInsert = $this->CartService->AddToCart($insert);
            if($isInsert) {
                $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Book added in cart.'));
            } else {
                $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
            }
        } else {
            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => 'Already added in cart.'));
        }
        $currentCart = $this->CartService->CartCount($where);
        $this->session->set_userdata('CartCount', $currentCart);
        redirect($this->agent->referrer());
    }
    
    public function remove_item($item){
        $cart_id = dsecure($item);
        $where = array();
        $where['user_id'] = $this->user_id;
        $where['cid'] = $cart_id;
        $isExist = $this->CartService->GetProduct($where);
        if($isExist){
            $update = array('is_delete' => 1);
            $isDelete = $this->CartService->UpdateCart($where, $update);
            if($isDelete){
                $cartCount = array();
                $cartCount['user_id'] = $this->user_id;
                $currentCart = $this->CartService->CartCount($cartCount);
                $this->session->set_userdata('CartCount', $currentCart);
                $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Remove product in cart.'));
            } else {
                $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
            }
        }
        redirect($this->agent->referrer());
    }

    public function VerifyOrder()
    {
        $data = $this->getCartData();
        $data['addresses'] = $this->CartService->GetUserAddesses(array('user_id' => $this->user_id), array('ua.*'));
        $this->load->view('verify_before_order', $data);
    }
    
    private function getCartData(){
        $data = array();
        $where = array('user_id' => $this->user_id);
        $select = array('pd.id AS product_id','pd.title','pd.mrp', 'pd.isbn', 'pd.cover_image','pd.buying_mode','pd.offerprice','pd.author_id','uc.cid AS cart_id', 'uc.qty');
        $data['ExistCart'] = $this->CartService->GetProductDetaisByCart($where,$select);
        $AllAuthor = $this->HomeService->GetAllAuthor(array('slug !=' => '', 'is_active' => 1));
        foreach ($AllAuthor as $author) {
            $authorList[$author->id] = $author;
        }
        $data['AllAuthor'] = $authorList;
        return $data;
    }
    
}
