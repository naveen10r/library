<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dashboard extends CI_Controller {

    private $userId = '';
    private $userCode = '';
    public function __construct() {
        parent::__construct();
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = $this->session->userdata('userAuth');
            $userAuth = (object) $userAuth;
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
        } else {
            redirect('mx-admin/logout');
        }
        $this->load->model(array('mx-admin/AdminAuthModel'));
        $this->load->helper(array('admin-links/admin_menu','admin-links/admin_head', 'admin-links/admin_foot'));
    }
    
    public function index()
    {
        auth_uri_menu('mx-admin-dashboard');
        $this->load->view('adminDashboard');
    }
    
}

