<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class General extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }
    
    public function setLanguage(string $language='')
    {
        $haystack = array('english','hindi');
        if(!empty($language) AND in_array($language, $haystack))
        {
            $this->session->set_userdata('LANGUAGE','');
            $this->session->set_userdata('LANGUAGE',$language);
        } else {
            $this->session->set_userdata('LANGUAGE','english');
        }
        redirect($this->agent->referrer());
    }
    
	public function uploadXlsx()
	{
		if($this->input->post())
		{
			require(APPPATH.'third_party/library/php-excel-reader/excel_reader2.php');
			require(APPPATH.'third_party/library/SpreadsheetReader.php');
		} else
		{
			$this->load->view('xlsx');;
		}
	}
	
    public function downloadLink()
    {
        echo 'Comming Soon';
    }
    
    public function responce()
    {
        echo $this->session->tempdata('responce');
    }
    
    public function Page404()
    {
        $this->load->view('page_not_found');
    }
    
    public function refresh_captcha() {
        $this->load->helper(array('captcha_security'));
        ajax_refresh_captcha();
    }
    
    public function AllCategory()
    {
        $alphabet = $this->uri->segment(2);
        if (strtolower($alphabet) == 'all') { $alphabet = ''; } else { $alphabet = strtolower($alphabet); }
        $this->load->helper(array('site-link/web_view_header', 'site-link/web_view_footer'));
        $this->load->model('product/ProductService');
        $data['all_category'] = $this->ProductService->get_product_category(array(), $alphabet);
        $data['alphabet'] = strtoupper($alphabet);
        $this->load->view('categories', $data);
    }
}