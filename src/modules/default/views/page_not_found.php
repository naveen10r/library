<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Home | <?php echo STATIC_SITE_NAME; ?></title>
        <link href="<?php echo base_url('web-www/css/bootstrap.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/font-awesome.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/prettyPhoto.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/price-range.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/animate.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/main.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('web-www/css/responsive.css'); ?>" rel="stylesheet" />
        <!--[if lt IE 9]>
        <script src="<?php echo base_url('web-www/js/html5shiv.js'); ?>"></script>
        <script src="<?php echo base_url('web-www/js/respond.min.js'); ?>"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="<?php echo base_url('web-www/images/ico/favicon.ico'); ?>" />
    </head>

    <body>
        <div class="container text-center">
            <div class="logo-404">
                <a href="<?php echo site_url(); ?>"><img src="<?php // echo base_url('web-www/images/home/logo.png'); ?>" /></a>
            </div>
            <div class="content-404">
                <img src="<?php echo base_url('web-www/images/404/404.png'); ?>" class="img-responsive" alt="" />
                <h1><b>OPPS!</b> We Couldn't Find this Page</h1>
                <p>Uh... So it looks like you brock something. The page you are looking for has up and Vanished.</p>
                <h2><a href="<?php echo site_url($this->agent->referrer()); ?>">Bring me back page</a></h2>
            </div>
            <div class="clearfix"></div>
        </div>
        <script src="<?php echo base_url('web-www/js/jquery.js'); ?>"></script>
        <script src="<?php echo base_url('web-www/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('web-www/js/jquery.scrollUp.min.js'); ?>"></script>
        <script src="<?php echo base_url('web-www/js/price-range.js'); ?>"></script>
        <script src="<?php echo base_url('web-www/js/jquery.prettyPhoto.js'); ?>"></script>
        <script src="<?php echo base_url('web-www/js/main.js'); ?>"></script>
    </body>
</html>