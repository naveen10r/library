<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . ' All Categories' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <meta name="title" content="<?php echo SITE_DEFAULT_META_TITLE; ?>" />
        <meta name="keywords" content="<?php echo SITE_DEFAULT_META_KEYWORDS; ?>" />
        <meta name="description" content="<?php echo SITE_DEFAULT_META_DESCRIPTION; ?>" />

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js',
            'web_view/catalog/view/javascript/jquery/swiper/js/swiper.jquery.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js'
        );
        echo load_js($js);
        $css = array(
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/swiper.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/opencart.css',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css',
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_css($css);
        ?>
        <link href="<?php echo site_url('author'); ?>" rel="canonical" />
        <style type="text/css">
            .customNavigation > a {
                background-color: #81b851 !important;
            }
            a { color: #4C345E; text-decoration: none;}
            .custom_class { margin-left: 5%; }
            .custom_select { color: #ffffff !important; background-color: #81b851 !important; border-color: #81b851 !important; }
        </style>
    </head>
    <body class="product-product-47 layout-2 left-col">
        <header>
            <?php
            header_container();
            header_top_inner();
            ?>
        </header>
    </div>
    <div class="wrap-breadcrumb parallax-breadcrumb">
        <div class="container"></div>
    </div>

    <div id="product-product" class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">All Categories</a></li>
        </ul>
        <div class="row">

            <nav aria-label="Page navigation example">
                <ul class="pagination custom_class">
                    <li class="page-item"><a class="page-link <?php echo empty($alphabet) ? 'custom_select' : ''; ?>" href="<?php echo site_url('categories/All'); ?>">All</a></li>
                    <?php
                    for ($i = 'A'; $i <= 'Z'; $i++) {
                        $url = site_url('categories/' . $i);
                        $custom_select = '';
                        if ($alphabet == $i) {
                            $custom_select = 'custom_select';
                        }
                        echo '<li class="page-item"><a class="page-link ' . $custom_select . '" href="' . $url . '">' . $i . '</a></li>';
                        if ($i == 'Z') {
                            break;
                        }
                    }
                    ?>
                </ul>
            </nav>
            <hr />
            <div id="content12" class="col-sm-12">
                <div class="row">
                    <?php
                    if (!empty($all_category)) {
                        foreach ($all_category as $cat) {
                            ?> 
                            <div class="col-sm-6">
                                <div class="col-sm-4" style="max-height: 150px;"><a href="<?php echo site_url('home/books/' . $cat->slug) ?>"> <img src="<?php echo base_url('web_view/image/icons/category.png'); ?>" height="130" width="135"> </a></div>
                                <div class="col-sm-8" style="height: 200px;">
                                    <h2><a href="<?php echo site_url('home/books/' . $cat->slug) ?>"> <?php echo $cat->name; ?></a></h2> 
                                    <div style="text-align: justify;"><?php echo $cat->description; ?></div> 
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo '<center><strong class="danger">No any category found from '. strtoupper($alphabet).'</strong><center><br />';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.load_more').click(function () {
			var page_count = $('#page_count').val();
			$(this).html('<img src="<?php echo LOADING_IMG_GIF; ?>" height="20px" width="20px" /> Loading...');
			$.ajax({
				type: "POST",
				url: '<?php echo site_url('author/products/load_more'); ?>',
				data: {"start_point": page_count, 'category_id': category_id, 'query_search' : query_search},
				success: function (returntxt)
				{
					$('.load_more').html('');
					$('.load_more').html('Load More');
					var parseJson = JSON.parse(returntxt);
					if (parseJson.responce.next_itr === false) {
						$('.load_more').remove();
					}
					if (parseJson.status === true && parseInt(parseJson.responce.counter) > 0) {
						new_pageCount = parseInt(page_count) + 1;
						$('#page_count').val(new_pageCount);
						$('.box-product').append(parseJson.responce.data);
					} else {
						$('#server_response').addClass('alert alert-danger');
						$('#review_response').html(parseJson.responce.error);
					}
				}
			});
		});
    });

   
</script>	
<?php footer_tag(); ?>
</body>
</html> 
