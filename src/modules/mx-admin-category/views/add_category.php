<?php 
$id = '';
$parent_id = '';
$name = '';
$slug = '';
$slug_code = '';
$description = '';
$is_active = '';
$modified_at = '';
if (!empty($CategoryDetails))
{
    $id = $CategoryDetails->id;
    $parent_id = $CategoryDetails->parent_id;
    $name = $CategoryDetails->name;
    $slug = $CategoryDetails->slug;
    $slug_code = $CategoryDetails->slug_code;
    $description = $CategoryDetails->description;
    $is_active = $CategoryDetails->is_active;
    $modified_at = $CategoryDetails->modified_at;
}
if (!empty(set_value('title'))){ $name = set_value('title'); }
if (!empty(set_value('slug'))){ $slug = set_value('slug'); }
if (!empty(set_value('slug_code'))){ $slug_code = set_value('slug_code'); }
if (!empty(set_value('description'))){ $description = set_value('description'); }
if (!empty(set_value('status'))){ $is_active = set_value('status'); }
if (!empty(set_value('genre_code'))){ $parent_id = set_value('genre_code'); }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Matrix Admin</title>
        <link rel="icon" href="<?php echo base_url(FAVICON); ?>" type="image/gif" sizes="16x16" />
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/uniform.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/matrix-media.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/bootstrap-wysihtml5.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/datepicker.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('web-inf/css/select2.css'); ?>" />
        <link href="<?php echo base_url('web-inf/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    </head>
    <body>

        <!--Header-part-->
        <?php echo admin_head(); ?>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <?php echo admin_head_menu(); ?>

        <!--start-top-serch-->
        <?php echo admin_head_search(); ?>
        <!--close-top-serch--> 

        <!--sidebar-menu-->
        <?php echo admin_menu('Category'); ?>
        <!--sidebar-menu-->
        <!--close-left-menu-stats-sidebar-->

        <div id="content">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="alertResponce">
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?> 
                    </div>
                    <div class="span10">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                <h5>Add Category</h5>
                                <?php echo MANDATORY; ?>
                            </div>
                            <div class="widget-content nopadding">
                                <?php
                                echo form_open_multipart('mx-admin-category/category/addCategory', array('class' => 'form-horizontal', 'id' => 'saveProduct'));
                                echo input_csrf();
                                ?>
                                <div class="control-group">
                                    <label class="control-label">Name <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Title of Name [Ex: Law, History]"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text" autocomplete="off" id="inputError" maxlength="255" class="span11 title" name="title" value="<?php echo $name; ?>" required="true" style="text-transform: capitalize;" />
                                        <span class="help-inline red_error"><?php echo form_error('name'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Description 
                                        <a class="tip-top" data-original-title="Alphabets and space allowed only."><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls-ckeditor">
                                        <textarea id="description" maxlength="230" name="description" rows="4"><?php echo $description; ?></textarea>

                                        <span class="help-inline red_error"><?php echo form_error('description'); ?></span> 
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Slug <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Slug"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <input type="text"  autocomplete="off" id="inputError" maxlength="255" class="span11 slug" name="slug" value="<?php echo $slug; ?>" required="true" />
                                        <input type="hidden" id="slug_code" maxlength="255" class="span11 slug_code" name="slug_code" value="<?php echo $slug_code; ?>" />

                                        <span class="help-inline red_error"><?php echo form_error('slug'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Parent Genre <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Parent Genre"><i class="icon-info-sign"></i></a>
                                    </label>

                                    <div class="controls">
                                        <select name="genre_code" class="span4">
                                            <option value="0" selected="selected">--- None ---</option>
                                            <?php
                                            if (isset($Genre_code_list) && count($Genre_code_list) > 0)
                                                foreach ($Genre_code_list as $scl) {
                                                    if ($parent_id == $scl->id){
                                                        echo '<option value="' . $scl->id . '" selected="selected">' . $scl->name . '</option>';
                                                    } else {
                                                        echo '<option value="' . $scl->id . '">' . $scl->name . '</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                        <span class="help-inline red_error"><?php echo form_error('MRP'); ?></span> 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Status <sup class="red_error">&#042;</sup>
                                        <a class="tip-top" data-original-title="Status"><i class="icon-info-sign"></i></a>
                                    </label>
                                    <div class="controls">
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="1" <?php if(!empty($is_active) and $is_active == 1) { echo 'checked="checked"'; } ?> /> Publish
                                            <input type="radio" name="status" value="0" <?php if(empty($is_active)) { echo 'checked="checked"'; } ?> /> Un-Publish
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Check Robotic <sup class="red_error">&#042;</sup>
                                    </label>
                                    <div class="controls">
                                        <div class="span2">
                                            <img src="<?php echo base_url('captcha/' . $captcha_security['filename']); ?>"  id="captcha_security" />
                                        </div>
                                        <div class="span1"><a href="javascript:void(0)" onclick="refresh_image();"><i class="icon-repeat"></i></a></div>
                                        <input type="text" name="captchaWord" class="span3" required="true" />
                                        <span class="help-inline red_error"><?php echo form_error('captchaWord'); ?></span> 
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <?php
                                    if (isset($CategoryDetails->id) && !empty($CategoryDetails->id)) {
                                        echo '<input type="hidden" name="category_id" id="category_id" value="' . $CategoryDetails->id . '" />';
                                        echo '<button type="submit" class="btn btn-success"><i class="icon-save"></i> Update</button>';
                                    } else {
                                        echo '<button type="submit" class="btn btn-success"><i class="icon-save"></i> Save</button>';
                                    }
                                    ?>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 2013 &copy; Matrix Admin.</div>
        </div>
        <!--end-Footer-part--> 
        <script src="<?php echo base_url('web-inf/js/jquery.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.ui.custom.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/jquery.uniform.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/bootstrap-wysihtml5.js'); ?>"></script> 
        <script src="<?php echo base_url('web-inf/js/matrix.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/matrix.popover.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/bootstrap-datepicker.js'); ?>"></script>
        <script src="<?php echo base_url('web-inf/js/select2.min.js'); ?>"></script> 
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                CKEDITOR.replace('description');
            });
            
            function randomString(lenString) { 
                var characters = "ABCDEFGHIJKLMNOPQRSTUVWXTZABCDEFGHIJKLMNOPQRSTUVWXTZ";
                var randomstring = '';
                for (var i=0; i<lenString; i++) {  
                    var rnum = Math.floor(Math.random() * characters.length);  
                    randomstring += characters.substring(rnum, rnum+1);  
                }
                return randomstring;
            }

            $('.title').keyup(function () {
                var dInput = this.value;
                dInput = dInput.toLowerCase();
                dInput = dInput.replace(/[^a-zA-Z0-9]+/g, '-');
                $(".slug").val(dInput);
                var xt = dInput.split('-').map(x => x[0]).join('');
                x_length = xt.length;
                if (x_length < 4){
                    if (x_length === 1){
                        xt = xt + randomString(3);
                    }
                    if (x_length === 2){
                        xt = xt + randomString(2);
                    }
                    if (x_length === 3){
                        xt = xt + randomString(1);
                    }
                }
                const x = xt;
                $(".slug_code").val(x.toUpperCase());
            });
            
            function refresh_image(){
                $.ajax({
                    url: "<?php echo base_url('default/general/refresh_captcha'); ?>",
                    type: 'get',
                    success: function (response) {
                        json_str = JSON.parse(response);
                        if (json_str.status === true) {
                            filename_txt = json_str.response_data.filename;
                            source_img = "<?php echo base_url('captcha/'); ?>" + filename_txt;
                            $('#captcha_security').attr("src", source_img);
                        }
                    }
                });
            }
        </script>
    </body>
</html>