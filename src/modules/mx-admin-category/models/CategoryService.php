<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 */

class CategoryService extends TableName
{
    public function __construct() {
        parent::__construct();
    }
    
    public function saveproductCategory(array $insert):int
    {
        $this->db->trans_start();
        $this->db->insert($this->productCategory, $insert);
        return $this->trans_insert_data();
    }
	
	public function UpdateCategory(array $update, array $where) {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->productCategory, $update);
        return $this->trans_update_data();
    }
	
	public function getproductCategoryDetails(array $where)
    {
        $this->db->select('*');
        $this->db->from($this->productCategory);
        $this->db->where($where);
        return $this->db->get()->result();
    }
	
	public function getallCategory(array $where)
    {
		
        $this->db->select('pc.id,pc.parent_id,pc.name,pc.is_active,parent.name as Pname,pc.slug');
        $this->db->from($this->productCategory.' as pc');
        $this->db->join($this->productCategory.' as parent', 'pc.parent_id = parent.id', 'left');
        $this->db->where($where);
        $this->db->order_by('pc.name', 'asc');
        return $this->db->get()->result();
    }
	
	
}