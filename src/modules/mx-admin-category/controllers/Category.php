<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Category extends CI_Controller {

    final function __construct() {
        parent::__construct();
        $this->load->helper(array('admin-links/admin_menu', 'admin-links/admin_head', 'admin-links/admin_foot', 'string', 'captcha_security'));
        $this->load->model(array('CategoryService'));
        $this->load->library('upload_lib');
        if ($this->session->has_userdata('userAuth')) {
            $userAuth = (object) $this->session->userdata('userAuth');
            $this->userId = $userAuth->u_id;
            $this->userCode = $userAuth->u_code;
        } else {
            redirect('mx-admin/logout');
        }
    }

    public function index() {
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        $data['parentList'] = 0;
        $data['allCategories'] = $this->CategoryService->getallCategory(array());
        $this->load->view('list_categories', $data);
    }

    public function pcategory() {
        $data['captcha'] = captcha_security();
        $data['breadcrumb'] = current_url();
        $data['parentList'] = 1;
        $data['allCategories'] = $this->CategoryService->getallCategory(array('pc.parent_id' => NULL, 'pc.is_active' => 1));
        $this->load->view('list_categories', $data);
    }
    
    public function validate_captcha(){
        if($this->input->post('captchaWord') != $this->session->tempdata('captcha_word'))
        {
            $this->form_validation->set_message('validate_captcha', 'Wrong captcha code, hmm are you the Terminator?');
            return false;
        }else{
            return true;
        }

    }

    public function addCategory() {
        $data['captcha_security'] = captcha_security();
        $data['breadcrumb'] = current_url();

        if (!$this->input->post()) {
            if (!empty($this->uri->segment('4')) && $this->uri->segment('4') != "") {
                $where = array('id' => $this->uri->segment('4'));
                $CategoryDetails = $this->CategoryService->getproductCategoryDetails($where);
                $data['CategoryDetails'] = $CategoryDetails[0];
            }

            $data['Genre_code_list'] = $this->CategoryService->getallCategory(array('pc.parent_id' => NULL, 'pc.is_active' => 1));
            $this->session->set_tempdata('captcha_word', $data['captcha_security']['word'], 600);
            $this->load->view('add_category', $data);
        } else {
            $this->form_validation->set_rules('title', 'Title', 'required|trim|xss_clean');
            $this->form_validation->set_rules('description', 'description', 'trim|xss_clean');
            $this->form_validation->set_rules('slug', 'Slug', 'required|trim|xss_clean');
            $this->form_validation->set_rules('slug_code', 'Slug Code', 'required|trim|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean');
            $this->form_validation->set_rules('genre_code', 'genre_code', 'required|trim|xss_clean');
            $this->form_validation->set_rules('captchaWord', 'captcha word', 'required|trim|xss_clean|callback_validate_captcha');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_tempdata('captcha_word', $data['captcha']['word'], 600);
                $this->load->view('add_category', $data);
            } else {
                $title = $this->input->post('title', TRUE);
                $description = $this->input->post('description', TRUE);
                $slug = $this->input->post('slug', TRUE);
                $slug_code = $this->input->post('slug_code', TRUE);
                $status = $this->input->post('status', TRUE);
                $genre_code = $this->input->post('genre_code', TRUE);
                $genre_code_value = NULL;
                if (isset($genre_code) && $genre_code != 0) {
                    $genre_code_value = $genre_code;
                }
                $save = array(
                    'name' => ucwords($title),
                    'description' => $description,
                    'slug' => $slug,
                    'slug_code' => $slug_code,
                    'is_active' => $status,
                    'parent_id' => $genre_code_value,
                    'modified_at' => date(SYS_DB_DATE_TIME)
                );
                
                if (!empty($this->input->post('category_id', TRUE))) {
                    $where = array('id' => $this->input->post('category_id', TRUE));
                    $this->CategoryService->UpdateCategory($save, $where);
                    $isSave = 0;
                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Category Update successfully.'));
                    redirect(base_url() . 'mx-admin-category/category');
                } else {
                    $isSave = $this->CategoryService->saveproductCategory($save);
                }

                if ($isSave) {

                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Category Saved successfully.'));
                    redirect(base_url() . 'mx-admin-category/category');
                }
            }
        }
    }

    /*
     * @author : Sandeep 
     */

    public function updateStatusCatgory() {


        if (!$this->input->post()) {
            $response = array(
                'status' => 'error',
                'message' => "<p>Something went wrong.</p>"
            );
        } else {

            $this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean');
            $this->form_validation->set_rules('categoryid', 'Product ID', 'required|trim|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $response = array(
                    'status' => 'error',
                    'message' => validation_errors()
                );
            } else {
                $status = $this->input->post('status', TRUE);
                $categoryid = $this->input->post('categoryid', TRUE);

                $update = array(
                    'is_active' => $status,
                    'modified_at' => date(SYS_DB_DATE_TIME)
                );
                $where = array('id' => $categoryid);
                $isSave = $this->CategoryService->UpdateCategory($update, $where);
                $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Category Update successfully.'));
                $response = array(
                    'status' => 'success',
                    'message' => "<p>Update successfully.</p>"
                );
                $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Category Update successfully.'));
            }
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
    }

    /*
     * @author : Naveen 
     */

    public function createIndexes() {
        
    }

    private function fileWithValidation($picture, $fileExt) {
        $pictures = !empty($picture) ? $picture : 'pictures';
        $upImg = $_FILES['uploadImg'];
        if (empty($upImg['name'])) {
            $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is required.');
            return false;
        } else {
            $realExt = strtolower($upImg['name']);
            $ext = explode('.', $realExt);
            $extension = end($ext);
            $fileExt = explode('|', $fileExt);
            if (!in_array($extension, $fileExt)) {
                $this->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is not in the correct format.');
                return false;
            }
        }
        return true;
    }

    private function captchaWord($word) {
        $captcha_word = $this->session->tempdata('captcha_word');
        if (empty($word)) {
            $this->form_validation->set_message('captchaWord', 'The captcha field is required.');
            return false;
        } elseif ($captcha_word != $word) {
            $this->form_validation->set_message('captchaWord', 'The captcha security code doesn\'t matched.');
            return false;
        } else {
            return true;
        }
    }

}
