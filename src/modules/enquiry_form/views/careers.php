<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Vacancies in LeafLIB | Careers | Jobs | Vacancies | Recruitment <?php echo STATIC_SITE_NAME; ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <meta name="title" content="Vacancies in LeafLIB | Careers | Jobs | Vacancies | Recruitment" />		
        <meta name="description" content="LeafLIB is Hiring. Applications are invited from young and dynamic professionals for the vacancies. Vacancies includes: Customer Support, Android Developers, Business Development, Onboarding- Technical support, Technical Customer Support, PHP Developers, Python Developers, Explore LeafLIB Job Openings  Now! " >
        <meta name="keywords" content="Jobs Opening, Hiring, Vacancy, Jobs Openings, Vacancies, Opening in LeafLIB, Jobs in LeafLIB, LeafLIB Hiring, LeafLIB Careers, LeafLIB Jobs, LeafLIB Vacancies, LeafLIB Openings, Jobs LeafLIB, Vacancies LeafLIB, Apply job in LeafLIB, Government Job, Apply for govt job, work with LeafLIB, Atmanirbhar Bharat, Digital India Hiring, Digital India Careers, Digital India Jobs, Digital India Vacancies, Digital India Openings, naukri, sarkari naukri, govt naukri, Oppertunity for Graduates, jobs for Graduates, opening for Graduates, Customer Support, Android Developers, Business Development, Onboarding- Technical support, Technical Customer Support, PHP Developers, Python Developers, computer job, job in computer, Developer Opening, Recruitment, Govt Recruitment, LeafLIB Recruitment  " >

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/swiper.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/opencart.css',
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/jquery/swiper/js/swiper.jquery.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js',
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_js($js);
        ?>
        <style>
            .panel-title {
                margin-top: 0;
                margin-bottom: 0;
                font-size: 14px;
                color: #000;
                background-color: #eef2cb;
                padding:10px;
            }
            .career_list{
                list-style: none;
            }

        </style>
    </head>
    <body class="common-home layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>

        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>

        </div>
        <div class="swiper-slide"><img src="<?php echo INDEX_URL . 'web_view/image/banner/careers_banner.png'; ?>" alt="Left banner" class="img-responsive" /></div>
        <div class="container">
            <div class="content-top">
                <div class="box" id="czservicecmsblock">
                    <div class="service_container">
                        <div class="service-area">
                            <h2>OUR CURRENT OPENING</h2>
                        </div>
                    </div>
                </div>

            </div>
        </div>  
        <div id="common-home" class="container">
            <div class="row">
                <div class="col-sm-12"> 
                    <div class="hometab box">
                        <div class="container">
                            <div class="panel-group">

                                <div class="panel panel-default">
                                    <a data-toggle="collapse" href="#collapse1"> 
                                        <div class="panel-heading">
                                            <h4 class="panel-title" >
                                                Digital Marketing Trainee
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="career_data">   
                                                <div class="row">
                                                    <div class="col-md-11">	
                                                        <h5>Experience: 0-3 Years<br>
                                                            Location: Noida [ Work from home ]</h5>		  
                                                    </div>	
                                                    <div class="col-md-1">
                                                        <a href="https://docs.google.com/forms/d/e/1FAIpQLSeJbwgaY2vFJ4n6juEtyDjipBlGMq39fdk0dqb6CgGZO4drRw/viewform?usp=sf_link" target="_blank">
                                                            <button type="button" class="btn btn-primary" ><i class="fa fa-envelope"></i></button>	</A>			  
                                                    </div>
                                                </div>  


                                                <h5>Job Description</h5>
                                                <p>We are looking for a <strong>Digital Marketing Trainee</strong> to assist in the planning, execution and optimization of our online marketing efforts. The promotion of products and services through digital channels is a complex procedure with great potential which becomes increasingly useful for companies such as ours.</p>

                                                <p>The ideal candidate will have a passion for all things marketing and technology. You will be well-versed in the concepts surrounding digital marketing and how the Internet can become a strong asset to securing growing revenue. You will be tech-savvy and intuitive with great ideas to reinforce our marketing campaign.</p>


                                                <ul class="career_list">
                                                    <h5>Skills And Qualifications</h5>
                                                    <li><i class="fa fa-angle-double-right"></i> Assist in the formulation of strategies to build a lasting digital connection with consumers.</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Plan and monitor the ongoing company presence on social media (Twitter, Facebook etc).</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Launch optimized online adverts through Google Adwords, Facebook etc. to increase company and brand awareness.</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Be actively involved in SEO efforts (keyword, image optimization etc).</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Prepare online newsletters and promotional emails and organize their distribution through various channels.</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Provide creative ideas for content marketing and update website.</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Collaborate with designers to improve user experience.</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Measure performance of digital marketing efforts using a variety of Web analytics tools (Google Analytics, WebTrends etc).</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Acquire insight in online marketing trends and keep strategies up-to-date.</li>
                                                </ul>
                                                <p><strong>Role:</strong> Digital Marketing Trainee</p>
                                                <p><strong>Employment Type:</strong> Full Time/Part Time</p>
                                                <h5>Key skills</h5>
                                                <p>SMS marketing, SEO, SMO, Media Marketing, Branding, Google adward, Online Marketing, Campaign Marketing,Ownership, Responsibility , Leadership with positive attitude.</p>
                                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSeJbwgaY2vFJ4n6juEtyDjipBlGMq39fdk0dqb6CgGZO4drRw/viewform?usp=sf_link" target="_blank">
                                                    <button type="button" class="btn btn-primary mt-3 btn-lg">Apply for this Job <i class="fa fa-angle-double-right"></i> </button>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <a data-toggle="collapse" href="#collapse2">
                                        <div class="panel-heading">
                                            <h4 class="panel-title" >
                                                Content Writer Trainee
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse2" class=" panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="career_data">   
                                                <div class="row">
                                                    <div class="col-md-11">	
                                                        <h5>Experience: 0-3 Years<br>
                                                            Location: Noida [ Work from home ]</h5>		  
                                                    </div>	
                                                    <div class="col-md-1">
                                                        <a href="https://docs.google.com/forms/d/e/1FAIpQLSeJbwgaY2vFJ4n6juEtyDjipBlGMq39fdk0dqb6CgGZO4drRw/viewform?usp=sf_link" target="_blank">
                                                            <button type="button" class="btn btn-primary" ><i class="fa fa-envelope"></i></button>	</A>			  
                                                    </div>
                                                </div>  


                                                <h5>Job Description</h5>
                                                <p>We are looking for a <strong>Content Writer Trainee</strong> to join our team and enrich our websites with new blog posts, guides and marketing copy. The ideal candidate will have a passion for all things writing skills and thinking. </p>


                                                <ul class="career_list">
                                                    <h5>Skills And Qualifications</h5>
                                                    <li><i class="fa fa-angle-double-right"></i> Excellent communication, interpersonal and analytical skills, Mandate experience in E-learning content creation.</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Excellent written verbal communication skills in English.</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Creative in thought, imagination and writing.</li>

                                                    <li><i class="fa fa-angle-double-right"></i> We are offering flexible schedules to the our team members so that we will create a friendly environment. </li>

                                                    <li><i class="fa fa-angle-double-right"></i> Evaluate learning effectiveness.</li>
                                                    <li><i class="fa fa-angle-double-right"></i> Create knowledge and skills assessments.</li>

                                                    <li><i class="fa fa-angle-double-right"></i> Knowledge of online content strategy and creation.</li>
                                                </ul>
                                                <p><strong>Role:</strong> Content Writer Trainee</p>
                                                <p><strong>Employment Type:</strong> Full Time/Part Time</p>
                                                <h5>Key skills</h5>
                                                <p>Writing skills, Research, Knowledge of social media, Branding, Communication,Ownership, Responsibility , Leadership with positive attitude.</p>
                                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSeJbwgaY2vFJ4n6juEtyDjipBlGMq39fdk0dqb6CgGZO4drRw/viewform?usp=sf_link" target="_blank">
                                                    <button type="button" class="btn btn-primary mt-3 btn-lg">Apply for this Job <i class="fa fa-angle-double-right"></i> </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container"></div>
        <?php footer_tag(); ?>
    </body>
</html>
