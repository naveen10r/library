<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo STATIC_SITE_NAME; ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <meta name="title" content="<?php echo !empty($pdetails->title) ? $pdetails->title. " | " . SITE_DEFAULT_META_TITLE : SITE_DEFAULT_META_TITLE; ?>" />
        <meta name="keywords" content="<?php echo !empty($pdetails->meta_keywords) ? $pdetails->meta_keywords : SITE_DEFAULT_META_KEYWORDS; ?>" />
        <meta name="description" content="<?php echo !empty($pdetails->meta_description) ? $pdetails->meta_description : SITE_DEFAULT_META_DESCRIPTION; ?>" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/swiper.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/opencart.css',
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/jquery/swiper/js/swiper.jquery.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js',
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_js($js);
        ?>
    </head>
    <body class="common-home layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>

        </header>
        
        <div class="container">
            <div class="content-top">
                <div class="box" id="czservicecmsblock">
                   
                </div>
                
            </div>
        </div>  
		<div class="parallax-breadcrumb">
			<div class="container">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
					<li><a href="#">Contact Us</a></li>
				</ul>
			</div>
		</div>
        <div id="common-home" class="container">
		
            <div class="row">
				<div class="col-sm-12" >
					<div class="col-sm-2"> </div>
					<div class="col-sm-8"> 
					<div class="hometab box">
						<div class="container">
							<h1 class="page-title">Contact Us</h1>
							<form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
								<fieldset>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-name">Your Name</label>
										<div class="col-sm-10">
											<input type="text" name="name" value="" id="input-name" class="form-control" />
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-email">E-Mail Address</label>
										<div class="col-sm-10">
											<input type="text" name="email" value="" id="input-email" class="form-control" />
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-enquiry">Enquiry</label>
										<div class="col-sm-10">
											<textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"></textarea>
										</div>
									</div>
								</fieldset>
								<div class="buttons">
									<div class="pull-right">
										<input class="btn" type="submit" value="Cancel" />
										<input class="btn btn-primary" type="submit" value="Submit" />
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
				<div class="col-sm-2"> </div>
            </div>
        </div>
        <?php footer_tag(); ?>
    </body>
</html>
<script type="text/javascript">
</script>