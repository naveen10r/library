<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo STATIC_SITE_NAME; ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <meta name="title" content="<?php echo!empty($pdetails->title) ? $pdetails->title . " | " . SITE_DEFAULT_META_TITLE : SITE_DEFAULT_META_TITLE; ?>" />
        <meta name="keywords" content="<?php echo!empty($pdetails->meta_keywords) ? $pdetails->meta_keywords : SITE_DEFAULT_META_KEYWORDS; ?>" />
        <meta name="description" content="<?php echo!empty($pdetails->meta_description) ? $pdetails->meta_description : SITE_DEFAULT_META_DESCRIPTION; ?>" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/swiper.min.css',
            'web_view/catalog/view/javascript/jquery/swiper/css/opencart.css',
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/jquery/swiper/js/swiper.jquery.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js',
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_js($js);
        ?>
    </head>
    <body class="common-home layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>

        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>
        <div class="container">
            <div class="content-top">
                <div class="box" id="czservicecmsblock">
                    <div class="service_container">
                        <div class="service-area">
                            <h2>PRIVACY POLICY</h2>
                        </div>
                    </div>
                </div>

            </div>
        </div>  
        <div id="common-home" class="container">
            <div class="row">
                <div class="col-sm-12"> 
                    <div class="hometab box">
                        <div class="container">

                            <p><b>LeafLIB</b> is strongly committed to protecting the privacy of our subscribers. This privacy policy document contains information which you should go through so that you fully understand the terms involved in your subscribing to our service, and how we use the personal information submitted by you to avail this service. We focus on highest standards for secure transactions and customer data privacy. </p>

                            <p><b>Personal information:</b><br/>
                                As part of our service, we collect and store your provide personal information used to identify and contact you. Our primary goal in doing so is to provide you a safe, efficient, smooth and customized experience. This allows us to provide services and features that most likely meet your needs, and to customize our services to make your experience better and easier.</p>

                            <p>You can access your account from every page on the site by clicking the “My Account” tab, to update and correct your name, password, postal address. We have implemented technology and security features, as well as strict internal guidelines, to safeguard the privacy of your personal information from unauthorized access or improper use. We will continue to enhance our security procedures as new technology becomes available.</p>

                            <p>You can browse the Website without becoming our subscriber and member.</p>

                            <p>We use data collection devices such as "cookies" on certain pages of the Website to help analyse our web page flow, measure promotional effectiveness, We do not control the use of cookies by third parties.</p>

                            <p>We collect personally identifiable information (email address, name, phone number, etc.) from you when you set up a free account with us.</p>

                            <p>
                                <b>Note:</b><br/>

                                We may make changes to this policy as and when needed. You should check back for updates to this policy from time to time. The privacy policy can be viewed any time by visiting the Privacy Policy page on LeafLIB website.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php footer_tag(); ?>
    </body>
</html>
