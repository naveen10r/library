<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EnquiryFormService extends TableName
{
    public function __construct() {
        parent::__construct();
    }
    
    public function GetAllCategory(array $where=array(), array $select=array(), $join=false)
    {
        $this->db->select($select);
        $this->db->from($this->productCategory.' AS pcc');
        if($join===true)
        {
            $this->db->join($this->productCategory.' AS pcm','pcc.id = pcm.parent_id');
        }
        $this->db->where($where);
        $this->db->order_by('pcc.name','ASC');
        return $this->db->get()->result();
    }
}