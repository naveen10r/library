<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Enquiry_form extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('site-link/web_view_header','site-link/web_view_footer'));
        $this->load->model(array('EnquiryFormService'));
    }
    
    public function contact_us() {
        $data['cat_slug'] = "";
        $this->load->view('contact_us', $data);
    }
	
	public function request_a_book() {
        $data['cat_slug'] = "";
        $this->load->view('request_a_book', $data);
		
    }
	
	public function refer_a_friend() {
        $data['cat_slug'] = "";
        $this->load->view('refer_a_friend', $data);
    }
	
	public function about_us() {
        $data['cat_slug'] = "";
        $this->load->view('about_us', $data);
    }
	
	public function privacy_policy() {
        $data['cat_slug'] = "";
        $this->load->view('privacy_policy', $data);
    }
	
	public function terms_and_condition() {
        $data['cat_slug'] = "";
        $this->load->view('terms_and_condition', $data);
    }
	
	public function careers() {
        $data['cat_slug'] = "";
        $this->load->view('careers', $data);
    }
	
    
    
}