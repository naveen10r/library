<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait ServiceOrders
{
    
    public function MyOrders()
    {
        $SubTitle= !empty($this->uri->segment(2)) ? $this->uri->segment(2) : 'all-orders';
        $data = array();
        $data['SubTitle'] = $SubTitle;
        $this->load->view('UserOrders', $data);
    }
    
}