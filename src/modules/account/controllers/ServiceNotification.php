<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait ServiceNotification {

    public function Notification() {
        $this->load->library('pagination');

        $config['base_url'] = site_url('account/Services/Notification');
        $config['total_rows'] = 200;
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div>';
        $config['full_tag_close'] = '<div>';
        $this->pagination->initialize($config);
        $select = array('un.title', 'un.description', 'un.create_at', 'un.is_visible');
        $where = array('un.user_id' => $this->user_id);
        $data['notify'] = $this->ServicesModel->GetUserNotify($where, $select, array('is_row' => false, 'limit' => 20));
        $this->VisibleAllNotification($this->user_id);
        $this->load->view('UserNotification', $data);
    }

    private function VisibleAllNotification(int $user_id): bool {
        $where = array('user_id' => $user_id, 'is_visible' => 0);
        $update = array('is_visible' => 1, 'visible_at' => date(SYS_DB_DATE_TIME));
        $isUpdate = $this->ServicesModel->UpdateUserNotify($where, $update);
        if ($isUpdate) {
            $ExUserSess = $this->session->userdata('ExUserSess');
            $ExUserSess['new_notification'] = 0;
            $this->session->set_userdata('ExUserSess', $ExUserSess);
            return true;
        }
        return false;
    }

}
