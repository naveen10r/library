<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait ServicesMailNotfy
{
    private function ChangePasswordNotification($userdata)
    {
        $mailTemp = $this->MailModel->getAllTemplate(array('unique_title' => 'USR_CHANGE_PWD'));
        $subject = $mailTemp[0]->title;
        $template = $mailTemp[0]->template;
        $Arrkey = array('{%SITE_NAME%}', '{%PK_ID%}', '{%IP_ADDRESS%}', '{%TECH_SUPPORT_EMAIL%}', '{%THANKS_FROM%}', '{%SUPPORT_EMAIL%}', '{%TOLL_FREE_NO%}', '{%SUPPORT_MOBILES%}');
        $arrValue = array(EMAIL_THANKS, $userdata['id'], $userdata['ip'], MOBILE_TECH_SUPPORT, EMAIL_THANKS, EMAIL_SUPPORT, TOLL_FREE_NO, MOBILE_SUPPORT);
        $newText = str_replace($Arrkey,$arrValue, $template);
        $email = array(
            'SUBJECT' => $subject,
            'MESSAGE' => $newText,
            'MAIL_TO' => $userdata['to_email']
        );
        $this->email_lib->send_email($email);
        $Arrkey[] = 'unique_title';
        $arrValue[] = 'USR_CHANGE_PWD';
        $Arrkey[] = 'added_at';
        $arrValue[] = date(SYS_DB_DATE_TIME);
        $logger = array_combine($Arrkey, $arrValue);
        $this->mongodb->insert('send_email_log',$logger);
        return true;
    }
}