<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once realpath(dirname(__FILE__)) . '/ServicesMailNotfy.php';
require_once realpath(dirname(__FILE__)) . '/ServiceNotification.php';
require_once realpath(dirname(__FILE__)) . '/ServiceOrders.php';
class Services extends CI_Controller {

    use ServicesMailNotfy;
    use ServiceNotification;
    use ServiceOrders;
    private $user_id;
    private $user_name;

    public function __construct() {
        parent::__construct();
        if (!empty($this->session->userdata('ExUserSess'))) {
            $ExUserSess = $this->session->userdata('ExUserSess');
            $this->user_id = $ExUserSess['id'];
            $this->user_name = $ExUserSess['name'];
        } else {
            redirect('');
        }
        $this->load->helper(array('site-link/web_view_header', 'string', 'site-link/web_view_footer'));
        $this->load->library(array('email_lib','MongoDb'));
        $this->lang->load(array('itagpolicy'));
        $this->load->model(array('ServicesModel','connect/MailModel'));
    }

    private function MobileVerificationSMS($getMobileOTP) {
        $this->load->model('connect/SmsService');
        $getSMStemp = $this->SmsService->getAllSms(array('id' => 101));
        $template = $getSMStemp[0];
        $text = str_replace(array('{%OTP%}'), array($getMobileOTP['mobile_otp']), $template->template);
        $smsArray = array('numbers' => $getMobileOTP['mobile'], 'text' => $text);
        return sendSMS($smsArray);
    }

    public function resendOTP() {
        $select = array('mobile', 'is_mobile_verify');
        $where = array('id' => $this->user_id);
        $user_data = $this->ServicesModel->getUserData($where, $select, TRUE);
        $return = array();
        if ($user_data->is_mobile_verify == 0) {
            $mobileOTP = rand(10000, 99999);
            $getMobileOTP = array();
            $getMobileOTP['mobile'] = $user_data->mobile;
            $getMobileOTP['mobile_otp'] = $mobileOTP;
            $isSend = $this->MobileVerificationSMS($getMobileOTP);
            if ($isSend[0] == true) {
                $SaveOTP = array();
                $SaveOTP['user_id'] = $this->user_id;
                $SaveOTP['otp'] = $mobileOTP;
                $SaveOTP['otp_for'] = 'M';
                $SaveOTP['otp_for_value'] = $user_data->mobile;
                $SaveOTP['create_at'] = date(SYS_DB_DATE_TIME);
                $SaveOTP['valid_at'] = date(SYS_DB_DATE_TIME, strtotime('+7days'));
                $SaveOTP['is_verify'] = 0;
                $this->ServicesModel->InsertUserOTP($SaveOTP);
                $return = array(
                    'status' => true,
                    'response' => array(
                        'error' => '',
                        'message' => 'SMS sent'
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            }
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => $isSend[1]['code'],
                    'message' => $isSend[1]['message']
                ),
                'code' => 300
            );
            exit(json_encode($return));
        } else {
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => '',
                    'message' => $this->lang->line('err_1003')
                ),
                'code' => 200
            );
            exit(json_encode($return));
        }
    }
    
    private function update_session(string $key, array $update)
    {
        $updateKey = key($update);
        $ExUserSess = $this->session->userdata($key);
        $ExUserSess[$updateKey] = $update[$updateKey];
        $this->session->set_userdata($key, $ExUserSess);
        return true;
    }
    
    public function reg_min_max_date($dob)
    {
        $dob = date('Y-m-d', strtotime($dob));
        $today = date('Y-m-d');
        $end = dateDifference($today, $dob, '');
        if($end->y > 100 OR $end->y < REG_START_YEAR)
        {
            $this->form_validation->set_message('reg_min_max_date', $this->lang->line('err_1004'));
            return false;
        } 
        return true;
    }
    
    public function MyAccount() {
        $select = array('id', 'email', 'mobile', 'is_mobile_verify', 'dob', 'gender', 'name','is_email_verify', 'is_password_auto');
        $where = array('id' => $this->user_id);
        $data['user_data'] = $this->ServicesModel->getUserData($where, $select, TRUE);
        $data['user_name'] = $this->user_name;
        $this->update_session('ExUserSess', array('name' => $data['user_data']->name));
        if (!$this->input->post()) {
            $this->load->view('MyAccount', $data);
        } else {
            $this->form_validation->set_rules('fullname', 'name', 'trim|required|alpha_space');
            $this->form_validation->set_rules('gender', 'gender', 'trim|required');
            $this->form_validation->set_rules('dob', 'dob', 'trim|required|callback_reg_min_max_date');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('MyAccount', $data);
            } else {
                $fullname = $this->input->post('fullname', TRUE);
                $gender = $this->input->post('gender', TRUE);
                $dob = $this->input->post('dob', TRUE);
                $update = array(
                    'name' => ucwords($fullname),
                    'gender' => $gender,
                    'dob' => date(SYS_DB_DATE, strtotime($dob))
                );
                $where = array('id' => $this->user_id);
                $isUpdate = $this->ServicesModel->UpdateUser($where, $update);
                if ($isUpdate) {
                    $ExUserSess = $this->session->userdata('ExUserSess');
                    $ExUserSess['user_name'] = ucwords($fullname);
                    $this->session->set_userdata('ExUserSess', $ExUserSess);
                    $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Update success.'));
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
                }
                redirect('account/Services/MyAccount', 'refresh');
            }
        }
    }

    public function VerifyOTP() {
        $otp = $this->input->post('OTP', TRUE);
        $mobile = $this->input->post('MOBILE', TRUE);
        $this->form_validation->set_rules('OTP', 'otp', 'trim|required|is_natural');
        if ($this->form_validation->run() == FALSE) {
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => '302',
                    'message' => validation_errors()
                ),
                'code' => 200
            );
            exit(json_encode($return));
        }
        $where = array('user_id' => $this->user_id, 'is_verify' => 0);
        $select = array('id', 'otp_for_value', 'otp');
        $verifySelect = $this->ServicesModel->getUserOTP($where, $select);
        if (!empty($verifySelect)) {
            $error = '';
            if ($verifySelect->otp_for_value == $mobile) {
                if ($otp != $verifySelect->otp) {
                    $error = 'OTP not matched!';
                }
            } else {
                $error = $this->lang->line('err_1006');
            }
            if (empty($error)) {
                $where = array('id' => $this->user_id);
                $update = array('is_mobile_verify' => 1);
                $this->ServicesModel->UpdateUser($where, $update);
                $whereOtp = array('id' => $verifySelect->id);
                $updateOtp = array('is_verify' => 1);
                $this->ServicesModel->UpdateUserOTP($whereOtp, $updateOtp);
                $return = array(
                    'status' => true,
                    'response' => array(
                        'error' => '',
                        'message' => 'Mobile verified!'
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            } else {
                $return = array(
                    'status' => false,
                    'response' => array(
                        'error' => '303',
                        'message' => $error
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            }
        }
    }

    public function ChangePassword() {
        $data = array();
        if (!$this->input->post()) {
            $this->load->view('ChangePassword', $data);
        } else {
            $this->form_validation->set_rules('current_pwd', 'current password', 'trim|required');
            $this->form_validation->set_rules('new_pwd', 'new password', 'trim|required|xss_clean|regex_match[/' . PASSWORD_PATTERN . '/]');
            $this->form_validation->set_rules('confirm_pwd', 'confirm password', 'trim|required|xss_clean|matches[new_pwd]');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('ChangePassword', $data);
            } else {
                $current_pwd = $this->input->post('current_pwd', TRUE);
                $new_pwd = $this->input->post('new_pwd', TRUE);
                $select = array('id', 'password', 'email');
                $where = array('id' => $this->user_id);
                $getCuurentPwd = $this->ServicesModel->getUserData($where, $select, TRUE);
                if (!empty($getCuurentPwd)) {
                    if (password_verify($current_pwd, $getCuurentPwd->password)) {
                        $hashPwd = password_hash($new_pwd, PASSWORD_DEFAULT);
                        $update = array('password' => $hashPwd);
                        $isUpdate = $this->ServicesModel->UpdateUser($where, $update);
                        if ($isUpdate) {
                            $notify = array(
                                'id' => $this->user_id,
                                'ip' => $this->input->ip_address(),
                                'to_email' => $getCuurentPwd->email
                            );
                            $this->ChangePasswordNotification($notify);
                            $this->session->set_flashdata('alert', array('color' => 'success', 'responce' => 'Password changed successfully!'));
                        } else {
                            $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
                        }
                    } else {
                        $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1007')));
                    }
                } else {
                    $this->session->set_flashdata('alert', array('color' => 'danger', 'responce' => $this->lang->line('err_1005')));
                }
                redirect('account/Services/ChangePassword');
            }
        }
    }

    public function Addresses() {
        $data['user_name'] = $this->user_name;
        $data['user_id'] = $this->user_id;
        $where = array('ua.user_id' => $this->user_id, 'ua.is_delete' => 0);
        $select = array('ua.id','ua.address_as','ua.address_line_1','ua.address_line_2','ua.address_line_3',
            'ua.city_id','ua.pin_code','ua.landmark','ua.is_current', 'mpc.area_name', 'mpc.pin_code', 
            'mpc.district', 'mpc.state_name AS state');
        $data['all_address'] = $this->ServicesModel->GetAllUserAddress($where, $select);
        $this->load->view('addresses', $data);
    }

    public function AddressAsDefault($address_id)
    {
        list($id, $name) = explode('_', dsecure($address_id));
        $whereALl = array('user_id' => $this->user_id); 
        $updateAll = array('is_current' => 0);
        $where = array('user_id' => $this->user_id, 'id' => $id); 
        $update = array('is_current' => 1);
        $this->ServicesModel->UpdateAddress($whereALl, $updateAll);
        $this->ServicesModel->UpdateAddress($where, $update);
        redirect('account/Services/Addresses');
    }
    
    public function GetPinDetails() {
        $this->form_validation->set_rules('pincode', 'pincode', 'trim|required|is_natural_no_zero|exact_length[6]');
        if ($this->form_validation->run() == false) {
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => validation_errors(),
                    'error_code' => 1001,
                    'message' => ''
                ),
                'code' => 200
            );
            exit(json_encode($return));
        } else {
            $pincode = $this->input->post('pincode', TRUE);
            $select = array('id', 'pin_code', 'area_name', 'district', 'state_name AS state');
            $where = array('pin_code' => $pincode);
            $getPin = $this->ServicesModel->GetPinMasterData($where, $select);
            if (!empty($getPin)) {
                $return = array(
                    'status' => true,
                    'response' => array(
                        'error' => '',
                        'error_code' => 0,
                        'message' => count($getPin) . ' row(s) found!',
                        'data' => $getPin
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            } else {
                $return = array(
                    'status' => false,
                    'response' => array(
                        'error' => $this->lang->line('err_1009'),
                        'error_code' => 0,
                        'message' => '0 row(s) found!'
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            }
        }
    }

    public function SaveAddress() {
        $this->form_validation->set_rules('address_as', 'name', 'trim|required|alpha_space');
        $this->form_validation->set_rules('pin_code', 'pincode', 'trim|required|is_natural_no_zero|exact_length[6]');
        $this->form_validation->set_rules('city_state', 'city & state', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('address_1', 'address line 1', 'trim|required');
        $this->form_validation->set_rules('address_2', 'address line 2', 'trim|xss_clean');
        $this->form_validation->set_rules('address_3', 'address line 3', 'trim|xss_clean');
        $this->form_validation->set_rules('landmark', 'landmark', 'trim|required');
        if ($this->form_validation->run() == false) {
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => validation_errors(),
                    'error_code' => 1001,
                    'message' => ''
                ),
                'code' => 200
            );
            exit(json_encode($return));
        } else {
            $address_as = $this->input->post('address_as', TRUE);
            $pin_code = $this->input->post('pin_code', TRUE);
            $city_state = $this->input->post('city_state', TRUE);
            $address_1 = $this->input->post('address_1', TRUE);
            $address_2 = $this->input->post('address_2', TRUE);
            $address_3 = $this->input->post('address_3', TRUE);
            $landmark = $this->input->post('landmark', TRUE);
            $SubmitFlag = $this->input->post('SubmitFlag', TRUE);
            $isAdd = array(
                'user_id' => $this->user_id,
                'address_as' => ucwords($address_as),
                'address_line_1' => $address_1,
                'address_line_2' => $address_2,
                'address_line_3' => $address_3,
                'city_id' => $city_state,
                'pin_code' => $pin_code,
                'landmark' => $landmark,
                'is_delete' => 0,
                'is_current' => 0
            );
            $isInsert = 0;
            if(empty($SubmitFlag) OR $SubmitFlag == 'save'){
                $isInsert = $this->ServicesModel->insertUserAddress($isAdd);
            }
            if(!empty($SubmitFlag) AND $SubmitFlag == 'update'){
                $SubmitId = $this->input->post('SubmitId', TRUE);
                $isCurrent = $this->input->post('isCurrent', TRUE);
                $isAdd['is_current'] = $isCurrent;
                $where = array('id' => $SubmitId);
                $isInsert = $this->ServicesModel->UpdateAddress($where, $isAdd);
            }
            if($isInsert)
            {
                $message = ($SubmitFlag == 'update') ? 'Upadated successful' : 'Address added.';
                $return = array(
                    'status' => true,
                    'response' => array(
                        'error' => '',
                        'error_code' => 0,
                        'message' => $message
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            } else {
                $return = array(
                    'status' => false,
                    'response' => array(
                        'error' => $this->lang->line('err_1010'),
                        'error_code' => 1001,
                        'message' => ''
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            }
        }
    }
    
    public function EmailForVerify()
    {
        $this->form_validation->set_rules('EmailUserOTP', 'otp', 'trim|required');
        if ($this->form_validation->run() == false) {
            $return = array(
                'status' => false,
                'response' => array(
                    'error' => validation_errors(),
                    'error_code' => 1001,
                    'message' => ''
                ),
                'code' => 200
            );
            exit(json_encode($return));
        } else {
            $where = array(
                'user_id' => $this->user_id,
                'otp_for' => 'E',
                'otp_title' => 'USER_EMAIL_VERIFICATION',
                'is_verify' => 0
            );
            $select = array('id','otp');
            $isExist = $this->ServicesModel->getUserOTP($where, $select);
            if(!empty($isExist))
            {
                $EmailUserOTP = $this->input->post('EmailUserOTP', TRUE);
                if($isExist->otp == $EmailUserOTP)
                {
                    $this->ServicesModel->UpdateUserOTP(array('id' => $isExist->id), array('is_verify' => 1));
                    $this->ServicesModel->UpdateUser(array('id' => $this->user_id), array('is_email_verify' => 1));
                    $return = array(
                        'status' => true,
                        'response' => array(
                            'error' => '',
                            'error_code' => 0,
                            'message' => 'Email Successful verify.'
                        ),
                        'code' => 200
                    );
                    exit(json_encode($return));
                } else {
                    $return = array(
                        'status' => false,
                        'response' => array(
                            'error' => $this->lang->line('err_1011'),
                            'error_code' => 0,
                            'message' => ''
                        ),
                        'code' => 200
                    );
                    exit(json_encode($return));
                }
            } else {
                $return = array(
                    'status' => false,
                    'response' => array(
                        'error' => $this->lang->line('err_1012'),
                        'error_code' => 1003,
                        'message' => ''
                    ),
                    'code' => 200
                );
                exit(json_encode($return));
            }
        }
    }
    
    public function RemoveAddress($address_id)
    {
        echo 'Please wait...';
        list($id, $name) = explode('_', dsecure($address_id));
        $where = array('user_id' => $this->user_id, 'id' => $id); 
        $update = array('is_delete' => 1);
        $this->ServicesModel->UpdateAddress($where, $update);
        redirect('account/Services/Addresses');
    }


    public function DeactivateAccount()
    {
        
    }
    
}
