<?php

class ServicesModel extends TableName
{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getUserData(array $where, array $select=array(), $isRow=false)
    {
        if(!empty($select))
        {
            $this->db->select($select);
        }
        $this->db->from($this->users);
        $this->db->where($where);
        if($isRow)
        {
            return $this->db->get()->row();
        } else {
            return $this->db->get()->result();
        }
    }
    
    
    public function InsertUserOTP($insert)
    {
        $this->db->trans_start();
        $this->db->insert($this->usersOtp,$insert);
        return $this->trans_insert_data();
    }
    
    public function getUserOTP(array $where, array $select=array())
    {
        $this->db->select($select);
        $this->db->from($this->usersOtp);
        $this->db->where($where);
        $this->db->order_by('create_at', 'DESC');
        $this->db->limit(1);
        return $this->db->get()->row();        
    }
    
    public function UpdateUser($where, $update)
    {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->users, $update);
        return $this->trans_update_data();
    }
    
    
    public function UpdateUserOTP($where, $update)
    {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->usersOtp, $update);
        return $this->trans_update_data();
    }
    
    public function GetPinMasterData($where, $select, $isRow=FALSE)
    {
        $this->db->select($select);
        $this->db->from($this->masterPinCode);
        $this->db->where($where);
        if($isRow){
            return $this->db->get()->row();
        } else {
            return $this->db->get()->result();
        }
    }
    
    public function insertUserAddress(array $insert)
    {
        $this->db->trans_start();
        $this->db->insert($this->usersAddress,$insert);
        return $this->trans_insert_data();
    }
    
    public function GetAllUserAddress($where, $select, $isRow=false)
    {
        $this->db->select($select);
        $this->db->from($this->usersAddress.' AS ua');
        $this->db->join($this->masterPinCode.' AS mpc', 'mpc.id = ua.city_id');
        $this->db->where($where);
        if($isRow){
            return $this->db->get()->row();
        } else {
            return $this->db->get()->result();
        }
    }
    
    public function UpdateAddress($where, $update)
    {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->usersAddress, $update);
        return $this->trans_update_data();
    }
    
    public function GetUserNotify($where, $select, array $config=array())
    {
        $this->db->select($select);
        $this->db->from($this->usersNotify.' AS un');
        $this->db->join($this->product.' AS pdt', 'pdt.id = un.is_product','LEFT');
        $this->db->where($where);
        $this->db->order_by('un.create_at', 'DESC');
        if($config['limit']){
            $this->db->limit($config['limit']);
        }
        if($config['is_row']){
            return $this->db->get()->row();
        } else {
            return $this->db->get()->result();
        }
        
    }
    
    
    public function UpdateUserNotify($where, $update)
    {
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($this->usersNotify, $update);
        return $this->trans_update_data();
    }
    
    
}