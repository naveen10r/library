<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . 'My Account' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css'
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js',
            'web_view/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js'
        );
        echo load_js($js);
        ?>
        <style type="text/css">
            .span-href{color: #0044cc; cursor: pointer;}
        </style>
    </head>
    <body class="account-register layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>
        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>

        <!-- ======= Quick view JS ========= -->
        <script>

            function quickbox() {
                if ($(window).width() > 767) {
                    $('.quickview-button').magnificPopup({
                        type: 'iframe',
                        delegate: 'a',
                        preloader: true,
                        tLoading: 'Loading image #%curr%...',
                    });
                }
            }
            jQuery(document).ready(function () {
                quickbox();
            });
            jQuery(window).resize(function () {
                quickbox();
            });

        </script>
        <style type="text/css">
            .validation_error{ color:#FF0000; }
        </style>
        <div id="account-register" class="container">
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Account</a></li>
                <li><a href="#">My Account</a></li>
            </ul>
            <div class="row">
                <aside id="column-left" class="col-sm-3 hidden-xs">
                    <?php account_service('MyAccount') ?>
                    <?php information(); ?>
                </aside>

                <div id="content" class="col-sm-9">
                    <?php
                    echo form_open('account/Services/MyAccount', array('method' => 'post', 'class' => 'form-horizontal'));
                    ?>
                    <fieldset id="account">
                        <legend>My Profile</legend>
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-email">E-Mail </label>
                            <div class="col-sm-6">
                                <input type="email" value="<?php echo @$user_data->email; ?>" class="form-control" readonly="readonly" />
                                <?php
                                if (empty($user_data->is_email_verify) && $user_data->is_password_auto == 1):
                                    ?>
                                    <p>Your email is not verified, please <span class="span-href" data-toggle="modal" data-target="#EmailOTP">click here</span> and verify your email.</p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Name <i class="fa fa-info-circle" title="<?php echo $this->lang->line('itag_name'); ?>"></i></label>
                            <div class="col-sm-6">
                                <input type="text" autocomplete="off" name="fullname" value="<?php @print($user_data->name); ?>" class="form-control" maxlength="24" />
                                <?php echo form_error('fullname', '<div class="validation_error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Mobile No <i class="fa fa-info-circle" title="<?php echo $this->lang->line('itag_mobile'); ?>"></i></label>
                            <div class="col-sm-6">
                                <input type="text" autocomplete="off" value="<?php echo @$user_data->mobile; ?>" class="form-control" maxlength="10" readonly="readonly" />
                                <?php
                                if (empty($user_data->is_mobile_verify)):
                                    ?>
                                    <p>Your mobile no is not verified, please <a href="javascript:void(0);" data-toggle="modal" data-target="#VerifyOTP">click here</a> for verification.</p>
                                <?php endif; ?>
                                <?php echo form_error('mobile_no', '<div class="validation_error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Gender </label>
                            <div class="col-sm-6">
                                <label class="radio-inline"><input type="radio" name="gender" value="M" <?php
                                    if ($user_data->gender == 'M') {
                                        echo 'checked="checked"';
                                    }
                                    ?> /> Male</label>
                                <label class="radio-inline"><input type="radio" name="gender" value="F" <?php
                                    if ($user_data->gender == 'F') {
                                        echo 'checked="checked"';
                                    }
                                    ?> /> Female</label>
                                <label class="radio-inline"><input type="radio" name="gender" value="T" <?php
                                    if ($user_data->gender == 'T') {
                                        echo 'checked="checked"';
                                    }
                                    ?> /> Transgender</label>
                                    <?php echo form_error('gender', '<div class="validation_error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Date of Birth <i class="fa fa-info-circle" title="<?php echo $this->lang->line('itag_dob'); ?>"></i></label>
                            <div class="form-group col-sm-6">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type="text" name="dob" class="form-control" value="<?php echo date(VIEW_DOB, strtotime($user_data->dob)); ?>" readonly="readonly" />

                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <?php echo form_error('dob', '<div class="validation_error">', '</div>'); ?>
                            </div>
                        </div>
                    </fieldset>
                    <div class="buttons">
                        <div class="pull-right">
                            <input type="submit" value="Continue" class="btn btn-primary" />
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <a href="#">Deactivate Account</a>
                </div>
            </div>
        </div>
        <?php footer_tag(); ?>
    </body>
    <!-- Modal -->
    <div class="modal fade" id="VerifyOTP" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:#FFFFFF; font-weight: bold;">Mobile Verification</h5>
                </div>
                <div class="modal-body">
                    <div id="responseId"></div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Mobile No </label>
                        <div class="col-sm-5">
                            <input type="text" autocomplete="off" value="<?php echo @$user_data->mobile; ?>" class="form-control" readonly="readonly" />
                        </div>
                        <div class="col-sm-4">
                            <a href="javascript:void(0)" id="resendOTP">Send OTP</a>
                        </div>
                    </div>
                    <div class="form-group row" id="verifyInput">
                        <label class="col-sm-3 control-label">OTP </label>
                        <div class="col-sm-5">
                            <input type="text" autocomplete="off" class="form-control" id="VerifyUserOTP" minlength="5" maxlength="5" required="required" readonly="readonly" />
                        </div>
                    </div>
                    <p>If you have already a OTP, Please <a href="javascript:void(0);" id="enableVerify">click here</a></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeVerify">Close</button>
                    <button type="button" class="btn btn-primary" id="SubmitOTP">Verify</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="EmailOTP" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:#FFFFFF; font-weight: bold;">Email Verification</h5>
                </div>
                <div class="modal-body">
                    <div id="eresponseId"></div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Email Id </label>
                        <div class="col-sm-5">
                            <input type="text" autocomplete="off" value="<?php echo @$user_data->email; ?>" class="form-control" readonly="readonly" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">OTP </label>
                        <div class="col-sm-5">
                            <input type="text" autocomplete="off" class="form-control" id="EmailUserOTP" />
                        </div>
                    </div>
                    <p>Email OTP sent at the time of registration, Please check your mail.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="SubmitEmailOTP">Verify</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.closeVerify').click(function () {
            location.reload();
        })
        $('#enableVerify').click(function () {
            $('#VerifyUserOTP').prop("readonly", false);
        });
        $('#resendOTP').click(function () {
            $.ajax({
                url: '<?php echo site_url("account/Services/resendOTP"); ?>',
                method: 'post',
                success: function (response) {
                    var jsonData = JSON.parse(response);
                    var alert_flag = 'alert-danger';
                    if (jsonData.status == true)
                    {
                        alert_flag = 'alert-info';
                        $('#VerifyUserOTP').prop("readonly", false);
                        $(this).html('Re-send OTP');
                    }
                    $('#responseId').html('<div class="alert ' + alert_flag + '">' + jsonData.response.message + '</div>');
                }
            });
        });
        $('#SubmitOTP').click(function () {
            var otp = $('#VerifyUserOTP').val();
            var mobile_no = '<?php echo $user_data->mobile; ?>';
            $.ajax({
                url: '<?php echo site_url("account/Services/VerifyOTP"); ?>',
                method: 'post',
                data: {'OTP': otp, 'MOBILE': mobile_no},
                success: function (otp_response) {
                    var jsonData = JSON.parse(otp_response);
                    var alert_flag = 'alert-danger';
                    if (jsonData.status == true)
                    {
                        alert_flag = 'alert-info';
                        $('#SubmitOTP').remove();
                        $('#closeVerify').addClass('closeVerify');
                    }
                    $('#responseId').html('<div class="alert ' + alert_flag + '">' + jsonData.response.message + '</div>');
                }
            });
        });
        $(function () {
            var reg_start_age = new Date("<?php echo REGISTER_START_AGE; ?>");
            var reg_off_age = new Date("<?php echo REGISTER_OFF_AGE; ?>");
            $('#datetimepicker1').datetimepicker({
                minDate: reg_start_age,
                maxDate: reg_off_age,
                format: 'DD-MM-YYYY',
                pickTime: false
            });
        });
        $('#SubmitEmailOTP').click(function(){
            var EmailUserOTP = $('#EmailUserOTP').val();
            $.ajax({
                url: '<?php echo site_url("account/Services/EmailForVerify"); ?>',
                method: 'post',
                data:{'EmailUserOTP':EmailUserOTP},
                success: function (response) {
                    console.log(response);
                    var jsonData = JSON.parse(response);
                    var alert_flag = 'alert-danger';
                    var alert_mgs = '';
                    if (jsonData.status == true)
                    {
                        alert_flag = 'alert-info';
                        alert_mgs = jsonData.response.message;
                    } else {
                        alert_mgs = jsonData.response.error;
                    }
                    $('#eresponseId').html('<div class="alert ' + alert_flag + '">' + alert_mgs + '</div>');
                }
            });
        });
    </script>
</html> 