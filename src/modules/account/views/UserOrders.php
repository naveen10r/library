<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . 'Addresses' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css'
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            /* 'web_view/catalog/javascript/jquery/magnific/jquery.magnific-popup.min.js',  */
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_js($js);
        ?>
        <style type="text/css">
            .steps-form {
                display: table;
                width: 100%;
                position: relative; }
            .steps-form .steps-row {
                display: table-row; }
            .steps-form .steps-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc; }
            .steps-form .steps-row .steps-step {
                display: table-cell;
                text-align: center;
                position: relative; }
            .steps-form .steps-row .steps-step p {
                margin-top: 0.5rem; }
            .steps-form .steps-row .steps-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important; }
            .steps-form .steps-row .steps-step .btn-circle {
                width: 30px;
                height: 30px;
                text-align: center;
                padding: 6px 0;
                font-size: 12px;
                line-height: 1.428571429;
                border-radius: 15px;
                margin-top: 0; }
            </style>
            <script type="text/javascript">
                $(document).ready(function () {
                    var navListItems = $('div.setup-panel div a'),
                            allWells = $('.setup-content'),
                            allNextBtn = $('.nextBtn'),
                            allPrevBtn = $('.prevBtn');

                    allWells.hide();

                    navListItems.click(function (e) {
                        e.preventDefault();
                        var $target = $($(this).attr('href')),
                                $item = $(this);

                        if (!$item.hasClass('disabled')) {
                            navListItems.removeClass('btn-indigo').addClass('btn-default');
                            $item.addClass('btn-indigo');
                            allWells.hide();
                            $target.show();
                            $target.find('input:eq(0)').focus();
                        }
                    });

                    allPrevBtn.click(function () {
                        var curStep = $(this).closest(".setup-content"),
                                curStepBtn = curStep.attr("id"),
                                prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

                        prevStepSteps.removeAttr('disabled').trigger('click');
                    });

                    allNextBtn.click(function () {
                        var curStep = $(this).closest(".setup-content"),
                                curStepBtn = curStep.attr("id"),
                                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                                curInputs = curStep.find("input[type='text'],input[type='url']"),
                                isValid = true;

                        $(".form-group").removeClass("has-error");
                        for (var i = 0; i < curInputs.length; i++) {
                            if (!curInputs[i].validity.valid) {
                                isValid = false;
                                $(curInputs[i]).closest(".form-group").addClass("has-error");
                            }
                        }

                        if (isValid)
                            nextStepWizard.removeAttr('disabled').trigger('click');
                    });

                    $('div.setup-panel div a.btn-indigo').trigger('click');
                });
            </script>
        </head>
        <body class="account-register layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>
        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>

        <style type="text/css">
            .validation_error{ color:#FF0000; }
        </style>
        <?php
        if ($this->session->flashdata('alert')) {
            $alert = $this->session->flashdata('alert');
            echo $alert['color']($alert['responce']);
        }
        ?>
        <div id="account-register" class="container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Account</a></li>
                <li><a href="#">My Order(s)</a></li>
            </ul>
            <div class="row">
                <aside id="column-left" class="col-sm-3 hidden-xs">
                    <?php account_service('My Order', true) ?>
                    <?php information(); ?>
                </aside>
                <div id="content" class="col-sm-9">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo site_url('my-orders/all-orders'); ?>" <?php
                            if ($SubTitle == 'all-orders') {
                                echo 'style="color:#e91e76;"';
                            }
                            ?>>All Orders</a></li>
                        <li><a href="<?php echo site_url('my-orders/open-orders'); ?>" <?php
                            if ($SubTitle == 'open-orders') {
                                echo 'style="color:#e91e76;"';
                            }
                            ?>>Open Orders</a></li>
                        <li><a href="<?php echo site_url('my-orders/cancel-orders'); ?>" <?php
                            if ($SubTitle == 'cancel-orders') {
                                echo 'style="color:#e91e76;"';
                            }
                            ?>>Cancel Orders</a></li>
                        <li><a href="<?php echo site_url('my-orders/return-refund'); ?>" <?php
                            if ($SubTitle == 'return-refund') {
                                echo 'style="color:#e91e76;"';
                            }
                            ?>>Return/Refund Orders</a></li>
                    </ul>
                    <div class="panel-default">
                        <?php
                        for ($i = 0; $i < 10; $i++) {
                            $notifyDiv = '';
                            $notifyDiv .= '<div class="panel panel-default">';
                            $notifyDiv .= '<div class="panel-body">';
                            /* NOTIFY TITLE */
                            $notifyDiv .= '<div class="blog-left-content">';
                            $notifyDiv .= '<div class="row">';
                            $notifyDiv .= '<div class="col-md-2"><strong>ORDER PLACED</strong> <br />23-June-2020</div>';
                            $notifyDiv .= '<div class="col-md-2"><strong>TOTAL AMOUNT</strong> <br />' . RUPEE_LOGO . ' 2343.00</div>';
                            $notifyDiv .= '<div class="col-md-3"><strong>SHIP TO</strong> <br /><a href="#" data-toggle="tooltip" title="Naveen Rastogi Villa No C-6, Kamakhya Villa Society Sector-4, Shahberi Greater Noida West, UTTAR PRADESH 201016 Phone: 9015473225" data-placement="bottom">Naveen Rastogi <i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a></div>';
                            $notifyDiv .= '<div class="col-md-5 pull-right"><strong>ORDER #</strong> 102019299191 <br /><a href="#">Order Details</a> | <a href="#" >Download Invoice</a></div>';
                            $notifyDiv .= '</div>';
                            $notifyDiv .= '<hr />';
                            $notifyDiv .= '<h5 class="blog-title"><a href="#">Programming Language C by Balaguruswami</a></h5>';
                            $notifyDiv .= '</div>';
                            /* NOTIFY DESCRIPTION */
                            $notifyDiv .= '<div class="blog-right-content">';
                            $notifyDiv .= '<div class="blog-date-comment">';
                            $notifyDiv .= '<div class="blog-date">Delivered : 24-June-2020</div>';
                            $notifyDiv .= '<div class="comment-wrapper">';
                            $notifyDiv .= '<div class="write-comment"><a href="#">Return window closed on 21-Jun-2020</a></div>';
                            $notifyDiv .= '</div>';
                            $notifyDiv .= '</div>';
                            $notifyDiv .= '</div>';

                            $notifyDiv .= '<div class="blog-desc">';
                            $notifyDiv .= '<br />';
                            $notifyDiv .= '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#TrackPath">Track Order</button>&nbsp;';
                            $notifyDiv .= '<button type="button" class="btn btn-primary">Return/Replace</button>&nbsp;';
                            $notifyDiv .= '<button type="button" class="btn btn-primary">Archive</button>&nbsp;';
                            $notifyDiv .= '<button type="button" class="btn btn-primary">Buy Again</button>&nbsp;';
                            $notifyDiv .= '<button type="button" class="btn btn-primary">Leave Issue</button>&nbsp;';
                            $notifyDiv .= '<button type="button" class="btn btn-primary">0 Comment</button>';
                            $notifyDiv .= '</div>';

                            $notifyDiv .= '</div>';
                            $notifyDiv .= '</div>';
                            echo $notifyDiv;
                        }
                        ?>
                    </div>

                    <nav aria-label="My-Order">
                        <ul class="pagination pull-right">
                            <li class="page-item disabled">
                                <span class="page-link">Previous</span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active">
                                <span class="page-link">
                                    2
                                    <span class="sr-only">(current)</span>
                                </span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="TrackPath"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style="color:#fff;">Shipment Details : Programming Language C By Balaguruswami</h5>
                    </div>
                    <div class="modal-body">
                        <!-- Steps form -->
                        <div class="card">
                            <div class="card-body mb-4">
                                <hr class="my-5">
                                <!-- Stepper -->
                                <div class="steps-form">
                                    <div class="steps-row setup-panel">
                                        <div class="steps-step">
                                            <span class="btn btn-success btn-circle" disabled="disabled">1</span>
                                            <p>Ordered</p>
                                        </div>
                                        <div class="steps-step">
                                            <span class="btn btn-success btn-circle" disabled="disabled">2</span>
                                            <p>Packed</p>
                                        </div>
                                        <div class="steps-step">
                                            <span class="btn btn-success btn-circle" disabled="disabled">3</span>
                                            <p>Shipped</p>
                                        </div>
                                        <div class="steps-step">
                                            <span class="btn btn-success btn-circle">4</span>
                                            <p>Delivered</p>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td>23-Jun-2020</td>
                                            <td>11:09 AM</td>
                                            <td>Your order has been handed to courier partner.</td>
                                        </tr>
                                        <tr>
                                            <td>23-Jun-2020</td>
                                            <td>05:19 PM</td>
                                            <td>Your order is out for delivery.</td>
                                        </tr>
                                        <tr>
                                            <td>24-Jun-2020</td>
                                            <td>10:24 AM</td>
                                            <td>Your order has been delivered.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Steps form -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <?php footer_tag(); ?>
    </body>
</html>
