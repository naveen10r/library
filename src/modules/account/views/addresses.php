<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . 'Addresses' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css'
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            /* 'web_view/catalog/javascript/jquery/magnific/jquery.magnific-popup.min.js',  */
            'web_view/catalog/view/javascript/common.js',
            'bootbox/bootbox.all.min.js'
        );
        echo load_js($js);
        ?>
    </head>
    <body class="account-register layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>
        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>

        <style type="text/css">
            .validation_error{ color:#FF0000; }
        </style>
        <div id="account-register" class="container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Account</a></li>
                <li><a href="#">Address Book</a></li>
            </ul>
            <div class="row">
                <aside id="column-left" class="col-sm-3 hidden-xs">
                    <?php account_service('Addresses') ?>
                    <?php information(); ?>
                </aside>

                <div id="content" class="col-sm-9">
                    <?php
                    echo form_open('account/Services/ChangePassword', array('method' => 'post', 'class' => 'form-horizontal'));
                    ?>
                    <fieldset id="account">
                        <legend>Address(s)</legend>
                        <p>If you want to add new address, please <a href="javascript:void(0)" data-toggle="modal" data-target="#AddAddress">click here</a>.</p>
                        <?php
                        if ($this->session->flashdata('alert')) {
                            $alert = $this->session->flashdata('alert');
                            echo $alert['color']($alert['responce']);
                        }
                        ?>
                        <?php
                        if (!empty($all_address)) {
                            foreach ($all_address as $ads) {
                                $forUpdate = json_encode($ads);
                                $key = esecure($ads->id . '_' . $user_name);
                                $default = '';
                                $default = '<a class="pull-right" href="' . site_url("account/Services/AddressAsDefault/" . $key) . '" style="font-weight:bold; color:#4444dc;">MAKE AS DEFAULT</a>';
                                if ($ads->is_current == 1) {
                                    $default = '<span class="pull-right" style="font-weight:bold; color:#e91e76;">DEFAULT ADDRESS</span>';
                                }
                                $addBr = '';
                                if(strlen($ads->address_line_1 . ', ' . $ads->address_line_2 . ', ' . $ads->address_line_3) < 60)
                                {
                                    $addBr = '<span><br /></span>';
                                }
                                $address = $ads->address_line_1 . ', ' . $ads->address_line_2 . ', ' . $ads->address_line_3 . $addBr .'<br /><strong>Area : </strong>' . $ads->area_name . '<br /><strong>City & State : </strong>' . $ads->district . ', ' . $ads->state . '<br /><strong>Pin Code : </strong>' . $ads->pin_code;

                                // ' . site_url("account/Services/RemoveAddress/" . $key) . '
                                echo '<div class="col-sm-6"><input type="hidden" id="edit_data_'.$ads->id.'" value=\'' . $forUpdate . '\' /><div class="well"><p><strong>' . $ads->address_as . '</strong>' . $default . '</p><p>' . $address . '</p><a href="" class="btn btn-success" data-toggle="modal" data-target="#AddAddress" onclick="return ChangeTheAddress(\'' . $ads->id . '\');">Update</a><span class="btn btn-danger pull-right delete-address" data-value="'.$key.'">Delete</span></div></div>';
                            }
                        }
                        ?>

                    </fieldset>
<?php echo form_close(); ?>
                </div>
            </div>
        </div>
<?php footer_tag(); ?>
    </body>
    <!-- Modal -->
    <div class="modal fade" id="AddAddress" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:#FFFFFF; font-weight: bold;">Add Address</h5>
                </div>
                <div class="modal-body">
                    <div id="responseId"></div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Name </label>
                        <div class="col-sm-7">
                            <input type="text" autocomplete="off" class="form-control" id="address_as" value="<?php echo $user_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Pin Code </label>
                        <div class="col-sm-7">
                            <input type="text" autocomplete="off" class="form-control" id="pin_code" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">City & State</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="city_state">
                                <option value="0">--- select ---</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Address Line 1</label>
                        <div class="col-sm-7">
                            <input type="text" autocomplete="off" class="form-control" id="address_1" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Address Line 2</label>
                        <div class="col-sm-7">
                            <input type="text" autocomplete="off" class="form-control" id="address_2" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Address Line 3</label>
                        <div class="col-sm-7">
                            <input type="text" autocomplete="off" class="form-control" id="address_3" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Landmark</label>
                        <div class="col-sm-7">
                            <input type="text" autocomplete="off" class="form-control" id="landmark" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="SubmitId" value="0" />
                    <input type="hidden" id="isCurrent" value="0" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="SubmitAdd" value="save">Save</button>
                </div>
            </div>
        </div>
    </div>        
    <script type="text/javascript">
        $('#pin_code').on('blur', function () {
            var pincode = $('#pin_code').val();
            var postalUrl = '<?php echo site_url("account/Services/GetPinDetails"); ?>';
            $.ajax({
                url: postalUrl,
                method: 'POST',
                data: {'pincode': pincode},
                success: function (pin_details) {
                    var JsonData = JSON.parse(pin_details);
                    if (JsonData.status == true)
                    {
                        var PostOffice = JsonData.response.data;
                        for (var i = 0; i < PostOffice.length; i++)
                        {
                            $('#city_state').html('');
                            var city_state = '<option value="' + PostOffice[i].id + '">' + PostOffice[i].area_name + ', ' + PostOffice[i].district + ', ' + PostOffice[i].state + '</option>';
                            $('#city_state').append(city_state);
                        }
                    } else {
                        $('#city_state').html('');
                        $('#city_state').append('<option value="0">' + JsonData.response.error + '</option>');
                    }
                }
            });
        });

        $('#SubmitAdd').click(function () {
            var address_as = $('#address_as').val();
            var pin_code = $('#pin_code').val();
            var city_state = $('#city_state').val();
            var address_1 = $('#address_1').val();
            var address_2 = $('#address_2').val();
            var address_3 = $('#address_3').val();
            var landmark = $('#landmark').val();
            var SubmitFlag = $(this).val();
            var SubmitId = $('#SubmitId').val();
            var isCurrent = $('#isCurrent').val();

            $.ajax({
                url: '<?php echo site_url("account/Services/SaveAddress"); ?>',
                method: 'POST',
                data: {'address_as': address_as, 'pin_code': pin_code, 'city_state': city_state, 'address_1': address_1, 'address_2': address_2, 'address_3': address_3, 'landmark': landmark, 'SubmitFlag' : SubmitFlag, 'SubmitId' : SubmitId, 'isCurrent' : isCurrent},
                success: function (save_add) {
                    var JsonData = JSON.parse(save_add);
                    var alert_class = '';
                    var alert_response = '';
                    if (JsonData.status === true)
                    {
                        alert_class = 'alert-success';
                        alert_response = JsonData.response.message;
                    } else {
                        alert_class = 'alert-danger';
                        alert_response = JsonData.response.error;
                    }
                    $('#responseId').html('');
                    $('#responseId').html('<div class="alert ' + alert_class + '">' + alert_response + '</div>');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
            });
        });

        function ChangeTheAddress(key_id)
        {
            var edit_data = $('#edit_data_'+key_id).val();
            var edit_data_obj = JSON.parse(edit_data);
            var option = '';
            option = '<option value="'+edit_data_obj.city_id+'">'+edit_data_obj.area_name+', '+edit_data_obj.district+', '+edit_data_obj.state+'</option>';
            $('#address_as').val(edit_data_obj.address_as);
            $('#pin_code').val(edit_data_obj.pin_code);
            $('#city_state').html('');
            $('#city_state').append(option);
            $('#address_1').val(edit_data_obj.address_line_1);
            $('#address_2').val(edit_data_obj.address_line_2);
            $('#address_3').val(edit_data_obj.address_line_3);
            $('#landmark').val(edit_data_obj.landmark);
            $('#SubmitId').val(edit_data_obj.id);
            $('#isCurrent').val(edit_data_obj.is_current);
            $('#SubmitAdd').val('update');
            $('#SubmitAdd').html('Update');
        }
        
        $('.delete-address').click(function(){
            var DeleteReq = $(this).data('value');
            bootbox.confirm({
                message: "Are you sure want to delete this address ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result === true){
                        window.location.href = "<?php echo site_url("account/Services/RemoveAddress/") ?>" + DeleteReq;
                    }
                }
            });
        });
    </script>
</html>
