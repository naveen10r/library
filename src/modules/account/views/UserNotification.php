<!DOCTYPE html>
<html dir="ltr" lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . 'Addresses' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <?php
        $css = array(
            'web_view/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/stylesheet.css',
            'web_view/catalog/view/javascript/jquery/magnific/magnific-popup.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css',
            'web_view/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css'
        );
        echo load_css($css);
        $js = array(
            'web_view/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            'web_view/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
            'web_view/catalog/view/javascript/codezeel/custom.js',
            'web_view/catalog/view/javascript/codezeel/jstree.min.js',
            'web_view/catalog/view/javascript/codezeel/carousel.min.js',
            'web_view/catalog/view/javascript/codezeel/codezeel.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.custom.min.js',
            'web_view/catalog/view/javascript/codezeel/jquery.formalize.min.js',
            'web_view/catalog/view/javascript/lightbox/lightbox-2.6.min.js',
            'web_view/catalog/view/javascript/codezeel/tabs.js',
            'web_view/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js',
            'web_view/catalog/view/javascript/codezeel/bootstrap-notify.min.js',
            'web_view/catalog/view/javascript/codezeel/doubletaptogo.js',
            'web_view/catalog/view/javascript/codezeel/owl.carousel.min.js',
            /* 'web_view/catalog/javascript/jquery/magnific/jquery.magnific-popup.min.js',  */
            'web_view/catalog/view/javascript/common.js'
        );
        echo load_js($js);
        ?>
    </head>
    <body class="account-register layout-2 left-col">
        <header>
            <?php header_container(); ?>

            <?php header_top_inner(); ?>
        </header>
        <div class="wrap-breadcrumb parallax-breadcrumb">
            <div class="container"></div>
        </div>

        <style type="text/css">
            .validation_error{ color:#FF0000; }
        </style>
        <?php
        if ($this->session->flashdata('alert')) {
            $alert = $this->session->flashdata('alert');
            echo $alert['color']($alert['responce']);
        }
        ?>
        <?php
        $notifyDiv = '';
        $isVerify = [];
        if ($notify):
            foreach ($notify as $newNotify):
                $isVerify[] = $newNotify->is_visible;
                $notifyDiv .= '<div class="panel panel-default">';
                $notifyDiv .= '<div class="panel-body">';
                /* NOTIFY TITLE */
                $notifyDiv .= '<div class="blog-left-content">';
                $notifyDiv .= '<h5 class="blog-title"><a href="#">' . $newNotify->title . '</a></h5>';
                $notifyDiv .= '</div>';
                /* NOTIFY DESCRIPTION */
                $notifyDiv .= '<div class="blog-right-content">';
                $notifyDiv .= '<div class="blog-date-comment">';
                $notifyDiv .= '<div class="blog-date">' . date(SYS_DATE_TIME, strtotime($newNotify->create_at)) . '</div>';
                $notifyDiv .= '<div class="comment-wrapper">';
                $notifyDiv .= '<div class="write-comment-count"> 0  Comment </div>';
                $notifyDiv .= '<div class="write-comment"><a href="#">Leave Issue</a></div>';
                $notifyDiv .= '</div>';
                $notifyDiv .= '</div>';
                $notifyDiv .= '</div>';

                $notifyDiv .= '<div class="blog-desc">';
                if (strlen($newNotify->description) < 160):
                    $notifyDiv .= $newNotify->description;
                else :
                    $notifyDiv .= substr($newNotify->description, 0, 160) . '...';
                    $notifyDiv .= '<br /><div class="read-more pull-right"> <a href="#"  class="btn btn-default"> <i class="fa fa-link"></i> More Details</a> </div>';
                endif;
                $notifyDiv .= '</div>';

                $notifyDiv .= '</div>';
                $notifyDiv .= '</div>';
            endforeach;
        else :
            $notifyDiv .= '<div class="panel panel-default"><div class="panel-body"><div class="blog-desc">No any notification</div></div></div>';
        endif;
        ?>
        <div id="account-register" class="container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Account</a></li>
                <li><a href="#">All Notifications</a></li>
            </ul>
            <div class="row">
                <aside id="column-left" class="col-sm-3 hidden-xs">
                    <?php account_service('notification', true) ?>
                    <?php information(); ?>
                </aside>

                <div id="content" class="col-sm-9">
                    <h1>All Notifications</h1>
                    <div class="panel-default">
                        <?php echo $notifyDiv; ?>
                    </div>

                    <nav aria-label="My-Order">
                        <ul class="pagination pull-right">
                            <li class="page-item disabled">
                                <span class="page-link">Previous</span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active">
                                <span class="page-link">
                                    2
                                    <span class="sr-only">(current)</span>
                                </span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <?php footer_tag(); ?>
    </body>
</html>
