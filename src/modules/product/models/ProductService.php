<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ProductService extends TableName {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function GetProductDetails(array $where, array $select=array(), $isRow=true){
        if(!empty($select))
        {
            $this->db->select($select, false);
        }
        $this->db->from($this->product);
        $this->db->where($where);
        if($isRow)
        {
            return $this->db->get()->row();
        } else {
            return $this->db->get()->result();
        }
    }
    
    public function ProductDetails(array $where=array()){
        $where['bpm.is_visible'] = 'Y';
        $where['bp.is_delete'] = 0;
        $this->db->select('bp.id, bp.date, bp.content, bp.publish_year, bp.publisher_id, bp.title, bp.isbn, bp.buying_mode');
        $this->db->select('bp.baseprice, bp.offerprice, bp.mrp, bp.meta_description, bp.meta_keywords, bp.cover_image');
        $this->db->select('ba.author_image AS author_pic');
        $this->db->select('GROUP_CONCAT(DISTINCT bpc.name) AS category_name', FALSE);
        $this->db->select('GROUP_CONCAT(DISTINCT bpc.id) AS category_id', FALSE);
        $this->db->select("GROUP_CONCAT(DISTINCT ba.name,  '__#__', ba.id, '__#__', ba.slug) AS author_name_id", FALSE);
        $this->db->select("GROUP_CONCAT(DISTINCT bpm.file_for_label,  '__#__', bpm.file_name, '__#__', REGEXP_REPLACE(bpm.client_name, '[^[:alnum:]]+', ' '), '__#__', bpm.full_path) AS image_info", FALSE);
        $this->db->from($this->product.' AS bp');
        $this->db->join($this->product_category_junction.' AS bpcj','bpcj.product_id = bp.id','LEFT');
        $this->db->join($this->productCategory.' AS bpc','bpc.id = bpcj.category_id','LEFT');        
        $this->db->join($this->product_author_junction.' AS bpaj','bpaj.product_id = bp.id','LEFT');
        $this->db->join($this->author.' AS ba','ba.id = bpaj.author_id','LEFT');
        $this->db->join($this->productMedia.' AS bpm','bpm.product_id = bp.id','LEFT');
        $this->db->where($where);
        return $this->db->get()->row();
    }
    
    public function ProductByCategoryId(string $array_in, $notIn){
        $this->db->select('bp.id AS pid, bp.title, bp.cover_image, bp.mrp, bp.buying_mode, bp.offerprice, bp.author_id');
        $this->db->from($this->product_category_junction.' AS bpcj');
        $this->db->join($this->product.' AS bp','bp.id = bpcj.product_id');
        $this->db->where_in('bpcj.category_id', explode(',', $array_in));
        $this->db->where(array('bp.id !=' => $notIn, 'bp.cover_image !=' => ''));
        $this->db->group_by('bp.id');
        $this->db->order_by('RAND()',FALSE);
        $this->db->limit(7);
        return $this->db->get()->result();
    }
    
    public function geProductMedia(array $where, array $select = array()){
        $where['is_visible'] = 'Y';
        $where['is_delete'] = 'N';
        $this->db->select($select);
        $this->db->from($this->productMedia);
        $this->db->where($where);
        return $this->db->get()->result();
    }
    
    public function RequesterDetails(array $insert):int
    {
        $this->db->trans_start();
        $this->db->insert($this->productRequester, $insert);
        return $this->trans_insert_data();
    }
    
    public function SaveProductReview(array $insert): int {
        $this->db->trans_start();
        $this->db->insert($this->productReview, $insert);
        return $this->trans_insert_data();
    }
    
    public function GetMoreProduct(array $where, array $select=array(), $start=0, $join=false){
        $this->db->select($select);
        $this->db->from($this->product.' AS bp');
        if($join){
            $this->db->join($this->product_category_junction.' AS bpcj', 'bpcj.product_id = bp.id', 'LEFT');
            $this->db->join($this->productCategory.' AS bpc', 'bpc.id = bpcj.category_id', 'LEFT');
        }
        if(!empty($where['bp.title'])){
            $this->db->like('bp.title', $where['bp.title']);
            unset($where['bp.title']);
        }
        $this->db->where($where);
        $this->db->group_by('bp.id');
        $this->db->order_by('bp.modified_at','DESC');
        $this->db->limit(HOME_PAGE_PER_RECORDS, $start);
        return $this->db->get()->result();
    }
    
    public function SaveSubscriber(array $insert)
    {
        $this->load->library('user_agent');
        $insert['insert_at'] = date(SYS_DB_DATE_TIME);
        $insert['insert_from'] = $this->input->ip_address();
        $insert['insert_addr'] = $this->agent->agent_string();
        $insert['unsubscribe'] = 0;
        $this->db->trans_start();
        $this->db->insert($this->subscribe, $insert);
        return $this->trans_insert_data();
    }
    
    public function get_product_category(array $where=array(), $like=''){
        $where['is_active'] = 1;
        $this->db->select(array('id', 'name', 'slug', 'description'));
        $this->db->from($this->productCategory);
        $this->db->where($where);
        if (!empty($like)){
            $this->db->like('slug', $like, 'after', false);
        } else {
            $this->db->where('parent_id', NULL);
        }
        $this->db->order_by('name', 'ASC');
        return $this->db->get()->result();
    }
}
