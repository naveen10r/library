<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    private $login_user_id = '';
    private $login_user_name = '';

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('site-link/menus', 'site-link/web_view_header', 'site-link/web_view_footer', 'captcha_security'));
        $this->load->model(array('home/HomeService', 'ProductService', 'connect/MailModel'));
        $this->lang->load('itagpolicy');
        $this->load->library(array('email_lib'));
        if (!empty($this->session->userdata('ExUserSess'))) {
            $ExUserSess = $this->session->userdata('ExUserSess');
            $this->login_user_id = $ExUserSess['id'];
            $this->login_user_name = $ExUserSess['name'];
        }
    }

    public function index() {
        $data = array();
        $this->load->view('landing', $data);
    }

    private function load_captcha() {
        $captcha = captcha_security();
        $this->session->set_userdata('captcha_product_request', array('word' => $captcha['word'], 'create_ts' => $captcha['time']));
        return $captcha;
    }

    public function product_details() {
        $data['captcha'] = $this->load_captcha();
        $select = array('pcc.id', 'pcc.name', 'pcc.slug');
        $isChild = FALSE;
        $where = array('pcc.slug != ' => '', 'pcc.parent_id' => NULL, 'pcc.is_active' => 1);
        $data['all_category'] = $this->HomeService->GetAllCategory($where, $select, $isChild);
        $product_id = $this->uri->segment(2);
        $pid = @end(explode('-', $product_id));
        $pmwhere = array('bp.id' => toNum($pid));
        $data['pdetails'] = $this->ProductService->ProductDetails($pmwhere);
        $category_ids = $data['pdetails']->category_id;
        $data['similarP'] = array();
        if (!empty($category_ids)) {
            $data['similarP'] = $this->ProductService->ProductByCategoryId($category_ids, dsecure($pid));
        }
        $AllAuthor = $this->HomeService->GetAllAuthor(array('slug !=' => '', 'is_active' => 1));
        $authorList = [];
        foreach ($AllAuthor as $author) {
            $authorList[$author->id] = $author;
        }
        $data['authorList'] = $authorList;
        $this->load->view('product_details', $data);
    }

    public function save_request() {
        $captchaProductRequest = $this->session->userdata('captcha_product_request');
        $this->session->unset_userdata('captcha_product_request');
        $requester_name = $this->input->post('requester_name', TRUE);
        $requester_email = $this->input->post('requester_email', TRUE);
        $requester_mobile = $this->input->post('requester_mobile', TRUE);
        $requester_txt = $this->input->post('requester_txt', TRUE);
        $requester_captch = $this->input->post('requester_captch', TRUE);
        $product_id = $this->input->post('product_id', TRUE);
        $product_title = $this->input->post('product_title', TRUE);
        $captcha = $this->load_captcha();
        unset($captcha['word']);
        if ($captchaProductRequest['word'] != $requester_captch) {
            $responce = array(
                'status' => FALSE,
                'responce' => array(
                    'error' => 'Captcha code not matched!',
                    'message' => '',
                    'data' => $captcha
                ),
                'code' => 1001
            );
            $this->ajaxResponse($responce);
        }
        $this->form_validation->set_rules('requester_name', 'name', 'required|trim|xss_clean|min_length[3]|regex_match[/' . NAME_PATTERN . '/]');
        $this->form_validation->set_rules('requester_email', 'email', 'required|trim|xss_clean|valid_email');
        $this->form_validation->set_rules('requester_mobile', 'mobile', 'required|trim|xss_clean|regex_match[/' . FOR_REG_MOBILE . '/]');
        $this->form_validation->set_rules('requester_txt', 'message', 'trim|xss_clean|max_length[250]');
        if ($this->form_validation->run() == false) {
            $responce = array(
                'status' => FALSE,
                'responce' => array(
                    'error' => validation_errors(),
                    'message' => '',
                    'data' => $captcha
                ),
                'code' => 1002
            );
            $this->ajaxResponse($responce);
        } else {
            $saveRequest = array(
                'ur_product_id' => $product_id,
                'ur_name' => $requester_name,
                'ur_email' => $requester_email,
                'ur_mobile' => $requester_mobile,
                'ur_message' => $requester_txt,
                'ur_create_at' => date(SYS_DB_DATE_TIME),
                'ur_is_response' => 'N',
                'ur_response_text' => NULL,
                'ur_response_at' => NULL
            );
            $isSave = $this->ProductService->RequesterDetails($saveRequest);
            if ($isSave) {
                $saveRequest['p_title'] = $product_title;
                $this->acknowledgement($saveRequest);
                $this->notify_team($saveRequest);
                $responce = array(
                    'status' => TRUE,
                    'responce' => array(
                        'error' => '',
                        'message' => 'Thank you showing interest for buying this book.',
                        'data' => $captcha
                    ),
                    'code' => 200
                );
                $this->ajaxResponse($responce);
            } else {
                $responce = array(
                    'status' => FALSE,
                    'responce' => array(
                        'error' => 'Something went wrong. Please try again.',
                        'message' => '',
                        'data' => $captcha
                    ),
                    'code' => 1003
                );
                $this->ajaxResponse($responce);
            }
        }
    }

    private function ajaxResponse($data) {
        exit(json_encode($data));
    }

    private function acknowledgement(array $userdata) {
        $mailTemp = $this->MailModel->getAllTemplate(array('unique_title' => 'PRODUCT_ACK_TO_CUSTOMER'));
        $subject = $mailTemp[0]->title;
        $template = $mailTemp[0]->template;
        $pDetails = preety_url($userdata['p_title']) . '-' . toAlpha($userdata['ur_product_id']);
        $product_url = site_url('product-details/' . $pDetails);
        $mail_txt = str_replace(
                array('{%USER_NAME%}', '{%PRODUCT_TITLE%}', '{%SITE_NAME%}'),
                array($userdata['ur_name'], '<a href="' . $product_url . '">' . $userdata['p_title'] . '</a>', EMAIL_THANKS),
                $template
        );
        $ack_mail = array('SUBJECT' => $subject, 'MESSAGE' => $mail_txt, 'MAIL_TO' => $userdata['ur_email']);
        $res = $this->email_lib->send_email($ack_mail);
        if ($res) {
            return ['status' => true, 'response' => $res];
        } else {
            return ['status' => false, 'response' => $res];
        }
    }

    private function notify_team(array $userdata) {
        $mailTemp = $this->MailModel->getAllTemplate(array('unique_title' => 'NEW_BOOK_BUYING_REQUEST'));
        $subject = $mailTemp[0]->title;
        $template = $mailTemp[0]->template;
        $pDetails = preety_url($userdata['p_title']) . '-' . toAlpha($userdata['ur_product_id']);
        $product_url = site_url('product-details/' . $pDetails);
        $mail_txt = str_replace(
                array('{%PRODUCT_DETAILS%}', '{%USER_NAME%}', '{%USER_EMAIL%}', '{%USER_MOBILE%}', '{%MESSAGE%}', '{%THANKS_FROM%}'),
                array('<a href="' . $product_url . '">' . $userdata['p_title'] . '</a>', $userdata['ur_name'], $userdata['ur_email'], $userdata['ur_mobile'], $userdata['ur_message'], EMAIL_THANKS),
                $template
        );
        $modify_subject = str_replace(
                array('{%USER_NAME%}', '{%PRODUCT_NAME%}'),
                array(strtoupper($userdata['ur_name']), $userdata['p_title']),
                $subject
        );
        $ack_mail = array('SUBJECT' => $modify_subject, 'MESSAGE' => $mail_txt, 'MAIL_TO' => EMAIL_BOOK_BUYING_REQUEST);
        $res = $this->email_lib->send_email($ack_mail);
        if ($res) {
            return ['status' => true, 'response' => $res];
        } else {
            return ['status' => false, 'response' => $res];
        }
    }

    public function save_review() {
        $this->form_validation->set_rules('reviewer_text', 'your review', 'trim|xss_clean|required|max_length[500]|alpha_numeric_spaces|min_length[3]');
        $this->form_validation->set_rules('reviewer_rating', 'your rating', 'trim|xss_clean|required|less_than[6]|greater_than[0]|is_natural_no_zero');
        if ($this->form_validation->run() == false) {
            $responce = array(
                'status' => FALSE,
                'responce' => array(
                    'error' => validation_errors(),
                    'message' => '',
                    'data' => array()
                ),
                'code' => 1002
            );
            $this->ajaxResponse($responce);
        } else {
            $review = $this->input->post('reviewer_text', TRUE);
            $rating = $this->input->post('reviewer_rating', TRUE);
            $product_id = $this->input->post('product_id', TRUE);
            $saveReview = array(
                'product_id' => $product_id,
                'user_id' => $this->login_user_id,
                'review' => htmlentities($review),
                'star_point' => $rating,
                'create_at' => date(SYS_DB_DATE_TIME),
                'is_active' => 0,
            );
            $isSave = $this->ProductService->SaveProductReview($saveReview);
            if ($isSave) {
                $responce = array(
                    'status' => TRUE,
                    'responce' => array(
                        'error' => '',
                        'message' => 'Thank you post you review & reting for this book.',
                        'data' => array()
                    ),
                    'code' => 200
                );
                $this->ajaxResponse($responce);
            } else {
                $responce = array(
                    'status' => FALSE,
                    'responce' => array(
                        'error' => $this->lang->line('err_1005'),
                        'message' => '',
                        'data' => array()
                    ),
                    'code' => 1003
                );
                $this->ajaxResponse($responce);
            }
        }
    }

    public function load_more() {
        $start = $this->input->post('start_point');
        if (!empty($start) && is_numeric($start) && $start > 0) {
            $join = false;
            $category_id = $this->input->post('category_id');
            $query_search = $this->input->post('query_search');
            $selectL = array('bp.title', 'bp.author_id', 'bp.offerprice', 'bp.mrp', 'bp.id', 'bp.status', 'bp.cover_image', 'bp.buying_mode');
            $whereL = array('bp.status' => 'publish', 'bp.cover_image !=' => '');
            if (empty($query_search)) {
                if (!empty($category_id) && is_numeric($category_id) && $category_id > 0) {
                    $join = true;
                    $whereL['bpc.id'] = $category_id;
                }
            } else {
                if (!empty($query_search)) {
                    $join = true;
                    $whereL['bp.title'] = $query_search;
                }
            }

            $start = $start * HOME_PAGE_PER_RECORDS;
            $GetAllLatest = $this->ProductService->GetMoreProduct($whereL, $selectL, $start, $join);
            $count = count($GetAllLatest);
            $next_itr = ($count >= HOME_PAGE_PER_RECORDS) ? TRUE : FALSE;
            if ($count > 0) {
                $ulli = '';
                $AllAuthor = $this->HomeService->GetAllAuthor(array('slug !=' => '', 'is_active' => 1));
                $authorList = [];
                foreach ($AllAuthor as $author) {
                    $authorList[$author->id] = $author;
                }
                foreach ($GetAllLatest as $latestRow) {
                    $PROD_IMG_DIR = PRODUCT_DIR . $latestRow->id . '/image/' . $latestRow->cover_image;
                    $PRODUCT_IMG = '';
                    if (file_exists($PROD_IMG_DIR)) {
                        $PRODUCT_IMG = base_url(PRODUCT_DIR_URL . $latestRow->id . '/image/' . $latestRow->cover_image);
                    } else {
                        continue;
                    }
                    $pDetails = preety_url($latestRow->title) . '-' . toAlpha($latestRow->id);
                    $href = site_url('product-details/' . $pDetails);
                    $SubTitle = strlen($latestRow->title) <= 40 ? $latestRow->title : substr($latestRow->title, 0, 40) . '...';
                    $buyingMode = !empty($latestRow->buying_mode) ? json_decode($latestRow->buying_mode, TRUE) : array();
                    $discount_percentage = (100 - get_percentage($latestRow->mrp, $latestRow->offerprice));
                    $authors = '';
                    if (!empty($latestRow->author_id)) {
                        $author_id = explode(',', $latestRow->author_id);
                        $a = 0;
                        $info = '';
                        $auth = [];
                        foreach ($author_id as $aid) {
                            $auth[] = $authorList[$aid]->name;
                            if ($a == 0) {
                                $main_author = (strlen($authorList[$aid]->name) <= 16) ? $authorList[$aid]->name : substr($authorList[$aid]->name, 0, 15) . '...';
                                if (count($author_id) == 1) {
                                    $authors = '<p title="' . $authorList[$aid]->name . '" style="cursor: help;">By - ' . $main_author;
                                } else {
                                    $authors = '<p>By - ' . $main_author;
                                }
                            } else {
                                $info = ' <i class="fa fa-info-circle" title="' . join(', ', $auth) . '"></i>';
                            }
                            $a++;
                        }
                        $authors = $authors . $info . '</p>';
                    }
                    $products_li = '';
                    $products_li .= '<li class="double-slideitem slider-item">';
                    $products_li .= '<div class="slider-item">';
                    $products_li .= '<div class="product-block product-thumb transition">';
                    $products_li .= '<div class="product-block-inner">';
                    $products_li .= '<div class="image">';
                    $products_li .= '<a href="' . $href . '">';
                    $products_li .= '<img src="' . $PRODUCT_IMG . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '" class="img-responsive reg-image" style="height: 260px;" />';
                    $products_li .= '<img class="img-responsive hover-image" src="' . $PRODUCT_IMG . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '" style="height: 260px;" />';
                    $products_li .= '</a>';
                    if ($discount_percentage > 0){
                        $products_li .= '<div class="ribbon ribbon-top-right"><span>'.$discount_percentage.'% OFF</span></div>';
                    }
                    if (in_array('RENT', $buyingMode)) {
                        $products_li .= '<div class="percentsaving">RENT</div>';
                    }
                    $products_li .= '</div>';
                    $products_li .= '<div class="product-details">';
                    $products_li .= '<div class="caption">';
                    $products_li .= '<h4><a href="' . $href . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '">' . $SubTitle . '</a></h4>';
                    $products_li .= $authors;
                    $products_li .= '<p class="price">';
                    if ($latestRow->offerprice == $latestRow->mrp) {
                        $products_li .= '<span class="price-new">' . RUPEE_LOGO . number_format($latestRow->offerprice, 2) . '</span>';
                    } else {
                        $products_li .= '<span class="price-new">' . RUPEE_LOGO . number_format($latestRow->offerprice, 2) . '</span> <span class="price-old">' . RUPEE_LOGO . $latestRow->mrp . '</span>';
                    }
                    $products_li .= '<span class="price-tax">Ex Tax: ' . RUPEE_LOGO . $latestRow->offerprice . '</span>';
                    $products_li .= '</p>';
                    $products_li .= '</div>';
                    $products_li .= '</div>';
                    $products_li .= '</div>';
                    $products_li .= '</div>';
                    $products_li .= '</div>';
                    $products_li .= '</li>';
                    $ulli .= $products_li;
                }

                $responce = array(
                    'status' => TRUE,
                    'responce' => array(
                        'error' => '',
                        'message' => '',
                        'counter' => $count,
                        'data' => $ulli,
                        'next_itr' => $next_itr
                    ),
                    'code' => 200
                );
                $this->ajaxResponse($responce);
            } else {
                $responce = array(
                    'status' => FALSE,
                    'responce' => array(
                        'error' => 'No more data available.',
                        'message' => '',
                        'counter' => 0,
                        'data' => array(),
                        'next_itr' => false
                    ),
                    'code' => 1003
                );
                $this->ajaxResponse($responce);
            }
        } else {
            $responce = array(
                'status' => FALSE,
                'responce' => array(
                    'error' => $this->lang->line('err_1005'),
                    'message' => '',
                    'counter' => 0,
                    'data' => array(),
                    'next_itr' => false
                ),
                'code' => 1003
            );
            $this->ajaxResponse($responce);
        }
    }

    public function subscribe() {
        $this->form_validation->set_rules('subscriber_now', 'email', 'required|trim|xss_clean|valid_email');
        if ($this->form_validation->run() == false) {
            $responce = array(
                'status' => FALSE,
                'responce' => array(
                    'error' => validation_errors(),
                    'message' => ''
                ),
                'code' => 1002
            );
            $this->ajaxResponse($responce);
        } else {
            $subscriber_now = $this->input->post('subscriber_now');
            $save_subscriber = array(
                'email' => strtolower($subscriber_now)
            );
            $isSave = $this->ProductService->SaveSubscriber($save_subscriber);
            if ($isSave) {
                $responce = array(
                    'status' => TRUE,
                    'responce' => array(
                        'error' => '',
                        'message' => 'Thanks you for subscribing with us.'
                    ),
                    'code' => 200
                );
                $this->ajaxResponse($responce);
            } else {
                $responce = array(
                    'status' => FALSE,
                    'responce' => array(
                        'error' => $this->lang->line('err_1005'),
                        'message' => ''
                    ),
                    'code' => 1003
                );
                $this->ajaxResponse($responce);
            }
        }
    }

}
