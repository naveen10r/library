<?php
$author_name = [];
$mainImg = '';
$others = '';
if (!empty($pdetails->author_name_id)) {
    $author = explode(',', $pdetails->author_name_id);
    foreach ($author as $exp) {
        list($name, $id, $slug) = explode('__#__', $exp);
        $url = site_url('author-details/' . $slug);
        $author_name[] = '<a href="' . $url . '" class="write-review">' . trim($name) . '</a>';
    }
}
$youTubeVideo = '';
$youTubeImage = '';
if (!empty($pdetails->image_info)) {
    $image_info = explode(',', $pdetails->image_info);
    foreach ($image_info as $media) {
        list($type, $fileName, $clientName, $file_path) = explode('__#__', $media);
        if ($type == 'VIDEO') {
            $youTubeImage = 'https://img.youtube.com/vi/' . $file_path . '/mqdefault.jpg';
            $youTubeVideo = $file_path;
            continue;
        }
        $prodImg = base_url(PRODUCT_DIR_URL . $pdetails->id . '/image/' . $fileName);
        if ($type == 'MAIN') {
            $mainImg = $prodImg;
        }
        $others .= '<div class="slider-item">';
        $others .= '<div class="product-block">';
        $others .= '<a href="' . $prodImg . '" title="' . $pdetails->title . '" class="elevatezoom-gallery" data-image="' . $prodImg . '" data-zoom-image="' . $prodImg . '"><img src="' . $prodImg . '" width="85" height="115" title="' . $pdetails->title . '" alt="' . $pdetails->title . '" /></a>';
        $others .= '</div>';
        $others .= '</div>';
    }
}
?>


<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_TITLE . ' Search any books' ?></title>
        <?php echo FAVICON_TAG; ?>
        <base  />
        <meta name="title" content="<?php echo $pdetails->title . " | " . SITE_DEFAULT_META_TITLE; ?>" />
        <meta name="keywords" content="<?php echo!empty($pdetails->meta_keywords) ? $pdetails->meta_keywords : SITE_DEFAULT_META_KEYWORDS; ?>" />
        <meta name="description" content="<?php echo!empty($pdetails->meta_description) ? $pdetails->meta_description : SITE_DEFAULT_META_DESCRIPTION; ?>" />

        <base  />
        <script src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="<?php echo site_url('web_view'); ?>/catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="<?php echo site_url('web_view'); ?>/catalog/view/theme/PublicHub/stylesheet/stylesheet.css" rel="stylesheet" />
        <!-- Codezeel - Start -->
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/magnific/magnific-popup.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('web_view'); ?>/catalog/view/theme/PublicHub/stylesheet/codezeel/carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('web_view'); ?>/catalog/view/theme/PublicHub/stylesheet/codezeel/custom.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('web_view'); ?>/catalog/view/theme/PublicHub/stylesheet/codezeel/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('web_view'); ?>/catalog/view/theme/PublicHub/stylesheet/codezeel/lightbox.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('web_view'); ?>/catalog/view/theme/PublicHub/stylesheet/codezeel/animate.css" />
        <link href="style.html" type="text/css" rel="style.rel" media="style.media" />
        <link href="style.html" type="text/css" rel="style.rel" media="style.media" />
        <link href="style.html" type="text/css" rel="style.rel" media="style.media" />
        <link href="style.html" type="text/css" rel="style.rel" media="style.media" />
        <link href="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/magnific/magnific-popup.css" type="text/css" rel="stylesheet" media="screen" />
        <link href="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
        <link href="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/swiper/css/swiper.min.css" type="text/css" rel="stylesheet" media="screen" />
        <link href="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/swiper/css/opencart.css" type="text/css" rel="stylesheet" media="screen" />
        <script src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/swiper/js/swiper.jquery.js" type="text/javascript"></script>
        <link href="<?php echo site_url('product-details/' . $pDetailsLink); ?>" rel="canonical" />
        <link href="image/<?php echo site_url('web_view'); ?>/catalog/cart.png" rel="icon" />
        <!-- Codezeel - Start -->
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/custom.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/jstree.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/codezeel.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/jquery.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/jquery.formalize.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/lightbox/lightbox-2.6.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/tabs.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/jquery.elevatezoom.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/bootstrap-notify.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/doubletaptogo.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/codezeel/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
        <!-- Codezeel - End -->
        <script src="<?php echo site_url('web_view'); ?>/catalog/view/javascript/common.js" type="text/javascript"></script>


        <style type="text/css">
            /*
            .customNavigation > a {
                background-color: #81b851 !important;
            }
            */
            .section_set {
                min-height: 348px;
                text-align: justify;
            }

        </style>
    </head>
    <body class="product-product-47 layout-2 left-col">
        <header>
            <?php
            header_container();
            header_top_inner();
            ?>
        </header>
    </div>

</header>
<div class="wrap-breadcrumb parallax-breadcrumb">
    <div class="container"></div>
</div>

<!-- ======= Quick view JS ========= -->

<div id="product-product" class="container">
    <ul class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#">Product Details</a></li>
    </ul>
    <div class="row">
        <aside id="column-left" class="col-sm-3 hidden-xs">
            <?php
            if (!empty($all_category)) {
                ?>
                <div id="verticalmenublock" class="box category box-category ">
                    <div class="box-heading">All Categories</div>
                    <div class="box-content box-content-category">
                        <ul id="nav-one" class="dropmenu">
                            <?php
                            $i = 0;
                            foreach ($all_category as $cat) {
                                echo '<li class="top_level main"><a href="' . site_url('home/books/' . $cat->slug) . '">' . $cat->name . '</a></li>';
                                if ($i == 9) {
                                    break;
                                }
                                $i++;
                            }
                            echo '<li class="top_level"><a href="' . site_url('categories/all') . '">All</a></li>';
                            ?>
                        </ul>
                    </div>
                </div>
<?php } ?>
        </aside>
        <div id="content" class="col-sm-9 productpage">
            <div id="alertResponce">
                <?php
                if ($this->session->flashdata('alert')) {
                    $alert = $this->session->flashdata('alert');
                    echo $alert['color']($alert['responce']);
                }
                ?> 
            </div>
            <div class="row">                         
                <div class="col-sm-6 product-left"> 
                    <div class="product-info">		
                        <div class="left product-image thumbnails">

                            <!-- Codezeel Cloud-Zoom Image Effect Start -->
                            <div class="image"><a class="thumbnail" href="<?php echo $mainImg; ?>" title="<?php $pdetails->title; ?>"><img id="tmzoom" src="<?php echo $mainImg; ?>" data-zoom-image="<?php echo $mainImg; ?>" title="<?php $pdetails->title; ?>" alt="<?php $pdetails->title; ?>" style="height: 400px; width: 300px;" /></a>
                            </div> 
                            <div class="additional-carousel">	
                                <div class="customNavigation">
                                    <a class="fa prev fa-arrow-left">&nbsp;</a>
                                    <a class="fa next fa-arrow-right">&nbsp;</a>
                                </div> 
                                <div id="additional-carousel" class="image-additional product-carousel">
<?php echo $others; ?>		
                                </div>
                                <span class="additional_default_width" style="display:none; visibility:hidden"></span>
                            </div>
                            <!-- Codezeel Cloud-Zoom Image Effect End-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 product-right">
                    <h3 class="product-title"><?php echo $pdetails->title; ?></h3>
                    <div class="rating-wrapper">
<?php echo!empty($author_name) ? '<em><strong>By</strong></em> ' . join(',', $author_name) : ''; ?>
                    </div>
                    <div class="section_set">
                        <h5 class="product-option">About the book</h5>
<?php echo!empty($pdetails->content) ? $pdetails->content : ""; ?>
                    </div>
                    <div id="product">
                        <div class="price-cartbox">
                            <div class="form-group qty">
                                <label class="control-label" for="input-quantity">Qty</label>
                                <?php
                                if (!empty($this->session->userdata('ExUserSess'))) {
                                    ?>
                                    <input type="text" value="1" size="2" id="input-quantity" class="form-control" disabled="disabled" />
                                    <a href="<?php echo site_url("user_cart/cart/AddToCart/" . $pdetails->id); ?>" class="btn btn-primary btn-lg btn-block" id="AddToCart">Add To Cart</a>
                                    <a href="<?php echo site_url("user_cart/cart/BuyNow/" . $pdetails->id); ?>" class="btn btn-warning btn-lg btn-block">Buy Now</a>
                                <?php } else { ?>
                                    <a href="<?php echo site_url("login"); ?>" class="btn btn-primary btn-lg btn-block" id="AddToCart">Add To Cart</a>
                                    <a href="<?php echo site_url("login"); ?>" class="btn btn-warning btn-lg btn-block">Buy Now</a>
<?php } ?>
                                <button type="button" data-toggle="tooltip" class="btn btn-default wishlist" title="Add to Wish List"></button>
                            </div>
                        </div>

                        <!-- AddThis Button BEGIN -->
                        <?php
                        /*
                          <div class="addthis_toolbox addthis_default_style" data-url="indexd21c.html?route=product/product&amp;product_id=47"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_                                            counter addthis_pill_style"></a></div>
                         */
                        ?>
                        <!-- AddThis Button END --> 
                    </div>
                </div>

                <!-- product page tab code start-->

                <div id="tabs_info" class="product-tab col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-description" data-toggle="tab">Available Options</a></li>
                        <li><a href="#tab-specification" data-toggle="tab">Specification<?php echo!empty($youTubeImage) ? ' / Video' : ''; ?></a></li>
                        <li id="tab_qestions"><a href="#tab-qestions" data-toggle="tab">Request this book</a></li>
                        <li><a href="#tab-review" data-toggle="tab">Reviews</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-description">
                            <ul class="list-unstyled price" style="float: none;">
                                <li>
                                    <h3 class="special-price"><?php echo RUPEE_LOGO . number_format($pdetails->offerprice, 2); ?></h3>
                                </li>
                                <?php
                                if ($pdetails->mrp != $pdetails->offerprice) {
                                    ?>
                                    <li>
                                        <span class="old-price" style="text-decoration: line-through;"><?php echo RUPEE_LOGO . number_format($pdetails->mrp, 2); ?></span>
                                    </li>
                                    <?php
                                }
                                /*
                                  <li class="price-tax">Ex Tax: <?php echo RUPEE_LOGO . '18%'; ?></li>
                                  <li class="rewardpoint">Price in reward points: 100</li>
                                 */
                                ?>                                
                            </ul>
                            <div class="form-group">
                                <label class="control-label" for="input-option225">Delivery Date</label>
                                <div class="input-group date">
                                    <input type="text" value="<?php echo date(SYS_DATE, strtotime('+20 Days')); ?>" data-date-format="DD-MMM-YYYY" class="form-control" disabled="disabled" />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-qestions">
                            <div id="message_req"></div>
                            <p> 
                            <form class="form-horizontal" method="POST" id="request_form">
                                <input type="hidden" name="product_id" value="<?php echo $pdetails->id; ?>" />
                                <input type="hidden" name="product_title" value="<?php echo $pdetails->title; ?>" />
<?php echo input_csrf(); ?>
                                <div class="form-group required">
                                    <div class="col-sm-4">
                                        <label class="control-label" for="input-name">Your Name</label>
                                        <input type="text" name="requester_name" id="requester_name" class="form-control" required="required" pattern="<?php echo NAME_PATTERN; ?>" maxlength="64" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label" for="input-name">Your Email</label>
                                        <input type="text" name="requester_email" id="requester_email" class="form-control" required="required" pattern="<?php echo EMAIL_PATTERN; ?>" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label" for="input-name">Your Mobile</label>
                                        <input type="text" name="requester_mobile" id="requester_mobile" class="form-control" required="required" pattern="<?php echo FOR_REG_MOBILE; ?>" maxlength="10" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="input-name">Message</label>
                                        <textarea name="requester_txt" id="requester_txt" class="form-control" maxlength="250"></textarea>
                                        <div class="help-block"><span id="remain_length" class="text-muted">250 characters remain</span></div>

                                    </div>
                                </div>
                                <div class="buttons clearfix">
                                    <div class="col-sm-3 required">
                                        <label class="control-label" for="input-name">Image Security</label>
                                    </div>

                                    <div class="col-sm-3" id="captcha_img">
<?php echo $captcha['image']; ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" name="requester_captch" id="requester_captch" class="form-control" required="required" />
                                    </div>
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                            </p>
                        </div>
                        <div class="tab-pane" id="tab-specification">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list-unstyled attr">
                                        <li><span class="desc">Publisher: </span><a href="#"><?php echo $pdetails->publisher_id; ?></a></li>
                                        <li><span class="desc">Product Code: </span> <?php echo $pdetails->isbn; ?></li>
                                        <li><span class="desc">Reward Points: </span> 10</li>
                                        <li><span class="desc">Availability: </span> In Stock</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
<?php if (!empty($youTubeImage)): ?>
                                        <ul class="list-unstyled attr">
                                            <li>
                                                <div class="image"><img src="<?php echo $youTubeImage; ?>" style="height: 130px; width: 110px;" data-toggle="modal" data-target="#YouTubeModal" />
                                                </div> 
                                            </li>
                                        </ul>
<?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-review">
                            <?php
                            if (!empty($this->session->userdata('ExUserSess'))) {
                                ?>
                                <div id="review_response"></div>
                                <form class="form-horizontal" id="form-review">
                                    <input type="hidden" name="product_id" value="<?php echo $pdetails->id; ?>" />
                                    <div id="review"></div>
                                    <h3>Write a review</h3>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-review">Your Review</label>
                                            <textarea name="reviewer_text" rows="5" id="reviewer_text" class="form-control"></textarea>
                                            <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label">Rating</label>
                                            &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                            <input type="radio" name="reviewer_rating" value="1" />
                                            &nbsp;
                                            <input type="radio" name="reviewer_rating" value="2" />
                                            &nbsp;
                                            <input type="radio" name="reviewer_rating" value="3" />
                                            &nbsp;
                                            <input type="radio" name="reviewer_rating" value="4" />
                                            &nbsp;
                                            <input type="radio" name="reviewer_rating" value="5" checked="checked" />
                                            &nbsp;Good</div>
                                    </div>

                                    <div class="buttons clearfix">
                                        <div class="pull-right">
                                            <button type="submit" id="button-review" class="btn btn-primary">Continue</button>
                                        </div>
                                    </div>
                                </form>
                                <?php
                            } else {
                                $login_required = site_url('users/ExternalUser/login_required');
                                echo '<p><strong>Login required before review</strong></p><p>Dear User, <br />You login is required for reviewing the page, If you don\'t have an account, and want to sign up for our services, you can usually register for a new account from the login page.</p><div class="pull-right"><a href="' . $login_required . '" id="button-review" class="btn btn-primary">Continue</a></div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ==================== featured Product ==================== -->
        <?php
        if (!empty($similarP)):
            ?>
            <div class="box featured">
                <div class="container">
                    <div class="box-head">
                        <div class="box-heading">RECOMMENDED FOR YOU</div>
                    </div>
                    <div class="box-content">
                        <div class="customNavigation">
                            <a class="fa prev fa-arrow-left">&nbsp;</a>
                            <a class="fa next fa-arrow-right">&nbsp;</a>
                        </div>
                        <?php
                        /*
                         */
                        ?>
                        <div class="box-product product-carousel" id="featured-carousel" style="opacity: 1; display: block;">
                            <div class="slider-wrapper-outer">
                                <div class="slider-wrapper" style="width: 3776px; left: 0px; display: block;">
                                    <?php
                                    $imc = 0;
                                    foreach ($similarP as $latestRow) {
                                        $imc++;
                                        if ($imc > 6) {
                                            break;
                                        }
                                        $PROD_IMG_DIR = PRODUCT_DIR . $latestRow->pid . '/image/' . $latestRow->cover_image;
                                        $PRODUCT_IMG = '';
                                        if (file_exists($PROD_IMG_DIR)) {
                                            $PRODUCT_IMG = base_url(PRODUCT_DIR_URL . $latestRow->pid . '/image/' . $latestRow->cover_image);
                                        } else {
                                            continue;
                                        }
                                        $pDetails = preety_url($latestRow->title) . '-' . toAlpha($latestRow->pid);
                                        $href = site_url('product-details/' . $pDetails);
                                        $SubTitle = strlen($latestRow->title) <= 45 ? $latestRow->title : substr($latestRow->title, 0, 45) . '...';
                                        $buyingMode = !empty($latestRow->buying_mode) ? json_decode($latestRow->buying_mode, TRUE) : array();
                                        $discount_percentage = (100 - get_percentage($latestRow->mrp, $latestRow->offerprice));
                                        $authors = '';
                                        if (!empty($latestRow->author_id)) {
                                            $author_id = explode(',', $latestRow->author_id);
                                            $a = 0;
                                            $info = '';
                                            $auth = [];
                                            foreach ($author_id as $aid) {
                                                $auth[] = $authorList[$aid]->name;
                                                if ($a == 0) {
                                                    $main_author = (strlen($authorList[$aid]->name) <= 16) ? $authorList[$aid]->name : substr($authorList[$aid]->name, 0, 15) . '...';
                                                    if (count($author_id) == 1) {
                                                        $authors = '<p title="' . $authorList[$aid]->name . '" style="cursor: help;">By - ' . $main_author;
                                                    } else {
                                                        $authors = '<p>By - ' . $main_author;
                                                    }
                                                } else {
                                                    $info = ' <i class="fa fa-info-circle" title="' . join(', ', $auth) . '"></i>';
                                                }
                                                $a++;
                                            }
                                            $authors = $authors . $info . '</p>';
                                        }
                                        $products_li = '';
                                        $products_li .= '<li class="double-slideitem slider-item">';
                                        $products_li .= '<div class="slider-item">';
                                        $products_li .= '<div class="product-block product-thumb transition">';
                                        $products_li .= '<div class="product-block-inner">';
                                        $products_li .= '<div class="image">';
                                        $products_li .= '<a href="' . $href . '">';
                                        $products_li .= '<img src="' . $PRODUCT_IMG . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '" class="img-responsive reg-image" style="height: 260px;" />';
                                        $products_li .= '<img class="img-responsive hover-image" src="' . $PRODUCT_IMG . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '" style="height: 260px;" />';
                                        $products_li .= '</a>';
                                        if (in_array('RENT', $buyingMode)) {
                                            $products_li .= '<div class="percentsaving">RENT</div>';
                                        }
                                        $products_li .= '</div>';
                                        $products_li .= '<div class="product-details">';
                                        $products_li .= '<div class="caption">';
                                        $products_li .= '<h4 style="height: 40px;"><a href="' . $href . '" title="' . $latestRow->title . '" alt="' . $latestRow->title . '">' . $SubTitle . '</a></h4>';
                                        $products_li .= $authors;
                                        $products_li .= '<p class="price">';
                                        if ($latestRow->offerprice == $latestRow->mrp) {
                                            $products_li .= '<span class="price-new">' . RUPEE_LOGO . number_format($latestRow->offerprice, 2) . '</span>';
                                        } else {
                                            $products_li .= '<span class="price-new">' . RUPEE_LOGO . number_format($latestRow->offerprice, 2) . '</span> <span class="price-old">' . RUPEE_LOGO . $latestRow->mrp . '</span> <font color="#388e3c" weight="100">' . $discount_percentage . '% off</font>';
                                        }
                                        $products_li .= '<span class="price-tax">Ex Tax: ' . RUPEE_LOGO . $latestRow->offerprice . '</span>';
                                        $products_li .= '</p>';
                                        $products_li .= '</div>';
                                        $products_li .= '</div>';
                                        $products_li .= '</div>';
                                        $products_li .= '</div>';
                                        $products_li .= '</div>';
                                        $products_li .= '</li>';
                                        echo $products_li;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <!-- ==================== featured Product ==================== -->

    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="YouTubeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color:#FFFFFF; font-weight: 900;"><?php echo $pdetails->title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe width="860" height="383" src="https://www.youtube.com/embed/<?php echo $youTubeVideo; ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<?php footer_tag(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        if ($(window).width() > 767) {
            $("#tmzoom").elevateZoom({
                gallery: 'additional-carousel',
                zoomType: "inner",
                cursor: "crosshair"
            });
            var z_index = 0;
            $(document).on('click', '.thumbnail', function () {
                $('.thumbnails').magnificPopup('open', z_index);
                return false;
            });

            $('#additional-carousel a').click(function () {
                alert('517');
                var smallImage = $(this).attr('data-image');
                alert('520');
                var largeImage = $(this).attr('data-zoom-image');
                alert('522');
                var ez = $('#tmzoom').data('elevateZoom');
                alert('524', ez);
                $('.thumbnail').attr('href', largeImage);
                alert('526');
                ez.swaptheimage(smallImage, largeImage);
                z_index = $(this).index('#additional-carousel a');
                return false;
            });

        } else {
            $(document).on('click', '.thumbnail', function () {
                $('.thumbnails').magnificPopup('open', 0);
                return false;
            });
        }
    });
    $(document).ready(function () {
        $('.thumbnails').magnificPopup({
            delegate: 'a.elevatezoom-gallery',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-with-zoom',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title');
                }
            }
        });
    });

    $("#request_form").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = '<?php echo site_url('product/products/save_request'); ?>';
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function (returntxt)
            {
                var parseJson = JSON.parse(returntxt);
                $('#captcha_img').html(parseJson.responce.data.image);
                $('#message_req').removeClass('alert alert-success alert-danger');
                if (parseJson.status == true) {
                    $('#message_req').addClass('alert alert-success');
                    $('#message_req').html(parseJson.responce.message);
                } else {
                    $('#message_req').addClass('alert alert-danger');
                    $('#message_req').html(parseJson.responce.error);
                }
                closeAlert()
            }
        });
    });

    $("#form-review").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = '<?php echo site_url('product/products/save_review'); ?>';
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function (returntxt)
            {
                var parseJson = JSON.parse(returntxt);
                $('#review_response').removeClass('alert alert-success alert-danger');
                if (parseJson.status === true) {
                    $('#review_response').addClass('alert alert-success');
                    $('#review_response').html(parseJson.responce.message);
                    closeAlert();
                } else {
                    $('#review_response').addClass('alert alert-danger');
                    $('#review_response').html(parseJson.responce.error);
                }

            }
        });
    });

    function closeAlert() {
        setTimeout(function () {
            $(".alert").hide("slow");
        }, 5000);
    }
    $('#requester_txt').keyup(function () {
        var text_length = $(this).val().length;
        var remain_lgth = 250 - parseInt(text_length);
        $('#remain_length').html(remain_lgth.toString() + ' characters remain');
    });

    $('#requestTheBook').click(function () {
        $('.nav-tabs li.active').removeClass('active');
        $('#tab_qestions').addClass('active');
    });

    $("#YouTubeModal").on('hidden.bs.modal', function (e) {
        $("#YouTubeModal iframe").attr("src", $("#YouTubeModal iframe").attr("src"));
    });
    $(document).ready(function () {
        setTimeout(function () {
            $(".alert").hide("slow");
        }, 5000);
    });

</script>
</body>
</html> 
