<?php

class ServerJobs extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function remove_captcha(){
        $last_date = strtotime('Yesterday');
        $files = glob(getcwd()."/captcha/*.{jpg}", GLOB_BRACE);
        $unlink_files = [];
        foreach($files as $file){
            if (file_exists($file)) {
                $create_time = filemtime($file);
                if ($create_time < $last_date){
                    $unlink_files[$file] = unlink($file);
                }
            }
        }
        echo '<pre>';
        print_r($unlink_files);
        echo '</pre>';
    }
    
}