<?php

/**
 * @author :: Naveen Rastogi
 *
 * Create :: Feb-2015
 * common helper for some common function and these are call default without including page
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * Get random string on every time
 *
 * @access	public
 * @param	string
 * @return	string
 */
if (!function_exists('rand_string')) {

    function rand_string($length, $isInt = false) {
        if ($isInt === true) {
            $numbers = time() . rand(10000, 99999) . time();
            return substr($numbers, 0, $length);
        } else {
            $numericAlphabet = '98765431ABCDEFGHIJKLMNPRSTUVWXYabcdefghjklmnpqrstuvwxyz13456789';
            $output = "";
            for ($i = 0; $i < $length; $i++) {
                $string = "";
                $string = str_shuffle($numericAlphabet);
                $output .= $string[0];
            }
            return substr($output, 0, $length);
        }
    }

}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * Get unique value
 *
 * @access	public
 * @param	string
 * @return	string
 */
if (!function_exists('get_unique_value')) {

    function get_unique_value() {
        return time() . rand(1000, 9999) . rand(1000, 9999);
    }

}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * preety_url function by session
 *
 * @access  public
 * @param   Boolean
 * @return  Boolean
 */
if (!function_exists('preety_url')) {

    function preety_url($string) {
        if (strlen($string) > 100) {
            $string = substr($string, 0, 99);
        }
        $urlStr = str_replace(' ', '-', strtolower(trim($string)));
        $urlStr = str_replace('.', '', $urlStr);
        $urlStr = preg_replace('/[^A-Za-z0-9\-]/', '', $urlStr);
        $urlStr = str_replace('--', '-', $urlStr);
        return $urlStr;
    }

}
// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * char_set function for set UTF setting into system
 *
 * @access  public
 * @param   Boolean
 * @return  Boolean
 */
if (!function_exists('char_set')) {

    function char_set() {
        $array = array(
            "big5" => "Chinese Traditional",
            "euc-kr" => "Korean",
            "iso-8859-1" => "Western Alphabet",
            "iso-8859-2" => "Central European Alphabet",
            "iso-8859-3" => "Latin 3 Alphabet",
            "iso-8859-4" => "Baltic Alphabet",
            "iso-8859-5" => "Cyrillic Alphabet",
            "iso-8859-6" => "Arabic Alphabet",
            "iso-8859-7" => "Greek Alphabet",
            "iso-8859-8" => "Hebrew Alphabet",
            "koi8-r" => "Cyrillic Alphabet",
            "shift-jis" => "Japanese",
            "x-euc" => "Japanese",
            "utf-8" => "Universal Alphabet",
            "windows-1250" => "Central European Alphabet",
            "windows-1251" => "Cyrillic Alphabet",
            "windows-1252" => "Western Alphabet Windows",
            "windows-1253" => "Greek Alphabet Windows",
            "windows-1254" => "Turkish Alphabet",
            "windows-1255" => "Hebrew Alphabet Windows",
            "windows-1256" => "Arabic Alphabet Windows",
            "windows-1257" => "Baltic Alphabet Windows",
            "windows-1258" => "Vietnamese Alphabet",
            "windows-874" => "Thai",
            'x-iscii-de' => 'Devanagari Hindi/Sanskrit'
        );
        ksort($array);
        return $array;
    }

}
// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * Get alias from string
 *
 * @access	public
 * @param	string
 * @return	string
 */
if (!function_exists('alias')) {

    function alias($string) {
        $adjective = array('and', 'of', 'or', 'if', 'else', 'so');
        $string = strtolower($string);
        $string = str_replace($adjective, '', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
        $string = str_replace('--', '-', $string);
        $alias = '';
        $strToArray = explode('-', $string);
        for ($i = 0; $i < count($strToArray); $i++) {
            $alias .= @$strToArray[$i][0];
        }
        return $alias;
    }

}
// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * captcha_img function by session
 *
 * @access  public
 * @param   Boolean
 * @return  Boolean
 */
if (!function_exists('captcha_img')) {

    function captcha_img() {
        $ci = & get_instance();
        $ci->load->helper('captcha');
        chdir(getcwd() . '/core-system/fonts');
        $font = glob('*.*');
        $rand_key = rand(0, (count($font) - 1));
        $captcha_font = $font[$rand_key];
        $captcha_word = strtoupper(rand_string(8));
        $values = array(
            'word' => $captcha_word,
            'word_length' => 8,
            'img_path' => './uploaded/captcha/',
            'img_url' => base_url() . 'uploaded/captcha/',
            'font_path' => base_url() . 'core-system/fonts/' . $captcha_font,
            'img_width' => '150',
            'img_height' => 50,
            'expiration' => 3600
        );
        $arrCaptcha = create_captcha($values);
        $ci->session->set_userdata('C_IMG', $arrCaptcha['word']);
        return $arrCaptcha;
    }

}
/**
 * File for security
 *
 * Create a file on server by creating every dynamic directrices
 *
 * @create  Naveen Rastogi
 * @date 	July 2015
 * @access	public
 * @param	string
 * @return	boolean(TRUE/FALSE)
 */
if (!function_exists('index_html')) {

    function index_html($path = '') {
        $div = '<div class="isa_error"><center>403 forbidden</center></div>';
        $css = '<style type="text/css">
            .isa_error {margin: 0px auto; padding:12px; max-width: 20%; }
            .isa_error {color: #D8000C;background-color: #ECE9D8;}
            .isa_error i {margin:10px 22px; font-size:2em;vertical-align:middle;}</style>';
        $htmlFile = fopen($path . "/index.html", "w") or die("Unable to open file!");
        $textData = "<html><head><title>403 Forbidden</title>" . $css . "</head><body>";
        $textData .= $div . "</body></html>";
        fwrite($htmlFile, $textData);
        fclose($htmlFile);
        return true;
    }

}
/**
 * For access array
 *
 * Create a file on server by creating every dynamic directrices
 *
 * @create  Naveen Rastogi
 * @date 	Oct 2015
 * @access	public
 * @param	string
 * @return	Array
 */
if (!function_exists('stringToArray')) {

    function stringToArray($str = '') {
        $arr = explode(',', $str);
        $restOfArray = array();
        for ($i = 0; $i < count($arr); $i++) {
            $arrray = explode('@', $arr[$i]);
            $restOfArray[$arrray[0]] = array($arrray[1], $arrray[2]);
        }
        return $restOfArray;
    }

}

//////////////////////////////////////////////////////////////////////
//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                        =>  14 Days
// '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                        =>  468 Days
//////////////////////////////////////////////////////////////////////
function dateDifference($date_1, $date_2, $differenceFormat = '%a') {
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    $interval = date_diff($datetime1, $datetime2);
    if (empty($differenceFormat)) {
        return $interval;
    }
    return $interval->format($differenceFormat);
}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * monthsListBetweenTwoDates function by for show all months beetween two dates
 *
 * @access  public
 * @param   Boolean
 * @return  array
 */
if (!function_exists('monthsListBetweenTwoDates')) {

    function monthsListBetweenTwoDates($date_1, $date_2) {
        $result = array();
        $start = (new DateTime($date_1))->modify('first day of this month');
        $end = (new DateTime($date_2))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $result[] = $dt->format("F-Y");
        }
        return $result;
    }

}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * is_date_exist function by for show correct date
 *
 * @access  public
 * @param   Boolean
 * @return  Boolean
 */
if (!function_exists('is_date_exist')) {

    function is_date_exist($string, $time = false, $ampm = false) {
        $ampm = ($ampm === true) ? DATE_TIME_AM_PM : DATE_TIME_FORMAT;
        if (!empty($string)) {
            $year = date_parse($string);
            if (!empty($year['year'])) {
                if ($time === true) {
                    return date($ampm, strtotime($string));
                } else {
                    return date(DATE_FORMAT, strtotime($string));
                }
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

}
// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * Access function by session
 *
 * @access  public
 * @param   Boolean
 * @return  Boolean
 */
if (!function_exists('language_encode')) {

    function language_encode(array $langKey = array()) {
        $langKey[] = 'html_lang_encode';
        $return = array();
        $CI = & get_instance();
        $CI->load->config('my_config', TRUE);
        $LANGUAGE = $CI->session->userdata('LANGUAGE');
        $LANGUAGE = !empty($LANGUAGE) ? $LANGUAGE : 'english';
        /* $LANGUAGE = 'hindi'; */
        $CI->db->select('lang_key,' . $LANGUAGE);
        $CI->db->from($CI->config->item('language_encoding', 'my_config'));
        $CI->db->where_in('lang_key', $langKey);
        $data = $CI->db->get()->result();
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $return[$value->lang_key] = ucwords($value->$LANGUAGE);
            }
        }
        return $return;
    }

}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * cleanHTML function by session
 *
 * @access  public
 * @param   string
 * @return  string
 */
function cleanHTML($string) {
    $string = preg_replace('/[^A-Za-z0-9\/\ <>=#;&"]/', '', $string);
    $string = str_replace(array('&quot;', '&#39;', '&#38;'), '', $string);
    return html_entity_decode($string);
}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * cleanHTML function by session
 *
 * @access  public
 * @param   string
 * @return  string
 */
function getPercentage($obt, $max) {
    return round((($obt * 100 ) / $max), 2);
}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * getGrade function for search grade by percentage marks
 *
 * @access  public
 * @param   array, intger
 * @return  string
 */
function getGrade($search, $array) {
    $flipArray = array_flip($array);
    sort($flipArray);
    $return = null;
    foreach ($flipArray as $data) {
        if ($search < $data) {
            break;
        }
        $return = $data;
    }
    return $array[$return];
}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * convertNumberToWord function by session
 *
 * @access  public
 * @param   number
 * @return  string
 */
function convertNumberToWord($num = false) {
    if (!is_numeric($num)) {
        return $num;
    }
    $num = str_replace(array(',', ' '), '', trim($num));
    if (!$num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
    );
    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ( $hundreds == 1 ? '' : 's' ) . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ($tens < 20) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
        } else {
            $tens = (int) ($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . ( ( $levels && (int) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
    } /* end for loop */
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    $words = implode(' ', $words);
    return ucwords($words);
}

// ------------------------------------------------------------------------
/**
 * @author :: Naveen Rastogi
 *
 * sendSMS function for send sms in robotic format
 *
 * @access  public
 * @param   array
 * @return  string
 */
if (!function_exists('sendSMS')) {

    function sendSMS($array) {
        $message = urlencode($array['text']);
        $ci = & get_instance();
        $ci->load->config('my_config');
        if ($ci->config->item('sms_service') === false) {
            return [true, $message];
        }
        $test = "0";
        $sender = "TXTLCL";
        if (is_array($array['numbers'])) {
            $numbers = join('91,', $array['numbers']);
        } else {
            $numbers = $array['numbers'];
        }

        $data = "username=" . SMS_USER . "&hash=" . SMS_HASH . "&message=" . $message . "&sender=" . $sender . "&numbers=" . $numbers . "&test=" . $test;
        $ch = curl_init(SMS_API_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); // This is the result from the API
        $curlErr = curl_error($ch);
        curl_close($ch);
        prx(array('[SMS][CURL_RESPONCE]', $result));
        $dataResult = json_decode($result, TRUE);
        if ($dataResult['status'] == 'success') {
            return [true, 'success'];
        } elseif ($dataResult['status'] == 'failure') {
            $error = $dataResult['errors'];
            return [false, $error[0]];
        } else {
            return [false, $curlErr];
        }
    }

}

// ------------------------------------------------------------------------
/**
 * Get Geo Location by Given/Current IP address
 *
 * @access    public
 * @param    string
 * @return    array
 */
if (!function_exists('get_geolocation')) {

    function get_geolocation($ip) {
        $d = @file_get_contents("http://ipinfo.io/$ip/json");
        $d = !empty($d) ? $d : '';
        return json_decode($d, TRUE);
    }

}

// ------------------------------------------------------------------------
/**
 * Clean HTML Input
 *
 * @access    public
 * @param    string
 * @return    array
 */
if (!function_exists('cleanHTMLInput')) {

    function cleanHTMLInput($data) {
        return addslashes(nl2br(htmlentities($data)));
    }

}

// ------------------------------------------------------------------------
/**
 * convert To Hours & Mins
 *
 * @access    public
 * @param    string
 * @return    array
 */
function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// ------------------------------------------------------------------------
if (!function_exists('pr')) {

    function pr($data, $isLineNo = TRUE) {
        if ($isLineNo) {
            $bt = debug_backtrace();
            $caller = array_shift($bt);
            echo 'At line <b>' . $caller['line'] . '</b> from file <b>' . $caller['file'] . '</b><br /><pre>';
        } else {
            echo '<pre>';
        }
        print_r($data);
        echo '</pre>';
    }

}

// ------------------------------------------------------------------------
if (!function_exists('prx')) {

    function prx($data, $isLineNo = TRUE) {
        if ($isLineNo) {
            $bt = debug_backtrace();
            $caller = array_shift($bt);
            echo 'At line <b>' . $caller['line'] . '</b> from file <b>' . $caller['file'] . '</b><br /><pre>';
        } else {
            echo '<pre>';
        }
        print_r($data);
        die('</pre>');
    }

}

// ------------------------------------------------------------------------
if (!function_exists('vr')) {

    function vr($data, $isLineNo = TRUE) {
        if ($isLineNo) {
            $bt = debug_backtrace();
            $caller = array_shift($bt);
            echo 'At line <b>' . $caller['line'] . '</b> from file <b>' . $caller['file'] . '</b><br /><pre>';
        } else {
            echo '<pre>';
        }
        var_dump($data);
        echo '</pre>';
    }

}

// ------------------------------------------------------------------------
if (!function_exists('vrx')) {

    function vrx($data, $isLineNo = TRUE) {
        if ($isLineNo) {
            $bt = debug_backtrace();
            $caller = array_shift($bt);
            echo 'At line <b>' . $caller['line'] . '</b> from file <b>' . $caller['file'] . '</b><br /><pre>';
        } else {
            echo '<pre>';
        }
        var_dump($data);
        die('</pre>');
    }

}

function timetosec($str_time) {
    $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
    sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
    $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
    return $time_seconds;
}

function dsecure($string) {
    $CI = & get_instance();
    $key = ($CI->config->item('encryption_key'));
    $result = '';
    $string = base64_decode($string);
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) - ord($keychar));
        $result .= $char;
    }
    return $result;
}

function esecure($string) {
    $CI = & get_instance();
    $key = $CI->config->item('encryption_key');
    $result = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) + ord($keychar));
        $result .= $char;
    }
    return base64_encode($result);
}

function post_lenth_count($post = array(), $length) {
    if (count($post) == $length) {
        return true;
    } else {
        return false;
    }
}

if (!function_exists('hide_email')) {

    function hide_email($string) {
        $responce = '';
        $email = strtolower($string);
        $emailToArray = explode('@', $email);
        $emailCurrent = current($emailToArray);
        $emailEnd = end($emailToArray);
        for ($i = 0; $i < strlen($emailCurrent); $i++) {
            if ($i == 0) {
                $responce .= $emailCurrent[$i];
            } else {
                $responce .= '*';
            }
        }
        return $responce . '@' . $emailEnd;
    }

}


if (!function_exists('get_real_path')) {

    function get_real_path($path) {
        require_once realpath(dirname(__FILE__)) . '/' . $path . '.php';
    }

}


if (!function_exists('hide_mobile')) {

    function hide_mobile($mobile) {
        $responce = '';
        for ($i = 0; $i < 10; $i++) {
            if ($i == 0) {
                $responce .= @$mobile[$i];
            } elseif (0 < $i AND $i < 6) {
                $responce .= '*';
            } else {
                $responce .= @$mobile[$i];
            }
        }
        return $responce;
    }

}


if (!function_exists('breadcrumb')) {

    function breadcrumb($breadcrumb = array()) {
        $responce = '';
        if (is_array($breadcrumb) AND ! empty($breadcrumb)) {
            $flag = true;
            foreach ($breadcrumb as $href => $value) {
                if ($flag === true) {
                    $responce .= '<a href="' . $href . '" class="tip-bottom"><i class="icon-home"></i> ' . $value . '</a>';
                    $flag = false;
                } else {
                    $responce .= '<a href="' . $href . '" class="tip-bottom">' . $value . '</a>';
                }
            }
        }
        return $responce;
    }

}

// ------------------------------------------------------------------------
/**
 * Access Production module only on app
 *
 * @access    public
 * @return    bool
 */
if (!function_exists('is_web_view')) {

    function is_web_view() {
        $CI = & get_instance();
        $CI->load->library('user_agent');
        if (ENVIRONMENT == 'production') {
            if ($CI->agent->is_mobile()) {
                return true;
            } else {
                redirect('default/General/downloadLink');
            }
        } return true;
    }

}


// ------------------------------------------------------------------------
/**
 * CSRF Protection
 *
 * @access    public
 * @return    string
 */
if (!function_exists('input_csrf')) {

    function input_csrf() {
        $CI = & get_instance();
        if ($CI->config->item('csrf_protection') === TRUE) {
            return '<input type="hidden" name="' . $CI->security->get_csrf_token_name() . '" value="' . $CI->security->get_csrf_hash() . '">';
        } else {
            return '';
        }
    }

}


// ------------------------------------------------------------------------
/**
 * Array value inside an array
 *
 * @author Naveen Rastogi <naveen.rastogi@bigstocks.in>
 * @access  public
 * @return  boolean
 * @param   array, array
 */
if (!function_exists('in_array_to_array')) {

    function in_array_to_array(array $needel, array $hashtrack) {
        $diff = array_diff($needel, $hashtrack);
        if (empty($diff)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
/**
 * SaveLogData
 *
 * @author Naveen Rastogi <naveen.rastogi@bigstocks.in>
 * @access  public
 * @return  boolean
 * @param   array, array
 */
if (!function_exists('SaveLogData')) {

    function SaveLogData(array $where, array $update, $table_name, $user_id) {
        $ci = & get_instance();
        $insertLog = array(
            'user_id' => $user_id,
            'select_data' => NULL,
            'where_data' => $where,
            'update_data' => $update,
            'table_name' => $table_name,
            'change_at' => date(SYS_DB_DATE_TIME)
        );
        /*
        $ci->load->library('MongoDb');
        $ci->mongodb->insert('log_for_update', $insertLog);
        */
        return true;
    }

}

// ------------------------------------------------------------------------
/**
 * SaveLogData
 *
 * @author Naveen Rastogi <naveen.rastogi@bigstocks.in>
 * @access  public
 * @return  boolean
 * @param   array, array
 */
if (!function_exists('SaveInsertLogData')) {

    function SaveInsertLogData(array $select, array $last_id, $table_name, $user_id) {
        $ci = & get_instance();
        $insertLog = array(
            'user_id' => $user_id,
            'select_data' => $select,
            'where_data' => $last_id,
            'update_data' => NULL,
            'table_name' => $table_name,
            'change_at' => date(SYS_DB_DATE_TIME)
        );
        $ci->load->library('MongoDb');
        $ci->mongodb->insert('log_for_update', $insertLog);
        return true;
    }

}

if (!function_exists('load_js')) {

    function load_js(array $jspaths) {
        $jsLoad = array();
        foreach ($jspaths as $js) {
            $jsLoad[] = '<script type="text/javascript" src="' . base_url($js) . '"></script>';
        }
        return join('', $jsLoad);
    }

}

if (!function_exists('load_css')) {

    function load_css(array $csspaths) {
        $css = array();
        foreach ($csspaths as $js) {
            $css[] = '<link rel="stylesheet" type="text/css" href="' . base_url($js) . '" rel="stylesheet" />';
        }
        return join('', $css);
    }

}


if (!function_exists('api_response')) {

    function api_response(array $array) {
        $status = $array['status'] === true ? true : false;
        $error_text = !empty($array['error_text']) ? $array['error_text'] : '';
        $success_text = !empty($array['success_text']) ? $array['success_text'] : '';
        $success_data = !empty($array['success_data']) ? $array['success_data'] : '';
        $code = !empty($array['code']) ? $array['code'] : 404;
        $data_type = !empty($array['data_type']) ? $array['data_type'] : 'json';
        $return = array(
            'status' => $status,
            'response' => array(
                'error' => $error_text,
                'message' => $success_text,
                'data' => $success_data
            ),
            'code' => $code
        );
        switch ($data_type) {
            case 'json':
                exit(json_encode($return));
                break;
            case 'xml':
                $ci =& get_instance();
                $ci->load->library('xmlrpc');
                exit($ci->xmlrpc->send_response($return));
                break;
            case 'yaml':
                exit(yaml_emit($return));
                break;
            default :
                exit(json_encode($return));
                break;
        }
    }

}

if(!function_exists('substr_word')){
    function substr_word($body,$maxlength){
        if (strlen($body)<$maxlength) return $body;
        $body = substr($body, 0, $maxlength);
        $rpos = strrpos($body,' ');
        if ($rpos>0) $body = substr($body, 0, $rpos);
        return $body;
    }
}

if (!function_exists('get_percentage')) {

    function get_percentage($total, $number) {
        if ($total > 0) {
            return floor((int)$number / ((int)$total / 100));
        } else {
            return 0;
        }
    }

}


function toAlpha($data){
    $alphabet =   array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
    $alpha_flip = array_flip($alphabet);
    if($data <= 25){
      return $alphabet[$data];
    }
    elseif($data > 25){
      $dividend = ($data + 1);
      $alpha = '';
      $modulo;
      while ($dividend > 0){
        $modulo = ($dividend - 1) % 26;
        $alpha = $alphabet[$modulo] . $alpha;
        $dividend = floor((($dividend - $modulo) / 26));
      } 
      return $alpha;
    }
}


function toNum($data) {
    $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    $alpha_flip = array_flip($alphabet);
    $return_value = -1;
    $length = strlen($data);
    for ($i = 0; $i < $length; $i++) {
        $return_value +=
            ($alpha_flip[$data[$i]] + 1) * pow(26, ($length - $i - 1));
    }
    return $return_value;
}



?>