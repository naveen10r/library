<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('csrf_token'))
{
    function csrf_token($is_not_input_tag = false)
    {
        $token = generate_token();
        $value = generate_value();
        $ci =& get_instance();
        $ci->session->set_userdata('csrf_token_auth', array('csrf_token' => $token, 'csrf_token_value' => $value));
        if($is_not_input_tag === true)
        {
            return array($token => $value);
        } else {
            return '<input type="hidden" name="'.$token.'" value="'.$value.'" />';
        }
    }
}


if(!function_exists('generate_token'))
{
    function generate_token()
    {
        return hash('ripemd128',uniqid().mt_rand().time().rand(11111,999999));
    }
}

if(!function_exists('generate_value'))
{
    function generate_value()
    {
        $permitted_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return hash('gost',uniqid().mt_rand().time().substr(str_shuffle($permitted_chars), 0, 6));
    }
}

if(!function_exists('verify_csrf'))
{
    function verify_csrf()
    {
        $ci =& get_instance();
        $isToken = $ci->session->userdata('csrf_token_auth');
        $csrf_token_value = $ci->input->post($isToken['csrf_token']);
        if(empty($csrf_token_value) OR empty($isToken)){ return false; }
        if($csrf_token_value == $isToken['csrf_token_value'])
        {
            return true;
        } else {
            return false;
        }
    }
}