<?php

if(!function_exists('admin_footer'))
{
    function admin_footer()
    {
        return '<div class="row-fluid">
            <div id="footer" class="span12"> '.date('Y').' &copy; Matrix Admin. </div>
        </div>';
    }
}