<?php


if(!function_exists('get_product_isbn')){
    function get_product_isbn($insert_id, $sort_code){
        $CI =& get_instance();
        $CI->load->model(array('mx-admin-product/ProductService'));
        return $CI->ProductService->get_last_isbn($insert_id, $sort_code);
    }
}
