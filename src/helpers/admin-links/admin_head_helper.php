<?php

function admin_head() {
    echo '<div id="header">
    <h1><a href="'.base_url('web-inf/dashboard.html').'">Matrix Admin</a></h1>
    </div>';
}

function admin_head_menu() {
    $ci =& get_instance();
    $sessionName = $ci->session->userdata('userAuth');
    $lang = language_encode(array('menu_head_sign_out','welcome','language','message'));
    echo '<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">'.$lang['welcome'].' '.$sessionName['u_code'].'</span><b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="'.base_url('users/SystemUsers/MyProfile').'"><i class="icon-user"></i> My Profile</a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="icon-check"></i> My Tasks</a></li>
                <li class="divider"></li>
                <li><a href="'.base_url('mx-admin/logout').'"><i class="icon-key"></i> '.$lang['menu_head_sign_out'].'</a></li>
            </ul>
        </li>
        <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">'.$lang['message'].'</span> <span class="label label-important">5</span> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> new message</a></li>
                <li class="divider"></li>
                <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
                <li class="divider"></li>
                <li><a class="sOutbox" title="" href="#"><i class="icon-arrow-up"></i> outbox</a></li>
                <li class="divider"></li>
                <li><a class="sTrash" title="" href="#"><i class="icon-trash"></i> trash</a></li>
            </ul>
        </li>
        <li class="dropdown" id="menu-messages">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                <i class="icon icon-language" aria-hidden="true"></i> 
                <span class="text">'.$lang['language'].'</span><b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a href="'. base_url('default/general/setLanguage/english').'">English</a></li>
                <li class="divider"></li>
                <li><a href="'. base_url('default/general/setLanguage/hindi').'">हिन्दी</a></li>
                <li class="divider"></li>
                <li><a href="'. base_url('default/general/setLanguage/sanskrit').'">संस्कृत</a></li>
            </ul>
        </li>
        <li class=""><a title="" href="'.base_url('mx-admin/logout').'"><i class="icon icon-share-alt"></i> <span class="text">'.$lang['menu_head_sign_out'].'</span></a></li>
		<li class=""><a target="_blank" href="'.base_url().'"><span class="text">'.STATIC_SITE_NAME.'</span></a></li>
    </ul>
    </div>';
}


function admin_head_search()
{
    echo '<div id="search">
    <input type="text" placeholder="Search here..."/>
    <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
    </div>';
}
