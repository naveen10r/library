<?php

if (!function_exists('captcha_security')) {

    function captcha_security() {
        try{
            $CI = & get_instance();
            $CI->load->helper('captcha');
            $CI->load->model('ExtraWork');
            $word = $CI->ExtraWork->getCaptchaWord();
            $vals = array(
                'word' => $word->word,
                'img_path' => getcwd() . '/captcha/',
                'img_url' => base_url('captcha'),
                'font_path' => base_url('web-inf/font-awesome/Chunkfive.otf'),
                'img_width' => 150,
                'img_height' => 40,
                'font_size' => 6,
                'colors' => array(
                    'background' => array(209, 209, 209),
                    'border' => array(0, 119, 204),
                    'text' => array(201, 20, 10),
                    'grid' => array(255, 255, 255)
                )
            );
            return create_captcha($vals);
        } catch(Exception $e) {
          echo 'Message: ' .$e->getMessage();
        }        
    }
}

if (!function_exists('valid_captcha')) {

    function valid_captcha($word) {
        $CI = & get_instance();
        $CI->load->library('session');
        $captcha_word = $CI->session->tempdata('captcha_word');
        if (empty($word)) {
            $CI->form_validation->set_message('captchaWord', 'The captcha field is required.');
            return false;
        } elseif ($captcha_word != $word) {
            $CI->form_validation->set_message('captchaWord', 'The captcha security code doesn\'t matched.');
            return false;
        } else {
            return true;
        }
    }

}

if (!function_exists('valid_files_ext')) {

    function valid_files_ext($picture, $fileExt) {
        $pictures = !empty($picture) ? $picture : 'pictures';
        $upImg = $_FILES['uploadImg'];
        $CI = & get_instance();
        $CI->load->library('session');
        if (empty($upImg['name'])) {
            $CI->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is required.');
            return false;
        } else {
            $realExt = strtolower($upImg['name']);
            $ext = explode('.', $realExt);
            $extension = end($ext);
            $fileExt = explode('|', $fileExt);
            if (!in_array($extension, $fileExt)) {
                $CI->form_validation->set_message('fileWithValidation', 'The ' . $pictures . ' field is not in the correct format.');
                return false;
            }
        }
        return true;
    }

}

if (!function_exists('ajax_refresh_captcha')){
    function ajax_refresh_captcha(){
        $CI = & get_instance();
        $CI->load->library('session');
        $CI->session->unset_tempdata('captcha_word');
        $new_captcha = captcha_security();
        $CI->session->set_tempdata('captcha_word', $new_captcha['word'], 600);
        unset($new_captcha['image']);
        unset($new_captcha['word']);
        $response = array('status'=> true, 'response_data' => $new_captcha);
        exit(json_encode($response));
    }
}
