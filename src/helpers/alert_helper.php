<?php
ob_start();
if (!function_exists('success')) {
    function success($message)
    {
        $suc = '';
        $suc .= '<div class="alert alert-success alert-white rounded">';
        $suc .= '<div class="icon"><i class="fa fa-check"></i> ';
        $suc .= $message;
        $suc .= '</div></div>';
        return $suc;
    }
}
if (!function_exists('info')) {
    function info($message)
    {
        $suc = '';
        $suc .= '<div class="alert alert-info alert-white rounded">';
        $suc .= '<div class="icon"><i class="fa fa-info-circle"></i> ';
        $suc .= $message;
        $suc .= '</div></div>';
        return $suc;
    }
}
if (!function_exists('warning')) {
    function warning($message)
    {
        $suc = '';
        $suc .= '<div class="alert alert-warning alert-white rounded">';
        $suc .= '<div class="icon"><i class="fa fa-warning"></i> ';
        $suc .= $message;
        $suc .= '</div></div>';
        return $suc;
    }
}
if (!function_exists('danger')) {
    function danger($message)
    {
        $suc = '';
        $suc .= '<div class="alert alert-danger alert-white rounded">';
        $suc .= '<div class="icon"><i class="fa fa-times-circle"></i> ';
        $suc .= $message;
        $suc .= '</div></div>';
        return $suc;
    }
} 
?>