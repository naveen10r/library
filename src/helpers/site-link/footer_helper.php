<?php

if (!function_exists('footer_up')) {

    function footer_up() {
        $fup = '';
        $fup .= '<div class="footer-top">';
        $fup .= '<div class="container">';
        $fup .= '<div class="row">';
        $fup .= '<div class="col-sm-2">';
        $fup .= '<div class="companyinfo">';
        $fup .= '<h2><span>e</span>-shopper</h2>';
        $fup .= '<p>Something of website</p>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '<div class="col-sm-7">';
        $fup .= '<div class="col-sm-3">';
        $fup .= '<div class="video-gallery text-center">';
        $fup .= '<a href="#">';
        $fup .= '<div class="iframe-img">';
        $fup .= '<img src="' . base_url('web-www/images/home/iframe1.png') . '" />';
        $fup .= '</div>';
        $fup .= '<div class="overlay-icon">';
        $fup .= '<i class="fa fa-play-circle-o"></i>';
        $fup .= '</div>';
        $fup .= '</a>';
        $fup .= '<p>Circle of Hands</p>';
        $fup .= '<h2>24 DEC 2014</h2>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '<div class="col-sm-3">';
        $fup .= '<div class="video-gallery text-center">';
        $fup .= '<a href="#">';
        $fup .= '<div class="iframe-img">';
        $fup .= '<img src="' . base_url('web-www/images/home/iframe2.png') . '" />';
        $fup .= '</div>';
        $fup .= '<div class="overlay-icon">';
        $fup .= '<i class="fa fa-play-circle-o"></i>';
        $fup .= '</div>';
        $fup .= '</a>';
        $fup .= '<p>Circle of Hands</p>';
        $fup .= '<h2>24 DEC 2014</h2>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '<div class="col-sm-3">';
        $fup .= '<div class="video-gallery text-center">';
        $fup .= '<a href="#">';
        $fup .= '<div class="iframe-img">';
        $fup .= '<img src="' . base_url('web-www/images/home/iframe3.png') . '" />';
        $fup .= '</div>';
        $fup .= '<div class="overlay-icon">';
        $fup .= '<i class="fa fa-play-circle-o"></i>';
        $fup .= '</div>';
        $fup .= '</a>';
        $fup .= '<p>Circle of Hands</p>';
        $fup .= '<h2>24 DEC 2014</h2>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '<div class="col-sm-3">';
        $fup .= '<div class="video-gallery text-center">';
        $fup .= '<a href="#">';
        $fup .= '<div class="iframe-img">';
        $fup .= '<img src="' . base_url('web-www/images/home/iframe4.png') . '" alt="" />';
        $fup .= '</div>';
        $fup .= '<div class="overlay-icon">';
        $fup .= '<i class="fa fa-play-circle-o"></i>';
        $fup .= '</div>';
        $fup .= '</a>';
        $fup .= '<p>Circle of Hands</p>';
        $fup .= '<h2>24 DEC 2014</h2>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '<div class="col-sm-3">';
        $fup .= '<div class="address">';
        $fup .= '<img src="' . base_url('web-www/images/home/map.png') . '" />';
        $fup .= '<p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '</div>';
        $fup .= '</div>';
        return $fup;
    }

}


if (!function_exists('footer_menu')) {

    function footer_menu() {
        $fMenu = '';
        $fMenu .= '<div class="footer-widget">';
        $fMenu .= '<div class="container">';
        $fMenu .= '<div class="row">';
        $fMenu .= '<div class="col-sm-2">';
        $fMenu .= '<div class="single-widget">';
        $fMenu .= '<h2>Service</h2>';
        $fMenu .= '<ul class="nav nav-pills nav-stacked">';
        $fMenu .= '<li><a href="#">Online Help</a></li>';
        $fMenu .= '<li><a href="#">Contact Us</a></li>';
        $fMenu .= '<li><a href="#">Order Status</a></li>';
        $fMenu .= '<li><a href="#">Change Location</a></li>';
        $fMenu .= '<li><a href="#">FAQs</a></li>';
        $fMenu .= '</ul>';
        $fMenu .= '</div>';
        $fMenu .= '</div>';
        $fMenu .= '<div class="col-sm-2">';
        $fMenu .= '<div class="single-widget">';
        $fMenu .= '<h2>Quock Shop</h2>';
        $fMenu .= '<ul class="nav nav-pills nav-stacked">';
        $fMenu .= '<li><a href="#">T-Shirt</a></li>';
        $fMenu .= '<li><a href="#">Mens</a></li>';
        $fMenu .= '<li><a href="#">Womens</a></li>';
        $fMenu .= '<li><a href="#">Gift Cards</a></li>';
        $fMenu .= '<li><a href="#">Shoes</a></li>';
        $fMenu .= '</ul>';
        $fMenu .= '</div>';
        $fMenu .= '</div>';
        $fMenu .= '<div class="col-sm-2">';
        $fMenu .= '<div class="single-widget">';
        $fMenu .= '<h2>Policies</h2>';
        $fMenu .= '<ul class="nav nav-pills nav-stacked">';
        $fMenu .= '<li><a href="#">Terms of Use</a></li>';
        $fMenu .= '<li><a href="#">Privecy Policy</a></li>';
        $fMenu .= '<li><a href="#">Refund Policy</a></li>';
        $fMenu .= '<li><a href="#">Billing System</a></li>';
        $fMenu .= '<li><a href="#">Ticket System</a></li>';
        $fMenu .= '</ul>';
        $fMenu .= '</div>';
        $fMenu .= '</div>';
        $fMenu .= '<div class="col-sm-2">';
        $fMenu .= '<div class="single-widget">';
        $fMenu .= '<h2>About Shopper</h2>';
        $fMenu .= '<ul class="nav nav-pills nav-stacked">';
        $fMenu .= '<li><a href="#">Company Information</a></li>';
        $fMenu .= '<li><a href="#">Careers</a></li>';
        $fMenu .= '<li><a href="#">Store Location</a></li>';
        $fMenu .= '<li><a href="#">Affillate Program</a></li>';
        $fMenu .= '<li><a href="#">Copyright</a></li>';
        $fMenu .= '</ul>';
        $fMenu .= '</div>';
        $fMenu .= '</div>';
        $fMenu .= '<div class="col-sm-3 col-sm-offset-1">';
        $fMenu .= '<div class="single-widget">';
        $fMenu .= '<h2>About Shopper</h2>';
        $fMenu .= '<form action="#" class="searchform">';
        $fMenu .= '<input type="text" placeholder="Your email address" />';
        $fMenu .= '<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>';
        $fMenu .= '<p>Get the most recent updates from <br />our site and be updated your self...</p>';
        $fMenu .= '</form>';
        $fMenu .= '</div>';
        $fMenu .= '</div>';
        $fMenu .= '</div>';
        $fMenu .= '</div>';
        $fMenu .= '</div>';
        return $fMenu;
    }

}

if (!function_exists('footer_contact_us')) {

    function footer_contact_us() {
        $fCU = '';
        $fCU .= '<div class="footer-bottom">';
        $fCU .= '<div class="container">';
        $fCU .= '<div class="row">';
        $fCU .= '<p class="pull-left">Copyright © 2018 @All rights reserved.</p>';
        $fCU .= '<p class="pull-right">Developed by <span><a target="_blank" href="#">'.MAIN_LOGO_TXT.'</a></span></p>';
        $fCU .= '</div>';
        $fCU .= '</div>';
        $fCU .= '</div>';
    }
}


if (!function_exists('footer_access_links')) {

    function footer_access_links() {
        $fal = '';
        $fal .= '<ul class="pull-right list-inline">';
        $fal .= '<li><img src="'.base_url().'/images/payment-icon/cirrus.png" alt="PaymentGateway" /></li>';
        $fal .= '<li><img src="'.base_url().'/images/payment-icon/paypal.png" alt="PaymentGateway" /></li>';
        $fal .= '<li><img src="'.base_url().'/images/payment-icon/visa.png" alt="PaymentGateway" /></li>';
        $fal .= '<li><img src="'.base_url().'/images/payment-icon/mastercard.png" alt="PaymentGateway" /></li>';
        $fal .= '<li><img src="'.base_url().'/images/payment-icon/americanexpress.png" alt="PaymentGateway" /></li>';
        $fal .= '</ul>';
        echo $fal;
    }
}