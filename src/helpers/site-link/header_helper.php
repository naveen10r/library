<?php

/*
 * @Auther : Naveen Rastogi
 * @Email : naveen.workmail@gmail.com
 * @function : This function for check valid uri in session user
 * @created : July 2017
 * @param : string url
 * @return : boolean
 */

if (!function_exists('header_top')) {

    function header_top($isOld = true) {
        $ci = & get_instance();
        $ExUserSess = $ci->session->userdata('ExUserSess');
        $topHear = '';
        if ($isOld === FALSE) {
            $topHear .= '<div class="col-sm-8 col-xs-12">';
            $topHear .= '<div class="header-links">';
            $topHear .= '<ul class="nav navbar-nav pull-left">';
            $topHear .= '<li><a href="' . site_url() . '"><i class="fa fa-home"></i><span class="hidden-sm hidden-xs">Home</span></a></li>';
            $topHear .= '<li><a href="#"><i class="fa fa-heart"></i><span class="hidden-sm hidden-xs">Wish List(0)</span></a></li>';
            $topHear .= '<li><a href="#"><i class="fa fa-shopping-cart"></i><span class="hidden-sm hidden-xs">Shopping Cart</span></a></li>';
            if (empty($ExUserSess) AND ! isset($ExUserSess)) {
                $topHear .= '<li><a href="#"><i class="fa fa-unlock"></i><span class="hidden-sm hidden-xs">Register</span></a></li>';
            }
            $topHear .= '</ul>';
            $topHear .= '</div>';
            $topHear .= '</div>';
        } else {
            $topHear .= '<div class="header_top">';
            $topHear .= '<div class="container">';
            $topHear .= '<div class="row">';
            $topHear .= '<div class="col-sm-6">';
            $topHear .= '<div class="contactinfo">';
            $topHear .= '<ul class="nav nav-pills">';
            $topHear .= '<li><a href="#"><i class="fa fa-phone"></i>' . SITE_LINK['landLine'] . '</a></li>';
            $topHear .= '<li><a href="#"><i class="fa fa-whatsapp"></i>' . SITE_LINK['wtsap'] . '</a></li>';
            $topHear .= '<li><a href="#"><i class="fa fa-envelope"></i>' . SITE_LINK['ccEmail'] . '</a></li>';
            $topHear .= '</ul>';
            $topHear .= '</div>';
            $topHear .= '</div>';
            $topHear .= '<div class="col-sm-6">';
            $topHear .= '<div class="btn-group pull-right">';
            $topHear .= '<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">';
            $topHear .= 'Language';
            $topHear .= '<span class="caret"></span>';
            $topHear .= '</button>';
            $topHear .= '<ul class="dropdown-menu">';
            $topHear .= '<li><a href="#">English</a></li>';
            $topHear .= '<li><a href="#">हिन्दी</a></li>';
            $topHear .= '</ul>';
            $topHear .= '</div>';
            $topHear .= '<div class="social-icons pull-right">';
            $topHear .= '<ul class="nav navbar-nav">';
            $topHear .= '<li><a href="' . SITE_LINK['fb'] . '"><i class="fa fa-facebook"></i></a></li>';
            $topHear .= '<li><a href="' . SITE_LINK['tw'] . '"><i class="fa fa-twitter"></i></a></li>';
            $topHear .= '<li><a href="' . SITE_LINK['ln'] . '"><i class="fa fa-linkedin"></i></a></li>';
            $topHear .= '</ul>';
            $topHear .= '</div>';
            $topHear .= '</div>';
            $topHear .= '</div>';
            $topHear .= '</div>';
            $topHear .= '</div>';
        }
        return $topHear;
    }

}

if (!function_exists('header_top_part_two')) {

    function header_top_part_two() {
        $ci = & get_instance();
        $ExUserSess = $ci->session->userdata('ExUserSess');
        $topPartTwo = '';
        $topPartTwo .= '<div class="col-sm-4 col-xs-12"><div class="pull-right">';

        $topPartTwo .= '<div class="btn-group">';
        if (!empty($ExUserSess) AND isset($ExUserSess)) {
            $topPartTwo .= '<button class="btn btn-link dropdown-toggle" data-toggle="dropdown">Welcome ' . $ExUserSess['name'] . '<i class="fa fa-caret-down"></i></button>';
            $topPartTwo .= '<ul class="dropdown-menu">';
            $topPartTwo .= '<li><a tabindex="-1" href="' . site_url('users/ExternalUser/MyAccount') . '">My Account</a></li>';
            $topPartTwo .= '<li><a tabindex="-1" href="' . site_url() . '">My Order</a></li>';
            $topPartTwo .= '<li><a tabindex="-1" href="' . site_url('sign-out') . '">Logout</a></li>';
            $topPartTwo .= '</ul>';
        } else {
            $topPartTwo .= '<a href="' . site_url('users/ExternalUser/loginRegister') . '"><button class="btn btn-link">Login</button></a>';
        }
        $topPartTwo .= '</div>';
        $topPartTwo .= '<div class="btn-group">';
        $topPartTwo .= '<button class="btn btn-link dropdown-toggle" data-toggle="dropdown">Language<i class="fa fa-caret-down"></i></button>';
        $topPartTwo .= '<ul class="pull-right dropdown-menu">';
        $topPartTwo .= '<li><a href="' . site_url() . '" tabindex="-1">English</a></li>';
        $topPartTwo .= '<li><a href="' . site_url() . '" tabindex="-1">हिन्दी</a></li>';
        $topPartTwo .= '</ul>';
        $topPartTwo .= '</div></div></div>';
        echo $topPartTwo;
    }

}


if (!function_exists('header_middle')) {

    function header_middle() {
        $CI = & get_instance();
        $userSession = $CI->session->userdata('ExUserSess');
        $midHead = '';
        $midHead .= '<div class="header-middle">';
        $midHead .= '<div class="container">';
        $midHead .= '<div class="row">';
        $midHead .= '<div class="col-sm-4">';
        $midHead .= '<div class="logo pull-left">';
        $midHead .= '<a href="' . site_url() . '"><img src="' . base_url('web-www/images/home/logo.png') . '" /></a>';
        $midHead .= '</div>';
        $midHead .= '</div>';
        $midHead .= '<div class="col-sm-8">';
        $midHead .= '<div class="shop-menu pull-right">';
        $midHead .= '<ul class="nav navbar-nav">';
        $midHead .= '<li>';
        $midHead .= '<ul class="dropdown-menu">';
        $midHead .= '<li><a href="' . site_url() . '">English</a></li>';
        $midHead .= '<li><a href="' . site_url() . '">हिन्दी</a></li>';
        $midHead .= '</ul>';
        $midHead .= '</li>';
        $midHead .= '<li><a href="' . site_url() . '"><i class="fa fa-star"></i> Wishlist</a></li>';
        $midHead .= '<li><a href="' . site_url() . '"><i class="fa fa-crosshairs"></i> Checkout</a></li>';
        $midHead .= '<li><a href="' . site_url() . '"><i class="fa fa-shopping-cart"></i> Cart</a></li>';
        if (isset($userSession) OR ! empty($userSession)) {
            $midHead .= '<li class="dropdown"><a href="' . site_url() . '"><i class="fa fa-user"></i> ' . $userSession['name'] . '<i class="fa fa-angle-down"></i></a>';
            $midHead .= '<ul role="menu" class="sub-menu top-head">';
            $midHead .= '<li><a href="' . site_url('users/ExternalUser/MyAccount') . '">My Account</a></li>';
            $midHead .= '<li><a href="' . site_url() . '">My Order</a></li>';
            $midHead .= '<li><a href="' . site_url('sign-out') . '">Logout</a></li>';
            $midHead .= '</ul>';
            $midHead .= '</li>';
        } else {
            $midHead .= '<li><a href="' . site_url('users/ExternalUser/loginRegister') . '"><i class="fa fa-lock"></i> Login/Register</a></li>';
        }
        $midHead .= '</ul>';
        $midHead .= '</div>';
        $midHead .= '</div>';
        $midHead .= '</div>';
        $midHead .= '</div>';
        $midHead .= '</div>';
        return $midHead;
    }

}

if (!function_exists('header_bottom')) {

    function header_bottom() {
        $hb = '';
        $hb .= '<div class="header-bottom">';
        $hb .= '<div class="container">';
        $hb .= '<div class="row">';
        $hb .= '<div class="col-sm-9">';
        $hb .= '<div class="navbar-header">';
        $hb .= '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">';
        $hb .= '<span class="sr-only">Toggle navigation</span>';
        $hb .= '<span class="icon-bar"></span>';
        $hb .= '<span class="icon-bar"></span>';
        $hb .= '<span class="icon-bar"></span>';
        $hb .= '</button>';
        $hb .= '</div>';
        $hb .= '<div class="mainmenu pull-left">';
        $hb .= '<ul class="nav navbar-nav collapse navbar-collapse">';
        $hb .= '<li><a href="#" class="active">Home</a></li>';
        $hb .= '<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>';
        $hb .= '<ul role="menu" class="sub-menu">';
        $hb .= '<li><a href="#">Products</a></li>';
        $hb .= '<li><a href="#">Product Details</a></li>';
        $hb .= '<li><a href="#">A</a></li>';
        $hb .= '<li><a href="#">Cart</a></li>';
        $hb .= '<li><a href="#">Lin</a></li>';
        $hb .= '</ul>';
        $hb .= '</li>';
        $hb .= '<li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>';
        $hb .= '<ul role="menu" class="sub-menu">';
        $hb .= '<li><a href="#">Blog List</a></li>';
        $hb .= '<li><a href="#">Blog Single</a></li>';
        $hb .= '</ul>';
        $hb .= '</li>';
        $hb .= '<li><a href="#">404</a></li>';
        $hb .= '<li><a href="#">Contact</a></li>';
        $hb .= '</ul>';
        $hb .= '</div>';
        $hb .= '</div>';
        $hb .= '<div class="col-sm-3">';
        $hb .= '<div class="search_box pull-right">';
        $hb .= '<input type="text" placeholder="Search" />';
        $hb .= '</div>';
        $hb .= '</div>';
        $hb .= '</div>';
        $hb .= '</div>';
        $hb .= '</div>';
        return $hb;
    }

}

if (!function_exists('logo_and_search')) {

    function logo_and_search($onlyLogo = TRUE) {
        $logo = '';
        $logo .= '<div class="col-md-6">';
        $logo .= '<div id="logo">';
        $logo .= '<a href="' . site_url() . '"><img src="' . base_url('upload/images/logo.png') . '" title="Spice Shoppe" alt="Spice Shoppe" class="img-responsive" /></a>';
        $logo .= '</div></div>';

        $search = '';
        if ($onlyLogo) {
            $search .= '<div class="col-md-3"><div id="search"><div class="input-group">';
            $search .= '<input type="text" class="form-control input-lg" placeholder="Search">';
            $search .= '<span class="input-group-btn">';
            $search .= '<button class="btn btn-lg" type="button">';
            $search .= '<i class="fa fa-search"></i></button></span></div></div></div>';
        }
        echo $logo . ' <!-- SEARCH --> ' . $search;
    }

}
