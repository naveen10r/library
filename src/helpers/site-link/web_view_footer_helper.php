<?php

if (!function_exists('footer_tag')) {

    function footer_tag() {
        $footer = '';
        $footer .= '<footer>';
        $footer .= '<div class="footerbefore">';
        $footer .= '<div class="container">';
        $footer .= '<div class="newsletter">';
        $footer .= '<h5 class="news-title">Subsribe Now</h5>';
        $footer .= '<div class="newsletter-message">Get Daily Update Into Your Mail For join Now</div>';
        $footer .= '<div class="newsright">';
        $footer .= '<form action="#" method="post">';
        $footer .= '<div class="form-group required">';
        $footer .= '<label class="col-sm-2 control-label" for="input-firstname">Email</label>';
        $footer .= '<div class="input-news">';
        $footer .= '<input type="email" id="subscriber_now" value="" placeholder="Enter Your Email Address" class="form-control input-lg" />';
        $footer .= '<div id="suscribe_response"></div>';
        $footer .= '</div>';
        $footer .= '<div class="subscribe-btn">';
        $footer .= '<button type="button" class="btn btn-default btn-lg suscribe_now_btn">Subscribe</i></button>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '</form>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '<div id="footer" class="container">';
        $footer .= '<div class="row">';
        $footer .= '<div class="footer-blocks">';
        $footer .= '<div class="col-sm-3 column footerleft">';
        $footer .= '<div id="czfootercmsblock" class="footer-cms-block">';
        $footer .= '<div id="footerlogo">';
        $footer .= '<div class="footerdiv">';
        $footer .= '<div class="footerlogo">';
        $footer .= '<img src="'.base_url('web_view/').'image/catalog/footer-logo.png" />';
        $footer .= '</div>';
        $footer .= '<div class="footerdesc">LeafLIB is an education organization that promotes neighborhood book exchanges. The joy of sharing books is a gift for others.</div>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '<div class="social-block">';
        $footer .= '<ul>';
        $footer .= '<li class="facebook"><a href="https://www.facebook.com/Kovitus-104734314938424" target="_blank"><span>Facebook</span></a></li>';
        $footer .= '<li class="instagram"><a href="https://www.instagram.com/teamkovitus/" target="_blank"><span>Instagram</span></a></li>';
        /*
        $footer .= '<li class="twitter"><a href="#"><span>Twitter</span></a></li>';
        $footer .= '<li class="youtube"><a href="#"><span>YouTube</span></a></li>';
        $footer .= '<li class="googleplus"><a href="#"><span>Google +</span></a></li>';
        
        */
        $footer .= '</ul>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '<div id="extra-link" class="col-sm-3 column">';
        $footer .= '<h5>Extras</h5>';
        $footer .= '<ul class="list-unstyled">';
        $footer .= '<li><a href="'.site_url('contact-us').'">Contact Us</a></li>';
        $footer .= '<li><a href="'.site_url('refer-a-friend').'">Refer A Friend</a></li>';
        $footer .= '<li><a href="'.site_url('request-a-book').'">Request A Book</a></li>';
        $footer .= '<li><a href="'.site_url('about-us').'">About Us</a></li>';
        $footer .= '<li><a href="'.site_url('privacy-policy').'">Privacy Policy</a></li>';
        $footer .= '<li><a href="'.site_url('terms-and-condition').'">Terms&condition</a></li>';
        $footer .= '</ul>';
        $footer .= '</div>';
        $footer .= '<div class="col-sm-3 column">';
        $footer .= '<h5>My Account</h5>';
        $footer .= '<ul class="list-unstyled">';
        $login = site_url('login');
        $footer .= '<li><a href="'.$login.'">My Account</a></li>';
        $footer .= '<li><a href="'.$login.'">Order History</a></li>';
        $footer .= '</ul>';
        $footer .= '</div>';

        $footer .= '<div class="col-sm-3 column footerright">';
        $footer .= '<div class="contact-block">';
        $footer .= '<h5>Store information</h5>';
        $footer .= '<ul>';
        $footer .= '<li>';
        $footer .= '<i class="fa fa-map-marker"></i>';
        $footer .= '<span>LeafLIB.com<br> Noida, India</span>';
        $footer .= '</li>';
        /* 
        $footer .= '<li><i class="fa fa-phone"></i><span>000-000-0000</span></li>'; 
        $footer .= '<li><i class="fa fa-fax"></i><span>123456</span></li>';
        */
        $footer .= '';
        $footer .= '';
        $footer .= '';
        $footer .= '<li><a href="mailto:info@kovitus.com">';
        $footer .= '<i class="fa fa-envelope-o"></i>';
        $footer .= '<span>info@leaflib.com</span></a>';
        $footer .= '</li>';
        $footer .= '</ul>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '</div>';
        /*
        $footer .= '<div id="bottom-footer" class="bottomfooter">';
        $footer .= '<div class="container">';
        $footer .= '<div class="authorlink-block">';
        $footer .= '<h5>Top Authors:</h5>';
        $footer .= '<ul>';
        $footer .= '<li>';
        $footer .= '<a href="#" title="Stephen King">Stephen King</a>';
        $footer .= '</li>';
        $footer .= '<li>';
        $footer .= '<a href="#" title="Stephenie Meyer">Stephenie Meyer</a>';
        $footer .= '</li>';
        $footer .= '<li>';
        $footer .= '<a href="#" title="Dan Brown">Dan Brown</a>';
        $footer .= '</li>';
        $footer .= '<li>';
        $footer .= '<a href="#" title="John Grisham">John Grisham</a>';
        $footer .= '</li>';
        $footer .= '<li>';
        $footer .= '<a href="#" title="Nora Roberts">Nora Roberts</a>';
        $footer .= '</li>';
        $footer .= '</ul>';
        $footer .= '</div>';
        $footer .= '<div class="paiement_logo_block footer-block">';
        $footer .= '<img src="'.base_url('web_view/').'image/catalog/amex.png" alt="amex" width="33" height="21">';
        $footer .= '<img src="'.base_url('web_view/').'image/catalog/maestro.png" alt="maestro" width="33" height="21">';
        $footer .= '<img src="'.base_url('web_view/').'image/catalog/paypal.png" alt="paypal" width="33" height="21">';
        $footer .= '<img src="'.base_url('web_view/').'image/catalog/master_card.png" alt="master_card" width="33" height="21">';
        $footer .= '</div>';
        $footer .= '<p id="powered" class="powered">Powered By <a href="#">OpenCart</a> KovitUS - Books Store & copy;
        2020</p>';
        */
        $footer .= '</div>';
        $footer .= '</div>';
        $footer .= '</footer>';
        $footer .= '<script type="text/javascript">';
        $footer .= '$(".suscribe_now_btn").click(function () {';
        $footer .= 'var subscriber_now = $("#subscriber_now").val();';
        $footer .= '$.ajax({';
        $footer .= 'type: "POST",';
        $footer .= 'url: "'.site_url('product/products/subscribe').'",';
        $footer .= 'data: {"subscriber_now": subscriber_now},';
        $footer .= 'success: function (returntxt)';
        $footer .= '{';
        $footer .= 'var parseJson = JSON.parse(returntxt);';
        $footer .= 'if (parseJson.status === true) {';
        $footer .= '$("#suscribe_response").html("<font color=\"#FFFFFF\">"+parseJson.responce.message+"</font>");';
        $footer .= '} else {';
        $footer .= '$("#suscribe_response").html("<font color=\"#F3949C\">"+parseJson.responce.error+"</font>");';
        $footer .= '}';
        $footer .= '}';
        $footer .= '});';
        $footer .= '});';
        $footer .= '</script>';
        echo $footer;
    }

}
