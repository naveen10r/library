<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('account_setting')) {

    function account_setting($linkOf = '') {
        $as = '';
        $pi = '';
        $ma = '';
        switch ($linkOf) {
            case 'Personal-Info':
                $pi = 'class="acnt-active"';
                break;
            case 'Address':
                $ma = 'class="acnt-active"';
                break;
            default :
                $pi = 'class="acnt-active"';
                break;
        }

        $as .= '<p '.$pi.'><a href="'. site_url('users/ExternalUser/MyAccount').'">Personal Information</a></p>';
        $as .= '<p '.$ma.'><a href="'. site_url('users/ExternalUser/MyAccount/Address').'">Manage Addresses</a></p>';
        $as .= '<p><a href="">Notification Preferences</a></p>';
        $as .= '<p><a href="">PAN Card Information</a></p>';
        echo $as;
    }

}
