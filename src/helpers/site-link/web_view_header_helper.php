<?php

if (!function_exists('header_container')) {

    function header_container() {
        $CI = & get_instance();
        $userName = 'Login-Register';
        $is_session = false;
        $ExUserSess = array();
        $currentCart = 0;
        if (!empty($CI->session->userdata('ExUserSess'))) {
            $ExUserSess = $CI->session->userdata('ExUserSess');
            if (!empty($ExUserSess['new_notification'])) {
                $userName = $ExUserSess['name'] . '<span id="cart-quantity"> ' . $ExUserSess['new_notification'] . '</span>';
            } else {
                $userName = $ExUserSess['name'];
            }
            $is_session = true;
            $currentCart = $CI->session->userdata('CartCount');
        }
        $div = '';
		

        $div .= ' <!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-214140113-1"></script>
			<script>
			  window.dataLayer = window.dataLayer || [];
			  function gtag(){dataLayer.push(arguments);}
			  gtag("js", new Date());

			  gtag("config", "UA-214140113-1");
			</script>

			';
       $div .= '<div class="header-container">';
        $div .= '<div class="container">';
        $div .= '<div class="row">';
        $div .= '<div class="header-main">';
        $div .= '<div class="header-logo">';
        $div .= '<div id="logo">';
        $div .= '<a href="' . site_url('') . '"><img src="' . base_url(MAIN_LOGO_PATH) . '" title="'.MAIN_LOGO_TXT.'" alt="'.MAIN_LOGO_TXT.'" class="img-responsive" /></a>';
        $div .= '</div>';
        $div .= '</div>';
        /* =========== CART =========== */
        if ($is_session === true){
            $view_cart = site_url('view-cart');
            $div .= '<div class="header-cart"><div id="cart" class="btn-group btn-block">';
            $div .= '<span class="cart_heading" style="line-height: 35px !important;" data-toggle="dropdown">Your Cart</span>';
            $div .= '<button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="btn btn-inverse btn-block btn-lg dropdown-toggle"><i class="fa fa-shopping-cart"></i></span>';
            $div .= '<span id="cart-quantity">'.$currentCart.'</span>';
            $div .= '</button>';
            $div .= '<ul class="dropdown-menu pull-right cart-menu">';
            $div .= '<li>';
            $div .= '<p class="text-center">'.$currentCart.' book(s) in your cart. ';
            $div .= '<a href="'.$view_cart.'" style="color:#E91E76">View Cart</a>';
            $div .= '</p>';
            $div .= '</li>';
            $div .= '</ul>';
            $div .= '</div>';
            $div .= '</div>';
        }
        /* =========== CART =========== */
        $div .= '<div class="dropdown myaccount">';
        $div .= '<a href="#" title="Your Account" class="dropdown-toggle">';
        $div .= '<span class="hidden-xs hidden-sm hidden-md">Account</span>';
        $div .= '<span class="login-text">' . $userName . ' </span>';
        $div .= '<i class="fa fa-angle-down" aria-hidden="true"></i></a>';
        $div .= '<ul class="dropdown-menu-right myaccount-menu">';
        if ($is_session) {
            $div .= '<li><a href="' . site_url("account/Services/MyAccount") . '" title="My Account">My Account</a></li>';
            if (!empty($ExUserSess['new_notification'])) {
                $div .= '<li><a href="' . site_url("account/Services/Notification") . '" title="Notification">My Notification (' . $ExUserSess['new_notification'] . ')</a></li>';
            } else {
                $div .= '<li><a href="' . site_url("account/Services/Notification") . '" title="Notification">My Notification </a></li>';
            }
            $div .= '<li><a href="#" id="wishlist-total" title="Wish List (0)">Wish List (0)</a></li>';
            $div .= '<li><a href="' . site_url("sign-out") . '">Sign Out</a></li>';
        } else {
            $div .= '<li><a href="' . site_url("register") . '">Register</a></li>';
            $div .= '<li><a href="' . site_url("login") . '">Login</a></li>';
        }
        $div .= '<li class="lang-curr">';
        $div .= '<div class="pull-left">';
        $div .= '<form action="#" method="post" enctype="multipart/form-data" id="form-language">';
        $div .= '<div class="btn-group">';
        $div .= '<button class="btn btn-link">Language</button>';
        $div .= '<ul class="language-menu">';
        $div .= '<li>';
        $div .= '<button class="btn btn-link btn-block language-select" type="button" name="en-gb">English</button>';
        $div .= '</li>';
        /*
        $div .= '<li>';
        $div .= '<button class="btn btn-link btn-block language-select" type="button" name="ar">Hindi</button>';
        $div .= '</li>';
        */
        $div .= '</ul>';
        $div .= '</div>';
        $div .= '</form>';
        $div .= '</div>';
        $div .= '</li>';
        $div .= '</ul>';
        $div .= '</div>';
        $div .= '</div>';
        $div .= '</div>';
        $div .= '</div>';
        $div .= '</div>';
        echo $div;
    }

}


if (!function_exists('header_top_inner')) {

    function header_top_inner() {
        $ci =& get_instance();
        $class = $ci->uri->segment(1);
        $function = $ci->uri->segment(2);
        $home = '';
        $latest = '';
        $author = '';
        $about_us = '';
        $contact_us = '';
        $careers = '';
        switch($class){
            case 'home':
                if ($function == 'books'){
                    $latest = 'main_menu';
                } else {
                    $home = 'main_menu';
                }
                break;
            case 'author' :
            case 'author-details' :
                $author = 'main_menu';
                break;
            case 'about-us' :
                $about_us = 'main_menu';
                break;
            case 'contact-us' :
                $contact_us = 'main_menu';
                break;
            case 'careers' :
                $careers = 'main_menu';
                break;
            default:
                $home = 'main_menu';
                break;
        }
        $search_url = site_url();
        $query = !empty($ci->input->get('query')) ? $ci->input->get('query') : '';
        $top_inner = '';
        $top_inner .= '<div class="header-top-inner">';
        $top_inner .= '<div class="container">';
        $top_inner .= '<div id="search" class="input-group">';
        $top_inner .= '<span class="search_button"></span>';
        $top_inner .= '<div class="search_toggle">';
        $top_inner .= '<div id="searchbox">';
        $top_inner .= '<form method="get" action="'.$search_url.'">';
        $top_inner .= '<input type="text" name="query" value="'.$query.'" placeholder="Search book title here" class="form-control input-lg" />';
        $top_inner .= '<span class="input-group-btn">';
        $top_inner .= '<button type="submit" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>';
        $top_inner .= '</form>';
        $top_inner .= '</span>';
        $top_inner .= '</div>';
        $top_inner .= '</div>';
        $top_inner .= '</div>';
        $top_inner .= '<div class="header-top-innerleft">';
        $top_inner .= '<nav class="nav-container" role="navigation">';
        $top_inner .= '<div class="nav-inner">';
        /* === === = Menu Code START === === === -->
          <!--Opencart 3 level Category Menu */
        
        $top_inner .= '<div id="menu" class="main-menu">';
        $top_inner .= '<ul class="nav navbar-nav">';
        $top_inner .= '<li class="top_level '.$home.'"><a href="' . site_url() . '">Home</a></li>';
        $top_inner .= '<li class="top_level '.$latest.'"><a href="' . site_url('home/books') . '">Latest</a></li>';
        $top_inner .= '<li class="top_level '.$author.'"><a href="' . site_url('author') . '">Authors</a></li>';
        $top_inner .= '<li class="top_level '.$about_us.'"><a href="' . site_url('about-us') . '">About Us</a></li>';
        $top_inner .= '<li class="top_level '.$careers.'"><a href="' . site_url('careers') . '">Careers</a></li>';
        $top_inner .= '<li class="top_level '.$contact_us.'"><a href="' . site_url('contact-us') . '">Contact Us</a></li>';
        $top_inner .= '</ul>';
        $top_inner .= '</div>';
        $top_inner .= '</div>';
        /* === === === Mobile menu start === === === */
        $top_inner .= '<div id="res-menu" class="main-menu nav-container1 container">';
        $top_inner .= '<div class="nav-responsive"><span>Menu</span><div class="expandable"></div></div>';
        $top_inner .= '<ul class="main-navigation">';
        $top_inner .= '<li class="top_level '.$home.'"><a href="' . site_url() . '">Home</a></li>';
        $top_inner .= '<li class="top_level '.$latest.'"><a href="' . site_url('home/books') . '">Latest</a></li>';
        $top_inner .= '<li class="top_level '.$author.'"><a href="' . site_url('author') . '">Authors</a></li>';
        $top_inner .= '<li class="top_level '.$about_us.'"><a href="' . site_url('about-us') . '">About Us</a></li>';
        $top_inner .= '<li class="top_level '.$contact_us.'"><a href="' . site_url('contact-us') . '">Contact Us</a></li>';
        $top_inner .= '<li class="top_level '.$contact_us.'"><a href="' . site_url('contact-us') . '">Contact Us</a></li>';
        $top_inner .= '</ul>';
        $top_inner .= '</div>';
        $top_inner .= '</nav>';
        $top_inner .= '</div>';
        $top_inner .= '</div>';
        $top_inner .= '</div>';
        echo $top_inner;
    }

}

if (!function_exists('account_service')) {

    function account_service($active) {
        $account = '';
        $account .= '<div id="verticalmenublock" class="box category box-category ">';
        $account .= '<div class="box-heading">Account Service</div>';
        $account .= '<div class="box-content box-content-category">';
        $account .= '<ul id="nav-one" class="dropmenu">';
        /*  */
        $login = $register = $fogtPwd = $myaccount = $addresses = $changePwd = $usersNotify = $myOrder = '';
        $newFlagNotify = '';
        $style = 'style="color:#e91e76; font-weight: bold;"';
        switch ($active) {
            case 'login':
                $login = $style;
                break;
            case 'registration':
                $register = $style;
                break;
            case 'ChangePassword':
                $changePwd = $style;
                break;
            case 'MyAccount':
                $myaccount = $style;
                break;
            case 'Addresses':
                $addresses = $style;
                break;
            case 'forgot-password':
                $fogtPwd = $style;
                break;
            case 'notification':
                $usersNotify = $style;
                break;
            case 'My Order':
                $myOrder = $style;
                break;
        }
        $CI = & get_instance();
        $is_session = false;
        $WithoutSession = '';
        $DeactivateAccount = '';
        $WithoutSession .= '<li class="top_level main"><a href="' . site_url('login') . '" ' . $login . '>Login</a></li>';
        $WithoutSession .= '<li class="top_level main"><a href="' . site_url('register') . '" ' . $register . '>Register</a></li>';
        $WithoutSession .= '<li class="top_level main"><a href="' . site_url('forgot-password') . '" ' . $fogtPwd . '>Forgotten Password</a></li>';
        if (!empty($CI->session->userdata('ExUserSess'))) {
            $ExUserSess = $CI->session->userdata('ExUserSess');
            $WithoutSession = '<li class="top_level main"><a href="' . site_url('account/Services/MyAccount') . '" ' . $myaccount . '>My Account</a></li>';
            $WithoutSession .= '<li class="top_level main"><a href="' . site_url('account/Services/ChangePassword') . '" ' . $changePwd . '>Change Password</a></li>';
            $WithoutSession .= '<li class="top_level main"><a href="' . site_url('account/Services/Addresses') . '" ' . $addresses . '>Address Book</a></li>';
            /*
            $WithoutSession .= '<li class="top_level main"><a href="' . site_url('account/Services/MyOrders') . '"  ' . $myOrder . '>My Orders</a></li>'; */
            $bellIcon = '';
            if (!empty($ExUserSess['new_notification'])) {
                $bellIcon = '(' . $ExUserSess['new_notification'] . ')<i class="fa fa-bell-o pull-right danger" aria-hidden="true"></i>';
            }
            $WithoutSession .= '<li class="top_level main"><a href="' . site_url('account/Services/Notification') . '"  ' . $usersNotify . '>Notification ' . $bellIcon . ' </a></li>';
            $is_session = true;
        }

        $account .= $WithoutSession;
        /*
          $account .= '<li class="top_level main"><a href="#">Wish List</a></li>';
          $account .= '<li class="top_level main"><a href="#">Order History</a></li>';
          $account .= '<li class="top_level main"><a href="#">Recurring payments</a></li>';
          $account .= '<li class="top_level main"><a href="#">Reward Points</a></li>';
          $account .= '<li class="top_level main"><a href="#">Returns</a></li>';
          $account .= '<li class="top_level main"><a href="#">Transactions</a></li>';
          $account .= '<li class="top_level main"><a href="#">Newsletter</a></li>';
         */
        $account .= '</ul>';
        $account .= '</div>';
        $account .= '</div>';
        echo $account;
    }

}

if (!function_exists('information')) {

    function information() {
        $AccountInfo = '';
        $AccountInfo .= '<div class="box">';
        $AccountInfo .= '<div class="box-heading">Information</div>';
        $AccountInfo .= '<div class="list-group">';
        $AccountInfo .= '<a class="list-group-item" href="#">About Us </a>';
        $AccountInfo .= '<a class="list-group-item" href="#">Delivery Information </a>';
        $AccountInfo .= '<a class="list-group-item" href="#">Privacy Policy </a>';
        $AccountInfo .= '<a class="list-group-item" href="#" > Terms & amp; Conditions </a>';
        $AccountInfo .= '<a class="list-group-item" href="#">Contact Us </a>';
        $AccountInfo .= '<a class="list-group-item" href="#">Site Map </a>';
        $AccountInfo .= '</div>';
        $AccountInfo .= '</div>';
        echo $AccountInfo;
    }

}