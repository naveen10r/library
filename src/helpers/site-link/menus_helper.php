<?php

if (!function_exists('main_menus')) {

    function main_menus() {
        $menu = '';
        $menu .= '<div class="collapse navbar-collapse navbar-cat-collapse"><ul class="nav navbar-nav">';
        $menu .= '<li><a href="#">Home</a></li>';
        $menu .= '<li class="dropdown">';
        $menu .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">Educational</a>';
        $menu .= '<ul class="dropdown-menu" role="menu">';
        $menu .= '<li><a tabindex="-1" href="#">Mathematics</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Physics</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Chemistry</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Biology</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Zoology</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Hindi</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">English</a></li>';

        $menu .= '</ul></li>';
        $menu .= '<li class="dropdown">';
        $menu .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">Professional</a>';
        $menu .= '<ul class="dropdown-menu" role="menu">';
        $menu .= '<li><a tabindex="-1" href="#">Home</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">About</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Category List</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Category Grid</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Product</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Product Full Width</a></li>';
        $menu .= '<li><a tabindex="-1" href="#">Cart</a></li>';

        $menu .= '</ul></li>';
        $menu .= '<li><a href="#">Time Pass</a></li>';
        $menu .= '<li><a href="#">Fiction</a></li>';
        $menu .= '<li><a href="#">Non-Fiction</a></li>';
        $menu .= '<li><a href="#">More...</a></li>';
        $menu .= '</ul></div>';
        return $menu;
    }

}